====
BUGS
====

.. _gitlab.com issues: https://gitlab.com/uplex/varnish/slash/-/issues

If you think you have found a bug, please use use `gitlab.com
issues`_. You might have already done that and come across a link to
this document.

Working with confidential / personal information
================================================

.. _PII: https://en.wikipedia.org/wiki/Personal_data

Before you continue reading, please make sure you are aware that varnish panic
messages and core dumps in particular will contain some / all information which
varnish processed at the time the respective information was created.

For panics, we recommend to redact `PII`_ like IP adresses, cookies/tokens and
any other sensitive information.

If you are unsure about sharing anything publicly, you can send email to
varnish-support@uplex.de\ . In this case, please reference the fact that you did
in the ticket.

.. _GPG: https://varnish-cache.org/security/gpg.html#nils-goroll

For additional confidentiality, you can send `GPG`_ encrypted email to Nils.

PLEASE HELP US HELP YOU
=======================

Depending on the type of bug, please make an effort to provide the
requested information:

Panics
------

For a Varnish Panic (crash), please include the full Panic
message. Please do not use the ``short_panic`` feature.

.. _gitlab.com snippets: https://gitlab.com/-/snippets/new

For the following information, you might want to use `gitlab.com
snippets`_, please put the snippet link in the issue.

Please provide us with the ``objdump -S`` output of exactly the
``libvmod_slash.so`` file which the panic occured with, e.g.::

  objdump -S ./src/.libs/libvmod_slash.so

If you have a core dump of the panic, please run some ``gdb`` commands
on it::

  cd $YOUR_VARNISH_WORKING_DIRECTORY
  gdb --batch -ex "thr app all bt full" $YOUR_VARNISHD_BINARY core

For a typical installation, the commands would be::

  cd /var/lib/varnish/$(uname -n)
  gdb --batch -ex "thr app all bt full" /usr/sbin/varnishd core


Lockups
-------

To analyze situations where varnish-cache hangs related to the use of
SLASH/ (e.g. the hang occurred only after introducing SLASH/), we need
to investigate the state of the program at the time of the hang.

There are basically two options to get there: Either analyze the still
running process with gdb, or get a core dump and analyze it
later.

Get thread dump of a running process
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``gdb`` invocation is almost the same as the example given above,
but you need the pid of the Varnish-Cache *worker* process. It can
usually be found by its name ``cache-main``, so the snippet given here
uses this simple method::

  cd $YOUR_VARNISH_WORKING_DIRECTORY
  pid=$(pgrep cache-main)
  gdb -p $pid --batch -ex "thr app all bt full"

For a typical installation, the commands would be::

  cd /var/lib/varnish/$(uname -n)
  pid=$(pgrep cache-main)
  gdb -p $pid --batch -ex "thr app all bt full"

If, for whatever reason, this method does not work, look for a
``varnishd`` process which is the *child* of another ``varnishd``
process and use the child's pid for the ``pid`` variable.

Getting a core dump
~~~~~~~~~~~~~~~~~~~

Getting the core dump itself is straight forward: Determine the pid of
the cache worker process (see above), then run ``gcore`` on it::

  pid=$(pgrep cache-main)
  gcore -o slash_issue.core $pid

But to be able to make good use of the core dump, we also need the
varnish working directory, so please save that, too::

  cp -pr $YOUR_VARNISH_WORKING_DIRECTORY slash_issue.workdir

For a typical installation, this is::

  cp -pr /var/lib/varnish/$(uname -n) slash_issue.workdir

Then, to get the thread dump from this data, use the same command
given under `Panics`_, but use the saved workdir instead of the actual
one.


Other
-----

(Instructions need to be written when they become relevant)
