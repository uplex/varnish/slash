..
	Copyright 2023,2024,2025 UPLEX Nils Goroll Systemoptimierung
	SPDX-License-Identifier: BSD-2-Clause
	See LICENSE file for full text of license

==============
SLASH/ Changes
==============

.. keep this section at the top!

This document contains a log of noteworthy changes to SLASH/ from new
to old.

Version 1.0.0-rc3 (NEXT RELEASE)
================================

This release contains a high number of small changes which are not explicitly
mentioned in here because they are considered not particularly relevant for
users. When in doubt, read the git history.

Common changes for buddy & fellow
---------------------------------

* The ``slash.get_tuning_json()`` function has been added to access the current
  tuning settings from VCL.

fellow
------

.. _#83: https://gitlab.com/uplex/varnish/slash/-/issues/83
.. _#88: https://gitlab.com/uplex/varnish/slash/-/issues/88

* A race condition has been fixed in ``fellow_cache_obj_lru_touch()``, which
  could lead to panics because of an inconsistent state of LRU-related metadata
  (`#83`_, `#88`_).

.. _#82: https://gitlab.com/uplex/varnish/slash/-/issues/82

* Two bugs have been fixed related to trimming and relocations of objects in
  memory, which could trigger an assertion failure (`#82`_).

.. _#84: https://gitlab.com/uplex/varnish/slash/-/issues/84

* A bug in reference counting has been fixed which could trigger an assertion
  failure (`#84`_).

* LRU related counters have been added, in particular to help sizing the
  reserves

  There are four counters for for ``dsk`` and ``mem`` each:

  * ``c_dsk_lru_wakeups`` / ``c_mem_lru_wakeups`` is the number of LRU thread
    wakeups

  * ``c_dsk_lru_reserve_used_bytes`` / ``c_mem_lru_reserve_used_bytes`` is the
    number of bytes returned from the reserve. Divided by ``c_dsk_lru_wakeups``
    / ``c_mem_lru_wakeups``, respectively, this should be a good measure for
    sizing the reserve.

  * ``c_dsk_lru_nuke_fill_reserve`` / ``c_mem_lru_evict_fill_reserve`` is the
    number of nukes / evictions issued while filling the reserve - that is, no
    request was actually waiting.

  * ``c_dsk_lru_nuke_reserve_drained`` / ``c_mem_lru_evict_reserve_drained`` is
    the number of nukes / evictions issued while the reserve was already drained
    - that is, while requests were delayed waiting for LRU to make room.

  Note that nukes concern full objects while evictions concern body segments
  individually and object metadata (segment lists, object data with headers).

* Large parts of the fellow cache code have been rewritten to support LRU
  eviction of dynamic segment lists also.

.. _#79: https://gitlab.com/uplex/varnish/slash/-/issues/79

* A substantial concurrency improvement has been implemented for the shared
  io_uring case in fellow_cache (`#79`_).

.. _#71: https://gitlab.com/uplex/varnish/slash/-/issues/71
.. _#22: https://gitlab.com/uplex/varnish/slash/-/issues/22

* Memory and disk usage for chunked responses, where the total body length is
  not known, and planning for disk layout in general has been significantly
  improved. (`#71`_, `#22`_).

* Fellow-specific panic output has been added, which is relatively
  terse by default. The ``panic_flags`` tunable has been added to
  increase verbosity where needed.

* The ``.happy`` VCL variable of a fellow storage now returns ``true``
  only when it is open (has completed loading).

  The ``b_happy`` VSL bitfield contains the happy state in individual
  bits, updated at ``logbuffer_flush_interval``. The least significant
  bit contains the most recent happy state.

  The semantics of the happy value are likely to change in the future.

.. _#44: https://gitlab.com/uplex/varnish/slash/-/issues/44

* A workaround for an inconsistent Varnish-Cache API has been
  added. Before this change, a panic with ``Condition(vdpe->end ==
  VDP_NULL) not true`` or ``Condition((vdpe) != NULL) not true`` could
  be triggered when streaming was enabled (`#44`_).

* A glitch has been fixed which could have lead to too aggressive LRU
  activity.

* Reporting of IO errors has been improved.

* Storage files of global fellow storage now have their ownership and
  permissions changed using the varnish-cache jail facility.

.. _#60: https://gitlab.com/uplex/varnish/slash/-/issues/60

* Reduce memory cache usage when adding objects to the cache with
  unknown size because of chunked encoding (`#60`_).

buddy
-----

* The buddy stevedore has been improved to embed metadata in other allocations,
  if possible.

* A bug has been fixed in the free page selection based on expected expiry which
  prevented all of the available memory to be used.

* By accident, the lowest possible ``minpage`` argument had not been 64 Bytes,
  but rather 128. This has been rectified.

* LRU related counters have been added, in particular to help sizing the reserve

  * ``c_lru_wakeups`` is the number of LRU thread wakeups

  * ``c_lru_reserve_used_bytes`` is the number of bytes returned from the
    reserve. Divided by ``c_lru_wakeups``, this should be a good measure
    for sizing the reserve.

  * ``c_lru_nuke_fill_reserve`` is the number of nukes issued while filling the
    reserve - that is, no request was actually waiting.

  * ``c_lru_nuke_reserve_drained`` is the number of nukes issued while the
    reserve was already drained - that is, while requests were delayed waiting
    for LRU to make room.

  * ``c_trim`` is the number of trim operations, with ``c_trimmed`` tracking the
    total number of bytes trimmed.

* ``c_req`` now only counts successful allocator requests, so a request is
  counted either towards ``c_req`` or ``c_fail``.

* ``chunk_bytes`` (``== 1 << chunk_exponent``) is now always the maximum
  allocation size for body data. Previously, for ``cram == 0``, the requested
  size was allocated as a single extent, but that does not make sense: If larger
  chunks are intended, they can be configured as such. Also, the ``chunk_bytes``
  documentation did already describe this behavior, and the section on ``cram``
  has been clarified.

  The corrected logic is now:

  - if more than ``chunk_bytes`` are requested, the request is lowered to
    exactly ``chunk_bytes`` (cram is inactive).

  - for smaller requests, cram is applied as configured.

  This reflects experience with *fellow* made a long time ago, which we should
  have ported over to *buddy* earlier: Requests for huge allocations trigger LRU
  and thus lead to too low occupancy of the cache.

* Memory usage for transient object has been reduced by optimizing the
  "freebehind" mechanism.

loadmasters
-----------

* A bug has been fixed where loadmaster ``hash`` always used the same hash for
  passes.

slashmap
--------

* The column width is now dynamically adjusted as needed

* Numbers are now right aligned

Version 1.0.0-rc2
=================

fellow
------

.. _`detailed performance report`: doc/perf/dev-03.rst

* Add a `detailed performance report`_.

* Fix a regression triggering a panic if objects are deleted while
  storage is being loaded

.. _#57: https://gitlab.com/uplex/varnish/slash/-/issues/57

* Avoid excessive stack usage caused by ``-fno-early-inlining`` with
  gcc, as is default on Ubuntu (`#57`_).

Version 1.0.0-rc1
=================

fellow
------

  .. https://gitlab.com/uplex/varnish/slash/-/commit/

* The ``objsize_hint`` tunable has been removed. The now internal
  varable is now automatically adjusted within bounds configured by
  the new tunables ``objsize_update_min_log2_ratio``,
  ``objsize_update_max_log2_ratio``, ``objsize_update_min_occupancy``
  and ``objsize_update_max_occupancy``.

* fellow now imposes stricter tests on the amount of memory configured
  for loading an existing cache.

* The ``ioerr_obj=purge`` and ``allocerr_obj=purge`` tunables are now
  fully implemented. The statistics counter ``c_mem_obj_fail`` as been
  added to track the number of times a failed object is hit.

* Reading cache objects has been made more asynchronous: Segment lists
  for extended (non-embedded) segments are now read only on demand and
  in a separate thread.

* Even more memory and disk space allocating functions have been
  rewritten to use asynchronous allocations to lower latencies.

* Dramatically increased memory efficiency. This change may lead to
  more memory being shown as free than before, and users of
  SLASH/fellow may wonder why memory is kept unused: This is a
  consequence of a deliberate decision in the buddy memory allocator
  to allocate objects only at multiples of their (rounded up) size,
  which helps to keep the allocator efficient.

.. _#41: https://gitlab.com/uplex/varnish/slash/-/issues/41

* Dramatically increased stability with minimal memory cache size
  configurations as long as a correct *objsize_hint* parameter is
  given (it needs to be *smaller* than the actual average object
  size). A zero *memsz* argument can now be given to configure the
  minimal memory cache size, which is NOT RECOMMENDED (the more memory
  the better). Motivated by `#41`_

* Fixed many bugs

* Improved code coverage and added Coverity for additional linting.

* Added g_mem_obj gauge.

* Added an absolute maximum of 31 and dynamic maximum to the readahead
  parameter to avoid single object deliveries holding more than 1/16
  of the available memory cache.

* The readahead implementation has been changed to only run when less
  than or equal to half (rounded down) the configured read ahead
  segments are already available.

* The ``readahead`` parameter default has been changed from 2 to 5 to
  enable the efficiency improvement by the aforementioned change: As 5
  / 2 = 2, read ahead will trigger for every 2 segments, instead of
  for every segment.

* Added a dynamic minimum to the dsk_reserve_chunks parameter to
  always keep the reserve at 2MB minimum. This is required for stable
  operation of LRU when the log is full.

* Added a workaround for the Varnish-Cache issue fixed by
  https://github.com/varnishcache/varnish-cache/pull/4013

* Support liburing without io_uring_free_probe()

* Added compatibility with xxHash 0.7.3

* Removed dependency on libvarnish, becasue required functions have
  been added to libvarnishapi.

* To cater for massively parallel systems with dozens of CPUs, the
  parameter ``lru_exponent`` has been introduced to scale the number
  of LRU lists (and corresponding eviction threads) between 1 and 64
  (corresponding to ``lru_exponent = 0`` to ``lru_exponent = 6``).

* The allocation policy for disk regions has been improved. This
  should reduce fragmentation and pressure on LRU as well as improve
  response times (`a0e8e8f779f4ad8569ccc9c3b7eaee08dc79cfa4`_).

  .. _a0e8e8f779f4ad8569ccc9c3b7eaee08dc79cfa4: https://gitlab.com/uplex/varnish/slash/-/commit/a0e8e8f779f4ad8569ccc9c3b7eaee08dc79cfa4

* The ``objsize_max`` tunable has been added, which defaults to 1/4 of
  *dsksize* (`bef3d014a6a31a6993169ef1aa95aedec2d4aa23`_).

  .. _bef3d014a6a31a6993169ef1aa95aedec2d4aa23: https://gitlab.com/uplex/varnish/slash/-/commit/bef3d014a6a31a6993169ef1aa95aedec2d4aa23

  The tunable now allows SLASH/ to make an educated guess about how
  much space at most to allocate in advance when the object size is
  not known upfront, for example with *chunked encoding*.

* Fixed two issues where reservations and their limits were not
  calculated correctly (`a02dd5c5a1c13c9fcdc8e1a5d5281ddf69b3148f`_,
  `8b700a275cb977d7c6d703a0e2a6ca8a7310f0c3`_).

  .. _a02dd5c5a1c13c9fcdc8e1a5d5281ddf69b3148f: https://gitlab.com/uplex/varnish/slash/-/commit/a02dd5c5a1c13c9fcdc8e1a5d5281ddf69b3148f
  .. _8b700a275cb977d7c6d703a0e2a6ca8a7310f0c3: https://gitlab.com/uplex/varnish/slash/-/commit/8b700a275cb977d7c6d703a0e2a6ca8a7310f0c3

* The ``chunk_bytes`` tunable now has a new hard cap at less than
  1/1024 of the memory cache size. This is to avoid lockups when the
  memory cache becomes too fragmented to serve larger allocation
  requests (`#11`_, `da63a5b836e37f27f1759a37446c117d37284650`_).

  .. _da63a5b836e37f27f1759a37446c117d37284650: https://gitlab.com/uplex/varnish/slash/-/commit/da63a5b836e37f27f1759a37446c117d37284650
  .. _#11: https://gitlab.com/uplex/varnish/slash/-/issues/11

* Previously, when multiple requests accessed the same disk object
  (not yet present in RAM), they would race to create it, allocating
  memory and, if they lost the race, freeing it again.

  This is now avoided using a "wait table": A hash table of
  configurable size serializes operations on each hash value, which is
  derived from the location and size of an object on disk.

  The size of the wait table approximates a limit to the number of
  objects which can be initially read from disk concurrently. Note
  that this does not affect reading object bodies, just the object
  metadata and headers.

  The size of the table can be adjusted using the new
  ``wait_table_exponent`` tunable, which is in powers of two, so, for
  example, the default of 10 results in a hash table of size 2^10 =
  1024. Each hash table entry uses approximately 128 bytes
  (`d5c1bbd7b14b8736a641da4c2c28d979e917d020`_).

.. _d5c1bbd7b14b8736a641da4c2c28d979e917d020: https://gitlab.com/uplex/varnish/slash/-/commit/d5c1bbd7b14b8736a641da4c2c28d979e917d020

* When cache memory is tight and LRU is running, allocation requests
  have always been prioritized (in this order): free operations > log
  operations > metadata > objects > reserves.

  This priority list has now been extended to weigh operations on
  object operations which are already in progress over creation of new
  objects - the idea being that, when cache memory is under pressure,
  any operations which are required to finish before memory can be
  freed again should be prioritized.

  The new list of priorities is: free operations > log operations >
  metadata > object body iterations > object fetch operations > new
  cache objects > initial object reads from disk > reserves
  (`4dcda2c00ad40f0c471c1f9fa078d8d1cc17d32e`_)

  As an exception, requests to ``Vary`` headers are prioritized like
  object body iterations, because they might happen while holding the
  ``objhead`` mutex during cache lookups. Also ``Vary`` access is
  handled like an object access with respect to LRU.

  .. _4dcda2c00ad40f0c471c1f9fa078d8d1cc17d32e: https://gitlab.com/uplex/varnish/slash/-/commit/4dcda2c00ad40f0c471c1f9fa078d8d1cc17d32e
  .. _491339c242c5a94f4b2ed448e531abe6deb60a88: https://gitlab.com/uplex/varnish/slash/-/commit/491339c242c5a94f4b2ed448e531abe6deb60a88
  .. _94d37731bd45ec0e53883ec256125277a74e92aa: https://gitlab.com/uplex/varnish/slash/-/commit/94d37731bd45ec0e53883ec256125277a74e92aa

* The ``oa_present`` bit field in varnish-cache is now restored when
  an object is read from disk for the first time after a cache
  start. This avoids I/O and reduces cache memory requirements when
  the ``Vary`` response header is not used
  (`fc1af457e52c272bdeea8bd3b207e6f96fad8dbe`_).

  .. _fc1af457e52c272bdeea8bd3b207e6f96fad8dbe: https://gitlab.com/uplex/varnish/slash/-/commit/fc1af457e52c272bdeea8bd3b207e6f96fad8dbe

* Memory requirements of cache misses ("busy objects") have been
  reduced substantially (from >12KB to <4KB) (`#16`_).

.. _#16: https://gitlab.com/uplex/varnish/slash/-/issues/16

Base of the changelog: `e3a454096759cafd30d9008f4b11cc754297b250`_

.. _e3a454096759cafd30d9008f4b11cc754297b250: https://gitlab.com/uplex/varnish/slash/-/commit/e3a454096759cafd30d9008f4b11cc754297b250
