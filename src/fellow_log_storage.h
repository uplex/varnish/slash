/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

/*
 * functions required by the storage layer
 */

#include "fellow_task.h"

/*
 * ------------------------------------------------------------
 * Disk Log Entries
 */

#define DLE_T_OBJ     0x0
#define DLE_T_REG     0x8
#define DLE_T_BAN_IMM 0x10
#define DLE_T_BAN_REG 0x18

#define DLE_TYPE(x) ((uint8_t)x & 0x18)

/*
 * DLE_OP_DEL_ALLOCED:
 * - still allocated at this point in the log
 * - freed after rewrite
 *
 * DLE_OP_DEL_FREE:
 * - already freed (as part of a flush with LBUF_CAN_REF)
 *
 * DLE_OP_DEL_THIN:
 * - we do not know the object's regions when we issue the
 *   delete (EXP of dskoc). Remember the delete, and turn
 *   _ADD of the object and regions into DEL_ALLOCED
 *   when seeing it
 */
#define DLE_OP_ADD		1
#define DLE_OP_CHG		2
#define DLE_OP_DEL_ALLOCED	3
#define DLE_OP_DEL_FREE	4
#define DLE_OP_DEL_THIN	5
#define DLE_OP(x) (x & 7)

enum dle_type {
#define DLE_N(e, short, val) e = val,
#include "tbl/dle.h"
DLE_TYPE_LIM
} __attribute__((packed));
/*
 * dle regions (for body data) always belong to a dle_obj they always need to be
 * written before a dle_obj in a single block such that when the log is read in
 * reverse, they are read after the obj
 *
 * dle_reg, dle_reg, ... dle_obj
 *
 * unused regions are zero
 */

#define DLE_REG_HASHPFXSZ 4
#define DLE_REG_NREGION 4
#define DLE_BAN_IMM_LEN 67
#define DLE_BAN_IM0_LEN (DLE_BAN_IMM_LEN \
	- (sizeof(int8_t) + sizeof(uint16_t) + sizeof(vtim_real)))
#define DLE_BAN_REG_NREGION 3

// roundup number of DLEs needed for x regions
#define DLE_N_REG(x) ((x + (DLE_REG_NREGION - 1)) / DLE_REG_NREGION)

struct fellow_dle_reg {
	// 4 - for sanity check
	uint8_t			hashpfx[DLE_REG_HASHPFXSZ];
	struct buddy_off_extent	region[DLE_REG_NREGION];
}  __attribute__((packed, aligned(4)));

#ifndef DIGEST_LEN
#define DIGEST_LEN 32
#elif DIGEST_LEN != 32
#error wrong DIGEST_LEN
#endif

// aligned(4) assumes that this will only be used
// at a position 4, otherwise alignment of the other
// data types will not match
//
// TODO #17 IF EXTENDING, ADD oa_present field
struct fellow_dle_obj {
	// 4
	float			keep;

	// 8
	uint8_t		hash[DIGEST_LEN];

	// 40
	double			ban;
	double			t_origin;
	// 56
	float			ttl;
	float			grace;
	// 64
	fellow_disk_block	start;	// first block
	// 72
}  __attribute__((packed, aligned(4)));

// for dlechg: minimized version to only keep the bare minimum of object data.
// defined here to hopefully not miss an update...
//
// For DLE_OBJ_DEL_THIN, all timers are -42.42
// otherwise it is a DLE_OBJ_CHG
struct fellow_dle_obj_chg {
	fellow_disk_block	start;	// first block
	// 8
	float			ttl;
	float			grace;
	// 16
	double			ban;
	double			t_origin;
	// 32
	float			keep;
	// 36
	// hash is only used for assertions
#define DLE_OBJ_CHG_HASH_LEN	4
	uint8_t		hash[DLE_OBJ_CHG_HASH_LEN];
	// 40
};

// XXX do not want cache_varnishd in fellow_log.h
#ifndef EXP_COPY
#define EXP_COPY(to,fm)							\
	do {								\
		(to)->t_origin = (fm)->t_origin;			\
		(to)->ttl = (fm)->ttl;					\
		(to)->grace = (fm)->grace;				\
		(to)->keep = (fm)->keep;				\
	} while (0)
#endif

/* ban_imm / ban_im0 cont:
 * how many IMM DLEs are following.
 * positive forwards, negative: backwards
 *
 * last DLE has negative entry to point to the first
 *
 */

struct fellow_dle_ban_imm {
	int8_t				cont;
	uint8_t				ban[DLE_BAN_IMM_LEN];
}  __attribute__((packed, aligned(4)));

/* first (or only) IMM */
struct fellow_dle_ban_im0 {
	int8_t				cont;
	int8_t				_pad;
	uint16_t			len;
	vtim_real			ban_time;
	uint8_t				ban[DLE_BAN_IM0_LEN];
}  __attribute__((packed, aligned(4)));


struct fellow_dle_ban_reg {
	uint32_t			len;
	vtim_real			ban_time;
	struct buddy_off_extent	region[DLE_BAN_REG_NREGION];
}  __attribute__((packed, aligned(4)));

struct fellow_dle {
	uint8_t		magic;
#define FELLOW_DLE_MAGIC	0x42
	uint8_t		seq;
	uint8_t		version;
	uint8_t		type;	// enum dle_type

	// actually belongs to obj, kept here for mem efficiency

	union {
		struct fellow_dle_obj	obj;
		struct fellow_dle_reg	reg;
		struct fellow_dle_ban_im0	ban_im0;
		struct fellow_dle_ban_imm	ban_imm;
		struct fellow_dle_ban_reg	ban_reg;
	} u;
};
#define DLEDSK(x) (uint8_t)(x)

// XXX should this be exposed? test needs it
#define FELLOW_DISK_LOG_BLOCK_ENTRIES	56

// max length for dle_ban_imm with continuations
#define FELLOW_DLE_BAN_IMM_MAX (DLE_BAN_IM0_LEN +	\
	DLE_BAN_IMM_LEN * (FELLOW_DISK_LOG_BLOCK_ENTRIES - 1))

/* called on alive objects during FP_INIT.
 *
 * return >0 if object is to be kept alive (otherwise disk memory
 * will not be taken)
 */
struct fellow_dle;
void fellow_dle_init(struct fellow_dle *entry, unsigned n);
void fellow_log_dle_submit(struct fellow_fd *ffd,
    struct fellow_dle *entry, unsigned n);
typedef int fellow_resurrect_f(void *priv, const struct fellow_dle *e);

struct VSC_fellow;
struct stvfe_tune;
struct fellow_fd *
fellow_log_init(const char *path, size_t wantsize, size_t objsz_hint,
    mapper *freemap_mapper, void *freemap_mapper_priv,
    buddy_t *membuddy, struct stvfe_tune *tune, fellow_task_run_t,
    struct VSC_fellow *stats);

void fellow_log_discardctl(struct fellow_fd *ffd, size_t min);

/*
 * register a callback to run just before the log transitions to open
 */
typedef void fellow_open_f(void *priv);
void
fellow_log_register_open_cb(struct fellow_fd *ffd,
    fellow_open_f *func, void *priv);

void
fellow_log_open(struct fellow_fd *ffd,
    fellow_resurrect_f *resur_f, void *resur_priv);

void fellow_log_flush(struct fellow_fd *ffd);

void fellow_log_close(struct fellow_fd **ffdp);

int fellow_log_ban(struct fellow_fd *ffd, uint8_t op,
    const uint8_t *bans, unsigned len, vtim_real time,
    struct buddy_off_extent space[DLE_BAN_REG_NREGION]);
struct buddy_ptr_extent
fellow_log_read_ban_reg(const struct fellow_fd *ffd,
    const struct fellow_dle *e);


size_t fellow_minsize(void);

// ballpark: 2x bitf for logs rewrite
#define MEMBUDDY_MINSIZE(dskbuddy_size, hint) (				\
	    (2 * dskbuddy_size / MIN_FELLOW_BLOCK) / 8 +	/* bitf */ \
	    MIN_FELLOW_BLOCK * (					\
		/* hdr */						\
		16 +							\
		/* approx 1/16 of log for all objects */		\
		((dskbuddy_size / hint) / FELLOW_DISK_LOG_BLOCK_ENTRIES) / 8 \
		))

typedef void fellow_log_diag_f(const char *fmt, ...);
void fellow_log_set_diag(struct fellow_fd *ffd, fellow_log_diag_f *diag);
void fellow_fd_update_stats(struct fellow_fd *ffd);
#ifdef UNUSED
size_t fellow_log_freeing(struct fellow_fd *ffd);
#endif
void fellow_logwatcher_kick_locked(struct fellow_fd *ffd);
buddy_t *fellow_dskbuddy(struct fellow_fd *ffd);
int fellow_is_open(const struct fellow_fd *ffd);
