/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include <stdlib.h>
#include <limits.h>
#include <pthread.h>

#include "slashmap.h"
#include "buddy_util.h"
#include "buddy_witness.h"
#include "vqueue.h"
#include "vtree.h"

#define MIN_BUDDY_BITS 6	/* size 64 */

struct buddy_minfo;
VRBT_HEAD(buddy_minfo_head, buddy_minfo);

struct buddy_reqs;
VTAILQ_HEAD(buddy_reqs_head, buddy_reqs);

#ifdef FREEPAGE_WHEN
#define BUDDY buddywhen
#define BUDDY_MAGIC		0x35ea5690
#define BUDDYF(x)	buddywhen_##x
#define WHEN_VAR	double when;
#else
#define BUDDY buddy
#define BUDDY_MAGIC		0x1488196a
#define BUDDYF(x)   buddy_##x
#define WHEN_VAR
#endif

#define BUDDY_WAIT_MAXPRI	8

struct BUDDY {
	unsigned		magic;
	unsigned char		*area, *end;	// end for assertions only

	pthread_mutex_t		map_mtx;
	struct slashmap	*map;

	pthread_mutex_t	minfo_mtx;
	struct buddy_minfo_head minfo_head;

	size_t			rsv_avail;

	// under map_mtx
	size_t			deficit;
	unsigned		waiting;
	unsigned		wait_working;
	unsigned		wait_pri;	// highest q with entries
	struct buddy_reqs_head	reqs_head[BUDDY_WAIT_MAXPRI + 1];
	pthread_cond_t		wait_kick_cond;  // to kick freeing space (LRU)
#ifdef BUDDY_WITNESS
	BWIT_DECL(witness[1])
#define HAS_WITNESS 1
#else
#define HAS_WITNESS 0
#endif
};
typedef struct BUDDY buddy_t;
#undef BUDDY


/************************************************************
 * util
 *
 */
void * BUDDYF(mmap)(size_t *, void *);
void BUDDYF(unmap)(void **, size_t, void **);
int8_t BUDDYF(cramlimit_page_minbits)(int8_t, int8_t, int8_t);
int8_t BUDDYF(cramlimit_extent_minbits)(size_t, int8_t, int8_t);
static inline int
abslimit(int val, int lim)
{
	if (abs(val) <= lim)
		return (val);
	return (val < 0 ? 0 - lim : lim);
}
static inline int8_t
abslimit8(int8_t val, int8_t lim)
{
	if (abs(val) <= lim)
		return (val);
	return (val < 0 ? 0 - lim : lim);
}
/************************************************************
 * user functions
 *
 */

// LRU waiting for kick
void BUDDYF(wait_needspace)(buddy_t *);
// LRU termination, reconfiguration
void BUDDYF(wait_kick)(buddy_t *);
// out of deadlock
void BUDDYF(wait_fail)(buddy_t *);

#ifdef FREEPAGE_WHEN
// malloc-ish interface
void	 *BUDDYF(malloc)(buddy_t *, size_t, double);
void	 *BUDDYF(malloc_wait)(buddy_t *, size_t, unsigned, double);
#else
// malloc-ish interface
void	 *BUDDYF(malloc)(buddy_t *, size_t);
void	 *BUDDYF(malloc_wait)(buddy_t *, size_t, unsigned);
#endif

void  BUDDYF(free)(buddy_t *, void *);

// init/fini
typedef void *mapper(size_t *, void *);
typedef void umapper(void **, size_t, void **);
void BUDDYF(init_)(unsigned, buddy_t *, unsigned, size_t,
    mapper *, void *,
    mapper *, void *);
static inline void
BUDDYF(init)(buddy_t *b, unsigned m, size_t s,
    mapper *am, void *ap,
    mapper *fm, void *fp)
{
	BUDDYF(init_)(HAS_WITNESS, b, m, s, am, ap, fm, fp);
}
void BUDDYF(fini)(buddy_t **,
    umapper *, void **,
    umapper *, void **);
// meta / util
size_t BUDDYF(size)(const buddy_t *);
size_t BUDDYF(space)(buddy_t *, int);
static inline size_t
BUDDYF(minsize)(const buddy_t *buddy)
{
	return ((size_t)1 << buddy->map->min);
}
static inline size_t
BUDDYF(rndup)(const buddy_t *buddy, size_t sz)
{
	struct slashmap *map;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	return (rup_min(sz, map->min));
}

static inline size_t
BUDDYF(reserve)(buddy_t *buddy, size_t sz)
{

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	if (sz > buddy->rsv_avail)
		sz = buddy->rsv_avail;
	buddy->rsv_avail -= sz;
	return (sz);
}

/* shall be defined as the larger of intptr_t and off_t */
typedef off_t buddyoff_t;

/************************************************************
 * page ops
 *
 */

static inline size_t
ipageof(const buddy_t *buddy, buddyoff_t p, uint8_t bits)
{
	const struct slashmap *map = buddy->map;
	size_t pp, page;

	assert(p >= 0);
	pp = (size_t)p;
	assert(pp < map->size);
	assert(bits >= map->min);
	assert(bits <= map->max);
	page = pp;
	assert((page & (((size_t)1 << bits) - 1)) == 0);
	page >>= bits;
	return (page);
}

/*
 * ============================================================
 * buddy2 interface
 */

#define BUDDY_OFF_NIL	-1

struct buddy_ptr_extent {
	void		*ptr;
	size_t		size;
};

struct buddy_off_extent {
	buddyoff_t	off;
	size_t		size;
};

#define BUDDY_PTR_EXTENT(p, size)					\
	(struct buddy_ptr_extent){(p), (size)}
#define BUDDY_OFF_EXTENT(o, size)					\
	(struct buddy_off_extent){(o), (size)}

#define buddy_ptr_extent_nil (struct buddy_ptr_extent){NULL, 0}
#define buddy_off_extent_nil (struct buddy_off_extent){BUDDY_OFF_NIL, 0}


/* the magic is chosen such that it very likely renders an invalid size then
 * extent and page are confused: for little endian, it results in an very high
 * value, and for big endian, an uneven size
 */
#define BUDDY_PAGE_MAGIC	0xfff42fff

struct buddy_ptr_page {
	void		*ptr;
	uint8_t	bits;
	unsigned	magic;
};

struct buddy_off_page {
	buddyoff_t	off;
	uint8_t	bits;
	unsigned	magic;
};

#define BUDDY_PTR_PAGE(p, bits)					\
	(struct buddy_ptr_page){p, bits, BUDDY_PAGE_MAGIC}
#define BUDDY_OFF_PAGE(o, bits)					\
	(struct buddy_off_page){o, bits, BUDDY_PAGE_MAGIC}

#define buddy_ptr_page_nil BUDDY_PTR_PAGE(NULL, 0)
#define buddy_off_page_nil BUDDY_OFF_PAGE(BUDDY_OFF_NIL, 0)

union buddy_off_alloc {
	struct buddy_off_extent extent;
	struct buddy_off_page	page;
};

struct i_req_extent {
	BWIT_DECL(*witness)
	WHEN_VAR
	struct bitf		**ff;
	size_t			rup, page;
	uint8_t		bits;
	int8_t			cram;
};

struct i_req_page {
	BWIT_DECL(*witness)
	WHEN_VAR
	struct bitf		**ff;
	size_t			page;
	uint8_t		bits;
	int8_t			cram;
};

union i_req {
	struct i_req_extent	extent;
	struct i_req_page	page;
};

struct i_reqalloc {
	unsigned			magic;
#define I_REQALLOC_MAGIC		0x3a55aae0
	uint8_t			type;
#define BUDDY_T_EXTENT	1
#define BUDDY_T_PAGE	2
	union i_req			i_req;
	union buddy_off_alloc		off_alloc;
};

struct i_return {
	unsigned			magic;
#define I_RETURN_MAGIC			0xf1b7b9c2
	uint8_t			bits;
	buddyoff_t			off;
	struct bitf			**ff;
	size_t				size;
	size_t				page;
};

enum i_wait_state {
	IW_INVAL = 0,
	IW_ARMED,
	IW_WAITING,
	IW_SIGNALLED
} __attribute__ ((__packed__));

struct i_wait {
	unsigned			magic;
#define I_WAIT_MAGIC			0x7f6303bc
	// owned by initiator
	uint8_t				finid;
	uint8_t				next;
	pthread_mutex_t			wait_mtx;
	pthread_cond_t			wait_cond;
	VTAILQ_ENTRY(buddy_reqs)	list;
	// updated under lock - KEEP SEPERATE FROM "owned by initiator"
	uint8_t				pri; // index to head for list
	uint8_t				alloced; // how many of n
	enum i_wait_state		state;
};

#define BUDDY_REQS_AUDIT

struct buddy_reqs {
	unsigned		magic;
#define BUDDY_REQS_MAGIC	0x3f2b4d6c
	uint8_t		space;

	uint8_t		n;	// how many requests
	uint8_t		pri;

	buddy_t		*buddy;
	size_t			sz;	// aproox req'ed bytes
	struct slashmap		*map;
	struct i_wait		i_wait;
	struct i_reqalloc	*i_reqalloc;
#ifdef BUDDY_REQS_AUDIT
	const char *		func;
	int			line;
#endif
};

struct buddy_returns {
	unsigned		magic;
#define BUDDY_RETURNS_MAGIC	0xc5015b57
	uint8_t		space;

	uint8_t		n;	// how many requests

	buddy_t		*buddy;
	size_t			size;
	struct i_return		*i_return;
};

// convert back and forth between off and ptr
static inline struct buddy_ptr_extent
buddy_off_ptr_extent(const buddy_t *buddy, struct buddy_off_extent off)
{
	struct buddy_ptr_extent ptr = buddy_ptr_extent_nil;
	unsigned char *area;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	area = buddy->area;
	AN(area);

	if (off.off != BUDDY_OFF_NIL) {
		ptr.ptr = area + off.off;
		ptr.size = off.size;
	}
	return (ptr);
}
static inline struct buddy_ptr_page
buddy_off_ptr_page(const buddy_t *buddy, struct buddy_off_page off)
{
	struct buddy_ptr_page ptr = buddy_ptr_page_nil;
	unsigned char *area;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	area = buddy->area;
	AN(area);
	assert(off.magic == BUDDY_PAGE_MAGIC);

	if (off.off != BUDDY_OFF_NIL) {
		ptr.ptr = area + off.off;
		ptr.bits = off.bits;
	}
	return (ptr);
}
static inline struct buddy_off_extent
buddy_ptr_off_extent(const buddy_t *buddy, struct buddy_ptr_extent ptr)
{
	struct buddy_off_extent off = buddy_off_extent_nil;
	unsigned char *area, *p;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	area = buddy->area;
	AN(area);
	if (ptr.ptr != NULL) {
		p = ptr.ptr;
		assert(p >= area);
		assert(p < buddy->end);
		off.off = p - area;
		off.size = ptr.size;
	}
	return (off);
}
static inline struct buddy_off_page
buddy_ptr_off_page(const buddy_t *buddy, struct buddy_ptr_page ptr)
{
	struct buddy_off_page off = buddy_off_page_nil;
	unsigned char *area, *p;

	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	area = buddy->area;
	AN(area);
	if (ptr.ptr != NULL) {
		p = ptr.ptr;
		assert(p >= area);
		assert(p < buddy->end);
		off.off = p - area;
		off.bits = ptr.bits;
	}
	return (off);
}

/*
 * alloc
 * [use allocations]
 *
 * alloc_async
 * [(use allocations)*]
 * [alloc_async_ready] to check for more ready allocations
 * [alloc_async_wait] for sync wait
 * alloc_async_done to finish and return unused
 *
 * alloc_wait
 * [use allocations]
 * [alloc_wait_done] to finish and return unused
 *
 */

/* immediate allocation or failure */
uint8_t
BUDDYF(alloc)(struct buddy_reqs *reqs);

/* return whatever immediately allocatable, enqueue for more */
uint8_t
BUDDYF(alloc_async)(struct buddy_reqs *reqs);

/* return when allocations done or failed definitively */
uint8_t
BUDDYF(alloc_async_wait)(struct buddy_reqs *reqs);

/* prepare ready allocations for use */
uint8_t
BUDDYF(alloc_async_ready)(struct buddy_reqs *reqs);

/* finish async allocation and return unused allocations */
void
BUDDYF(alloc_async_done)(struct buddy_reqs *reqs);

/* alloc_async and alloc_async_wait: return after pontentially waiting */
uint8_t
BUDDYF(alloc_wait)(struct buddy_reqs *reqs);

static inline void
BUDDYF(alloc_wait_done)(struct buddy_reqs *reqs)
{
	BUDDYF(alloc_async_done)(reqs);
}


/* free */
void
BUDDYF(return)(struct buddy_returns *rets);

/* single trim */
void
BUDDYF(trim1_off_extent)(buddy_t *buddy,
    struct buddy_off_extent *r, size_t nsz);

static inline void
BUDDYF(trim1_ptr_extent)(buddy_t *buddy,
    struct buddy_ptr_extent *ptr, size_t nsz)
{
	struct buddy_off_extent off;

	AN(ptr);
	off = buddy_ptr_off_extent(buddy, *ptr);
	BUDDYF(trim1_off_extent)(buddy, &off, nsz);
	*ptr = buddy_off_ptr_extent(buddy, off);
}

/* take n */
#ifdef FREEPAGE_WHEN
void
BUDDYF(take_off_extent)(buddy_t *buddy, const struct buddy_off_extent *rs,
    unsigned n, double when);

static inline void
BUDDYF(take_ptr_extent)(buddy_t *buddy, const struct buddy_ptr_extent *rs,
    unsigned n, double when)
{
	struct buddy_off_extent off[n];
	unsigned u;

	for (u = 0; u < n; u++)
		off[u] = buddy_ptr_off_extent(buddy, rs[u]);
	BUDDYF(take_off_extent)(buddy, off, n, when);
}
#else
void
BUDDYF(take_off_extent)(buddy_t *buddy, const struct buddy_off_extent *rs,
    unsigned n);

static inline void
BUDDYF(take_ptr_extent)(buddy_t *buddy, const struct buddy_ptr_extent *rs,
    unsigned n)
{
	struct buddy_off_extent off[n];
	unsigned u;

	for (u = 0; u < n; u++)
		off[u] = buddy_ptr_off_extent(buddy, rs[u]);
	//lint -e(772)
	BUDDYF(take_off_extent)(buddy, off, n);
}
#endif

#define BUDDY_REQS_MAX 255

/* unclear: Would like to reduce these to just one macro, but
 * flexelint complains if BUDDY_REQS_I_WAIT_INITIALIZER
 * used in BUDDY_REQS_STK()
 */
#define BUDDY_REQS_I_WAIT_ZERO						\
	{								\
		/*lint -e{708} union init*/				\
		.magic = I_WAIT_MAGIC,					\
		.alloced = 0,						\
		.finid = 0,						\
		.next = 0,						\
		.state = IW_ARMED,					\
		.list = { NULL },					\
		.wait_mtx = PTHREAD_MUTEX_INITIALIZER,			\
		.wait_cond = PTHREAD_COND_INITIALIZER			\
	}
#define BUDDY_REQS_I_WAIT_INITIALIZER		\
	(struct i_wait)BUDDY_REQS_I_WAIT_ZERO

#ifdef BUDDY_REQS_AUDIT
//lint -e773
#define COMMA ,
#define BUDDY_REQS_AUDIT_INIT(pfx,sep) \
	pfx.func = __func__ sep pfx.line = __LINE__ sep
//lint +e773
#else
#define BUDDY_REQS_AUDIT_INIT(pfx, sep)
#endif

#define BUDDY_REQS(name, size)				\
	struct name {					\
		struct i_reqalloc reqs_i[size];	\
		struct buddy_reqs reqs;		\
	}

#define BUDDY_REQS_INIT(parent, b)					\
	do {								\
		INIT_OBJ(&(parent)->reqs, BUDDY_REQS_MAGIC);		\
		memset((parent)->reqs_i, 0, sizeof (parent)->reqs_i);	\
		(parent)->reqs.space = sizeof (parent)->reqs_i /	\
		    sizeof *(parent)->reqs_i;				\
		(parent)->reqs.buddy = b;				\
		(parent)->reqs.map = (b)->map;				\
		(parent)->reqs.i_wait = BUDDY_REQS_I_WAIT_INITIALIZER;	\
		(parent)->reqs.i_reqalloc = (parent)->reqs_i;		\
		BUDDY_REQS_AUDIT_INIT((parent)->reqs,;)		\
	} while (0)

/* example

struct foo {
	BUDDY_REQS(reqs, 4);
};
*/

/*
 * a reqs pair which acts as an async, (hopefully) always filled reserve: one
 * req is active (allocations are taken from it), while the other is filling
 *
 * size is per reqs, so the overall size is twice that
 */

typedef void buddy_pool_fill_f(struct buddy_reqs *, const void *);

#define BUDDY_POOL_MAGIC 0x729c0ffd

#define BUDDY_POOL(name, size)				\
	struct name {					\
		unsigned magic;				\
		unsigned active;			\
		buddy_pool_fill_f *fill;		\
		BUDDY_REQS(,size) reqs[2];		\
	}


/*
 * i < 3: If both reqs are empty, at iteration 3 we
 * must hit the first filled alloc
 *
 * The XXXAN() (failed allocation) can trigger during load from
 * fellow_log_memfail()
 */
#define BUDDY_POOL_GET_FUNC(name, decl)			\
decl struct buddy_reqs *					\
name ## _get(struct name *poolp, const void *priv)		\
{								\
	struct buddy_reqs *reqs;				\
	int i;							\
								\
	CHECK_OBJ_NOTNULL(poolp, BUDDY_POOL_MAGIC);		\
	AN(poolp->fill);					\
	for (i = 0; i < 3; i++) {				\
		AZ(poolp->active & ~1);				\
		reqs = &((poolp)->reqs[poolp->active].reqs);	\
		CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);	\
		if (buddy_reqs_next_ready(reqs))		\
			return (reqs);				\
		XXXAN(BUDDYF(alloc_async_wait)(reqs));		\
		if (buddy_reqs_next_ready(reqs))		\
			return (reqs);				\
		BUDDYF(alloc_wait_done)(reqs);			\
		poolp->fill(reqs, priv);			\
		(void) BUDDYF(alloc_async)(reqs);		\
								\
		poolp->active = ! poolp->active;		\
	}							\
	WRONG("Expected second return() to be hit");		\
}

// return available allocations, does not block
#define BUDDY_POOL_AVAIL_FUNC(name, decl)			\
decl unsigned							\
name ## _avail(struct name *poolp)				\
{								\
	struct buddy_reqs *reqs;				\
	unsigned u, avail = 0;					\
								\
	CHECK_OBJ_NOTNULL(poolp, BUDDY_POOL_MAGIC);		\
	for (u = 0; u < 2; u++) {				\
		reqs = &((poolp)->reqs[u].reqs);		\
		CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);	\
		avail += buddy_reqs_next_ready(reqs);		\
	}							\
	return (avail);						\
}

#define BUDDY_POOL_INIT(poolp, b, prio, f, p) do {			\
	INIT_OBJ(poolp, BUDDY_POOL_MAGIC);				\
	poolp->fill = f;						\
	BUDDY_REQS_INIT(&(poolp)->reqs[0], b);				\
	BUDDY_REQS_INIT(&(poolp)->reqs[1], b);				\
	BUDDY_POOL_PRI(poolp, prio);					\
	f(&(poolp)->reqs[0].reqs, p);					\
	f(&(poolp)->reqs[1].reqs, p);					\
	(void) BUDDYF(alloc_async)(&(poolp)->reqs[0].reqs);		\
	(void) BUDDYF(alloc_async)(&(poolp)->reqs[1].reqs);		\
} while (0)

#define BUDDY_POOL_PRI(poolp, prio) do {				\
	BUDDY_REQS_PRI(&(poolp)->reqs[0].reqs, prio);			\
	BUDDY_REQS_PRI(&(poolp)->reqs[1].reqs, prio);			\
} while (0)

#define BUDDY_POOL_FINI(poolp) do {					\
	CHECK_OBJ_NOTNULL(poolp, BUDDY_POOL_MAGIC);			\
	BUDDYF(alloc_wait_done)(&((poolp)->reqs[0].reqs));		\
	BUDDYF(alloc_wait_done)(&((poolp)->reqs[1].reqs));		\
	memset(poolp, 0, sizeof *poolp);				\
} while (0);

#define BUDDY_REQS_LIT(b, size)						\
{							\
	.magic = BUDDY_REQS_MAGIC,					\
	.space = (size),						\
	.buddy = (b),							\
	.map = (b)->map,						\
	.i_wait = BUDDY_REQS_I_WAIT_ZERO,				\
	.i_reqalloc = (struct i_reqalloc[size + 1]){{0}},		\
	BUDDY_REQS_AUDIT_INIT(,COMMA)				\
}
#define BUDDY_REQS_STK(b, size) &(struct buddy_reqs)BUDDY_REQS_LIT(b, size)

#define BUDDY_RETURNS_MAX 255

#define BUDDY_RETURNS_STK(b, size) (struct buddy_returns[1]) {{	\
	.magic = BUDDY_RETURNS_MAGIC,					\
	.space = (size),						\
	.buddy = (b),							\
	.i_return = (struct i_return[size + 1]){{0}}			\
}}

#define BUDDY_REQS_PRI(reqs, prio)		\
	((reqs)->pri = (prio))

// a_emalloc
#ifdef FREEPAGE_WHEN
static inline int
buddywhen_req_extent(struct buddy_reqs *reqs, size_t size,
    int8_t cram, double when)
#else
static inline int
buddy_req_extent(struct buddy_reqs *reqs, size_t size, int8_t cram)
#endif
{
	struct i_reqalloc *ra;
	struct i_req_extent *r;
	struct slashmap *map;
	unsigned bits;
	size_t rup;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->n == reqs->space) {
		errno = ENOSPC;
		return (0);
	}
	assert(reqs->n < reqs->space);

	map = reqs->map;
	rup = rup_min(size, map->min);
	if (rup == 0) {
		errno = EINVAL;
		return (0);
	}

	bits = log2up(rup);
	assert(bits >= map->min);
	if (bits > map->max) {
		errno = EINVAL;
		return (0);
	}
	assert(bits <= UINT8_MAX);

	ra = &reqs->i_reqalloc[reqs->n++];
	ra->magic = I_REQALLOC_MAGIC;
	ra->type = BUDDY_T_EXTENT;
	r = &ra->i_req.extent;
	BWIT_SET(r->witness, reqs->buddy->witness);
#ifdef FREEPAGE_WHEN
	r->when = when;
#endif
	r->rup = rup;
	AZ(r->page);
	r->bits = (uint8_t)bits;
	r->cram = abslimit8(cram, (int8_t)bits);

	/* We need to fit the allocation if the round-up size is not the next
	 * power of two */
	if (rup != (size_t)1 << bits)
		r->ff = &map->freemap[bits - map->min];
	else
		AZ(r->ff);

	reqs->sz += (size_t)1 << bits;
	return (1);
}

//a_balloc
#ifdef FREEPAGE_WHEN
static inline int
buddywhen_req_page(struct buddy_reqs *reqs, unsigned bits, int8_t cram,
    double when)
#else
static inline int
buddy_req_page(struct buddy_reqs *reqs, unsigned bits, int8_t cram)
#endif
{
	struct i_reqalloc *ra;
	struct i_req_page *r;
	struct slashmap *map;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->n == reqs->space) {
		errno = ENOSPC;
		return (0);
	}
	assert(reqs->n < reqs->space);

	map = reqs->map;
	if (bits < map->min)
		bits = map->min;

	if (bits > map->max) {
		AN(cram);
		bits = map->max;
	}
	assert(bits < maxbits(size_t));

	ra = &reqs->i_reqalloc[reqs->n++];
	ra->magic = I_REQALLOC_MAGIC;
	ra->type = BUDDY_T_PAGE;

	r = &ra->i_req.page;
	BWIT_SET(r->witness, reqs->buddy->witness);
#ifdef FREEPAGE_WHEN
	r->when = when;
#endif
	r->ff = &map->freemap[bits - map->min];
	AZ(r->page);
	r->bits = (uint8_t)bits;
	r->cram = abslimit8(cram, (int8_t)bits);

	reqs->sz += (size_t)1 << bits;

	return (1);
}

static inline int
BUDDYF(return_off_page)(struct buddy_returns *rets, struct buddy_off_page *page)
{
	struct i_return *a;
	struct slashmap *map;
	buddy_t *buddy;

	CHECK_OBJ_NOTNULL(rets, BUDDY_RETURNS_MAGIC);
	buddy = rets->buddy;
	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	if (rets->n == rets->space)
		BUDDYF(return)(rets);

	assert(rets->n < rets->space);

	if (page->off < 0 ||
	    (size_t)page->off + ((size_t)1 << page->bits) > map->size) {
		errno = EINVAL;
		return (0);
	}

	a = &rets->i_return[rets->n++];

	INIT_OBJ(a, I_RETURN_MAGIC);
	a->bits = page->bits;
	a->off = page->off;
	a->size = (size_t)1 << page->bits;
	a->page = ipageof(buddy, page->off, page->bits);

	rets->size += a->size;

	*page = buddy_off_page_nil;

	return (1);
}

static inline int
BUDDYF(return_ptr_page)(struct buddy_returns *rets, struct buddy_ptr_page *ptr)
{
	struct buddy_off_page off;

	off = buddy_ptr_off_page(rets->buddy, *ptr);
	*ptr = buddy_ptr_page_nil;

	return (BUDDYF(return_off_page)(rets, &off));
}

static inline int
BUDDYF(return_off_extent)(struct buddy_returns *rets,
    struct buddy_off_extent *extent)
{
	struct i_return *a;
	struct slashmap *map;
	buddy_t *buddy;
	unsigned bits;

	CHECK_OBJ_NOTNULL(rets, BUDDY_RETURNS_MAGIC);
	buddy = rets->buddy;
	CHECK_OBJ_NOTNULL(buddy, BUDDY_MAGIC);
	map = buddy->map;
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	if (rets->n == rets->space)
		BUDDYF(return)(rets);

	assert(rets->n < rets->space);

	if (extent->off < 0 ||
	    (size_t)extent->off + extent->size > map->size) {
		errno = EINVAL;
		return (0);
	}

	a = &rets->i_return[rets->n++];

	INIT_OBJ(a, I_RETURN_MAGIC);
	bits = log2up(extent->size);
	assert(bits <= UINT8_MAX);
	a->bits = (uint8_t)bits;
	if (extent->size != (size_t)1 << a->bits)
		a->ff = &map->freemap[a->bits - map->min];
	a->off = extent->off;
	a->size = extent->size;
	a->page = ipageof(buddy, extent->off, a->bits);

	rets->size += a->size;

	*extent = buddy_off_extent_nil;

	return (1);
}

static inline int
BUDDYF(return_ptr_extent)(struct buddy_returns *rets,
    struct buddy_ptr_extent *ptr)
{
	struct buddy_off_extent off;

	off = buddy_ptr_off_extent(rets->buddy, *ptr);
	*ptr = buddy_ptr_extent_nil;

	return (BUDDYF(return_off_extent)(rets, &off));
}

/*
 * return allocations from reqs
 */
static inline struct buddy_off_extent
_buddy_get_off_extent(const struct buddy_reqs *reqs, uint8_t n)
{
	struct buddy_off_extent off;
	struct i_reqalloc *a;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	assert(n < reqs->i_wait.finid);
	a = &reqs->i_reqalloc[n];
	CHECK_OBJ(a, I_REQALLOC_MAGIC);
	assert(a->type == BUDDY_T_EXTENT);
	off = a->off_alloc.extent;
	a->off_alloc.extent = buddy_off_extent_nil;
	return (off);
}

static inline uint8_t
buddy_reqs_next_ready(struct buddy_reqs *reqs)
{
	(void) BUDDYF(alloc_async_ready)(reqs);
	assert(reqs->i_wait.finid >= reqs->i_wait.next);
	return (reqs->i_wait.finid - reqs->i_wait.next);
}

static inline int
buddy_reqs_done(struct buddy_reqs *reqs)
{
	uint8_t rdy;

	rdy = BUDDYF(alloc_async_ready)(reqs);
	assert(rdy <= reqs->n);
	return (rdy == reqs->n);
}

#ifndef FREEPAGE_WHEN
static inline struct buddy_off_extent
buddy_get_off_extent(const struct buddy_reqs *reqs, uint8_t n)
{

	return (_buddy_get_off_extent(reqs, n));
}

static inline struct buddy_off_extent
buddy_get_next_off_extent(struct buddy_reqs *reqs)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->i_wait.next == reqs->i_wait.finid)
		return (buddy_off_extent_nil);
	return (buddy_get_off_extent(reqs, reqs->i_wait.next++));
}

#endif

static inline struct buddy_ptr_extent
buddy_get_ptr_extent(const struct buddy_reqs *reqs, uint8_t n)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	return (buddy_off_ptr_extent(reqs->buddy,
		_buddy_get_off_extent(reqs, n)));
}
#define _buddy_get_ptr_extent(reqs, n)		\
	buddy_get_ptr_extent(reqs, n)

static inline struct buddy_ptr_extent
buddy_get_next_ptr_extent(struct buddy_reqs *reqs)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->i_wait.next == reqs->i_wait.finid)
		return (buddy_ptr_extent_nil);
	return (buddy_get_ptr_extent(reqs, reqs->i_wait.next++));
}

static inline struct buddy_off_page
_buddy_get_off_page(const struct buddy_reqs *reqs, uint8_t n)
{
	struct buddy_off_page off;
	struct i_reqalloc *a;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	assert(n < reqs->i_wait.finid);
	a = &reqs->i_reqalloc[n];
	CHECK_OBJ(a, I_REQALLOC_MAGIC);
	assert(a->type == BUDDY_T_PAGE);
	off = a->off_alloc.page;
	a->off_alloc.page = buddy_off_page_nil;
	return (off);
}

#ifndef FREEPAGE_WHEN
static inline struct buddy_off_page
buddy_get_off_page(const struct buddy_reqs *reqs, uint8_t n)
{
	return (_buddy_get_off_page(reqs, n));
}

static inline struct buddy_off_page
buddy_get_next_off_page(struct buddy_reqs *reqs)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->i_wait.next == reqs->i_wait.finid)
		return (buddy_off_page_nil);
	return (buddy_get_off_page(reqs, reqs->i_wait.next++));
}
#endif

static inline struct buddy_ptr_page
buddy_get_ptr_page(const struct buddy_reqs *reqs, uint8_t n)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	return (buddy_off_ptr_page(reqs->buddy,
		_buddy_get_off_page(reqs, n)));
}
#define _buddy_get_ptr_page(reqs, n)		\
	buddy_get_ptr_page(reqs, n)

static inline struct buddy_ptr_page
buddy_get_next_ptr_page(struct buddy_reqs *reqs)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	if (reqs->i_wait.next == reqs->i_wait.finid)
		return (buddy_ptr_page_nil);
	return (buddy_get_ptr_page(reqs, reqs->i_wait.next++));
}

/* single allocations simplified interface */

#define RETF(what)							\
static inline void							\
BUDDYF(return1_ ## what)(buddy_t *buddy, struct buddy_ ## what *ret) \
{									\
	struct buddy_returns *rets = BUDDY_RETURNS_STK(buddy, 1);	\
									\
	AN(BUDDYF(return_ ## what)(rets, ret));				\
	BUDDYF(return)(rets);						\
}

// XXX remove off interface for !FREEPAGE_WHEN
RETF(ptr_extent)
RETF(off_extent)
RETF(ptr_page)
RETF(off_page)

#define BUDDY_ALLOC1(lval, buddy, type, what, ...) do {		\
	struct buddy_reqs *reqs = BUDDY_REQS_STK(buddy, 1);		\
	int done;							\
									\
	done = BUDDYF(req_ ## what)(reqs, __VA_ARGS__);		\
	if (! done || ! BUDDYF(alloc)(reqs)) {				\
		lval = buddy_ ## type ## _ ## what ## _nil;		\
		break;							\
	}								\
	assert(done == 1);						\
	lval = _buddy_get_ ## type ## _ ## what(reqs, 0);		\
} while(0)

#define BUDDY_ALLOC1_WAIT(lval, buddy, type, what, pri, ...) do {	\
	struct buddy_reqs *reqs = BUDDY_REQS_STK(buddy, 1);		\
									\
	BUDDY_REQS_PRI(reqs, pri);					\
	if (BUDDYF(req_ ## what)(reqs, __VA_ARGS__) != 1) {		\
		lval = buddy_ ## type ## _ ## what ## _nil;		\
		break;							\
	}								\
	if (BUDDYF(alloc_wait)(reqs) == 1)				\
		lval = _buddy_get_ ## type ## _ ## what(reqs, 0);	\
	else								\
		lval = buddy_ ## type ## _ ## what ## _nil;		\
	BUDDYF(alloc_wait_done)(reqs);					\
} while(0)

#define REQF(macro, sfx, type, what, decl, args)			\
static inline struct buddy_ ## type ## _ ## what			\
BUDDYF(alloc1_ ## type ## _ ## what ## sfx)(buddy_t *buddy, decl)	\
{									\
	struct buddy_ ## type ## _ ## what r;				\
									\
	macro(r, buddy, type, what, args);				\
	return (r);							\
}

#ifdef FREEPAGE_WHEN
#define PAGE_DECL uint8_t bits, int8_t cram, double when
#define PAGE_ARG bits, cram, when
#else
#define PAGE_DECL uint8_t bits, int8_t cram
#define PAGE_ARG bits, cram
#endif

#define PAGE_DECL_WAIT uint8_t pri, PAGE_DECL
#define PAGE_ARG_WAIT pri, PAGE_ARG

//lint -e123
// XXX remove off interface for !FREEPAGE_WHEN
REQF(BUDDY_ALLOC1, , off, page, PAGE_DECL, PAGE_ARG)
REQF(BUDDY_ALLOC1, , ptr, page, PAGE_DECL, PAGE_ARG)
REQF(BUDDY_ALLOC1_WAIT, _wait, off, page, PAGE_DECL_WAIT, PAGE_ARG_WAIT)
REQF(BUDDY_ALLOC1_WAIT, _wait, ptr, page, PAGE_DECL_WAIT, PAGE_ARG_WAIT)

#ifdef FREEPAGE_WHEN
#define EXTENT_DECL size_t size, int8_t cram, double when
#define EXTENT_ARG size, cram, when
#else
#define EXTENT_DECL size_t size, int8_t cram
#define EXTENT_ARG size, cram
#endif

#define EXTENT_DECL_WAIT uint8_t pri, EXTENT_DECL
#define EXTENT_ARG_WAIT pri, EXTENT_ARG

// XXX remove off interface for !FREEPAGE_WHEN
REQF(BUDDY_ALLOC1, , off, extent, EXTENT_DECL, EXTENT_ARG)
REQF(BUDDY_ALLOC1, , ptr, extent, EXTENT_DECL, EXTENT_ARG)
REQF(BUDDY_ALLOC1_WAIT, _wait, off, extent, EXTENT_DECL_WAIT, EXTENT_ARG_WAIT)
REQF(BUDDY_ALLOC1_WAIT, _wait, ptr, extent, EXTENT_DECL_WAIT, EXTENT_ARG_WAIT)

/*
 * assertion functions
 *
 * so far, they only do anthing if the witness is active.
 * we could perform minimal checks on the buddy bitf, and
 * we even could perform them without holding the lock
 *
 * XXX TODO ptr & page variants
 */

static inline void
BUDDYF(assert_alloced_off_extent)(buddy_t *buddy,
   const struct buddy_off_extent *e)
{
#ifdef BUDDY_WITNESS
	AZ(pthread_mutex_lock(&buddy->map_mtx));
	BWIT_ASSERT_ALLOCED(buddy->witness, e->off, e->size);
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
#else
	(void) buddy;
	(void) e;
#endif
}

static inline void
BUDDYF(assert_free_off_extent)(buddy_t *buddy,
   const struct buddy_off_extent *e)
{
#ifdef BUDDY_WITNESS
	AZ(pthread_mutex_lock(&buddy->map_mtx));
	BWIT_ASSERT_FREE(buddy->witness, e->off, e->size);
	AZ(pthread_mutex_unlock(&buddy->map_mtx));
#else
	(void) buddy;
	(void) e;
#endif
}

//lint +e123
/*
 * buddy2 interface
 * ============================================================
 */
