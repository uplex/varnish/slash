/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

// shared between storage and slashmap util

#include "bitf.h"

#define VSM_CLASS_SLASH	"Slash"

struct slashmap {
	unsigned		magic;
#define BUDDY_MAP_MAGIC		0x57f959f8
	struct slashmap		*self;		// for vsm offset calculations
	size_t			mmap_size;	// size of the slashmap
	size_t			size;		// size of the buddy area
	unsigned		min;		// bits
	unsigned		max;		// bits
	struct bitf		*freemap[];
};

static inline struct bitf **
freemapsl(struct slashmap *map, unsigned bits)
{
	assert(bits >= map->min);
	assert(bits <= map->max);
	return (&map->freemap[bits - map->min]);
}

static inline struct bitf * const *
freemapsr(const struct slashmap *map, unsigned bits)
{
	assert(bits >= map->min);
	assert(bits <= map->max);
	return (&map->freemap[bits - map->min]);
}

// for slashmap, but included here for testing
struct mappage {
	int	level;
	size_t	page;
};


/*
 * we are looking at page "at" from the current zoom level
 *
 * return page + 1 or 0 for not found (like bitf_ffs)
 *
 * the lowest possible address is always on a smaller page.
 * so we run two passes:
 *
 * - check smaller pages (lower level)
 * - check larger pages (higher level), rounding up
 */

static inline struct mappage
next_freepage(const struct slashmap *map, struct mappage at, size_t *cache)
{
	struct mappage low, from = at;
	size_t b;
	unsigned u;
	int i, j;

	/*
	 * start down search at the highest page size as a multiple of
	 * the current page
	 */
	i = ffsszt((bitf_word_t)from.page);
	if (i > 0)
		i--;

	j = (int)map->max - from.level;
	if (i > j)
		i = j;
	assert(i >= 0);
	from.level += i;
	from.page >>= i;
	assert(from.level >= (int)map->min);

	at = from;

	/* find the lowest free address */
	low.page = 0;
	low.level = -1;
	while (from.level >= (int)map->min) {
		assert(from.level <= (int)map->max);

		u = (unsigned)from.level - map->min;
		if (from.page >= bitf_nbits(map->freemap[u]))
			break;
		b = bitf_ffs_from_cached(map->freemap[u],
		    &cache[from.level], from.page);
		assert(b <= bitf_nbits(map->freemap[u]));

		if (b-- == 0)
			goto next;

//		DBG("next down %d/%zu = %zu", from.level, from.page, b);

		/* lowest possible */
		if (b == from.page)
			return (from);

		if (low.level == -1 ||
		    b < (low.page << (low.level - from.level))) {
			low.page = b;
			low.level = from.level;
		}

	  next:
		from.level--;
		from.page <<= 1;
	}

	from = at;

	while (from.level < (int)map->max) {
		from.level++;
		from.page >>= 1;

		assert(from.level <= (int)map->max);

		u = (unsigned)from.level - map->min;
		if (from.page >= bitf_nbits(map->freemap[u]))
			break;
		b = bitf_ffs_from_cached(map->freemap[u],
		    &cache[from.level], from.page);
		assert(b <= bitf_nbits(map->freemap[u]));

		if (b == 0)
			continue;
		b--;

//		DBG("next up %d/%zu = %zu", from.level, from.page, b);

		/* lowest possible */
		if (b == from.page)
			return (from);

		if (low.level == -1 ||
		    (b << (from.level - low.level) < low.page)) {
			low.page = b;
			low.level = from.level;
		}
	}
	return (low);
}
