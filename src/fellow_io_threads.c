/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include <fcntl.h>
//#include <string.h>

// varnish
#include "miniobj.h"
#include "vdef.h"
#include "vas.h"

#include "bitf.h"

#include "fellow_io_backend.h"
#include "fellow_io_ioctl.h"

enum fellow_io_thread_cmd {
	FIOT_INVAL = 0,
	FIOT_READ,
	FIOT_WRITE,
	FIOT_FALLOCATE
};

struct fellow_io_thread_r {
	unsigned			magic;
#define FELLOW_IO_THREAD_R_MAGIC	0x054f93f1
	fellow_task_privstate		taskstate;
	struct fellow_io_thread		*ctx;
	enum fellow_io_thread_cmd	cmd;
	int				mode;
	void				*buf;
	size_t				bytes;
	off_t				off;
	struct fellow_io_status	status;
};

struct fellow_io_thread {
	unsigned			magic;
#define FELLOW_IO_THREAD_MAGIC		0xbf491fde
	int				fd;
	fellow_task_run_t		*taskrun;
	struct fellow_io_ioctl		*ioctl;
	unsigned			ioctl_outstanding;
	struct bitf			*free;
	struct bitf			*complete;
	pthread_mutex_t			mtx;
	pthread_cond_t			cond;
	struct fellow_io_thread_r	req[];
};

unsigned
fellow_io_blkdiscard_enq(fellow_ioctx_t *ctxp, uint64_t info,
    const struct fellow_io_discard *discards, unsigned n)
{
	struct fellow_io_thread *ctx;
	struct fellow_io_ioctl_blkdiscard_ret r;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);

	r = fellow_io_ioctl_blkdiscard_enq(ctx->ioctl, info, discards, n);

	ctx->ioctl_outstanding += r.nslots;
	return (r.ndiscards);
}

static void
test_task(struct worker *wrk, void *priv)
{
	int *answer = priv;

	(void) wrk;
	AN(answer);
	*answer = 42;
}

fellow_ioctx_t *
fellow_io_init(int fd, unsigned entries, void *base, size_t len,
    fellow_task_run_t taskrun)
{
	fellow_task_privstate taskstate;
	struct fellow_io_thread *ctx;
	size_t isz, bsz, sz, u;
	int answer = 0;
	char *p;

	(void) base;
	(void) len;

	AZ(taskrun(test_task, &answer, &taskstate));

	isz = sizeof *ctx + entries * sizeof *ctx->req;
	bsz = bitf_sz((size_t)entries, BITF_INDEX);
	sz = isz + 2 * bsz;

	p = malloc(sz);
	AN(p);
	memset(p, 0, sz);

	ctx = (void *)p;
	ctx->magic = FELLOW_IO_THREAD_MAGIC;
	ctx->fd = fd;
	ctx->taskrun = taskrun;
	p += isz;

	ctx->free = bitf_init(p, (size_t)entries, bsz, BITF_INDEX);
	p += bsz;
	ctx->complete = bitf_init(p, (size_t)entries, bsz, BITF_INDEX);
	AN(ctx->free);
	AN(ctx->complete);
	for (u = 0; u < entries; u++)
		AN(bitf_set(ctx->free, u));
	assert(ctx->free->nset == ctx->free->nbits);
	assert(ctx->complete->nset == 0);

	p += bsz;
	assert(p - sz == (void *)ctx);

	AZ(pthread_mutex_init(&ctx->mtx, NULL));
	AZ(pthread_cond_init(&ctx->cond, NULL));

	ctx->ioctl = fellow_io_ioctl_init(fd, taskrun, entries);
	AN(ctx->ioctl);

	while (answer != 42)
		(void) usleep(1000);

	return (IOCTX(ctx));
}

void
fellow_io_fini(fellow_ioctx_t **ctxp)
{
	struct fellow_io_thread *ctx;

	if (*ctxp == NULL)
		return;

	TAKE_OBJ_NOTNULL(ctx, (void **)ctxp, FELLOW_IO_THREAD_MAGIC);

	struct fellow_io_status results[ctx->free->nbits];

	(void) fellow_io_submit_and_wait(IOCTX(ctx), results,
	    (unsigned)ctx->free->nbits, UINT_MAX, NULL, NULL);

	fellow_io_ioctl_fini(&ctx->ioctl);

	AZ(pthread_cond_destroy(&ctx->cond));
	AZ(pthread_mutex_destroy(&ctx->mtx));

	assert(ctx->free->nset == ctx->free->nbits);
	assert(ctx->complete->nset == 0);

	FREE_OBJ(ctx);
}

unsigned
fellow_io_entries(fellow_ioctx_t *ctxp)
{
	struct fellow_io_thread *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);
	return ((unsigned)ctx->free->nbits);
}

unsigned
fellow_io_outstanding(fellow_ioctx_t *ctxp)
{
	struct fellow_io_thread *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);
	return (ctx->ioctl_outstanding +
	    ((unsigned)ctx->free->nbits - (unsigned)ctx->free->nset));
}

unsigned
fellow_io_unsubmitted(fellow_ioctx_t *ctxp)
{
	(void) ctxp;
	return (0);
}


static struct fellow_io_thread_r *
fellow_io_thread_get_req(struct fellow_io_thread *ctx)
{
	struct fellow_io_thread_r *req = NULL;
	size_t sz;

	AZ(pthread_mutex_lock(&ctx->mtx));
	sz = bitf_ffs(ctx->free);
	if (sz > 0) {
		sz--;
		assert(sz < UINT_MAX);

		AZ(bitf_get(ctx->complete, sz));
		AN(bitf_clr(ctx->free, sz));
		req = &ctx->req[sz];
	}
	AZ(pthread_mutex_unlock(&ctx->mtx));
	return (req);
}

static void
fellow_io_thread_task(struct worker *wrk, void *priv)
{
	struct fellow_io_thread *ctx;
	struct fellow_io_thread_r *req;
	unsigned slot;
	ssize_t r;

	(void)wrk;
	CAST_OBJ_NOTNULL(req, priv, FELLOW_IO_THREAD_R_MAGIC);
	ctx = req->ctx;

	switch (req->cmd) {
	case FIOT_READ:
		r = pread(ctx->fd, req->buf, req->bytes, req->off);
		break;
	case FIOT_WRITE:
		r = pwrite(ctx->fd, req->buf, req->bytes, req->off);
		break;
	case FIOT_FALLOCATE:
		r = fallocate(ctx->fd, req->mode, req->off, (off_t)req->bytes);
		break;
	default:
		WRONG("cmd in io_thread_task");
	}

	if (r < 0) {
		req->status.result = -errno;
	}
	else {
		assert(r < INT32_MAX);
		req->status.result = (int32_t)r;
	}
	req->status.flags = (unsigned)errno;

	assert(req >= req->ctx->req);
	slot = (unsigned)(req - req->ctx->req);

	AZ(pthread_mutex_lock(&ctx->mtx));
	AN(bitf_set(ctx->complete, (size_t)slot));
	AZ(pthread_cond_signal(&ctx->cond));
	AZ(pthread_mutex_unlock(&ctx->mtx));
}

static int
fellow_io_async_enq(fellow_ioctx_t *ctxp, enum fellow_io_thread_cmd cmd,
    uint64_t info, void *buf, size_t bytes, off_t off)
{
	struct fellow_io_thread_r *req;
	struct fellow_io_thread *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);
	req = fellow_io_thread_get_req(ctx);
	if (req == NULL)
		return (0);

	INIT_OBJ(req, FELLOW_IO_THREAD_R_MAGIC);
	req->ctx = ctx;
	req->cmd = cmd;
	req->buf = buf;
	req->bytes = bytes;
	req->off = off;
	req->status.info = info;

	AZ(ctx->taskrun(fellow_io_thread_task, req,
		&req->taskstate));
	return (1);
}

int
fellow_io_read_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, void *buf, size_t bytes, off_t off)
{
	return (fellow_io_async_enq(ctxp, FIOT_READ,
	    info, buf, bytes, off));
}

int
fellow_io_write_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, const void *buf, size_t bytes, off_t off)
{
	return (fellow_io_async_enq(ctxp, FIOT_WRITE,
	    info, TRUST_ME(buf), bytes, off));
}

int
fellow_io_fallocate_enq(fellow_ioctx_t *ctxp, uint64_t info,
    int mode, uint64_t offset, uint64_t len)
{
	struct fellow_io_thread_r *req;
	struct fellow_io_thread *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);
	req = fellow_io_thread_get_req(ctx);
	if (req == NULL)
		return (0);

	INIT_OBJ(req, FELLOW_IO_THREAD_R_MAGIC);
	req->ctx = ctx;
	req->cmd = FIOT_FALLOCATE;
	req->mode = mode;
	req->bytes = (size_t)len;
	req->off = (off_t)offset;
	req->status.info = info;

	AZ(ctx->taskrun(fellow_io_thread_task, req,
		&req->taskstate));
	return (1);
}

/*
 * completions are registered in the space provided
 * in addition, cb is called
 */
unsigned
fellow_io_submit_and_wait(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results,
    unsigned space, unsigned min, fellow_io_compl_cb cb, void *priv)
{
	struct fellow_io_thread *ctx;
	unsigned u, advance, ret = 0;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_THREAD_MAGIC);

	AN(results);
	AN(space);

	if (min > space)
		min = space;

	AZ(pthread_mutex_lock(&ctx->mtx));

	while (min > ret) {
		advance = 0;

		while (space) {
			u = (unsigned)bitf_ffs(ctx->complete);
			if (u == 0)
				break;

			u--;

			*results = ctx->req[u].status;
			advance++;
			results++;
			space--;

			AN(bitf_clr(ctx->complete, (size_t)u));
			AN(bitf_set(ctx->free, (size_t)u));
		}
		AZ(pthread_mutex_unlock(&ctx->mtx));

		u = fellow_io_ioctl_peek(ctx->ioctl, results, space);
		results += u;
		advance += u;
		assert(ctx->ioctl_outstanding >= u);
		ctx->ioctl_outstanding -= u;
		assert(u <= space);
		space -= u;

		ret += advance;

		if (cb && advance)
			cb(priv, results - advance, advance);

		if (min <= ret)
			goto out_unlocked;

		u = fellow_io_ioctl_wait(ctx->ioctl);

		AZ(pthread_mutex_lock(&ctx->mtx));

		if (u > 0 || bitf_nset(ctx->complete) > 0)
			continue;

		if (bitf_nset(ctx->free) == bitf_nbits(ctx->free))
			break;

		while (bitf_nset(ctx->complete) == 0)
			AZ(pthread_cond_wait(&ctx->cond, &ctx->mtx));
	}
	AZ(pthread_mutex_unlock(&ctx->mtx));

  out_unlocked:
	return (ret);
}

/* threads always submit directly (by starting a worker thread with the IO),
 * so both wait variants are identical
 */

unsigned
fellow_io_wait_completions_only(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results,
    unsigned space, unsigned min, fellow_io_compl_cb cb, void *priv)
{
	return (fellow_io_submit_and_wait(ctxp, results, space, min, cb, priv));
}
