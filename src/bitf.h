/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifndef BITF_H_INCLUDED
#define BITF_H_INCLUDED

#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "bitsof.h"

/*
 * bitf_index() / (struct bitf).idxoff:
 *
 * We accelerate lookup in large bitfields with an index which has a bit set IFF
 * a word is non-zero
 *
 * As each index is recursively indexed also, we effectively reduce lookup time
 * to log(size), equaling that of a btree without the memory overhead
 *
 * idxoff is relative to the address of the bitfield itself to support
 * relocation
 */

struct bitf {
	unsigned	magic;
#define BITF_MAGIC	0x91ebd5ff
	size_t		nbits;
	size_t		nset;	// count of set bits
	size_t		idxoff; // distance to next index or 0 if none
	void		*extra;
	bitf_word_t	bits[];
};
#define bitf_hdrsz (offsetof(struct bitf, bits))

// devide with round up
#define divceil(n, d) (((n) + ((d) - 1)) / (d))
#define wsz sizeof(bitf_word_t)
#define wbits (wsz * 8)
#define bitf_words(bits) (divceil(bits, wbits))
#define bitf_sz1(bits) (bitf_hdrsz + bitf_words(bits) * wsz)

#define BITF_INDEX (wbits * 8)
#define BITF_NOIDX SIZE_MAX

static inline size_t
bitf_sz(size_t bits, size_t index_thr)
{
	size_t sz;

	sz = bitf_sz1(bits);
	while (bits > index_thr) {
		bits = divceil(bits, wbits);
		sz += bitf_sz1(bits);
	}
	return (sz);
}

/*
 * for a given size, get the maximum number of bits we can fit
 *
 * This turns out quite hard to do. We basically have a geometric series
 *
 * S = sum_{n = 0}^{\infty}{ar^n}
 *
 * closed form: S = a / 1 - r
 *
 * here a = sizeof(struct bitf) + ceil(bits / 64) * 64
 *      r = 1/64
 *
 * this function approximates bitf_sz:
 */
#if 0
static inline double
bsz(size_t bits)
{
	double a;
	const double r = 1.0 / wbits;

	a = sizeof(struct bitf) * 8 + bits;

	return ((a / (1.0 - r) / 8.0));
}
#endif
/*
 * so this function is an attempt to reverse it, but it only returns a first
 * estimation
 */
static inline size_t
sz_nbits_est(size_t sz) {
	double a;
	const double r = 1.0 / wbits;
	size_t ret;

	a = (((double)sz - bitf_hdrsz) * 8) * (1.0 - r);

	ret = (typeof(ret))a;
	ret &= ~(((size_t)1 << wsz) - 1);

	return (ret);
}

/*
 * ... and to get the best value, we need to narrow it down numerically
 *
 * this isually hits the right result after 5 iterations
 */
static inline size_t
sz_nbits(const size_t sz, const size_t index_thr)
{
	size_t trysz;
	size_t d, hi, lo; // in units of bitf_words() / bitf_word_t

	assert(sz >= bitf_sz1(1));
	assert(sz < (size_t)1 << 62);	// otherwise overflow

	if (index_thr == BITF_NOIDX)
		return (((sz - bitf_hdrsz) / wsz) * wbits);

	lo = sz_nbits_est(sz) / wbits;
	trysz = bitf_sz(lo * wbits, index_thr);

	if (trysz > sz) {
		d = bitf_words((trysz - sz) * 8);

		hi = lo;
		if (lo > d)
			lo -= d;
	}
	else {
		// trysz <= sz
		d = bitf_words(((sz - trysz) + 1) * 8);

		hi = lo + d;
	}

	// opportunistic: right at first shot
	if (bitf_sz(lo * wbits, index_thr) <= sz &&
	    bitf_sz(lo * wbits + 1, index_thr) > sz)
		goto out;

	while (bitf_sz(lo * wbits, index_thr) <= sz &&
	    bitf_sz(hi * wbits, index_thr) > sz &&
	    hi - lo > 1) {
		d = (hi + lo) / 2;
		if (bitf_sz(d * wbits, index_thr) <= sz)
			lo = d;
		else if (bitf_sz(d * wbits, index_thr) > sz)
			hi = d;
		else
			assert(0);	//lint !e506 const val
	}
  out:
	assert(bitf_sz(lo * wbits, index_thr) <= sz);
	assert(bitf_sz(lo * wbits + 1, index_thr) > sz);
	return (lo * wbits);
}


// pointer to word from bits
#define bitf_word(bitf, bit)						\
	(assert((bitf)->magic == BITF_MAGIC),				\
	 assert((bit) < (bitf)->nbits),					\
	 &(bitf)->bits[bit / wbits])

// set bit in word
#define bitf_bit(bit) ((bitf_word_t)1 << (bit % wbits))

static inline struct bitf *
bitf_init(void *p, size_t bits, size_t space, size_t index_thr)
{
	struct bitf *bitf;
	size_t sz = bitf_sz1(bits);
	char *pp;

	assert(space >= sz);

	memset(p, 0, sz);
	bitf = p;
	bitf->magic = BITF_MAGIC;
	bitf->nbits = bits;

	if (bits > index_thr) {
		bits = divceil(bits, wbits);
		pp = p;
		pp += sz;
		space -= sz;
		bitf->idxoff = sz;
		p = bitf_init(pp, bits, space, index_thr);
		assert(p);
	}

	return (bitf);
}

static inline struct bitf *
bitf_indexl(struct bitf *bitf)
{
	assert(bitf->idxoff);
	//lint --e(826)
	return ((struct bitf *)((char *)bitf + bitf->idxoff));
}

static inline const struct bitf *
bitf_indexr(const struct bitf *bitf)
{
	assert(bitf->idxoff);
	//lint --e(826)
	return ((const struct bitf *)((const char *)bitf + bitf->idxoff));
}

// return if bit was set (1: was zero, 0: was already one)
static inline unsigned
bitf_set(struct bitf *bitf, size_t bit)
{
	bitf_word_t *w = bitf_word(bitf, bit);
	const bitf_word_t b = bitf_bit(bit);
	unsigned c, o;

	if (bitf->idxoff && !*w) {
		c = bitf_set(bitf_indexl(bitf), bit / wbits);
		assert(c);
	}

	o = !(*w & b);
	*w |= b;
	bitf->nset += o;
	return (o);
}

// number of bits set
static inline size_t
bitf_nset(const struct bitf *bitf)
{
	CHECK_OBJ_NOTNULL(bitf, BITF_MAGIC);
	return (bitf->nset);
}

// number of bit set
static inline size_t
bitf_nbits(const struct bitf *bitf)
{
	CHECK_OBJ_NOTNULL(bitf, BITF_MAGIC);
	return (bitf->nbits);
}

// return if bit was cleared (1: was one, 0: was already zero)
static inline unsigned
bitf_clr(struct bitf *bitf, size_t bit)
{
	bitf_word_t *w = bitf_word(bitf, bit);
	const bitf_word_t b = bitf_bit(bit);
	unsigned c, o;

	o = !!(*w & b);
	*w &= ~b;
	bitf->nset -= o;

	if (bitf->idxoff && !*w && o) {
		c = bitf_clr(bitf_indexl(bitf), bit / wbits);
		assert(c);
	}

	return (o);
}

static inline int
bitf_get(const struct bitf *bitf, size_t bit)
{
	const bitf_word_t *w = bitf_word(bitf, bit);

	return !!(*w & bitf_bit(bit));
}

/* return bit number PLUS ONE or 0 if none set */
static inline size_t
bitf_ffs(const struct bitf *bitf)
{
	const bitf_word_t *w = bitf->bits;
	const size_t l = bitf_words(bitf->nbits);
	size_t u = 0;
	int t;

	if (bitf->idxoff) {
		u = bitf_ffs(bitf_indexr(bitf));
		if (u == 0)
			return (0);
		u--;
	}

	for (w += u; u < l; u++, w++) {
		if (*w == 0)
			continue;
		t = ffsszt(*w);
		assert(t > 0);
		return (u * wbits + (unsigned)t);
	}

	return (0);
}

/* return bit number PLUS ONE or 0 if none set
 * at or after a start bit position
 *
 * should work approximately correct for dirty/unlocked/concurrent
 * reads
 */

static inline size_t
bitf_ffs_from(const struct bitf *bitf, size_t start)
{
	const size_t l = bitf_words(bitf->nbits);
	size_t s, u, b;
	int t;
	const bitf_word_t *w = bitf->bits;
	bitf_word_t ww;

	s = start / wbits;
	b = start % wbits;

	if (bitf->idxoff) {
		u = bitf_ffs_from(bitf_indexr(bitf), s);
		if (u == 0)
			return (0);
		u--;
		if (u > s) {
			s = u;
			b = 0;
		} else
			assert(u == s);
	}

	for (u = s, w += u; u < l; u++, w++) {
		ww = *w;
		if (ww == 0)
			continue;
		t = ffsszt(ww);
		// DIRTY assert(t > 0);

		if (u != s || (t > 0 && (unsigned)t > b))
			return (u * wbits + (unsigned)t);

		/* in our word, the first set bit is before the start
		 * we clear all bits bits before the start and check
		 * if another one is set
		 */
		AN(b);
		ww &= ~(((bitf_word_t)1 << b) - 1);
		t = ffsszt(ww);
		if (t > 0)
			return (u * wbits + (unsigned)t);
	}

	return (0);
}

/*
 * unless *cache is reset to SIZE_MAX, must be called with increasing start
 * values only
 */
static inline size_t
bitf_ffs_from_cached(const struct bitf *bitf, size_t *cache, size_t start)
{
	size_t b;

	AN(cache);
	b = *cache;
	if (b == 0 || (b > start && b < SIZE_MAX))
		return (b);
	b = bitf_ffs_from(bitf, start);
	*cache = b;
	return (b);
}
#endif
