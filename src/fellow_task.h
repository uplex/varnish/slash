/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifndef FELLOW_TASK_H
#define FELLOW_TASK_H
struct worker;

// big enough to hold a varnish struct pool_task
typedef struct fellow_task_privstate_s {
	void *p[4];
} fellow_task_privstate;

// == varnish task_func_t
typedef void fellow_task_func_t(struct worker *wrk, void *priv);
typedef int fellow_task_run_t(fellow_task_func_t func, void *priv,
    fellow_task_privstate *);
#endif
