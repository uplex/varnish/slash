/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "bitf.h"

/*
 * a struct bitf is always contigious, and allocating a long region of memory is
 * an issue for memory allocators, which usally are quite fragmented.
 *
 * this data structure trades some overhead for bitf segmentation, that is, an
 * overall bitfield comprised of several bitfs.
 */

struct bitfss {
	size_t		off;
	struct bitf	*bitf;
};

#define bitfs_sz(n) (sizeof(struct bitfs) + n * sizeof(struct bitfss))

struct bitfs {
	unsigned	magic;
#define BITFS_MAGIC	0xc18b98bd
	uint16_t	nbitfss;	// length of flexarray (used)
	uint16_t	lbitfss;	// space for flexarray
	size_t		space;		// for reset
	size_t		nbits;
	size_t		nset;		// count of set bits
	struct bitfss	s[];
};

#define bitfs_size(n) (sizeof(struct bitfs) + (n + 1) * sizeof(struct bitfss))

static inline struct bitfs *
bitfs_init(void *p, size_t space)
{
	struct bitfs *bitfs;

	AN(p);
	assert(space > sizeof *bitfs);
	memset(p, 0, space);
	bitfs = p;
	bitfs->magic = BITFS_MAGIC;
	bitfs->space = space;

	space -= sizeof *bitfs;
	space /= sizeof *bitfs->s;
	assert(space > 1);
	assert(space <= UINT16_MAX);

	// last element is next offset for simplicity of search
	bitfs->lbitfss = (uint16_t)space - 1;

	return (bitfs);
}

static inline void
bitfs_reset(struct bitfs *bitfs)
{

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	AN(bitfs->space);
	AN(bitfs_init(bitfs, bitfs->space));
}

static inline void
bitfs_add(struct bitfs *bitfs, struct bitf *bitf)
{
	struct bitfss *s;

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	CHECK_OBJ_NOTNULL(bitf, BITF_MAGIC);

	assert(bitfs->nbitfss < bitfs->lbitfss);
	assert(bitfs->nset == 0);

	assert(bitf_nset(bitf) == 0);
	assert(bitf_nbits(bitf) > 0);

	s = &bitfs->s[bitfs->nbitfss++];
	assert(s->off == bitfs->nbits);
	AZ(s->bitf);
	s->bitf = bitf;

	bitfs->nbits += bitf_nbits(bitf);
	s[1].off = bitfs->nbits;
}

static inline uint16_t
bitfs_idx(const struct bitfs *bitfs, size_t bit)
{
	const struct bitfss *s;
	uint16_t d, lo, hi;

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	assert(bit < bitfs->nbits);
	s = bitfs->s;

	lo = 0;
	hi = bitfs->nbitfss;
	assert(bit < s[hi].off);

	while (! (s[lo].off <= bit && s[lo + 1].off > bit)) {
		d = (hi + lo) / 2;
		if (s[d].off <= bit)
			lo = d;
		else if (s[d].off > bit)
			hi = d;
		else
			assert(0);	//lint !e506 const
	}
	return (lo);
}

static inline struct bitfss *
bitfs_lookup(struct bitfs *bitfs, size_t bit)
{

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	return (&bitfs->s[bitfs_idx(bitfs, bit)]);
}

static inline const struct bitfss *
bitfs_lookupc(const struct bitfs *bitfs, size_t bit)
{

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	return (&bitfs->s[bitfs_idx(bitfs, bit)]);
}

static inline size_t
bitfs_nset(const struct bitfs *bitfs)
{

	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	return (bitfs->nset);
}

// number of bit set
static inline size_t
bitfs_nbits(const struct bitfs *bitfs)
{
	CHECK_OBJ_NOTNULL(bitfs, BITFS_MAGIC);
	return (bitfs->nbits);
}

// return if bit was set (1: was zero, 0: was already one)
static inline unsigned
bitfs_set(struct bitfs *bitfs, size_t bit)
{
	struct bitfss *s;
	unsigned r;

	s = bitfs_lookup(bitfs, bit);
	r = bitf_set(s->bitf, bit - s->off);
	bitfs->nset += r;
	return (r);
}

static inline unsigned
bitfs_clr(struct bitfs *bitfs, size_t bit)
{
	struct bitfss *s;
	unsigned r;

	s = bitfs_lookup(bitfs, bit);
	r = bitf_clr(s->bitf, bit - s->off);
	AN(bitfs->nset);
	bitfs->nset -= r;
	return (r);
}

static inline int
bitfs_get(const struct bitfs *bitfs, size_t bit)
{
	const struct bitfss *s;

	s = bitfs_lookupc(bitfs, bit);
	return(bitf_get(s->bitf, bit - s->off));
}

/* NO FFS - inefficient ! */
