/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

/*
 * ============================================================
 * Not to be called by production code
 */

#ifdef TEST_DRIVER
// flush the logbuffer so it can hold n entries.
// used for testing
//
// explicit flush of the logbuffer should be avoided
// the log thread flushes in background (XXX TODO)
//
// this is racy
void fellow_log_flush_racy(struct fellow_fd *ffd, unsigned n);
void fellow_log_rewrite(struct fellow_fd *ffd);
buddy_t *fellow_dskbuddy(struct fellow_fd *ffd);
#endif

/*
 * ============================================================
 * API for the fellow log
 */

// DLE utility functions
unsigned fellow_dle_reg_fill(
    struct fellow_dle *entry, unsigned nentry,
    const struct buddy_off_extent *region, unsigned nregion,
    enum dle_type dle_type, const uint8_t hashpfx[DLE_REG_HASHPFXSZ]
    );

size_t fellow_rndup(const struct fellow_fd *ffd, size_t sz);
unsigned fellow_io_ring_size(const char *envvar);

// XXX restructure code, this should not be needed
int fellow_fd(const struct fellow_fd *ffd);
void fellow_logwatcher_kick(struct fellow_fd *ffd);
