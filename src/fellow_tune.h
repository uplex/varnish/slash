/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifdef HAVE_XXHASH_H
#include <xxhash.h>
#endif

struct stvfe_tune {
	unsigned		magic;
#define STVFE_TUNE_MAGIC	0x92d8d31c

#define TUNE(t, n, d, min, max)			\
	t	n;
#include "tbl/fellow_tunables.h"

	// constructor arguments
	size_t			dsksz;
	size_t			memsz;
	size_t			objsize_hint;
};

const char * stvfe_tune_init(struct stvfe_tune *tune,
    size_t memsz, size_t dsksz, size_t objsize_hint);
const char * stvfe_tune_check(struct stvfe_tune *tune);
