/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023,2024 UPLEX Nils Goroll Systemoptimierung.
 * All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * This file is a separate compilation unit because of the mgt/cache split in
 * varnish-cache, but sfe_mgt_tryopen() is also used for storage from vcl
 */

#include "config.h"

#include <stdlib.h>
#include <fcntl.h>	// open
#include <sys/file.h>	// flock
#include <sys/stat.h>	// stat
#include <string.h>	// strerror
#include <stdio.h>	// printf
#include <unistd.h>	// close

#include "mgt/mgt.h"	// VJ_*

#include "fellow_mgt.h"

// fellow_log.c
int fellow_sane_file_path(const char *path);

static char errbuf[1024] = "";
#define STVERR(...) do {					\
		bprintf(errbuf, __VA_ARGS__);			\
		return (errbuf);				\
} while (0)

#define INMGT(x) if (scope == STVFE_GLOBAL) x

const char *
sfe_mgt_tryopen(const char *path, enum stvfe_scope scope)
{
	struct stat st;
	int fd;

	if (stat(path, &st) != 0 &&
	    ! fellow_sane_file_path(path)) {
		STVERR("%s does not exist (stat failed),"
		    " but the path suggests that it should be an "
		    "existing block device. Do you have a typo?\n", path);
	}

	INMGT(VJ_master(JAIL_MASTER_STORAGE));
	fd = open(path, O_RDWR | O_CREAT | O_LARGEFILE, 0600);
	if (fd < 0) {
		INMGT(VJ_master(JAIL_MASTER_LOW));
		STVERR("open(%s) failed: %s (%d)",
		    path, strerror(errno), errno);
	}
	INMGT(VJ_fix_fd(fd, JAIL_FIXFD_FILE));
	if (flock(fd, LOCK_EX | LOCK_NB)) {
		INMGT(VJ_master(JAIL_MASTER_LOW));
		assert(errno == EWOULDBLOCK);
		STVERR("flock(%s) failed: %s (%d)"
		    " - a fellow file can only be used once",
		    path, strerror(errno), errno);
	}
	AZ(flock(fd, LOCK_UN));
	INMGT(VJ_master(JAIL_MASTER_LOW));
	AZ(close(fd));
	return (NULL);
}
