/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <float.h>
#include <stdio.h>

#include "miniobj.h"

#include "fellow_const.h"
#include "fellow_tune.h"
#include "buddy_util.h"

/* initialize with sane defaults */
const char *
stvfe_tune_init(struct stvfe_tune *tune, size_t memsz, size_t dsksz,
    size_t objsize_hint)
{
	INIT_OBJ(tune, STVFE_TUNE_MAGIC);

//lint -save -e773 -e835
#define TUNE(t, n, d, min, max)			\
	tune->n = (d);
#include "tbl/fellow_tunables.h"
//lint -restore
	tune->memsz = memsz;
	tune->dsksz = dsksz;
	tune->objsize_hint = objsize_hint;
	return (stvfe_tune_check(tune));
}

// XXX VSL for warnigs

const char *
stvfe_tune_check(struct stvfe_tune *tune)
{
	unsigned l;
	size_t sz;

	assert (tune->dsksz >= tune->memsz);

#define CHUNK_SHIFT 10

	l = log2down(tune->memsz);
	if (l < MIN_FELLOW_BITS + CHUNK_SHIFT)
		l = MIN_FELLOW_BITS;
	else
		l -= CHUNK_SHIFT;
	assert(l >= MIN_FELLOW_BITS);

	if (tune->chunk_exponent > l) {
		fprintf(stderr,"fellow: chunk_bytes (chunk_exponent) "
		    "limited to %zu (%u) "
		    "(less than 1/%zu of memory size, but at least %zu (%u))\n",
		    (size_t)1 << l, l,
		    (size_t)1 << CHUNK_SHIFT,
		    (size_t)1 << MIN_FELLOW_BITS, (unsigned)MIN_FELLOW_BITS);
		tune->chunk_exponent = l;
	}

	sz = tune->memsz >> (tune->chunk_exponent + 3);
	sz >>= tune->lru_exponent;
	assert(sz <= UINT_MAX);
	l = (unsigned)sz;
	if (tune->mem_reserve_chunks > l) {
		fprintf(stderr,"fellow: mem_reserve_chunks limited to %u "
		    "(less than 1/8 of memory size per lru)\n", l);
		tune->mem_reserve_chunks = l;
	}

	sz = tune->memsz >> (tune->chunk_exponent + 4);
	if (tune->readahead > sz) {
		assert(sz <= UINT_MAX);
		l = (unsigned)sz;
		fprintf(stderr,"fellow: readahead limited to "
		    "%u chunks * %zu chunk_bytes (%u chunk_exponent)"
		    " be less than 1/16 of memory\n",
		    l, (size_t)1 << tune->chunk_exponent, tune->chunk_exponent);
		tune->readahead = l;
	}

	// 2MB
	if (tune->chunk_exponent < 21U) {
		l = 1U << (21U - tune->chunk_exponent);
		if (tune->dsk_reserve_chunks < l) {
			fprintf(stderr,"fellow: dsk_reserve_chunks raised from "
			    "%u to %u (x %zu) for minimum size of 2MB\n",
			    tune->dsk_reserve_chunks, l,
			    (size_t)1 << tune->chunk_exponent);
			tune->dsk_reserve_chunks = l;
		}
	}

	sz = tune->dsksz >> (tune->chunk_exponent + 3);
	assert(sz <= UINT_MAX);
	l = (unsigned)sz;
	if (tune->dsk_reserve_chunks > l) {
		fprintf(stderr,"fellow: dsk_reserve_chunks limited to %u "
		    "(less than 1/8 of disk size)\n", l);
		tune->dsk_reserve_chunks = l;
	}

	if (tune->objsize_max == 0)
		tune->objsize_max = (tune->dsksz >> 2);

//lint --e{685,568} misc const comparisons
#define TUNE(t, n, d, min, max)						\
	if (tune->n < (min))						\
		return ("Value of " #n " is too small, minimum is " #min); \
	if (tune->n > (max))						\
		return ("Value of " #n " is too big, maximum is " #max);

//lint -save -e773 -e835
#include "tbl/fellow_tunables.h"
//lint -restore
	return (NULL);
}
