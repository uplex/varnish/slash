/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

/*
 * TODO: Solaris DKIOCFREE / check with DKIOC_CANFREE
 */
#include "config.h"

#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
#include <string.h>
#include <sys/ioctl.h>
#ifdef HAVE_LINUX_FS_H
#include <linux/fs.h>
#endif

// varnish
#include "miniobj.h"
#include "vdef.h"
#include "vas.h"

#include "bitf.h"

#include "fellow_io_backend.h"
#include "fellow_io_ioctl.h"

#define FIO_DISCARD_BATCH	16

struct fellow_io_ioctl_r {
	unsigned			magic;
#define FELLOW_IO_IOCTL_R_MAGIC	0x15d102dd
	unsigned			ndiscards;
	fellow_task_privstate		taskstate;
	struct fellow_io_ioctl		*fuioctl;
	struct fellow_io_status	status;
	struct fellow_io_discard	discards[FIO_DISCARD_BATCH];
};

struct fellow_io_ioctl {
	unsigned			magic;
#define FELLOW_IO_IOCTL_MAGIC		0xe872341b
	int				fd;
	fellow_task_run_t		*taskrun;
	struct bitf			*free;
	struct bitf			*complete;
	pthread_mutex_t			mtx;
	pthread_cond_t			cond;
	struct fellow_io_ioctl_r	req[];
};


struct fellow_io_ioctl *
fellow_io_ioctl_init(int fd, fellow_task_run_t *taskrun, unsigned entries)
{
	struct fellow_io_ioctl *fuioctl;
	size_t isz, bsz, sz, u;
	char *p;

	isz = sizeof *fuioctl + entries * sizeof *fuioctl->req;
	bsz = bitf_sz((size_t)entries, BITF_INDEX);
	sz = isz + 2 * bsz;

	p = malloc(sz);
	AN(p);
	memset(p, 0, sz);

	fuioctl = (void *)p;
	fuioctl->magic = FELLOW_IO_IOCTL_MAGIC;
	fuioctl->fd = fd;
	fuioctl->taskrun = taskrun;
	p += isz;

	fuioctl->free = bitf_init(p, (size_t)entries, bsz, BITF_INDEX);
	p += bsz;
	fuioctl->complete = bitf_init(p, (size_t)entries, bsz, BITF_INDEX);
	AN(fuioctl->free);
	AN(fuioctl->complete);
	for (u = 0; u < entries; u++)
		AN(bitf_set(fuioctl->free, u));
	assert(fuioctl->free->nset == fuioctl->free->nbits);
	assert(fuioctl->complete->nset == 0);

	p += bsz;
	assert(p - sz == (void *)fuioctl);

	AZ(pthread_mutex_init(&fuioctl->mtx, NULL));
	AZ(pthread_cond_init(&fuioctl->cond, NULL));

	return (fuioctl);
}

void
fellow_io_ioctl_fini(struct fellow_io_ioctl **fuioctlp)
{
	struct fellow_io_ioctl *fuioctl;

	TAKE_OBJ_NOTNULL(fuioctl, fuioctlp, FELLOW_IO_IOCTL_MAGIC);

	AZ(pthread_cond_destroy(&fuioctl->cond));
	AZ(pthread_mutex_destroy(&fuioctl->mtx));

	assert(fuioctl->free->nset == fuioctl->free->nbits);
	assert(fuioctl->complete->nset == 0);

	free(fuioctl);
}

/*
 * wait until some outstanding I/Os have completed and return
 * the number of completed I/Os
 */
unsigned
fellow_io_ioctl_wait(struct fellow_io_ioctl *fuioctl)
{
	size_t r;

	// base case unlocked: no ioctl in use
	if (fuioctl->free->nbits == fuioctl->free->nset)
		return (0);

	AZ(pthread_mutex_lock(&fuioctl->mtx));
	r = fuioctl->free->nbits - fuioctl->free->nset;
	assert(r >= fuioctl->complete->nset);
	r -= fuioctl->complete->nset;
	if (r)
		AZ(pthread_cond_wait(&fuioctl->cond, &fuioctl->mtx));
	r = fuioctl->complete->nset;
	AZ(pthread_mutex_unlock(&fuioctl->mtx));

	assert(r <= UINT_MAX);
	return ((unsigned)r);
}

unsigned
fellow_io_ioctl_peek(struct fellow_io_ioctl *fuioctl,
    struct fellow_io_status *results, unsigned spacea)
{
	size_t slot, r = 0;
	unsigned space = spacea;

	CHECK_OBJ_NOTNULL(fuioctl, FELLOW_IO_IOCTL_MAGIC);
	AN(results);
	if (space == 0 || fuioctl->complete->nset == 0)
		return (0);

	AZ(pthread_mutex_lock(&fuioctl->mtx));
	while (space) {
		slot = bitf_ffs(fuioctl->complete);
		if (slot == 0)
			break;
		slot--;
		*results++ = fuioctl->req[slot].status;
		space--;
		r++;
		AN(bitf_clr(fuioctl->complete, slot));
		AN(bitf_set(fuioctl->free, slot));
	}
	AZ(pthread_mutex_unlock(&fuioctl->mtx));
	assert(r <= spacea);
	return ((unsigned)r);
}

static void
fellow_io_ioctl_blkdiscard_task(struct worker *wrk, void *priv)
{
	struct fellow_io_ioctl_r *req;
	struct fellow_io_ioctl *fuioctl;
	unsigned u, slot;
	ssize_t diff;
	int r;

	(void)wrk;

	CAST_OBJ_NOTNULL(req, priv, FELLOW_IO_IOCTL_R_MAGIC);
	fuioctl = req->fuioctl;
	CHECK_OBJ_NOTNULL(fuioctl, FELLOW_IO_IOCTL_MAGIC);

	/*
	 * the following block is basically a no-op: req is from the req array,
	 * so the pointer difference is the index into the array. So all the
	 * assertions basically only assert that pointer arithmatic works. But we need
	 * that index (slot) in the bitf_set() later.
	 */
	assert(req >= fuioctl->req);
	diff = req - fuioctl->req;
	assert(diff >= 0);
	assert(diff <= UINT_MAX);
	slot = (typeof(slot))diff;
	assert(slot < fuioctl->complete->nbits);
	assert(req == &fuioctl->req[slot]);

	for (u = 0; u < req->ndiscards; u++) {
		errno = 0;
#ifdef HAVE_LINUX_FS_H
		r = ioctl(fuioctl->fd, (unsigned long)BLKDISCARD, (uint64_t[2]){
			    req->discards[u].offset,
			    req->discards[u].len});
		if (r > req->status.result) {
			req->status.result = r;
			req->status.flags = (unsigned)errno;
		}
#else
		(void) r;
		INCOMPL();
#endif
	}

	AZ(pthread_mutex_lock(&fuioctl->mtx));
	AN(bitf_set(fuioctl->complete, (size_t)slot));
	AZ(pthread_cond_signal(&fuioctl->cond));
	AZ(pthread_mutex_unlock(&fuioctl->mtx));
}

/*
 * because our simplified struct is shared between request
 * and completion, we need to take the lock
 *
 * returns number of discards accepted and number of slots used
 * (mapped onto IOs outstanding)
 */
struct fellow_io_ioctl_blkdiscard_ret
fellow_io_ioctl_blkdiscard_enq(struct fellow_io_ioctl *fuioctl, uint64_t info,
    const struct fellow_io_discard *discards, unsigned n)
{
	struct fellow_io_ioctl_r *req;
	unsigned slotl = (n + FIO_DISCARD_BATCH - 1) / FIO_DISCARD_BATCH;
	unsigned u, sl, slot[slotl], slotn, r = 0;
	size_t sz;

	CHECK_OBJ_NOTNULL(fuioctl, FELLOW_IO_IOCTL_MAGIC);

	memset(slot, 0, sizeof slot);

	AZ(pthread_mutex_lock(&fuioctl->mtx));
	for (slotn = 0; slotn < slotl ; slotn++) {
		sz = bitf_ffs(fuioctl->free);
		if (sz == 0)
			break;
		sz--;
		assert(sz < UINT_MAX);

		AN(bitf_clr(fuioctl->free, sz));
		AZ(bitf_get(fuioctl->complete, sz));
		slot[slotn] = (unsigned)sz;
	}
	AZ(pthread_mutex_unlock(&fuioctl->mtx));

	if (slotn == 0)
		return ((struct fellow_io_ioctl_blkdiscard_ret){0,0});

	for (sl = 0; n > 0 && sl < slotn; sl++) {
		req = &fuioctl->req[slot[sl]];
		INIT_OBJ(req, FELLOW_IO_IOCTL_R_MAGIC);
		req->fuioctl = fuioctl;
		req->status.info = info;

		u = n;
		if (u > FIO_DISCARD_BATCH)
			u = FIO_DISCARD_BATCH;

		memcpy(req->discards, discards, u * sizeof *discards);
		req->ndiscards = u;

		discards += u;
		n -= u;
		r += u;

		AZ(fuioctl->taskrun(fellow_io_ioctl_blkdiscard_task, req,
			&req->taskstate));
	}

	assert(sl == slotn);

	return ((struct fellow_io_ioctl_blkdiscard_ret){r,slotn});
}
