/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

struct fellow_cache_lru;
void fellow_cache_obj_evict_mutate(const struct fellow_cache_lru *lru,
    struct fellow_cache_obj *fco);
void fellow_cache_obj_slim(struct fellow_cache *fc,
    struct fellow_cache_obj *fco, struct fellow_busy *fbo);

void stvfe_oc_log_removed(struct objcore *oc);
void stvfe_oc_log_submitted(struct objcore *oc);
void stvfe_oc_dle_obj(struct objcore *oc, struct fellow_dle *e);

int stvfe_mutate(struct worker *wrk, struct fellow_cache_lru *lru,
    struct objcore *oc);
void stvfe_sumstat(struct worker *wrk); // wraps Pool_Sumstat(wrk);
void stvfe_setbusyobj(const struct busyobj *bo); // wraps THR_SetBusyobj(bo);

// https://probablydance.com/2018/06/16/fibonacci-hashing-the-optimization-that-the-world-forgot-or-a-better-alternative-to-integer-modulo/
static inline size_t
fib(uint64_t n, uint8_t bits)
{
	const size_t gr = 11400714819323198485LLU;
	size_t r;

	r = n * gr;
	r >>= (sizeof(gr) * 8) - bits;
	assert(r < (size_t)1 << bits);
	return (r);
}

// https://github.com/varnishcache/varnish-cache/pull/4013
#if VRT_MAJOR_VERSION < 18U || (VRT_MAJOR_VERSION == 18 && VRT_MINOR_VERSION < 1)
#define VC_4013_WORKAROUND
#endif
