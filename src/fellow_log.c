/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

// XXX NEEDS INCLUDE CLEANUP
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/file.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <math.h>
#include <fcntl.h>	// Solaris O_* flags

#include "vdef.h"
#include "vas.h"
#include "vtim.h"
#include "miniobj.h"

#include "debug.h"
#include "compiler.h"
#include "buddy.h"
#include "foreign/qdef.h"
#include "vqueue.h"
#include "bitf_segmentation.h"
#include "pow2_units.h"

#include "foreign/vend.h"

#include "VSC_fellow.h"

#ifdef DEBUG
size_t membuddy_low;
#define MEM_SET(x) membuddy_low = x
#define MEM(buddy) do {						\
		if (membuddy_low == 0)					\
			break;						\
		size_t spc = buddy_space(buddy, 0);			\
		if (spc < membuddy_low)				\
			membuddy_low = spc;				\
	} while(0)
#else
#define MEM(buddy) (void) 0
#define MEM_SET(x) (void) 0
#endif

// needed by fellow_regionlist.h
#define FFD_CAN_FALLOCATE_PUNCH	1
#define FFD_CAN_FALLOCATE_PUNCH_URING	(1<<1)
#define FFD_CAN_BLKDISCARD		(1<<2)
#define FFD_CAN_BLKDISCARD_URING	(1<<3)
#define FFD_CAN_ANY (				\
	    FFD_CAN_FALLOCATE_PUNCH |		\
	    FFD_CAN_BLKDISCARD			\
	    )
#define FFD_CAN_ASYNC (				\
	    FFD_CAN_FALLOCATE_PUNCH_URING |		\
	    FFD_CAN_BLKDISCARD_URING			\
	    )


#include "fellow_pri.h"
#include "fellow_io_backend.h"
#include "fellow_io.h"
#include "fellow_log_storage.h"
#include "fellow_log.h"
#include "fellow_debug.h"
#include "fellow_hash.h"
#include "fellow_tune.h"

#define N_HDR 16

#ifdef DEBUG
//lint -e773
#define DLE_N(x, n, v) [x] = #x,
//lint -e{785}
static const char * const dle_type[0x20] = {
#include "tbl/dle.h"
};
//lint +e773
#endif

struct dle_stats {
#define DLE_N(x, n, v) unsigned n;
#include "tbl/dle.h"
};

static inline void
dle_stats_add(struct dle_stats *to, const struct dle_stats *from)
{
#define DLE_N(x, n, v) (to)->n += (from)->n;
#include "tbl/dle.h"
}

static inline void
dle_stats_inc(struct dle_stats *stats, enum dle_type t)
{
	switch (t) {
#define DLE_N(x, n, v) case x: (stats)->n++; break;
#include "tbl/dle.h"
	default:
		WRONG("dle type");
	}
}

#define DLE_REG_FOREACH_(entry, counter, v_off, v_sz)			\
	for (unsigned counter = 0;					\
	     counter < DLE_REG_NREGION &&				\
		 (v_off = entry->u.reg.region[counter].off) != 0 &&	\
		 (v_sz = entry->u.reg.region[counter].size) != 0;	\
	     counter++)

#define DLE_REG_FOREACH(entry, off, size)			\
	DLE_REG_FOREACH_(entry, VUNIQ_NAME(i), off, size)

#define DLE_BAN_REG_FOREACH_(entry, counter, v_off, v_sz)		\
	for (unsigned counter = 0;					\
	     counter < DLE_BAN_REG_NREGION &&			\
		 (v_off = entry->u.ban_reg.region[counter].off) != 0 && \
		 (v_sz = entry->u.ban_reg.region[counter].size) != 0; \
	     counter++)

#define DLE_BAN_REG_FOREACH(entry, off, size)			\
	DLE_BAN_REG_FOREACH_(entry, VUNIQ_NAME(i), off, size)

enum f_phase_e {
	FP_INVAL = 0,
	FP_INIT,
	FP_OPEN,
	FP_FINI,
	FP_LIM
};


struct fellow_disk_log_block {
	// vvv not checksummed
	uint16_t			magic;
#define FELLOW_DISK_LOG_BLOCK_MAGIC	0x1ab7
	uint8_t				version;
	uint8_t				_reserved1[1];
	// off=4
	uint8_t				_pad1[3];
	uint8_t				fht;
	// ^^^ not checksummed

	// off = 8
	union fh			fh[1];

	// vvv CHECKSUMMED
#define FELLOW_DISK_LOG_BLOCK_CHK_START				\
	offsetof(struct fellow_disk_log_block, prev_off)
#define FELLOW_DISK_LOG_BLOCK_CHK_LEN \
	(sizeof(struct fellow_disk_log_block) - FELLOW_DISK_LOG_BLOCK_CHK_START)

	// off=40
	off_t				prev_off;
	off_t				next_off;

	// off=56
	struct fellow_dle
	    entry[FELLOW_DISK_LOG_BLOCK_ENTRIES];	// 4032

	// off=4088
	uint8_t				nentries;
	// off=4089
	uint8_t				id;
	uint8_t				_pad[6];
};

struct fellow_alloc_log_block {
	off_t				off;
	struct fellow_disk_log_block	*block;
};

enum lbuf_state_e {
	LBUF_INVAL = 0,
	LBUF_INIT,
	LBUF_LOGREG,
	LBUF_MEM,
	LBUF_INCOMPL,
	LBUF_PEND,
	LBUF_OPEN,
	LBUF_FINI,
	LBUF_LIM
};

#define LBUF_CAN_REF	1	// ref this log in header, write head
#define LBUF_CAN_LOGREG (1<<1)	// write data after block 0 in log region
#define LBUF_CAN_FLUSH	(1<<2)	// write data after block 0 outside log region

#define C(x) (LBUF_CAN_ ## x)

#define LBUF_ALL (C(REF) | C(LOGREG) | C(FLUSH))

static const unsigned lbuf_cap[LBUF_LIM] = {
	[LBUF_INVAL]	= 0,
	[LBUF_INIT]	= 0,
// during FP_INIT rewrite
	[LBUF_LOGREG]	= C(LOGREG),
	[LBUF_MEM]	= 0,
// during FP_OPEN rewrite: INCOMPL gets old logs, PEND gets new logs
	[LBUF_INCOMPL]	=	   C(FLUSH),
	[LBUF_PEND]	= C(REF) | C(FLUSH),
// during FP_OPEN normal ops
	[LBUF_OPEN]	= C(REF) | C(FLUSH),
	[LBUF_FINI]	= 0
};

struct fellow_log_region {
	unsigned				magic;
#define FELLOW_LOG_REGION_MAGIC		0x1f0f464d
	const struct buddy_off_extent	*region;
	off_t					free_off;
	unsigned				space;
	unsigned				free_n;
};

static inline int
region_contains(const struct buddy_off_extent *region, off_t block)
{
//	DBG("region contains %zu/%zu %zu", region->off, region->size, block);
	return (
	    block >= region->off &&
	    block < region->off + (off_t)region->size);
}

/*
 * an ioctx user needs to hold a lease as long as it has
 * IOs outstanding. Only ioctxes with no outstanding IO
 * can be returned
 */

struct fellow_fd_ioctx_lease {
	struct fellow_fd_ioctx	*fdio;
	/* pointer to the slot in fdio->ctx */
	fellow_ioctx_t		**ioctxp;
	/* while the lease is taken, the slot is nulled
	 * and the value copied here
	 */
	fellow_ioctx_t		*ioctx;
};

enum logoff_where {
	LOGOFF_INVAL = 0,
	LOGOFF_LOGBLK,
	LOGOFF_PENDBLK
};

// saving new offsets to be written at the right time
struct fellow_disk_log_off_todo {
	unsigned			magic;
#define FELLOW_DISK_LOG_OFF_TODO_MAGIC	0x6f6bd249
	enum logoff_where		where;
	off_t				off, alt;
};

#define FF_TRANSITION(ff, from, to) do {	\
		assert((ff)->state == from);	\
		(ff)->state = to;		\
	} while(0)

enum ff_state_e {
	FF_INVAL = 0,
	FF_SCHEDULED,
	FF_WAIT_OUTSTANDING,
	FF_HEAD,
	FF_HDR,
	FF_FREE,
	FF_DONE
};

/* flush finish
 *
 * flush finishes are organized a *reverse* VLIST with the HEAD
 * as the most recent element.
 *
 * HEAD IS GLOBAL XXX
 *
 * ex For flushes 1, 2, 3 in that order:
 *
 *		   head
 *		   V
 *  |1| -> |2| -> |3|
 *  | | <- | | <- | |
 *
 * lbuf_ff is where the logbuffer has a pointer to the _ff.
 *
 * lbuf finish code thus needs to wait for the flush finish to
 * complete before returning memory
 */

struct fellow_logbuffer_ff {
	unsigned			magic;
#define FELLOW_LOGBUFFER_FF_MAGIC	0xcb1341d3
	unsigned			can;
	enum ff_state_e			state;
	struct buddy_ptr_extent		alloc;
	VLIST_ENTRY(fellow_logbuffer_ff) list;
	struct fellow_logbuffer_ff	**lbuf_ff;
#ifdef DEBUG
	vtim_mono			t[2];
#endif
	struct fellow_disk_log_off_todo	todo;
	off_t				active_off;
	off_t				*tail_off;
	struct fellow_fd		*ffd;
	struct fellow_fd_ioctx_lease	fdil;
	struct fellow_alloc_log_block	head;
	struct regionlist		*regions_to_free;
	size_t				freeing; // from regionlist
	fellow_task_privstate		taskstate;
};

#ifdef BUDDY_WITNESS
buddy_t			*hack_dskbuddy = NULL;
#endif

// the sequence variable holds the next value or 0 for 'start'
#define seq_dec_start(seq, start) (			\
	    ((seq) == 0)				\
	    ? ((seq) = start, (seq)--)			\
	    : (((seq) == 1)				\
		? ((seq) -= 2, (typeof(seq))1)		\
		: (typeof(seq))(seq)--))

// the sequence variable holds the next value or 0
#define seq_inc(seq) (((seq) == 0)			\
	? (seq) = 2, (typeof(seq))1			\
	: (typeof(seq))(seq)++)

//lint -e{527}
static void __attribute__((constructor))
assert_seq_macros(void)
{
	uint8_t seq;

	seq = 0xfe;
	assert(seq_inc(seq) == 0xfe);
	assert(seq_inc(seq) == 0xff);
	assert(seq_inc(seq) == 0x01);
	assert(seq_inc(seq) == 0x02);

	assert(seq_dec_start(seq, 0x42) == 0x03);
	assert(seq_dec_start(seq, 0x42) == 0x02);
	assert(seq_dec_start(seq, 0x42) == 0x01);
	assert(seq_dec_start(seq, 0x42) == 0xff);

	seq = 0x00;
	assert(seq_dec_start(seq, 0x42) == 0x42);
	assert(seq_dec_start(seq, 0x42) == 0x41);
}

#define FDBG_LOGBLK(lblk)						\
	FDBG(D_LOGS_ITER_BLOCK,						\
	"block version 0x%x fht 0x%x (%s) nentries %d id %d seq %d...%d", \
	(lblk)->version, (lblk)->fht, fh_name[(lblk)->fht],		\
	(lblk)->nentries, (lblk)->id,					\
	(lblk)->entry[0].seq, (lblk)->entry[(lblk)->nentries - 1].seq)


/* basic sanity check of a lock block */
#define CHECK_BLK(logblk) do {						\
		CHECK_OBJ_NOTNULL(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);	\
		assert((logblk)->nentries);				\
	} while (0)

/* sanity check a lock block for correct squencing */
#define CHECK_BLK_SEQ(logblk, seqvar) do {				\
		CHECK_BLK(logblk);					\
		FDBG_LOGBLK(logblk);					\
		assert((seqvar) == (logblk)->entry[0].seq);		\
	} while(0)

/* sanity check a lock block for correct squencing and inc sequence*/
#define ADD_BLK_SEQ(logblk, seqvar) do {				\
		CHECK_BLK_SEQ(logblk, seqvar);				\
		const uint8_t start = (logblk)->entry[0].seq;		\
		(seqvar) = (logblk)->entry[(logblk)->nentries - 1].seq + 1; \
		if ((seqvar) == 0)					\
			(seqvar)++;					\
		if (start < (seqvar))					\
			assert((uint8_t)(start + (logblk)->nentries)	\
			    == (seqvar));				\
		else							\
			assert((uint8_t)(start + (logblk)->nentries + 1) \
			    == (seqvar));				\
	} while (0)

// <4K to fit in logblk
#define LBUF_DSKPOOL_SIZE					\
	((((1<<12) - sizeof(void *)) - 2 * (			\
	  sizeof(struct buddy_reqs) + sizeof(unsigned))) /	\
	(2 * sizeof(struct i_reqalloc)))
#define LOGBLK_MEMPOOL_SIZE 2
//lint -e{506}
static void __attribute__((constructor))
assert_lbuf_dsk_resv(void)
{
	assert(LBUF_DSKPOOL_SIZE <= BUDDY_REQS_MAX);
}

// dsk block pool
BUDDY_POOL(lbuf_dskpool, LBUF_DSKPOOL_SIZE);
// mem block pool
BUDDY_POOL(logblk_mempool, LOGBLK_MEMPOOL_SIZE);
BUDDY_POOL_GET_FUNC(logblk_mempool, static)
BUDDY_POOL_AVAIL_FUNC(logblk_mempool, static)
// memory for flush_finish
BUDDY_POOL(lbuf_ffpool, 4);
BUDDY_POOL_GET_FUNC(lbuf_ffpool, static)
BUDDY_POOL_AVAIL_FUNC(lbuf_ffpool, static)

static void
logblk_mempool_fill(struct buddy_reqs *reqs, const void *priv)
{
	unsigned u;

	(void) priv;

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_page(reqs, MIN_FELLOW_BITS, 0));
}

struct fellow_logbuffer {
	unsigned			magic;
#define FELLOW_LOGBUFFER_MAGIC	0xe8454b5a
	unsigned			n;	// entries in arr used
	unsigned			space;	// entries in arr
	unsigned			tot;	// total added
	unsigned			thr;	// flush threshold
	uint8_t				id;	// in block
	uint8_t				seq;	// in DLE
	uint8_t				flush_seq;	// next to flush
	uint8_t				head_seq[2];	// start/end of head
	struct dle_stats		dle_stats;
	enum lbuf_state_e		state;
	struct buddy_ptr_page		alloc;	// arr allocation
	enum f_phase_e			*phase;

	pthread_mutex_t			*phase_mtx;
	// protected by phase_mtx
	pthread_cond_t			*phase_cond;

	buddy_t			*membuddy;
	buddy_t			*dskbuddy;
	struct fellow_fd_ioctx_lease	fdil;
	struct fellow_log_region	*logreg;
	struct fellow_disk_log_off_todo	todo;
	struct fellow_alloc_log_block	head;	 // first logblk
	struct fellow_alloc_log_block	active;  // only element or after array
	struct fellow_alloc_log_block	*arr;

#ifdef DEBUG
	vtim_mono			flush_t0;
#endif
	/* disk regions to free after the next reg / write_hdr */
	struct regionlist		*regions_to_free;
	/* last seen active block offset, becomes tail_off when written */
	off_t				active_off;
	/* last written offset for coordination with _append() */
	off_t				tail_off;
	struct fellow_logbuffer_ff	*ff;
	struct lbuf_ffpool		ffpool[1];
	struct logblk_mempool		*mempool;
	struct lbuf_dskpool		*dskpool;
};

#define LBUF_OK(lbuf) (							\
	    (lbuf) != NULL &&						\
	    (lbuf)->magic == FELLOW_LOGBUFFER_MAGIC &&			\
	    (lbuf)->state > LBUF_INIT &&				\
	    (lbuf)->state < LBUF_FINI &&				\
	    (lbuf)->space > 0)

#define CHECK_LBUF_USABLE(lbuf) \
	do {								\
		CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);	\
		assert((lbuf)->state > LBUF_INIT);			\
		assert((lbuf)->state < LBUF_FINI);			\
		assert((lbuf)->space > 0);				\
	} while(0)

static inline int
cap(unsigned can, unsigned want)
{
	return ((can & want) == want);
}
static inline int
logbuffer_can(const struct fellow_logbuffer *lbuf, unsigned want)
{

	CHECK_LBUF_USABLE(lbuf);
	return (cap(lbuf_cap[lbuf->state], want));
}

#define LOGREGIONS 3
#define active_region(n) (n)
#define empty_region(n) (/*lint --e(679)*/((n) + 1) % LOGREGIONS)
#define pend_region(n) (/*lint --e(679)*/((n) + 2) % LOGREGIONS)

// offsets to latest active blocks and alternatives
struct fellow_disk_log_off {
	off_t				logblk;
	off_t				pendblk;
};

struct fellow_disk_log_info {
	unsigned			magic;
#define FELLOW_DISK_LOG_INFO_MAGIC	0xf36487f2
	unsigned			region;
	uint64_t			generation;
	struct buddy_off_extent		log_region[LOGREGIONS];
	struct fellow_disk_log_off	off;
	// alternative starting point, usually one block before
	struct fellow_disk_log_off	alt;
};

struct fellow_disk_data_hdr {
	uint32_t			magic;
#define FELLOW_DISK_DATA_HDR_MAGIC	0xdba29483
	uint32_t			version;

	// off = 8
	union fh			fh[1];
	// vvv CHECKSUMMED
#define FELLOW_DISK_DATA_HDR_CHK_START \
	offsetof(struct fellow_disk_data_hdr, log_info)
#define FELLOW_DISK_DATA_HDR_CHK_LEN \
	(sizeof(struct fellow_disk_data_hdr) - FELLOW_DISK_DATA_HDR_CHK_START)
	struct fellow_disk_log_info	log_info;
	uint64_t			objsize;
	uint64_t			objsize_hint;

	uint8_t			_pad[3943];
	uint8_t			fht;
};

enum fellow_watcher_state {
	FLW_S_WAITING = 0,
	FLW_S_FLUSHING,
	FLW_S_REWRITING
};

/* function to be called right before transitioning to FP_OPEN */
#define FELLOW_MAX_CB 2

struct fellow_open_cb {
	fellow_open_f	*func;
	void		*priv;
};

unsigned
fellow_io_ring_size(const char *envvar)
{
	static const unsigned def = 1024;
	unsigned long u;
	const char *e;

	e = getenv(envvar);
	if (e == NULL)
		return (def);

	u = strtoul(e, NULL, 0);
	if (u > UINT_MAX)
		return (UINT_MAX);
	if (u < 1)
		return (1);
	return ((unsigned)u);
}

/*
 * - pending log written
 * - current log read
 * - new log written
 */

#define FELLOW_NIOCTX 3

struct fellow_fd_ioctx {
	unsigned			magic;
#define FELLOW_FD_IOCTX_MAGIC		0x3b19f0e1
	unsigned			waits;
	vtim_mono			waits_t0;
	struct bitf			bitf[1];
	bitf_word_t			_spc[bitf_words(FELLOW_NIOCTX)];
	pthread_mutex_t			mtx;
	pthread_cond_t			cond;
	fellow_ioctx_t			*ioctx[FELLOW_NIOCTX];
};

static void
fellow_fd_ioctx_fini(struct fellow_fd_ioctx *fdio)
{
	int i;

	CHECK_OBJ_NOTNULL(fdio, FELLOW_FD_IOCTX_MAGIC);

	AZ(pthread_mutex_destroy(&fdio->mtx));
	AZ(pthread_cond_destroy(&fdio->cond));

	for (i = 0; i < FELLOW_NIOCTX; i++) {
		fellow_io_fini(&fdio->ioctx[i]);
		AZ(fdio->ioctx[i]);
	}

	memset(fdio, 0, sizeof *fdio);
}

static int
fellow_fd_ioctx_init(struct fellow_fd_ioctx *fdio,
    int fd, unsigned entries, void *base, size_t len,
    fellow_task_run_t taskrun)
{
	struct bitf *bitf;
	size_t i;

	AN(fdio);
	INIT_OBJ(fdio, FELLOW_FD_IOCTX_MAGIC);

	bitf = bitf_init(fdio->bitf, (size_t)FELLOW_NIOCTX,
			 bitf_hdrsz + sizeof fdio->_spc, BITF_INDEX);
	assert(bitf == fdio->bitf);

	AZ(pthread_mutex_init(&fdio->mtx, NULL));
	AZ(pthread_cond_init(&fdio->cond, NULL));

	for (i = 0; i < (size_t)FELLOW_NIOCTX; i++) {
		fdio->ioctx[i] = fellow_io_init(fd,
		    entries, base, len, taskrun);
		AN(bitf_set(fdio->bitf, i));
		if (fdio->ioctx[i] == NULL)
			break;
	}
	if (i == FELLOW_NIOCTX)
		return (0);

	fellow_fd_ioctx_fini(fdio);
	return (1);
}

struct fellow_fd {
	unsigned			magic;
#define FELLOW_FD_MAGIC		0x7d107880
	unsigned			cap;

	fellow_log_diag_f		*diag;

	enum f_phase_e			phase;
	int				fd;

	struct stvfe_tune		*tune;
	struct VSC_fellow		*stats;

	// open notification & logbuffer_flush_finish
	pthread_mutex_t			phase_mtx;
	pthread_cond_t			phase_cond;
	VLIST_HEAD(,fellow_logbuffer_ff) ffhead;
	unsigned			nff;

	fellow_task_run_t		*taskrun;

	struct fellow_fd_ioctx		fdio[1];
	size_t				size;
	buddy_t				*membuddy;
	buddy_t				dskbuddy[1];
	// mtx protects api functions on logbuf
	pthread_mutex_t			logmtx;
	// notify when new logbuffer
	pthread_cond_t			new_logbuf_cond;
	// watcher thread wakeup - under logmtx
	pthread_cond_t			watcher_cond;
	pthread_t			watcher_thread;
	unsigned			watcher_running;
	enum fellow_watcher_state	watcher_state;

	unsigned			active_hdr;
	unsigned			rewriting;
	struct fellow_disk_log_info	log_info;
	struct fellow_disk_log_info	last_log_info;
	struct fellow_log_region	logreg[LOGREGIONS];
	struct logblk_mempool		logblk_pool[1];
	struct fellow_logbuffer	logbuf[1];
	uint8_t			logbuf_id;

	struct fellow_open_cb		open_cbs[FELLOW_MAX_CB];
	unsigned			open_cbs_n;
};

void
fellow_fd_update_stats(struct fellow_fd *ffd)
{
	buddy_t *buddy;
	uint64_t happy;
	size_t sz;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	if (ffd->stats == NULL)
		return;

	/* we get the exact values (which implies lock acquisition)
	 * because this function is only to be called infrequently
	 */
	buddy = ffd->dskbuddy;
	sz = buddy_space(buddy, 1);
	ffd->stats->g_dsk_space = sz;
	ffd->stats->g_dsk_bytes = buddy_size(buddy) - sz;

	buddy = ffd->membuddy;
	sz = buddy_space(buddy, 1);
	ffd->stats->g_mem_space = sz;
	ffd->stats->g_mem_bytes = buddy_size(buddy) - sz;

	happy = ffd->stats->b_happy;
	happy <<= 1;
	happy |= !! fellow_is_open(ffd);	//lint !e514
	ffd->stats->b_happy = happy;
}

static void
fellow_fd_ioctx_get(struct fellow_fd *ffd, struct fellow_fd_ioctx_lease *fdil)
{
	struct fellow_fd_ioctx *fdio;
	unsigned waits = 0;
	vtim_dur waits_d = -1;
	size_t f;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	fdio = ffd->fdio;
	CHECK_OBJ_NOTNULL(fdio, FELLOW_FD_IOCTX_MAGIC);

	AZ(fdil->fdio);
	AZ(fdil->ioctxp);
	AZ(fdil->ioctx);

	AZ(pthread_mutex_lock(&fdio->mtx));
	f = bitf_ffs(fdio->bitf);
	while (f == 0) {
		/* happens when a previous flush is still in progress */
		/* this should never happen, but if it does,
		 * we do not want it to be fatal
		 */
		if (fdio->waits > 0)
			fdio->waits++;
		else {
			fdio->waits = 1;
			fdio->waits_t0 = VTIM_mono();
		}
		AZ(pthread_cond_wait(&fdio->cond, &fdio->mtx));
		f = bitf_ffs(fdio->bitf);
	}
	AN(f);
	f--;
	AN(bitf_clr(fdio->bitf, f));
	if (fdio->waits > 0 &&
	    (waits_d = VTIM_mono() - fdio->waits_t0) > 1.0) {
		waits = fdio->waits;
		fdio->waits = 0;
		fdio->waits_t0 = 0;
	}
	AZ(pthread_mutex_unlock(&fdio->mtx));

	fdil->fdio = fdio;
	fdil->ioctxp = &fdio->ioctx[f];
	TAKEZN(fdil->ioctx, *fdil->ioctxp);

	if (waits) {
		ffd->diag("WARN: %u ioctx waits in %.2fs (%.2f/s)",
		    waits, waits_d, (double)waits / waits_d);
	}
}

static void
fellow_fd_ioctx_return(struct fellow_fd_ioctx_lease *fdil)
{
	struct fellow_fd_ioctx *fdio;
	ssize_t b;
	size_t bb;

	AN(fdil);
	fdio = fdil->fdio;
	CHECK_OBJ_NOTNULL(fdio, FELLOW_FD_IOCTX_MAGIC);
	AN(fdil->ioctx);
	AN(fdil->ioctxp);

	AZ(fellow_io_outstanding(fdil->ioctx));
	TAKEZN(*fdil->ioctxp, fdil->ioctx);

	assert(fdil->ioctxp >= fdio->ioctx);
	b = fdil->ioctxp - fdio->ioctx;
	assert(b >= 0);
	assert(b < FELLOW_NIOCTX);
	bb = (size_t)b;

	memset(fdil, 0, sizeof *fdil);

	AZ(pthread_mutex_lock(&fdio->mtx));
	AN(bitf_set(fdio->bitf, bb));
	AZ(pthread_cond_signal(&fdio->cond));
	AZ(pthread_mutex_unlock(&fdio->mtx));
}

void
fellow_log_register_open_cb(struct fellow_fd *ffd,
    fellow_open_f *func, void *priv)
{
	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AN(func);
	AN(priv);
	assert(ffd->phase == FP_INIT);
	assert(ffd->open_cbs_n < FELLOW_MAX_CB);

	ffd->open_cbs[ffd->open_cbs_n].func = func;
	ffd->open_cbs[ffd->open_cbs_n].priv = priv;
	ffd->open_cbs_n++;
}

int
fellow_is_open(const struct fellow_fd *ffd)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	return (ffd->phase == FP_OPEN);
}

static inline void
fellow_wait_open(struct fellow_fd *ffd)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	if (ffd->phase == FP_OPEN)
		return;
	ffd->diag("notice: operation waiting for FP_OPEN\n");
	AZ(pthread_mutex_lock(&ffd->phase_mtx));
	while (ffd->phase == FP_INIT)
		AZ(pthread_cond_wait(&ffd->phase_cond, &ffd->phase_mtx));
	AZ(pthread_mutex_unlock(&ffd->phase_mtx));
	assert(ffd->phase == FP_OPEN);
}

static inline void
fellow_signal_open(struct fellow_fd *ffd)
{
	unsigned u;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	for (u = 0; u < ffd->open_cbs_n; u++)
		ffd->open_cbs[u].func(ffd->open_cbs[u].priv);

	// lockmtx is also held by caller
	AZ(pthread_mutex_lock(&ffd->phase_mtx));
	assert(ffd->phase == FP_INIT);
	ffd->phase = FP_OPEN;
	AZ(pthread_cond_broadcast(&ffd->phase_cond));
	AZ(pthread_mutex_unlock(&ffd->phase_mtx));
}

#include "fellow_regionlist.h"

#define fellow_log_prep_max_dles		\
	(3 * FELLOW_DISK_LOG_BLOCK_ENTRIES)

#if (((FELLOW_DISK_LOG_BLOCK_ENTRIES - 1) * DLE_REG_NREGION + 1) >	\
       FELLOW_DISK_LOG_BLOCK_ENTRIES * DLE_BAN_REG_NREGION)
#define fellow_log_prep_max_regions		\
	(fellow_log_prep_max_dles * DLE_REG_NREGION)
#else
#error Need to re-check fellow_log_prep_max_regions
#endif

struct fellow_log_prep_tofree regionlist_stk(fellow_log_prep_max_regions);
struct fellow_log_prep {
	unsigned magic;
#define FELLOW_LOG_PREP_MAGIC		0x3f99fab8
	unsigned n;
	struct fellow_log_prep_tofree tofree;
	const struct fellow_dle *entry;
	struct dle_stats dle_stats;
};

// fwd decl
static void
fellow_privatelog_submit(struct fellow_fd *ffd, struct fellow_logbuffer *lbuf,
    struct fellow_dle *entry, unsigned n);
static const char *
fellow_logblk_check(const struct fellow_disk_log_block *logblk, uint8_t *id);

enum flw_need_e {
	FLW_NEED_NONE = 0,
	FLW_MAYFLUSH,
	FLW_NEED_FLUSH,
	FLW_NEED_REWRITE,
	FLW_NEED_LIM
};
#ifdef DEBUG_DONTNEED
//lint -e773
static const char * const flw_need_s[FLW_NEED_LIM] = {
#define N(x) [x] = #x
	N(FLW_NEED_NONE),
	N(FLW_MAYFLUSH),
	N(FLW_NEED_FLUSH),
	N(FLW_NEED_REWRITE)
};
//lint +e773
#endif
struct flw_need_why {
	char	s[256];
};
static enum flw_need_e
fellow_logwatcher_need(struct fellow_fd *, struct flw_need_why *);

#include "fellow_log_dle_chg.h"

/* fellow logs iter *const* state */
struct flics {
	unsigned const			magic;
#define FLICS_MAGIC			0xf5cdc1a5
	struct fellow_fd * const	ffd;
	fellow_resurrect_f * const	resur_f;
	void * const			resur_priv;

};

// inline funcs
#include "fellow_log_iter_out.h"

/* variable state - the pointers can still be const */
struct flivs {
	unsigned const			magic;
#define FLIVS_MAGIC			0xb1c8e455
	unsigned			oob;

	uint8_t			seq;

	// const
	struct fellow_logbuffer * const lbuf;
	struct bitfs * const		bitfs;
	struct logblk_mempool * const	mempool;

	// non const
	struct regionlist		*tofree;
	struct iter_out			ban_dles[1];
	struct fellow_dlechg_top	fdct[1];
	vtim_real			ban_export_time;
};

int32_t
fellow_io_pread_sync(const struct fellow_fd *ffd, void *bufa,
    size_t sz, off_t off)
{
	ssize_t r, rdsz = 0;
	char *buf = bufa;

	// O_DIRECT alignment on 4K
	AZ((uintptr_t)bufa & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)sz & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)off & FELLOW_BLOCK_ALIGN);
	assert (sz <= INT32_MAX);

	do {
		r = pread(ffd->fd, buf, sz, off);
		if (r < 0)
			rdsz = -errno;
		if (r <= 0)
			break;
		rdsz += r;
		off += r;
		buf += r;
		sz -= (size_t)r;
	} while (sz);

	assert(rdsz >= INT32_MIN);
	assert(rdsz <= INT32_MAX);
	return ((int32_t)rdsz);
}

int32_t
fellow_io_pwrite_sync(const struct fellow_fd *ffd, const void *buf,
    size_t sz, off_t off)
{
	ssize_t wrsz;

	assert (ffd->phase == FP_OPEN);
	// O_DIRECT alignment on 4K
	AZ((uintptr_t)buf & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)sz & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)off & FELLOW_BLOCK_ALIGN);
	assert (sz <= INT32_MAX);

	wrsz = pwrite(ffd->fd, buf, sz, off);
	if (wrsz < 0)
		wrsz = -errno;
	assert(wrsz >= INT32_MIN);
	assert(wrsz <= INT32_MAX);
	return ((int32_t)wrsz);
}

#define ALIGN_BUF (16 * 1024)

/* align the read by copying if necessary */
static int32_t
fellow_io_pread_sync_align(const struct fellow_fd *ffd, void *bufa,
    size_t sz, off_t off)
{
	if (((uintptr_t)bufa & FELLOW_BLOCK_ALIGN) == 0 &&
	    ((uintptr_t)sz & FELLOW_BLOCK_ALIGN) == 0 &&
	    ((uintptr_t)off & FELLOW_BLOCK_ALIGN) == 0)
		return (fellow_io_pread_sync(ffd, bufa, sz, off));

	unsigned char cp[ALIGN_BUF] __attribute__((aligned(MIN_FELLOW_BLOCK)));
	unsigned char *p = bufa;
	int32_t rr, r = 0;
	size_t l;
	while (sz > 0) {
		if (sz > ALIGN_BUF)
			l = ALIGN_BUF;
		else
			l = sz;
		rr = fellow_io_pread_sync(ffd, cp, FELLOW_BLOCK_RNDUP(l), off);
		if (rr < 0)
			return (rr);

		assert((uint32_t)rr == ALIGN_BUF ||
		    (uint32_t)rr == FELLOW_BLOCK_RNDUP(l));

		memcpy(p, cp, l);

		sz -= l;

		off += (typeof(off))l;
		p += l;
		r += (typeof(r))l;
	}
	return (r);
}

/* align the write by copying if necessary */
static int32_t
fellow_io_pwrite_sync_align(const struct fellow_fd *ffd, const void *buf,
    size_t sz, off_t off)
{
	if (((uintptr_t)buf & FELLOW_BLOCK_ALIGN) == 0 &&
	    ((uintptr_t)sz & FELLOW_BLOCK_ALIGN) == 0 &&
	    ((uintptr_t)off & FELLOW_BLOCK_ALIGN) == 0)
		return (fellow_io_pwrite_sync(ffd, buf, sz, off));

	unsigned char cp[ALIGN_BUF] __attribute__((aligned(MIN_FELLOW_BLOCK)));
	const unsigned char *p = buf;
	int32_t rr, r = 0;
	size_t l;
	while (sz > 0) {
		if (sz > ALIGN_BUF)
			l = ALIGN_BUF;
		else
			l = sz;
		memcpy(cp, p, l);
		if (l < ALIGN_BUF)
			memset(cp + l, 0, ALIGN_BUF - l);
		rr = fellow_io_pwrite_sync(ffd, cp, FELLOW_BLOCK_RNDUP(l), off);
		if (rr < 0)
			return (rr);
		assert((uint32_t)rr >= l);
		sz -= l;

		off += (typeof(off))l;
		p += l;
		r += (typeof(r))l;
	}
	return (r);
}

struct fellow_logcache_entry {
	uint16_t				magic;
#define FELLOW_LOGCACHE_ENTRY_MAGIC		0xe2b2
	unsigned				incore:1;
	int					err;
	const char				*error;
	VTAILQ_ENTRY(fellow_logcache_entry)	list;
	struct buddy_ptr_page			alloc;
	off_t					off;
};

VTAILQ_HEAD(flehead, fellow_logcache_entry);

struct fellow_logcache {
	unsigned				magic;
#define FELLOW_LOGCACHE_MAGIC			0xc907fe58
	unsigned				n;
	unsigned				outstanding;
	uint8_t				id;
	// we do not keep blocks in the prune direction if != 0
	int					prune_direction;
	struct fellow_fd			*ffd;
	struct fellow_fd_ioctx_lease		fdil;
	const struct buddy_off_extent		*region;
	struct buddy_ptr_page			alloc_entry;
	struct logblk_mempool			*mempool;
	struct fellow_logcache_entry		*current;
	struct flehead				free;
	struct flehead				used;
};

static void
fellow_logcache_prune_direction(struct fellow_logcache *flc, int direction)
{
	AZ(flc->prune_direction);
	AN(direction);
	flc->prune_direction = direction;
}

static void
fellow_logcache_wait(struct fellow_logcache *flc, unsigned min)
{
	const struct fellow_disk_log_block *logblk;
	unsigned space = flc->outstanding + 1;
	struct fellow_io_status status[space];
	struct fellow_logcache_entry *fle;
	unsigned n, u;

	n = fellow_io_submit_and_wait(flc->fdil.ioctx,
	    status, space,
	    min, NULL, NULL);
	assert(n <= flc->outstanding);
	flc->outstanding -= n;
	for (u = 0; u < n; u++) {
		fle = (void *)status[u].info;
		CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		AZ(fle->incore);
		fle->incore = 1;
		if (status[u].result < 0) {
			fle->err = 0 - status[u].result;
			fle->error = "IO error see err";
			memset(fle->alloc.ptr, 0, (size_t)1 << fle->alloc.bits);
		}
		assert(status[u].result == 1 << MIN_FELLOW_BITS);
		logblk = fle->alloc.ptr;
		fle->error = fellow_logblk_check(logblk, &flc->id);
	}
}

static inline void
fellow_logcache_submit(struct fellow_logcache *flc)
{
	fellow_logcache_wait(flc, 0);
}

static void
fellow_logcache_fini(struct fellow_logcache *flc)
{
	struct fellow_logcache_entry *fle;
	struct buddy_returns *rets;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	rets = BUDDY_RETURNS_STK(flc->ffd->membuddy, BUDDY_RETURNS_MAX);

	/* wait for all IO to complete before returning memory and
	 * the ioctx
	 */
	fellow_logcache_wait(flc, UINT_MAX);

	fellow_fd_ioctx_return(&flc->fdil);

	VTAILQ_FOREACH(fle, &flc->free, list) {
		CHECK_OBJ(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		AZ(fle->incore);
		AZ(fle->alloc.ptr);
	}
	VTAILQ_FOREACH(fle, &flc->used, list) {
		CHECK_OBJ(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		AN(buddy_return_ptr_page(rets, &fle->alloc));
	}

	AN(buddy_return_ptr_page(rets, &flc->alloc_entry));
	buddy_return(rets);
}

static void
fellow_logcache_init(struct fellow_logcache *flc, struct fellow_fd *ffd,
    struct logblk_mempool *mempool,
    uint8_t id, const struct buddy_off_extent *region)
{
	struct fellow_logcache_entry *fle;
	size_t sz;
	unsigned n, u;

	AN(ffd);
	AN(flc);

	// sane upper limit to not eat up all of the membuddy
	sz = buddy_size(ffd->membuddy) / 8;
	sz >>= MIN_FELLOW_BITS;
	n = (typeof(n))sz;
	if (n > BUDDY_REQS_MAX)
		n = BUDDY_REQS_MAX;
	else if (n < 2)
		n = 2;	// fellow_logcache_(take|steal) assume 2 entries min

	INIT_OBJ(flc, FELLOW_LOGCACHE_MAGIC);
	flc->ffd = ffd;
	flc->mempool = mempool;
	flc->id = id;
	flc->region = region;

	VTAILQ_INIT(&flc->free);
	VTAILQ_INIT(&flc->used);

	sz = n * sizeof *fle;
	u = log2up(sz);

	if (u > UINT8_MAX)
		u = UINT8_MAX;

	/* init FLEs */
	flc->alloc_entry = buddy_alloc1_ptr_page_wait(ffd->membuddy,
	    FEP_MEM_FLC, (uint8_t)u, (int8_t)0);
	AN(flc->alloc_entry.ptr);
	sz = (size_t)1 << flc->alloc_entry.bits;
	assert(sz >= sizeof *fle);
	sz /= sizeof *fle;

	assert(sz < UINT_MAX);
	flc->n = (unsigned)sz;

	for (fle = flc->alloc_entry.ptr, u = 0;
	     u < flc->n;
	     u++, fle++) {
		INIT_OBJ(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		VTAILQ_INSERT_TAIL(&flc->free, fle, list);
	}

	fellow_fd_ioctx_get(ffd, &flc->fdil);
}

static void
fellow_logcache_return(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle)
{
	struct fellow_logcache_entry *fles;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);

	if (fle == flc->current) {
		flc->current = NULL;
		fellow_logcache_wait(flc, UINT_MAX);
		VTAILQ_FOREACH_SAFE(fle, &flc->used, list, fles)
			fellow_logcache_return(flc, fle);
		return;
	}

	AN(fle->alloc.ptr);
	AN(fle->incore);
	AN(fle->off);
	fle->incore = 0;
	fle->off = 0;
	fle->error = NULL;
	buddy_return1_ptr_page(flc->ffd->membuddy, &fle->alloc);

	VTAILQ_REMOVE(&flc->used, fle, list);
	VTAILQ_INSERT_HEAD(&flc->free, fle, list);
}

static inline void
fellow_logcache_return_current(struct fellow_logcache *flc)
{

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	if (flc->current == NULL)
		return;
	fellow_logcache_return(flc, flc->current);
}

/* steal an element from the opposite end */
static struct fellow_logcache_entry *
fellow_logcache_steal(struct fellow_logcache *flc, int direction)
{
	struct fellow_logcache_entry *fle;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);

	if (direction >= 0)
		fle = VTAILQ_FIRST(&flc->used);
	else
		fle = VTAILQ_LAST(&flc->used, flehead);

	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);

	if (fle == flc->current)
		return (NULL);

	while (! fle->incore)
		fellow_logcache_wait(flc, 1);
	VTAILQ_REMOVE(&flc->used, fle, list);
	AN(fle->alloc.ptr);
	AN(fle->incore);
	AN(fle->off);
	fle->incore = 0;
	fle->off = 0;
	fle->error = NULL;
	return (fle);
}

/*
 * get an element from the free list, if any
 *
 * prefetch uses it with need == 0 to not wait for allocations
 */
static struct fellow_logcache_entry *
fellow_logcache_take(struct fellow_logcache *flc, int direction, int need)
{
	struct fellow_logcache_entry *fle;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	fle = VTAILQ_FIRST(&flc->free);
	if (fle == NULL)
		return (fellow_logcache_steal(flc, direction));

	if (need == 0 && ! logblk_mempool_avail(flc->mempool))
		return (NULL);

	VTAILQ_REMOVE(&flc->free, fle, list);
	AZ(fle->alloc.ptr);

	fle->alloc = buddy_get_next_ptr_page(
	    logblk_mempool_get(flc->mempool, NULL));

	return (fle);
}

static inline struct fellow_logcache_entry *
fellow_logcache_next(struct fellow_logcache_entry *fle,
    int direction)
{
	fle = direction >= 0 ?
	    VTAILQ_NEXT(fle, list) :
	    VTAILQ_PREV(fle, flehead, list);
	return (fle);
}

static inline void
fellow_logcache_insert(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle,
    struct fellow_logcache_entry *next,
    int direction)
{
	if (direction >= 0)
		VTAILQ_INSERT_AFTER(&flc->used, fle, next, list);
	else
		VTAILQ_INSERT_BEFORE(fle, next, list);
}

static int
fellow_logcache_in(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle, off_t off)
{
	int r;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
	AZ(fle->incore);
	AZ(fle->error);
	AN(fle->alloc.ptr);
	AZ(fle->off);

#ifdef BUDDY_WITNESS
	if (flc->ffd->phase == FP_OPEN)
		buddy_assert_alloced_off_extent(flc->ffd->dskbuddy,
		    &BUDDY_OFF_EXTENT(off, (size_t)1 << MIN_FELLOW_BITS));
#endif

	r = fellow_io_read_async_enq(flc->fdil.ioctx, (uint64_t)fle,
		fle->alloc.ptr, (size_t)1 << MIN_FELLOW_BITS, off);
	if (r != 0) {
		fle->off = off;
		flc->outstanding++;
	}
	return (r);
}

/*
 * same as fellow_logcache_in(), but when we absolutely need the IO
 */
static void
fellow_logcache_need(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle, off_t off)
{
	if (! fellow_logcache_in(flc, fle, off)) {
		fellow_logcache_wait(flc, 1);
		AN(fellow_logcache_in(flc, fle, off));
	}
	fellow_logcache_submit(flc);
}

static off_t
fellow_logcache_next_off(const struct fellow_logcache *flc,
    const struct fellow_logcache_entry *fle, int direction)
{
	const struct fellow_disk_log_block *logblk;
	off_t off;

	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);

	if (fle->incore && fle->error)
		return (0);

	if (fle->incore) {
		logblk = fle->alloc.ptr;
		CHECK_OBJ_NOTNULL(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);
		return (direction >= 0 ? logblk->next_off : logblk->prev_off);
	}
	off = fle->off;
	AN(off);
	if (! region_contains(flc->region, off))
		return (0);

	if (direction >= 0)
		off += (typeof(off))MIN_FELLOW_BLOCK;
	else
		off -= (typeof(off))MIN_FELLOW_BLOCK;

	if (region_contains(flc->region, off))
		return (off);
	else
		return (0);
}

/* remove all entries in one direction if we mis-speculated on
 * the offset
 */
static void
fellow_logcache_prune(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle, int direction)
{
	struct fellow_logcache_entry *next;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);

	while (fle != NULL) {
		if (! fle->incore)
			fellow_logcache_wait(flc, UINT_MAX);
		AN(fle->incore);
		next = fellow_logcache_next(fle, direction);
		fellow_logcache_return(flc, fle);
		fle = next;
	}
}

static unsigned
fellow_logcache_prefetch(struct fellow_logcache *flc,
    struct fellow_logcache_entry *fle, int direction, unsigned limit)
{
	struct fellow_logcache_entry *next;
	unsigned hi, lo, r = 0;
	off_t noff;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
	AN(fle->off);

	/* do not add more IO if we are waiting already */
	if (flc->outstanding || limit == 0)
		return (0);

	// XXX tune?
	hi = flc->n / 2 + 1;
	if (hi > limit)
		hi = limit;
	lo = hi / 4 + 1;
	AN(hi);
	AN(lo);
	while (hi--) {
		AN(fle);
		next = fellow_logcache_next(fle, direction);
		noff = fellow_logcache_next_off(flc, fle, direction);
		if (next != NULL) {
			fle = next;
			if (--lo == 0)
				break;
			if (fle->error == NULL && fle->off == noff)
				continue;
			/* prune all in this direction */
			DBG("WRONG %zu (have %zu) %s", noff, fle->off,
			    direction < 0 ? "<-" : "->");
			fellow_logcache_prune(flc, fle, direction);
			return (r);
		}
		if (noff == 0)
			break;
		next = fellow_logcache_take(flc, direction, 0);
		if (next == NULL)
			break;
		//DBG("%zu %s", noff, direction < 0 ? "<-" : "->");
		if (! fellow_logcache_in(flc, next, noff)) {
			fellow_logcache_submit(flc);
			if (! fellow_logcache_in(flc, next, noff)) {
				fellow_logcache_return(flc, next);
				return (r);
			}
		}
		fellow_logcache_insert(flc, fle, next, direction);
		r++;
		fle = next;
	}
	return (r);
}

struct fellow_logcache_r {
	const char *error;
	union {
		struct fellow_disk_log_block *logblk;
		int err;
	} u;
};

static struct fellow_logcache_r
fellow_logcache_get(struct fellow_logcache *flc, off_t off, int direction,
    unsigned prefetch)
{
	struct fellow_logcache_entry *fle, *ofle;
	struct fellow_logcache_r r;
	unsigned pre;

	CHECK_OBJ_NOTNULL(flc, FELLOW_LOGCACHE_MAGIC);
	AN(off);

	if (flc->current == NULL) {
		DBG("1st  %zu %s", off, direction < 0 ? "<-" : "->");
		fle = fellow_logcache_take(flc, direction, 1);
		AN(fle);
		flc->current = fle;
		assert(VTAILQ_EMPTY(&flc->used));
		VTAILQ_INSERT_HEAD(&flc->used, fle, list);
		fellow_logcache_need(flc, fle, off);
	}
	else if (flc->current->off == off) {
		fle = flc->current;
		CHECK_OBJ_NOTNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		//DBG("curr  %zu %s", off, direction < 0 ? "<-" : "->");
	}
	else {
		AN(flc->current->incore);
		fle = fellow_logcache_next(flc->current, direction);
		CHECK_OBJ_ORNULL(fle, FELLOW_LOGCACHE_ENTRY_MAGIC);
		if (fle == NULL) {
			DBG("miss %zu %s", off, direction < 0 ? "<-" : "->");
			fle = fellow_logcache_take(flc, direction, 1);
			AN(fle);
			fellow_logcache_insert(flc, flc->current,
			    fle, direction);
			fellow_logcache_need(flc, fle, off);
		}
		else if (fle->off != off) {
			/* speculative read ahead was wrong */
			DBG("WRONG %zu (have %zu) %s", off, fle->off,
			    direction < 0 ? "<-" : "->");
			fellow_logcache_prune(flc, fle, direction);
			return (fellow_logcache_get(flc, off, direction,
				prefetch));
		}
		else {
			//DBG("hit  %zu %s", off, direction < 0 ? "<-" : "->");
		}
		assert(fle->off == off);

		ofle = flc->current;
		flc->current = fle;
		assert(fle != ofle);

		if (ofle != NULL &&
		    flc->prune_direction != 0 &&
		    flc->prune_direction != direction) {
			fellow_logcache_prune(flc, ofle, flc->prune_direction);
		}
	}

	pre = fellow_logcache_prefetch(flc, fle, direction, prefetch);
	if (fle->incore && pre)
		fellow_logcache_submit(flc);
	while (! fle->incore)
		fellow_logcache_wait(flc, 1);
	/* we might not have been able to prefetch before because we did not
	 * have the block
	 */
	if (pre == 0 && fellow_logcache_prefetch(flc, fle, direction, prefetch))
		fellow_logcache_submit(flc);
	r.error = fle->error;
	if (fle->error)
		r.u.err = fle->err;
	else
		r.u.logblk = fle->alloc.ptr;
	return (r);
}

// handle completed io based on info
static void
fellow_io_log_handle_info(struct buddy_returns *memrets,
    uint64_t info, int32_t result)
{
	struct fellow_disk_log_block *logblk;
	enum fellow_aio_info_type type = faio_info_type(info);
	void *ptr = faio_info_ptr(info);
	struct buddy_ptr_extent e;

	switch (type) {
	case FAIOT_DISCARD:
		break;
	case FAIOT_NONE:
		XXXAN(result);
		break;
	case FAIOT_DLB_FINISH:
		XXXAN(result);
		logblk = ptr;
		memset(logblk, 0, sizeof *logblk);
		e = BUDDY_PTR_EXTENT(logblk, sizeof *logblk);
		AN(buddy_return_ptr_extent(memrets, &e));
		break;
	default:
		WRONG("faio_info_type");
	}
}

static void
fellow_io_log_handle_status(buddy_t *membuddy,
    const struct fellow_io_status *status, unsigned n)
{
	struct buddy_returns *memrets;

	AN(membuddy);
	AN(status);
	AN(n);

	memrets = BUDDY_RETURNS_STK(membuddy, BUDDY_RETURNS_MAX);

	while (n--) {
		if (status->result < 0) {
			errno = 0 - status->result;
			INCOMPL();
		}
		fellow_io_log_handle_info(memrets,
		    status->info, status->result);
		status++;
	}
	buddy_return(memrets);
}

/*
 * the status array should match the uring size, otherwise
 * we issue additional, unnecessary io_uring_enter() syscalls
 *
 * for the log, we always wait for all completions, because
 * the new head/hdr can only be written when they are all
 * done
 */
static unsigned
fellow_io_log_wait_completions(buddy_t *membuddy,
    void *ioctx, unsigned min)
{
	unsigned entries = fellow_io_entries(ioctx);
	struct fellow_io_status status[entries];
	unsigned n;

	n = fellow_io_submit_and_wait(ioctx, status, entries, min,
	    NULL, NULL);
	if (n == 0)
		return (0);

	fellow_io_log_handle_status(membuddy, status, n);
	return (n);
}

static inline int
buddy_off_extent_cmp(const void *xa, const void *xb)
{
	const struct buddy_off_extent *a = xa, *b = xb;

	if (a->off < b->off)
		return (-1);
	if (a->off > b->off)
		return (1);
	return (0);
}

/* issue discards for an extents list
 * noop if no discard available, when we are under pressure or out of memory
 *
 * for regionlists, this will be called for each partial list block (array),
 * so the merge will also only be partial
 */
void
fellow_io_regions_discard(struct fellow_fd *ffd, void *ioctx,
   struct buddy_off_extent *fdr, unsigned na,
   size_t min, unsigned wait)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	if (! (ffd->cap & FFD_CAN_ANY))
		return;
	if (ioctx == NULL)
		wait = 0;

	if (wait == 0 && (na == 0 || ffd->dskbuddy->waiting))
		return;


#if defined(HAVE_LINUX_FS_H) || defined(HAVE_FALLOCATE)
	struct fellow_io_discard *todo, *merged;
	struct buddy_ptr_extent mem;
	unsigned u, n;
	int r;

	if (wait == 1 && (na == 0 || ffd->dskbuddy->waiting))
		goto discard_wait_only;

	mem = buddy_alloc1_ptr_extent(ffd->membuddy, na * sizeof *merged, 0);
	if (mem.ptr == NULL)
		goto nomem;

	AN(mem.ptr);
	assert(mem.size >= na * sizeof *merged);
	memset(mem.ptr, 0, mem.size);
	merged = mem.ptr;

	qsort(fdr, (size_t)na, sizeof *fdr, buddy_off_extent_cmp);

	assert(fdr[0].off >= 0);
	merged[0].offset = (uint64_t)fdr[0].off;
	merged[0].len = fdr[0].size;
	for (u = 1, n = 0; u < na; u++) {
		assert(fdr[u].off >= 0);
		if (merged[n].offset + merged[n].len == (uint64_t)fdr[u].off) {
			merged[n].len += fdr[u].size;
		} else {
			// we keep the previous entry only if it is large enough
			if (merged[n].len >= min)
				++n;
			merged[n].offset = (uint64_t)fdr[u].off;
			merged[n].len = fdr[u].size;
		}
	}
	if (merged[n].len >= min)
		++n;

	assert(n <= na);
	todo = merged;
	// no use of fdr below this point
	fdr = NULL;

  again:
	if (n == 0)
		goto nowork;

#ifdef HAVE_LINUX_FS_H
	AN(todo->offset);
	AN(todo->len);

	if (ioctx != NULL && ffd->cap & FFD_CAN_BLKDISCARD_URING) {
		while (n) {
			u = fellow_io_blkdiscard_enq(ioctx,
			    faio_ptr_info(FAIOT_DISCARD, NULL),
			    todo, n);
			assert(u <= n);
			if (u < n)
				AN(fellow_io_log_wait_completions(
				    ffd->membuddy,
				    ioctx, 1));
			todo += u;
			n -= u;
		}
		goto nowork;
	}
	if (ffd->cap & FFD_CAN_BLKDISCARD) {
		AN(n);
		r = ioctl(ffd->fd, (unsigned long)BLKDISCARD, (uint64_t[2]){
			    todo->offset, todo->len});
		if (r == 0) {
			if ((ffd->cap & FFD_CAN_BLKDISCARD_URING) == 0) {
				ffd->diag("fellow: blkdiscard works, "
				    "enabling async\n");
				ffd->cap |= FFD_CAN_BLKDISCARD_URING;
			}
			todo++;
			n--;
			goto again;
		}
		ffd->diag("fellow: blkdiscard disabled\n");
		AZ(ffd->cap & FFD_CAN_BLKDISCARD_URING);
		ffd->cap &= ~FFD_CAN_BLKDISCARD;
	}
#else
	if (ffd->cap & FFD_CAN_BLKDISCARD)
		ffd->cap &= ~FFD_CAN_BLKDISCARD;
#endif
#ifdef HAVE_FALLOCATE
	if (ioctx != NULL && ffd->cap & FFD_CAN_FALLOCATE_PUNCH_URING) {
		while (n) {
			r = fellow_io_fallocate_enq(ioctx,
			    faio_ptr_info(FAIOT_DISCARD, NULL),
			    FALLOC_FL_KEEP_SIZE | FALLOC_FL_PUNCH_HOLE,
			    todo->offset, todo->len);
			if (r) {
				todo++;
				n--;
			} else {
				AN(fellow_io_log_wait_completions(
				    ffd->membuddy,
				    ioctx, 1));
			}
		}
		goto nowork;
	}
	if (ffd->cap & FFD_CAN_FALLOCATE_PUNCH) {
		AN(n);
		r = fallocate(ffd->fd,
		    FALLOC_FL_KEEP_SIZE | FALLOC_FL_PUNCH_HOLE,
		    (off_t)todo->offset, (off_t)todo->len);
		if (r == 0) {
			if ((ffd->cap & FFD_CAN_FALLOCATE_PUNCH_URING) == 0) {
				ffd->diag("fellow: fallocate punch"
				    " hole works, enabling async\n");
				ffd->cap |= FFD_CAN_FALLOCATE_PUNCH_URING;
			}
			todo++;
			n--;
			goto again;
		}
		assert(errno == EOPNOTSUPP || errno == ENOSYS);
		ffd->diag("fellow: fallocate punch hole disabled\n");

		AZ(ffd->cap & FFD_CAN_FALLOCATE_PUNCH_URING);
		ffd->cap &= ~FFD_CAN_FALLOCATE_PUNCH;
	}
#else
	if (ffd->cap & FFD_CAN_FALLOCATE_PUNCH)
		ffd->cap &= ~FFD_CAN_FALLOCATE_PUNCH;
#endif

  nowork:
	if (ffd->cap & FFD_CAN_ANY)
		AZ(n);
	buddy_return1_ptr_extent(ffd->membuddy, &mem);
  nomem:
	if (! (ffd->cap & FFD_CAN_ANY))
		return;

	if (! (ioctx && ffd->cap & FFD_CAN_ASYNC))
		return;

	/* submit */
	(void) fellow_io_log_wait_completions(
	    ffd->membuddy,
	    ioctx, 0);

	if (! wait)
		return;

  discard_wait_only:
	AN(wait);
	while (fellow_io_log_wait_completions(
		ffd->membuddy,
		ioctx, 1)) {}
#endif
}

/*
 * prepare a logblk for writing to disk
 *
 */
static void
fellow_logblk_commit(struct fellow_disk_log_block *logblk)
{

	CHECK_OBJ_NOTNULL(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);
	assert(logblk->version == 1);
	assert(logblk->nentries <= FELLOW_DISK_LOG_BLOCK_ENTRIES);

	fh(logblk->fht, logblk->fh,
	    (char *)logblk + FELLOW_DISK_LOG_BLOCK_CHK_START,
	    FELLOW_DISK_LOG_BLOCK_CHK_LEN);
}

// XXX error handling
static unsigned
fellow_io_log_submit(const struct fellow_fd *ffd, void *ioctx,
    enum fellow_aio_info_type type, struct fellow_alloc_log_block *blk)
{
	struct fellow_disk_log_block *logblk;
	uint64_t info;
	int32_t r;
	off_t off;

	AN(blk);
	FDBG(D_LOG_IO, "%p off %zu", blk->block, blk->off);

	logblk = blk->block;
	fellow_logblk_commit(logblk);
	info = faio_ptr_info(type, logblk);
	off = blk->off;

	if (ioctx != NULL) {
		if (! fellow_io_write_async_enq(ioctx, info,
			logblk, sizeof *logblk, off))
			return (0);
		if (type == FAIOT_DLB_FINISH)
			memset(blk, 0, sizeof *blk);
		return (1);
	}

	if (type == FAIOT_DLB_FINISH)
		memset(blk, 0, sizeof *blk);

	r = fellow_io_pwrite_sync(ffd, logblk, sizeof *logblk, off);
	//lint -e{731} XXX
	XXXAN(r == (int32_t)sizeof *logblk);

	struct fellow_io_status status = {
		.result = r,
		.info = info,
		.flags = 0
	};
	fellow_io_log_handle_status(ffd->membuddy,
	    &status, 1);
	return (1);
}

// deduce logregion size from sz and calculate avg objsize
static uint64_t
fellow_objsize_locked(const struct fellow_fd *ffd, size_t sz)
{
	const struct dle_stats *s;
	size_t obj, del, spc;
	unsigned u;

	spc = N_HDR * MIN_FELLOW_BLOCK;

	for (u = 0; u < LOGREGIONS; u++)
		spc += ffd->log_info.log_region[u].size;

	if (sz > spc)
		sz -= spc;
	else
		sz = 0;

	s = &ffd->logbuf->dle_stats;
	obj = s->obj_add;
	del = s->obj_del_free;

	if (obj > del)
		obj -= del;

	if (obj == 0)
		return (0);

	return (sz / obj);
}

static uint64_t
fellow_objsize(struct fellow_fd *ffd)
{
	size_t spc, sz;

	if (ffd->rewriting)
		return (0);

	spc = buddy_space(ffd->dskbuddy, 1);
	sz  = buddy_size(ffd->dskbuddy);

	AN(sz);
	assert(sz >= spc);
	sz -= spc;

	if (pthread_mutex_trylock(&ffd->logmtx))
		return (0);

	if (ffd->rewriting)
		sz = 0;
	else
		sz = fellow_objsize_locked(ffd, sz);
	AZ(pthread_mutex_unlock(&ffd->logmtx)); //lint !e455

	return (sz);
}

static int
fellow_io_write_hdr(struct fellow_fd *ffd)
{
	struct fellow_disk_data_hdr hdr
	    __attribute__((aligned(MIN_FELLOW_BLOCK)));
	int32_t ssz;
	size_t objsz;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	if (! memcmp(&ffd->log_info, &ffd->last_log_info, sizeof hdr.log_info))
		return (0);

	ffd->active_hdr++;
	ffd->active_hdr %= N_HDR;

	ffd->log_info.generation++;

	DBG("gen %lu region %u logblk %zu pendblk %zu",
	    ffd->log_info.generation, ffd->log_info.region,
	    ffd->log_info.off.logblk,
	    ffd->log_info.off.pendblk);

	objsz = fellow_objsize(ffd);

	INIT_OBJ(&hdr, FELLOW_DISK_DATA_HDR_MAGIC);
	hdr.version = 1;
	hdr.fht = FH_SHA256;
	memcpy(&ffd->last_log_info, &ffd->log_info, sizeof hdr.log_info);
	memcpy(&hdr.log_info, &ffd->log_info, sizeof hdr.log_info);
	hdr.objsize = objsz;
	hdr.objsize_hint = ffd->tune->objsize_hint;
	fh(hdr.fht, hdr.fh,
	    (char *)&hdr + FELLOW_DISK_DATA_HDR_CHK_START,
	    FELLOW_DISK_DATA_HDR_CHK_LEN);
	ssz = fellow_io_pwrite_sync(ffd, &hdr,
	    sizeof hdr, ffd->active_hdr * sizeof hdr);
	//lint -e{731} XXX
	XXXAN(ssz == (int32_t)sizeof hdr);
	return (0);
}

static void
logregion_reset(struct fellow_log_region *logreg)
{
	const size_t blksz = sizeof(struct fellow_disk_log_block);
	size_t sz;

	AN(logreg->region);
	logreg->free_off = logreg->region->off;
	sz = logreg->region->size / blksz;
	assert(sz <= UINT_MAX);
	logreg->space = (unsigned)sz;
	logreg->free_n = logreg->space;
	assert(logreg->free_n * blksz == logreg->region->size);
}

static void
logregion_init(struct fellow_log_region *logreg,
    const struct buddy_off_extent *region)
{

	INIT_OBJ(logreg, FELLOW_LOG_REGION_MAGIC);
	logreg->region = region;
	logregion_reset(logreg);
}

static void
logbuffer_assert_empty(const struct fellow_logbuffer *lbuf)
{
	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);
	AZ(lbuf->n);
	AZ(lbuf->head.block);
	AZ(lbuf->head.off);
	AZ(lbuf->active.block);
	AZ(lbuf->active.off);
}

static void
logbuffer_wait_flush_fini(const struct fellow_logbuffer *lbuf)
{
	pthread_mutex_t *phase_mtx;
	pthread_cond_t *phase_cond;

	phase_mtx = lbuf->phase_mtx;
	phase_cond = lbuf->phase_cond;
	AN(phase_mtx);
	AN(phase_cond);

	AZ(pthread_mutex_lock(phase_mtx));
	while (lbuf->ff != NULL)
		AZ(pthread_cond_wait(phase_cond, phase_mtx));
	AZ(pthread_mutex_unlock(phase_mtx));
}

static void
logbuffer_fill_ffpool(struct buddy_reqs *reqs, const void *priv)
{
	const struct fellow_logbuffer *lbuf;
	unsigned u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	CAST_OBJ_NOTNULL(lbuf, priv, FELLOW_LOGBUFFER_MAGIC);

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_extent(reqs, sizeof *lbuf->ff, 0));
}

static void
logbuffer_fini_dskpool(struct fellow_logbuffer *lbuf)
{
	struct buddy_ptr_page alloc;

	if (lbuf->dskpool != NULL) {
		BUDDY_POOL_FINI(lbuf->dskpool);

		alloc = BUDDY_PTR_PAGE(lbuf->dskpool, MIN_FELLOW_BITS);
		buddy_return1_ptr_page(lbuf->membuddy, &alloc);
		lbuf->dskpool = NULL;
	}
	AZ(lbuf->dskpool);
}

static void
logbuffer_fini(struct fellow_logbuffer *lbuf)
{
	struct buddy_ptr_page palloc;

	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);

	logbuffer_assert_empty(lbuf);
	assert(lbuf->state == LBUF_FINI);
	AZ(lbuf->regions_to_free);

	logbuffer_fini_dskpool(lbuf);
	BUDDY_POOL_FINI(lbuf->ffpool);

	if (lbuf->fdil.ioctx != NULL) {
		(void)fellow_io_log_wait_completions(
		    lbuf->membuddy,
		    lbuf->fdil.ioctx, UINT_MAX);
		fellow_fd_ioctx_return(&lbuf->fdil);
	} else {
		AZ(lbuf->fdil.ioctxp);
	}

	logbuffer_wait_flush_fini(lbuf);
	TAKE(palloc, lbuf->alloc);
	buddy_return1_ptr_page(lbuf->membuddy, &palloc);
	memset(lbuf, 0, sizeof *lbuf);
}

static void
logbuffer_grow(struct fellow_logbuffer *lbuf, uint8_t initialbits)
{
	struct {
		struct fellow_alloc_log_block	*arr;
		struct buddy_ptr_page		alloc;	// arr allocation
		unsigned			n, space;
	} old;
	size_t sz, b;

	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);
	assert(lbuf->n == lbuf->space);

	old.arr		= lbuf->arr;
	old.alloc	= lbuf->alloc;
	old.n		= lbuf->n;
	old.space	= lbuf->space;

	if (old.alloc.bits == 0) {
		AN(initialbits);
		lbuf->alloc = buddy_alloc1_ptr_page_wait(
		    lbuf->membuddy, FEP_MEM_LBUF,
		    initialbits, (int8_t)initialbits);
	} else {
		AZ(initialbits);
		lbuf->alloc = buddy_alloc1_ptr_page_wait(
		    lbuf->membuddy, FEP_MEM_LBUF, old.alloc.bits + 1, 0);
	}
	MEM(lbuf->membuddy);
	lbuf->arr = lbuf->alloc.ptr;

	XXXAN(lbuf->arr);
	sz = (size_t)1 << lbuf->alloc.bits;
	memset(lbuf->arr, 0, sz);
	b = sz / sizeof *lbuf->arr;
	assert(b < UINT_MAX);
	lbuf->space = (unsigned)b;

	if (old.space == 0)
		return;
	assert(lbuf->space > old.space);
	if (old.n > 0)
		memcpy(lbuf->arr, old.arr, old.n * sizeof *lbuf->arr);
	buddy_return1_ptr_page(lbuf->membuddy, &old.alloc);
}

static void
logbuffer_need_ioctx(struct fellow_fd *ffd, struct fellow_logbuffer *lbuf)
{
	if (lbuf->fdil.ioctx != NULL)
		return;
	fellow_fd_ioctx_get(ffd, &lbuf->fdil);
	AN(lbuf->fdil.ioctx);
}

static void
logbuffer_flush_finish_need_ioctx(struct fellow_logbuffer_ff *ff)
{
	if (ff->fdil.ioctx != NULL)
		return;
	fellow_fd_ioctx_get(ff->ffd, &ff->fdil);
	AN(ff->fdil.ioctx);
}

static void
logbuffer_recycle(struct fellow_fd *ffd, struct fellow_logbuffer *lbuf,
    struct fellow_log_region *logreg)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AN(lbuf);

	logbuffer_fini_dskpool(lbuf);

	logbuffer_assert_empty(lbuf);
	logbuffer_wait_flush_fini(lbuf);

	AZ(lbuf->regions_to_free);
	lbuf->active_off = 0;
	lbuf->tail_off = 0;
	lbuf->tot = 0;
	assert(lbuf->membuddy == ffd->membuddy);
	assert(lbuf->space);
	assert(lbuf->phase == &ffd->phase);
	assert(lbuf->phase_mtx == &ffd->phase_mtx);
	assert(lbuf->phase_cond == &ffd->phase_cond);

	memset(&lbuf->dle_stats, 0, sizeof lbuf->dle_stats);
	lbuf->logreg = logreg;
	lbuf->state = LBUF_INIT;
	lbuf->id = seq_inc(ffd->logbuf_id);
	lbuf->seq = 1;
	lbuf->flush_seq = 1;
	lbuf->head_seq[0] = 0;
	lbuf->head_seq[1] = 0;
}

static void
logbuffer_init(struct fellow_fd *ffd, struct fellow_logbuffer *lbuf,
    struct logblk_mempool *mempool, struct fellow_log_region *logreg)
{
	const struct stvfe_tune *tune;
	size_t b;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AN(lbuf);
	tune = ffd->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	INIT_OBJ(lbuf, FELLOW_LOGBUFFER_MAGIC);
	lbuf->membuddy = ffd->membuddy;
	lbuf->dskbuddy = ffd->dskbuddy;
	lbuf->mempool = mempool;

	b = log2up(tune->logbuffer_size *
	    sizeof *lbuf->arr / (FELLOW_DISK_LOG_BLOCK_ENTRIES / 2));
	DBG("wantbits %zu", b);	// 13 for default tune
	assert(b <= UINT8_MAX);
	logbuffer_grow(lbuf, (uint8_t)b);

	// sane upper limit to not eat up all of the membuddy
	b = buddy_size(ffd->membuddy) / 8;
	b >>= MIN_FELLOW_BITS;
	if (b > lbuf->space)
		b = lbuf->space;
	lbuf->thr = (unsigned)b;

	BUDDY_POOL_INIT(lbuf->ffpool, lbuf->membuddy, FEP_MEM_LOG,
	    logbuffer_fill_ffpool, lbuf);

	lbuf->state = LBUF_INIT;
	lbuf->phase = &ffd->phase;
	lbuf->phase_mtx = &ffd->phase_mtx;
	lbuf->phase_cond = &ffd->phase_cond;
	logbuffer_recycle(ffd, lbuf, logreg);
}

/* the ffd->logbuf needs to use the ffd->logblk_pool */
static inline void
logbuffer_take(struct fellow_logbuffer *to, struct logblk_mempool *to_mempool,
    struct fellow_logbuffer *from)
{
	logbuffer_wait_flush_fini(from);

	*to = *from;
	to->mempool = to_mempool;
	BUDDY_POOL_INIT(to->ffpool, from->membuddy, FEP_MEM_LOG,
	    logbuffer_fill_ffpool, to);

	BUDDY_POOL_FINI(from->ffpool);

	memset(from, 0, sizeof *from);
}

static struct fellow_disk_log_block *
fellow_logblk_new(
    const struct fellow_logbuffer *lbuf,
    unsigned wait, uint8_t fht, uint8_t id)
{
	struct fellow_disk_log_block *logblk;
	struct buddy_ptr_page e;

	if (wait == 0 && logblk_mempool_avail(lbuf->mempool) == 0)
		return (NULL);

	e = buddy_get_next_ptr_page(logblk_mempool_get(lbuf->mempool, lbuf));

	AN(e.ptr);
	//lint -e{587} false positive, definitely -- assertion is true
	assert(((size_t)1 << e.bits) == sizeof *logblk);

	MEM(lbuf->membuddy);

	logblk = e.ptr;
	INIT_OBJ(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);
	logblk->version = 1;
	logblk->fht = fht;
	logblk->id = id;
	return (logblk);
}

/* pass in an array of log blocks
 * they get offsets assigned unless already present
 * return the number of log blocks which now have
 * an offset
 */

static unsigned
logblocks_alloc_from_logregion(struct fellow_log_region *logreg,
    struct fellow_alloc_log_block *arr, unsigned n)
{
	const size_t blksz = sizeof(struct fellow_disk_log_block);
	unsigned i;

	CHECK_OBJ_NOTNULL(logreg, FELLOW_LOG_REGION_MAGIC);
	AN(arr);
	AN(n);

	if (logreg->free_n == 0) {
		AZ(logreg->free_off);
		return (0);
	}
	AN(logreg->free_off);
	assert(region_contains(logreg->region, logreg->free_off));

	for (i = 0;
	     n > 0 && logreg->free_n > 0;
	     n--, arr++) {
		i++;
		if (arr->off != 0)
			continue;
		AN(logreg->free_n);
		FDBG(D_LOG_ALLOC, "next[reg] = %zu", logreg->free_off);
		arr->off = logreg->free_off;
		logreg->free_off += (off_t)blksz;
		logreg->free_n--;
	}

	if (n > 0)
		AZ(logreg->free_n);

	if (logreg->free_n == 0) {
		assert(logreg->free_off ==
		    logreg->region->off + (off_t)logreg->region->size);
		logreg->free_off = 0;
	} else {
		assert(region_contains(logreg->region, logreg->free_off));
	}
	return (i);
}

static void
log_blocks_alloc_from_reqs(struct buddy_reqs *reqs,
    struct fellow_alloc_log_block **arr, unsigned *n)
{

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	(void) BUDDYF(alloc_async_ready)(reqs);

	for (; *n; (*arr)++, (*n)--) {
		if ((*arr)->off > 0)
			continue;
		(*arr)->off = buddy_get_next_off_page(reqs).off;
		if ((*arr)->off == BUDDY_OFF_NIL)
			break;
		assert((*arr)->off > 0);
		FDBG(D_LOG_ALLOC, "next[extra] = %zu", (*arr)->off);
	}
	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
}

static void
logbuffer_fill_dskpool(struct buddy_reqs *reqs, const void *priv)
{
	const struct fellow_logbuffer *lbuf;
	uint8_t pri;
	unsigned u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	CAST_OBJ_NOTNULL(lbuf, priv, FELLOW_LOGBUFFER_MAGIC);

	if (lbuf->regions_to_free &&
	    regionlist_used(lbuf->regions_to_free))
		pri = FEP_DSK_LOG_PRI;
	else
		pri = FEP_DSK_LOG;

	BUDDY_REQS_PRI(reqs, pri);

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_page(reqs, MIN_FELLOW_BITS, 0));
}

// defines lbuf_dskpool_get(), _avail()
BUDDY_POOL_GET_FUNC(lbuf_dskpool, static)
BUDDY_POOL_AVAIL_FUNC(lbuf_dskpool, static)

static void
logbuffer_prep_dskpool(struct fellow_logbuffer *lbuf, unsigned urgent)
{
	struct lbuf_dskpool *dskpool;
	struct buddy_ptr_page alloc;

	if (lbuf->dskpool != NULL)
		return;

	if (urgent == 0 && logblk_mempool_avail(lbuf->mempool) == 0)
		return;

	assert(logbuffer_can(lbuf, LBUF_CAN_FLUSH));

	/*
	 * when the log region is becoming full, start
	 * pre-allocating disk blocks.
	 *
	 * XXX is 25% a good measure?
	 */
	if (lbuf->logreg == NULL ||
	    lbuf->logreg->free_n * 4 < lbuf->logreg->space ||
	    lbuf->logreg->free_n < lbuf->n * 4) {
		alloc = buddy_get_next_ptr_page(
		    logblk_mempool_get(lbuf->mempool, lbuf));
		AN(alloc.ptr);
		assert((size_t)1 << alloc.bits >= sizeof *lbuf->dskpool);

		dskpool = alloc.ptr;
		BUDDY_POOL_INIT(dskpool, lbuf->dskbuddy, FEP_DSK_LOG,
		    logbuffer_fill_dskpool, lbuf);
		lbuf->dskpool = dskpool;
	}
}

static void
logbuffer_alloc_some(struct fellow_logbuffer *lbuf,
    struct fellow_alloc_log_block *arr, unsigned n)
{
	const size_t blksz = sizeof(struct fellow_disk_log_block);
	unsigned i;

	//lint -e{506} constant boolean
	assert(blksz == ((size_t)1 << MIN_FELLOW_BITS));

	//lint -e{827} flexelint knows this can never be NULL
	AN(arr);
	AN(n);

	assert(logbuffer_can(lbuf, LBUF_CAN_FLUSH));

	if (lbuf->logreg != NULL)
		i = logblocks_alloc_from_logregion(lbuf->logreg, arr, n);
	else
		i = 0;
	assert(i <= n);
	arr += i;
	n -= i;

	while (n > 0 && arr->off > 0) {
		arr++;
		n--;
	}

	logbuffer_prep_dskpool(lbuf, n);

	if (n == 0)
		return;

	AN(lbuf->dskpool);

	while (n > 0) {
		log_blocks_alloc_from_reqs(
		    lbuf_dskpool_get(lbuf->dskpool, lbuf),
		    &arr, &n);
	}

}

static void
logbuffer_alloc(struct fellow_logbuffer *lbuf)
{
	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);

	if (lbuf->head.off == 0)
		logbuffer_alloc_some(lbuf, &lbuf->head, 1);
	if (lbuf->n > 0)
		logbuffer_alloc_some(lbuf, lbuf->arr, lbuf->n);
	if (lbuf->active.off == 0)
		logbuffer_alloc_some(lbuf, &lbuf->active, 1);
}

static void
log_off_todo_commit(struct fellow_fd *ffd,
    struct fellow_disk_log_off_todo *todo)
{
	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	CHECK_OBJ_NOTNULL(todo, FELLOW_DISK_LOG_OFF_TODO_MAGIC);

	switch(todo->where) {
	case LOGOFF_LOGBLK:
		ffd->log_info.off.logblk = todo->off;
		ffd->log_info.alt.logblk = todo->alt;
		break;
	case LOGOFF_PENDBLK:
		ffd->log_info.off.pendblk = todo->off;
		ffd->log_info.alt.pendblk = todo->alt;
		break;
	default:
		WRONG("todo->where");
	}
	memset(todo, 0, sizeof *todo);
}

static void
logbuffer_ref(struct fellow_logbuffer *lbuf)
{
	struct fellow_disk_log_off_todo *todo = &lbuf->todo;
	const struct fellow_disk_log_block *logblk;

	assert(logbuffer_can(lbuf, LBUF_CAN_REF));

	INIT_OBJ(todo, FELLOW_DISK_LOG_OFF_TODO_MAGIC);

	todo->off = lbuf->active.off;

	logblk = lbuf->active.block;
	if (todo->off != 0 && logblk != NULL) {
		CHECK_OBJ(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);
		todo->alt = logblk->prev_off;
	}

	if (lbuf->state == LBUF_PEND)
		todo->where = LOGOFF_PENDBLK;
	else
		todo->where = LOGOFF_LOGBLK;
}

// avoid code duplication
static unsigned
flush_active(const struct fellow_fd *ffd, struct fellow_logbuffer *lbuf,
    unsigned opts)
{
	struct fellow_alloc_log_block blk;
	struct fellow_disk_log_block *copy = NULL;
	enum fellow_aio_info_type type;
	unsigned r;

	AN(lbuf->fdil.ioctx);
	AN(lbuf->active.off);
	if (logbuffer_can(lbuf, LBUF_CAN_REF))
		logbuffer_ref(lbuf);

	blk = lbuf->active;
	lbuf->active_off = blk.off;

	/*
	 * for a close, the block will be finished (freed)
	 *
	 * if not closing, we are going to keep it for more updates, so make a
	 * copy to avoid race between IO and updates to the block.
	 *
	 * if allocation of the block fails, write sync
	 *
	 */

	if (opts == 0) {
		copy = fellow_logblk_new(lbuf, 0,
		    ffd->tune->hash_log, 0);
		if (copy == NULL) {
			opts |= FAIO_SYNC;
			type = FAIOT_NONE;
		}
		else {
			memcpy(copy, blk.block, sizeof *copy);
			blk.block = copy;
			type = FAIOT_DLB_FINISH;
		}
	}
	else if (opts & FAIO_CLOSE)
		type = FAIOT_DLB_FINISH;
	else if (opts & FAIO_SYNC)
		type = FAIOT_NONE;
	else
		INCOMPL();

	FDBG(D_LOG_FLUSH, "%s %s off %zu",
	    opts & FAIO_CLOSE ? "close" : "submit",
	    opts & FAIO_SYNC ? "sync" : "async",
	    blk.off);

	r = fellow_io_log_submit(ffd,
	    opts & FAIO_SYNC ? NULL : lbuf->fdil.ioctx,
	    type, &blk);

	if (r && (opts & FAIO_CLOSE)) {
		AZ(copy);
		AZ(blk.block);
		AZ(blk.off);
		memset(&lbuf->active, 0, sizeof lbuf->active);
		return (r);
	}
	if (r && copy) {
		// copy to be finished, but original alive
		AZ(blk.block);
		AZ(blk.off);
		AN(lbuf->active.block);
		AN(lbuf->active.off);
		return (r);
	}
	if (r) {
		// no copy and kept alive
		assert(blk.block == lbuf->active.block);
		assert(blk.off == lbuf->active.off);
		return (r);
	}

	// error, all kept alive, free copy, if any
	if (copy == NULL)
		return (r);

	struct buddy_ptr_extent e;
	e = BUDDY_PTR_EXTENT(copy, sizeof *copy);
	buddy_return1_ptr_extent(lbuf->membuddy, &e);

	return (r);
}

static void
logbuffer_flush_finish(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, unsigned doclose, unsigned can );
static void
logbuffer_flush_finish_work(struct worker *wrk, void *priv);

/*
 * for use under the lock. Uses the mempool if possible
 */
static void
logbuffer_need_regions_to_free(struct fellow_logbuffer *lbuf)
{

	if (lbuf->regions_to_free != NULL)
		return;

	if (logblk_mempool_avail(lbuf->mempool)) {
		lbuf->regions_to_free = regionlist_init(
		    buddy_get_next_ptr_page(
			logblk_mempool_get(lbuf->mempool, lbuf)),
		    lbuf->membuddy);
	}
	else
		lbuf->regions_to_free = regionlist_alloc(lbuf->membuddy);
}

static void
logbuffer_flush(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, unsigned doclose, unsigned can)
{
	const struct fellow_disk_log_block *logblk;
	struct fellow_alloc_log_block **arr;
	const struct stvfe_tune *tune;
	struct buddy_ptr_extent mem;
	unsigned r, u, v, n;
	size_t sz;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	tune = ffd->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);
	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);

	/* capability assertions on the logbuffer
	 * - we later limi by what we actually want to use
	 */
	assert(
	    (ffd->phase == FP_OPEN &&
	     logbuffer_can(lbuf, LBUF_CAN_FLUSH)) ||
	    (ffd->phase == FP_INIT &&
	     logbuffer_can(lbuf, LBUF_CAN_LOGREG)));

	if (logbuffer_can(lbuf, LBUF_CAN_LOGREG))
		AN(lbuf->logreg);

	if (doclose) {
		AN(logbuffer_can(lbuf, LBUF_CAN_REF));
		logbuffer_wait_flush_fini(lbuf);
		AZ(lbuf->ff);
	}

	// keep the options to flush at all
	can |= (LBUF_CAN_FLUSH | LBUF_CAN_LOGREG);
	can &= lbuf_cap[lbuf->state];

#ifdef DEBUG
	lbuf->flush_t0 = VTIM_mono();
#endif

	if (lbuf->active.block == NULL) {
		AZ(lbuf->n);
		AZ(lbuf->active.off);
		AZ(lbuf->head.block);
		AZ(lbuf->head.off);
		if (! cap(can, LBUF_CAN_REF) || lbuf->ff != NULL)
			return;
		logbuffer_wait_flush_fini(lbuf);
		AZ(lbuf->ff);
		logbuffer_ref(lbuf);
		logbuffer_flush_finish(ffd, lbuf, doclose, can);
		return;
	}

	logbuffer_need_regions_to_free(lbuf);

	if (lbuf->head.block == NULL) {
		if (! cap(can, LBUF_CAN_REF) || lbuf->ff != NULL)
			return;
		logbuffer_wait_flush_fini(lbuf);
		AZ(lbuf->ff);

		// the active block is the head block
		AZ(lbuf->n);

		CHECK_BLK_SEQ(lbuf->active.block, lbuf->flush_seq);

		if (lbuf->active.off == 0 && cap(can, LBUF_CAN_FLUSH))
			logbuffer_alloc_some(lbuf, &lbuf->active, 1);
		if (lbuf->active.off == 0 &&
		    logblocks_alloc_from_logregion(lbuf->logreg,
		    &lbuf->active, 1) != 1) {
			lbuf->state = LBUF_MEM;
			return;
		}

		logbuffer_need_ioctx(ffd, lbuf);

		while (! flush_active(ffd, lbuf, doclose ? FAIO_CLOSE : 0))
			(void) fellow_io_log_wait_completions(
			    lbuf->membuddy,
			    lbuf->fdil.ioctx, 1);
		if (doclose)
			AZ(lbuf->active.block);

		FDBG(D_LOG_FLUSH, "%p ref active single %zu / %zu",
		    lbuf, ffd->log_info.off.logblk, ffd->log_info.off.pendblk);
		logbuffer_flush_finish(ffd, lbuf, doclose, can);
		return;
	}

	AN(lbuf->head.block);
	AN(lbuf->active.block);
	assert(lbuf->active.block != lbuf->head.block);

	/* alloc blocks for all lofbuffer blocks */
	if (cap(can, LBUF_CAN_FLUSH))
		logbuffer_alloc(lbuf);
	else {
		/* we transition to LBUF_MEM when logregion is full */
		assert(cap(can, LBUF_CAN_LOGREG));
		assert(lbuf->state == LBUF_LOGREG);
		if (logblocks_alloc_from_logregion(lbuf->logreg,
			&lbuf->head, 1) != 1) {
			lbuf->state = LBUF_MEM;
			return;
		}
		if (lbuf->n > 0 &&
		    logblocks_alloc_from_logregion(lbuf->logreg,
		      lbuf->arr, lbuf->n) != lbuf->n) {
			lbuf->state = LBUF_MEM;
			return;
		}
		if (logblocks_alloc_from_logregion(lbuf->logreg,
			&lbuf->active, 1) != 1) {
			lbuf->state = LBUF_MEM;
			return;
		}
	}
	/* strategy
	 *
	 * - write all but the head
	 *   [array] + active
	 * - the head could have been written in a previous iteration and
	 *   not directly linked to the first element
	 * - write the head if CAN_REF
	 *   if yes, new head is active
	 * - write hdr if CAN_REF
	 *
	 * offset array order:
	 *
	 * head
	 * [arr]
	 * active
	 * 0		(next)
	 *
	 */

	AN(lbuf->active.off);
	AN(lbuf->head.off);

	/*lint --e{679}
	 * flexelint warns about the fact that we might run
	 * into integer truncation with i used as an array offset
	 * while the array pointer could hold it.
	 * this is good advise, but the point about the array offsets
	 * is to make the code easier to understand, so suppressing
	 * this advise is considered the lesser evil
	 */
	/*
	 * array of pointers to head, [arr], active
	 *
	 * not allocating on stack because this code might
	 * run in a varnish worker and the default stacksize
	 * is smaller than expected values here
	 */
	n = lbuf->n + 2;
	sz = (typeof(sz))n + 1;
	sz *= sizeof *arr;
	mem = buddy_alloc1_ptr_extent(ffd->membuddy, sz, 0);
	if (mem.ptr) {
		assert(mem.size >= sz);
		memset(mem.ptr, 0, mem.size);
		arr = mem.ptr;
	} else {
		/* arr is super important, so fall back to calloc */
		arr = calloc((size_t)n + 1, sizeof *arr);
		AN(arr);
	}

	arr[0] = &lbuf->head;
	for (u = 0; u < lbuf->n; u++)
		arr[u + 1] = &lbuf->arr[u];
	AZ(arr[n - 1]);
	arr[n - 1] = &lbuf->active;
	// last: pseudo entry with null offset
	arr[n] = &(struct fellow_alloc_log_block){
		.off = 0,
		.block = &(struct fellow_disk_log_block){0}};

#define THS u
#define PRV (u - 1)
#define NXT (u + 1)

	/* link the prev/next offsets if not present */
	for (u = 1; u < n; u++) {
		// when linking blocks, either both are zero
		// or both are set

		if (! (arr[PRV]->block->next_off > 0 &&
		       arr[THS]->block->prev_off > 0)) {
			AZ(arr[PRV]->block->next_off);
			AZ(arr[THS]->block->prev_off);
			arr[PRV]->block->next_off = arr[THS]->off;
			arr[THS]->block->prev_off = arr[PRV]->off;
		}
		if (! (arr[THS]->block->next_off > 0 &&
		       arr[NXT]->block->prev_off > 0)) {
			AZ(arr[THS]->block->next_off);
			AZ(arr[NXT]->block->prev_off);
			arr[THS]->block->next_off = arr[NXT]->off;
			arr[NXT]->block->prev_off = arr[THS]->off;
		}
	}

	/* On submitting early:
	 *
	 * As long as we calculate the hashes in the thread calling
	 * logbuffer_flush() (which, for the most part, should be the
	 * logwatcher), and in particular if the hash is SHA256, we actually do
	 * spend some time on the submissions, so it does pay off to
	 * send some of them to uring early
	 */
#define BATCH


#ifdef BATCH
	unsigned batch = n / 2;
	if (batch > tune->io_batch_max)
		batch = tune->io_batch_max;
	else if (batch < tune->io_batch_min)
		batch = tune->io_batch_min;
#endif
#undef THS
#undef PRV
#undef NXT

	/* head seq is either 0 or matches the unwritten head */
	logblk = lbuf->head.block;
	if (lbuf->head_seq[0] == 0 || lbuf->head_seq[1] == 0) {
		assert(lbuf->head_seq[0] == 0 && lbuf->head_seq[1] == 0);
		lbuf->head_seq[0] = logblk->entry[0].seq;
		lbuf->head_seq[1] = logblk->entry[logblk->nentries - 1].seq;
		assert(lbuf->flush_seq == lbuf->head_seq[0]);
		lbuf->flush_seq = lbuf->head_seq[1] + 1;
		if (lbuf->flush_seq == 0)
			lbuf->flush_seq = 1;
	}
	else {
		assert(lbuf->head_seq[0] != 0 && lbuf->head_seq[1] != 0);
		assert(lbuf->head_seq[0] ==
		    logblk->entry[0].seq);
		assert(lbuf->head_seq[1] ==
		    logblk->entry[logblk->nentries - 1].seq);
	}

	logbuffer_need_ioctx(ffd, lbuf);

	FDBG(D_LOG_FLUSH, "%p to submit %u", lbuf, n);

	AN(lbuf->regions_to_free);
	//lint -e{850} loop variable modified in body
	for (u = 1, v = 0; u < n - 1; u++) {
		ADD_BLK_SEQ(arr[u]->block, lbuf->flush_seq);
		r = fellow_io_log_submit(ffd, lbuf->fdil.ioctx,
		    FAIOT_DLB_FINISH, arr[u]);

#ifdef BATCH
		/*
		 * devide and conquer: we submit at half the
		 * remaining IOs, if at least that many came back
		 */
		if (u % batch == 0 && n - u > tune->io_batch_min) {
			unsigned w, remain;

			w = fellow_io_log_wait_completions(
			    lbuf->membuddy,
			    lbuf->fdil.ioctx, 0);
			v += w;
			FDBG(D_LOG_FLUSH, "batch=%u submit %u complete +%u %u",
			    batch, u - 1, w, v);

			remain = (n + tune->io_batch_min) - u;

			if (remain <= batch) {
				remain /= 2;
				if (w >= remain) {
					batch= remain;
					if (batch< tune->io_batch_min)
						batch= tune->io_batch_min;
				}
			}
		}
#endif

		if (r)
			continue;

		// need to resubmit - XXX more elegant?
		lbuf->flush_seq = arr[u]->block->entry[0].seq;
		u--;

		r = fellow_io_log_wait_completions(
		    lbuf->membuddy,
		    lbuf->fdil.ioctx, 1);
		AN(r);
		v += r;
		FDBG(D_LOG_FLUSH, "submit full complete %u", v);
	}

	assert(arr[u] == &lbuf->active);

	/*
	 * lbuf array was zeroed by submissions of type FAIOT_DLB_FINISH
	 * but not necessarily the active block
	 */
	for (u = 0; u < lbuf->n; u++) {
		AZ(lbuf->arr[u].block);
		AZ(lbuf->arr[u].off);
	}
	lbuf->n = 0;
	if (mem.ptr)
		buddy_return1_ptr_extent(ffd->membuddy, &mem);
	else
		free(arr);
	//lint -e{672}
	arr = NULL;
	(void) arr;

	/*
	 * if we can not ref the head:
	 * - do not write the active block (it can not be reached anyway)
	 * - do not wait for completions, but finish all available
	 */

	if (! cap(can, LBUF_CAN_REF)) {
		logbuffer_flush_finish(ffd, lbuf, doclose, can);
		return;
	}

	/* keep the active block unless closing */
	CHECK_BLK_SEQ(lbuf->active.block, lbuf->flush_seq);
	while (! flush_active(ffd, lbuf, doclose ? FAIO_CLOSE : 0))
		v += fellow_io_log_wait_completions(
		    lbuf->membuddy,
		    lbuf->fdil.ioctx, 1);
	if (doclose)
		AZ(lbuf->active.block);

	(void)v;

	// handled at top
	assert(lbuf->active.block != lbuf->head.block);

	// XXX same as above
	assert(lbuf->head_seq[0] != 0 && lbuf->head_seq[1] != 0);
	assert(lbuf->head_seq[0] == logblk->entry[0].seq);
	assert(lbuf->head_seq[1] == logblk->entry[logblk->nentries - 1].seq);
	lbuf->head_seq[0] = 0;
	lbuf->head_seq[1] = 0;

	logbuffer_flush_finish(ffd, lbuf, doclose, can);
}

static void
logbuffer_flush_finish(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, unsigned doclose, unsigned can)
{
	struct fellow_logbuffer_ff *ff, ffstk[1];
	struct buddy_ptr_extent alloc = buddy_ptr_extent_nil;
	unsigned need_work, can_thread;

	if (doclose == 0 && lbuf_ffpool_avail(lbuf->ffpool)) {
		alloc = buddy_get_next_ptr_extent(
		    lbuf_ffpool_get(lbuf->ffpool, lbuf));
		AN(alloc.ptr);
		assert(alloc.size >= sizeof *ff);
		ff = alloc.ptr;

		can_thread = 1;
	}
	else {
		ff = ffstk;

		can_thread = 0;
	}
	INIT_OBJ(ff, FELLOW_LOGBUFFER_FF_MAGIC);

	ff->alloc = alloc;
	ff->can = can;
	ff->ffd = ffd;
	TAKE(ff->fdil, lbuf->fdil);
	if (cap(can, LBUF_CAN_REF)) {
		ff->active_off = lbuf->active_off;
		ff->tail_off = &lbuf->tail_off;
		TAKE(ff->head, lbuf->head);
		TAKE(ff->regions_to_free, lbuf->regions_to_free);
		TAKE(ff->todo, lbuf->todo);
	}
	if (ff->regions_to_free != NULL)
		ff->freeing = ff->regions_to_free->size;

	FF_TRANSITION(ff, FF_INVAL, FF_SCHEDULED);

	AZ(pthread_mutex_lock(lbuf->phase_mtx));
	need_work = VLIST_EMPTY(&ffd->ffhead);
	VLIST_INSERT_HEAD(&ffd->ffhead, ff, list);
	ffd->nff++;
	lbuf->ff = ff;
	ff->lbuf_ff = &lbuf->ff;
	AZ(pthread_mutex_unlock(lbuf->phase_mtx));

#ifdef DEBUG
	ff->t[0] = lbuf->flush_t0;
	ff->t[1] = VTIM_mono();
#endif

	if (can_thread == 0) {
		if (need_work)
			logbuffer_flush_finish_work(NULL, ff);
		else
			logbuffer_wait_flush_fini(lbuf);
		return;
	}

	AN(can_thread);
	if (need_work == 0)
		return;

	if (ffd->taskrun(logbuffer_flush_finish_work, ff, &ff->taskstate))
		logbuffer_flush_finish_work(NULL, ff);
}

static struct fellow_logbuffer_ff *
logbuffer_flush_finish_work_one(struct fellow_logbuffer_ff *ff)
{
	struct fellow_logbuffer_ff *next;
	struct buddy_ptr_extent alloc;
	struct fellow_fd *ffd;
	pthread_mutex_t *phase_mtx;
	pthread_cond_t *phase_cond;
	unsigned n, u, v;
#ifdef DEBUG
	vtim_mono dt, t2, t3;
#endif
	CHECK_OBJ_NOTNULL(ff, FELLOW_LOGBUFFER_FF_MAGIC);
	ffd = ff->ffd;
	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	TAKE(alloc, ff->alloc);

	phase_mtx = &ffd->phase_mtx;
	phase_cond = &ffd->phase_cond;

	if (! cap(ff->can, LBUF_CAN_REF)) {
		AZ(ff->active_off);
		AZ(ff->tail_off);
		AZ(ff->head.off);
		AZ(ff->regions_to_free);
		AZ(ff->todo.magic);
		FF_TRANSITION(ff, FF_SCHEDULED, FF_FREE);
		goto ff_free;
	}

	FF_TRANSITION(ff, FF_SCHEDULED, FF_WAIT_OUTSTANDING);

	n = 0;
	v = 0;
	/* wait for any outstanding IO from the caller */
	if (ff->fdil.ioctx) {
		n = fellow_io_outstanding(ff->fdil.ioctx);
		do {
			FDBG(D_LOG_FLUSH, "wait %u/%u", v, n);
			u = fellow_io_log_wait_completions(
			    ffd->membuddy,
			    ff->fdil.ioctx, UINT_MAX);
			v += u;
		} while (u);
	}
	(void) n;
	(void) v;

	FF_TRANSITION(ff, FF_WAIT_OUTSTANDING, FF_HEAD);

	if (ff->head.block) {
		AN(ff->active_off);
		AN(ff->head.off);
		FDBG(D_LOG_FLUSH, "finish head blk %zu", ff->head.off);

		// NULL ioctx is sync
		XXXAN(fellow_io_log_submit(ffd, NULL,
			FAIOT_DLB_FINISH, &ff->head));
	}
	AZ(ff->head.block);
	AZ(ff->head.off);

	*ff->tail_off = ff->active_off;

#ifdef DEBUG
	t2 = VTIM_mono();
#endif

	FDBG(D_LOG_FLUSH, "ref active %zu / %zu", ffd->log_info.off.logblk,
	    ffd->log_info.off.pendblk);

	FF_TRANSITION(ff, FF_HEAD, FF_HDR);

	log_off_todo_commit(ffd, &ff->todo);
	AZ(fellow_io_write_hdr(ffd));

	FF_TRANSITION(ff, FF_HDR, FF_FREE);

	if (ff->regions_to_free) {
		logbuffer_flush_finish_need_ioctx(ff);
		regionlist_discard(ffd, ff->fdil.ioctx,
		    &ff->regions_to_free);
		ff->freeing = 0;
	}
	AZ(ff->regions_to_free);

  ff_free:
	if (ff->fdil.ioctx) {
		do {
			u = fellow_io_log_wait_completions(
			    ffd->membuddy,
			    ff->fdil.ioctx, UINT_MAX);
		} while (u);
		fellow_fd_ioctx_return(&ff->fdil);
	}

#ifdef DEBUG
	t3 = VTIM_mono();
	dt = t3 - ff->t[0];
#endif

	FDBG(D_LOG_FLUSH,
	    "flush_fini (%s) dt=%f "
	    "(thread1 %f%% (wait %u + head) %f%% active/ref/free %f%%)",
	    cap(ff->can, LBUF_CAN_REF) ? "ref" : "NOref",
	    dt,
	    (ff->t[1] - ff->t[0]) * 100 / dt,
	    n,
	    (t2 - ff->t[1]) * 100 / dt,
	    (t3 - t2) * 100 / dt);

	AZ(pthread_mutex_lock(phase_mtx));
	FF_TRANSITION(ff, FF_FREE, FF_DONE);

	AZ(VLIST_NEXT(ff, list));
	next = VLIST_PREV(ff, &ffd->ffhead, fellow_logbuffer_ff, list);
	VLIST_REMOVE(ff, list);

	AN(ff->lbuf_ff);
	/* are we the last flush of this logbuffer ?
	 * note: next can still be non-null for a flush from
	 * a different logbuffer
	 */
	if (*ff->lbuf_ff == ff) {
		*ff->lbuf_ff = NULL;
		AZ(pthread_cond_broadcast(phase_cond));
	}
	ff->lbuf_ff = NULL;

	AN(ffd->nff--);

	if (LIKELY(alloc.ptr != NULL))
		buddy_return1_ptr_extent(ffd->membuddy, &alloc);
	// for felow_log_close()
	if (ffd->nff == 0)
		AZ(pthread_cond_broadcast(phase_cond));
	AZ(pthread_mutex_unlock(phase_mtx));
	return (next);
}

static void
logbuffer_flush_finish_work(struct worker *wrk, void *priv)
{
	struct fellow_logbuffer_ff *ff;

	(void) wrk;
	CAST_OBJ_NOTNULL(ff, priv, FELLOW_LOGBUFFER_FF_MAGIC);

	do
		ff = logbuffer_flush_finish_work_one(ff);
	while (ff != NULL);
}

// XXX when should we flush anyway?
//
// - when logbuffer is full -> tunable
// - after some time

#define TAKE_BLK(to, from)						\
	do {								\
		AZ((to).block);						\
		AZ((to).off);						\
		CHECK_OBJ_NOTNULL((from).block,				\
		    FELLOW_DISK_LOG_BLOCK_MAGIC);			\
		TAKE(to, from);					\
	} while(0)

static unsigned
logbuffer_avail_dsk_blks(const struct fellow_logbuffer *lbuf)
{
	unsigned avail = 0;

	if (lbuf->logreg != NULL)
		avail += lbuf->logreg->free_n;
	if (lbuf->dskpool != NULL)
		avail += lbuf_dskpool_avail(lbuf->dskpool);
	return (avail);
}
static void
logbuffer_addblks(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf,
    struct fellow_alloc_log_block *blks, unsigned n)
{
	unsigned canflush;
	unsigned avail;
	unsigned kick;
	unsigned can;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	CHECK_LBUF_USABLE(lbuf);
	AN(blks);

	lbuf->tot += n;

	while (n) {
		if (lbuf->active.block == NULL) {
			TAKE_BLK(lbuf->active, *blks);
			blks++;
			n--;
			continue;
		}

		if (lbuf->head.block == NULL) {
			TAKE_BLK(lbuf->head, lbuf->active);
			continue;
		}

		AN(lbuf->active.block);
		AN(lbuf->head.block);

		canflush =
		    logbuffer_can(lbuf, LBUF_CAN_FLUSH) ||
		    logbuffer_can(lbuf, LBUF_CAN_LOGREG);

		avail = 0;
		kick = 0;
		can = 0;
		if (canflush)
			avail = logbuffer_avail_dsk_blks(lbuf);

		/* if we need memory, just flush blocks
		 */
		if (canflush && lbuf->space > 0) {
			if (lbuf->n >= lbuf->thr)
				can |= LBUF_CAN_FLUSH;
			else if (lbuf->n * 2 > lbuf->thr)
				kick = 1;
		}
		/* low water in mempool?
		 */
		if (canflush &&
		    logblk_mempool_avail(lbuf->mempool) < LOGBLK_MEMPOOL_SIZE)
			kick = 1;

		/* if we run short of disk blocks (25%), we want to
		 * ref in order to apply frees
		 *
		 * unless avail increases, this flushes for every half
		 */
		if (canflush && lbuf->n > 0) {
			if (lbuf->n * 4 >= avail * 3)
				can |= LBUF_CAN_REF;
			else if (!can && lbuf->n * 2 >= avail)
				kick = 1;
		}

		/*
		 * flush if logbuffer is full or not enough blocks
		 */
		if (can) {
			logbuffer_flush(ffd, lbuf, 0, can);
			if (lbuf->n != lbuf->space)
				continue;
		}
		else if (kick)
			fellow_logwatcher_kick_locked(ffd);

		// try to grow
		if (lbuf->n == lbuf->space)
			logbuffer_grow(lbuf, 0);
		// else we are doomed
		assert(lbuf->space > lbuf->n);

		AN(lbuf->active.block);
		AN(lbuf->head.block);

		TAKE_BLK(lbuf->arr[lbuf->n], lbuf->active);
		lbuf->n++;

		if (canflush == 0)
			avail = UINT_MAX;

		while (n > 1 && lbuf->n < lbuf->space && lbuf->n < avail) {
			TAKE_BLK(lbuf->arr[lbuf->n], *blks);
			lbuf->n++;
			blks++;
			n--;
		}

		assert(n >= 1);
		AZ(lbuf->active.block);
	}
	if(logbuffer_can(lbuf, LBUF_CAN_FLUSH))
		logbuffer_prep_dskpool(lbuf, 0);
}

enum getblk_e {
	GET_USED = 0,
	GET_NEW = 1
};

static struct fellow_disk_log_block *
logbuffer_getblk(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, enum getblk_e new)
{
	struct fellow_disk_log_block *blk;
	struct fellow_alloc_log_block ablk[1];
	unsigned canflush, wait;

	if (new == GET_USED &&
	    lbuf->active.block &&
	    lbuf->active.block->nentries < FELLOW_DISK_LOG_BLOCK_ENTRIES)
		return (lbuf->active.block);

	canflush =
	    logbuffer_can(lbuf, LBUF_CAN_FLUSH) ||
	    logbuffer_can(lbuf, LBUF_CAN_LOGREG);

	for (blk = NULL, wait = 0; blk == NULL && wait < 2; wait++) {
		blk = fellow_logblk_new(lbuf, wait,
		    ffd->tune->hash_log, lbuf->id);
		if (blk == NULL && canflush)
			logbuffer_flush(ffd, lbuf, 0, LBUF_ALL);
	}
	XXXAN(blk);

	ablk->block = blk;
	ablk->off = 0;
	logbuffer_addblks(ffd, lbuf, ablk, 1);
	AZ(ablk->block);
	return (blk);
}

/* returns next offset */
static off_t
logbuffer_alloc_log_block_submit(struct fellow_fd *ffd,
    struct buddy_returns *rets, struct fellow_logbuffer *lbuf,
    struct fellow_alloc_log_block *block, off_t *prev, off_t off,
    unsigned *tot, uint8_t id, uint8_t *seq)
{
	struct fellow_disk_log_block *logblk;
	struct buddy_ptr_extent e;

	AN(block);
	AN(prev);
	AN(tot);
	AN(id);
	AN(seq);

	logblk = block->block;
	/* block might not have the offset assigned yet */
	assert(off == block->off || block->off == 0);
	memset(block, 0, sizeof *block);
	if (logblk == NULL) {
		AZ(block->off);
		*prev = 0;
		return (0);
	}

	ADD_BLK_SEQ(logblk, *seq);

	if (logblk->id == 0)
		logblk->id = id;
	assert(logblk->id == id);

	if (logblk->prev_off)
		assert(*prev == logblk->prev_off);
	FDBG(D_LOGS_ITER_BLOCK, "append mem %p %zu <- (%zu) -> %zu",
	    logblk, logblk->prev_off, off, logblk->next_off);

	fellow_privatelog_submit(ffd, lbuf,
	    logblk->entry, logblk->nentries);
	(*tot)++;
	*prev = off;
	off = logblk->next_off;

	memset(logblk, 0, sizeof *logblk);
	e = BUDDY_PTR_EXTENT(logblk, sizeof *logblk);
	AN(buddy_return_ptr_extent(rets, &e));
	return (off);
}

/*
 * handle a logblk with an offset
 */
#define CHECK_DSK_BLK do {						\
	assert (! region_contains(active_logregion, off));		\
	assert (! region_contains(empty_logregion, off));		\
	CHECK_BLK(logblk);						\
	assert(prev == logblk->prev_off);				\
	} while (0)

#define APPEND_BLK(what) do {						\
	FDBG(D_LOGS_ITER_BLOCK, "append " what " %zu <- (%zu) -> %zu",	\
	    logblk->prev_off, off, logblk->next_off);			\
									\
	ADD_BLK_SEQ(logblk, seq);					\
									\
	fellow_privatelog_submit(ffd, to, logblk->entry,		\
	    logblk->nentries);						\
									\
	if (! region_contains(from->logreg->region, off))		\
		regionlist_stk_add(tofree, off, sizeof *logblk);	\
									\
	prev = off;							\
	off = logblk->next_off;						\
	tot++;								\
	} while (0)

/*
 * append all blocks' content from an LBUF_PEND logbuffer
 * to another logbuffer, possibly reading a written log from disk
 *
 * mtx protects the "from" logbuffer
 *
 * we get called with mtx held for consistency and shall
 * return with mtx held
 */
static void
logbuffer_append(struct fellow_fd *ffd,
    pthread_mutex_t *mtx,
    const struct buddy_off_extent *active_logregion,
    const struct buddy_off_extent *empty_logregion,
    struct fellow_logbuffer *to, struct fellow_logbuffer *from,
    struct regionlist *freelist)
{
	struct fellow_disk_log_block *logblk;
	struct fellow_logcache flc[1];
	struct fellow_logcache_r r;
	struct buddy_returns *rets;
	regionlist_stk_decl(tofree, 256, freelist);
	unsigned pre, u, locked = 1, restarts = 0, tot = 0;
	off_t off, prev, refreshed = 0, tail, head_seen = 0;
	const char *where = NULL;
	uint8_t seq = 1;

	ASSERT_MUTEX_OWNED(*mtx);

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AN(mtx);
	CHECK_OBJ_NOTNULL(to,   FELLOW_LOGBUFFER_MAGIC);
	CHECK_OBJ_NOTNULL(from, FELLOW_LOGBUFFER_MAGIC);
	assert(to->state == LBUF_INCOMPL ||
	       to->state == LBUF_OPEN);
	assert(from->state == LBUF_PEND);
	CHECK_OBJ_NOTNULL(from->logreg, FELLOW_LOG_REGION_MAGIC);
	AN(from->logreg->region);
	AN(freelist);

	rets = BUDDY_RETURNS_STK(ffd->membuddy, BUDDY_RETURNS_MAX);

	/* the log needs to start at the log region */
	off = from->logreg->region->off;
	prev = 0;

	/* could be that nothing has been written, or that the write is in
	 * progress */
	if (from->tail_off == 0)
		logbuffer_wait_flush_fini(from);
	/* if nothing has been written to disk, skip disk reading */
	if (from->tail_off == 0)
		goto mem;

	//lint --e{455} mutex unlocked
	//lint --e{456} Two execution paths are being combined...
	AZ(pthread_mutex_unlock(mtx));
	locked = 0;

	/* we read the log region written by "from" while potentially blocks are
	 * added -- not holding the mtx
	 *
	 * when it looks like we are done, we grab the log (prevent the from
	 * lbuf from changing) and re-check
	 *
	 * layout reminder:
	 *
	 *               v-- lock
	 * a) [dsk..dsk] head [ dsk..dsk ] arr active
	 * b) [dsk..dsk] head arr active
	 * c) [dsk..dsk] head active
	 * d) [dsk..dsk] active
	 *
	 *
	 * reading dsk, we can unlock again, until we
	 * hit the new head
	 * head and active can be the same block
	 */
  dsk_init:
	AN(from->id);
	fellow_logcache_init(flc, ffd, to->mempool,
	    from->id, from->logreg->region);
	fellow_logcache_prune_direction(flc, -1);
	AZ(locked);
  dsk:
	where = NULL;
	assert(from->tail_off);
	while (
	    off != (tail = from->tail_off) &&
	    off != from->head.off &&
	    (from->n == 0 || off != from->arr[0].off) &&
	    off != from->active.off
	    ) {
		AN(off);

		if (region_contains(from->logreg->region, tail)) {
			/*
			 * if the tail is in the logregion, the head must be as
			 * well
			 */
			assert(tail > off);
			pre = (typeof(pre))
			    ((size_t)(off - tail) / sizeof *logblk);
		}
		else {
			//* potentially prefetch until the end of the logregion
			pre = UINT_MAX;
		}

		r = fellow_logcache_get(flc, off, 1, pre);
		if (r.error) {
			// XXX IO ERROR HANDLING
			XXXAZ(r.u.err);
			// refresh below
			assert(refreshed != off);
			logblk = NULL;
		} else {
			logblk = r.u.logblk;

			CHECK_DSK_BLK;
			if (logblk->next_off != 0) {
				APPEND_BLK("dsk");
				refreshed = 0;
				continue;
			}
		}

		/*
		 * reasons to hit the last block (when we should not as per
		 * while condition):
		 *
		 * - logcache had an old entry or checksum error
		 * - while reading unlocked, our off(set) comparisons might be
		 *   outdated.
		 */
		assert(r.error != NULL ||
		    (logblk != NULL && logblk->next_off == 0));
		if (refreshed != off) {
			refreshed = off;
			logbuffer_wait_flush_fini(from);
			fellow_logcache_return_current(flc);
			continue;
		}

		AZ(locked);
		fellow_logcache_return_current(flc);
		break;
	}

	AN(off);

	if (locked == 0) {
		// flush destination to get disk blocks #28
		logbuffer_flush(ffd, to, 0, LBUF_ALL);
		/* avoid chasing the tail forever
		 * XXX good threshold?
		 */
		if (restarts++ < 3) {
			logbuffer_wait_flush_fini(from);
			if (tail != from->tail_off) {
				FDBG(D_LOGS_ITER_BLOCK,
				    "tail changed %zu->%zu restart %u",
				    tail, from->tail_off, restarts);
				goto dsk;
			}
		}

		AZ(pthread_mutex_lock(mtx));
		locked = 1;
		FDBG(D_LOGS_ITER_BLOCK, "locked=%u", locked);
		logbuffer_wait_flush_fini(from);
		goto dsk;
	}

	fellow_logcache_fini(flc);
  mem:
	AN(locked);

	/* where variable is a debugging aid */
	if (off == from->logreg->region->off &&
	    from->head.off == 0)
		where = "logregion start, head unassigned";
	else if (off == from->head.off)
		where = "head";
	else if (from->n && (off == from->arr[0].off))
		where = "arr[0]";
	else if (off == from->active.off)
		where = "active";
	else
		WRONG("append dsk stop");
	(void) where;
	FDBG(D_LOGS_ITER_BLOCK, "entered mem %s", where);

	/* disk reading might continue after the head */
	if (off == from->head.off) {
		AN(from->head.block);
		head_seen = off;

		logblk = from->head.block;

		CHECK_DSK_BLK;
		APPEND_BLK("head");

		/* a) case from above: blocks after the head.
		 *
		 * a previous flush might have happend completely async
		 * such that we do not know the tail. Trigger a flush
		 * to get one
		 */
		if (off != 0) {
			assert(off != from->active.off);
			logbuffer_flush(ffd, from, 0, LBUF_ALL);
			locked = 0;
			AZ(pthread_mutex_unlock(mtx));
			logbuffer_wait_flush_fini(from);
			assert(from->tail_off);
			FDBG(D_LOGS_ITER_BLOCK, "@head %zu goto dsk", off);
			goto dsk_init;
		}
		//restarts = 0;
	}

	AN(locked);
	/* if the head block does not have the right offset, we have already
	 * processed it above
	 */
	if (from->head.block == NULL) {
		FDBG(D_LOGS_ITER_BLOCK, "head %p", from->head.block);
		assert(from->n == 0);
	}
	else if (from->head.off == 0 || from->head.off == off) {
		AN(from->active.block);
		FDBG(D_LOGS_ITER_BLOCK, "append head %p", from->head.block);
		assert(head_seen != off);
		off = logbuffer_alloc_log_block_submit(ffd,
		    rets, to, &from->head, &prev, off, &tot, from->id, &seq);
		if (prev && ! region_contains(from->logreg->region, prev))
			regionlist_stk_add(tofree, prev, sizeof *logblk);
	} else {
		struct buddy_ptr_extent e;

		FDBG(D_LOGS_ITER_BLOCK, "previous head %zu", head_seen);
		assert(head_seen == from->head.off);
		AN(head_seen);
		logblk = from->head.block;
		AN(logblk);
		memset(&from->head, 0, sizeof from->head);
		e = BUDDY_PTR_EXTENT(logblk, sizeof *logblk);
		AN(buddy_return_ptr_extent(rets, &e));
	}

	for (u = 0; u < from->n; u++) {
		FDBG(D_LOGS_ITER_BLOCK, "append arr[%u] %p", u,
		    from->arr[u].block);
		off = logbuffer_alloc_log_block_submit(ffd,
		    rets, to, &from->arr[u], &prev, off, &tot, from->id, &seq);
		if (prev && ! region_contains(from->logreg->region, prev))
			regionlist_stk_add(tofree, prev, sizeof *logblk);
	}
	from->n = 0;

	FDBG(D_LOGS_ITER_BLOCK, "append active %p", from->active.block);
	off = logbuffer_alloc_log_block_submit(ffd,
	    rets, to, &from->active, &prev, off, &tot, from->id, &seq);
	AZ(off);
	if (prev && ! region_contains(from->logreg->region, prev))
		regionlist_stk_add(tofree, prev, sizeof *logblk);

	FDBG(D_LOGS_ITER_BLOCK, "tot %u from->tot %u", tot, from->tot);
	assert(tot == from->tot);
	buddy_return(rets);
	regionlist_stk_flush(tofree);

	/*
	 * our freelist argument is for log blocks, which can be freed once the
	 * append is complete.
	 *
	 * yet the the logbuffer's regionlist can only be freed with its log
	 * records are flush-referenced
	 */
	if (from->regions_to_free == NULL)
		(void) 0;
	else if (to->regions_to_free == NULL)
		TAKE(to->regions_to_free, from->regions_to_free);
	else
		regionlist_append(to->regions_to_free, &from->regions_to_free);
	AZ(from->regions_to_free);
}

// safe max obj_chg a logbuffer can take so rewrite does not run out of memory

static size_t
logbuffer_max_obj_chg(const struct fellow_logbuffer *lbuf)
{
	size_t sz;

	sz = buddy_size(lbuf->membuddy) / 8;
	return (sz / sizeof(struct fellow_dlechg));
}

/*
 * return a new disk region for the log if it needs
 * resizing
 */

#define OBJDLE_FACTOR (						\
	    2 * /* + 1 DLE_REG_ADD per obj */				\
	    2   /* one DEL per ADD */					\
	    )

static size_t
fellow_logregion_size(const struct fellow_fd *ffd, size_t hint)
{
	size_t nsz;

	if (hint == 0)
		hint = 1;
	hint = (size_t)1 << log2down(hint);

	/* calc blocks */
	nsz = 1 + ffd->size / hint;	/* DLE_OBJ_ADD */
	nsz *= OBJDLE_FACTOR;
	nsz /= FELLOW_DISK_LOG_BLOCK_ENTRIES;
	if (nsz == 0)
		nsz = 16;

#ifdef DEBUG
	if (nsz > 5)
		nsz = 5;
	else if (nsz == 1)
		nsz = 2;
#endif
	/* -> size */
	nsz *= MIN_FELLOW_BLOCK;

	return (nsz);
}

static void
fellow_logregion_init_resize(struct fellow_fd *ffd, size_t hint)
{
	struct fellow_disk_log_info *log_info = &ffd->log_info;
	struct buddy_off_extent *region;
	const char *use;
	unsigned u;

	assert(ffd->phase == FP_INIT);

	if (log_info->magic == 0)
		INIT_OBJ(log_info, FELLOW_DISK_LOG_INFO_MAGIC);
	else
		CHECK_OBJ(log_info, FELLOW_DISK_LOG_INFO_MAGIC);

	assert(log_info->region < LOGREGIONS);

	for (u = 0; u < LOGREGIONS; u++) {
		region = &log_info->log_region[u];
		if (region->off <= 0) {
			AZ(region->size);
			*region = buddy_alloc1_off_extent(ffd->dskbuddy,
			    hint, 0);
		}
		/*
		 * only called for first initialization, so the allocations
		 * need to succeed
		 */
		AN(region->off);
		AN(region->size);

		if (u == active_region(log_info->region))
			use = "active";
		else if (u == empty_region(log_info->region))
			use = "empty";
		else if (u == pend_region(log_info->region))
			use = "pend";
		else
			WRONG("region number");

		ffd->diag("fellow: region[%u = %s] %zu/%zu\n",
		    u, use, region->off, region->size);
	}
}

/*
 * given a list of entries, and a maximum destination size, determine how many
 * entries we can fit due to our rules:
 *
 * - DLE_REGs always need to be followed by the DLE_OBJ
 * - all DLE_BAN_IMM need to be contiguous, the last one has to have cont== 0
 * - DLE_T_BAN_REG is independent and can appear any time
 */
static inline int
fellow_log_block_valid_last(const struct fellow_dle *entry)
{
	uint8_t type = DLE_TYPE(entry->type);

	return (type == DLE_T_OBJ ||
		type == DLE_T_BAN_REG ||
		(type == DLE_T_BAN_IMM &&
		 entry->u.ban_imm.cont <= 0));
}

static unsigned
fellow_log_canfit(const struct fellow_dle entry[], unsigned n)
{

	AN(n);

	/*lint -e{662} possible out of bounds this is unfortunate, but see no
	 * way around
	 */
	entry += (n - 1);
	while (n > 0) {
		if (fellow_log_block_valid_last(entry))
			return (n);
		entry--;
		n--;
	}
	AZ(n);
	return (n);
}

// XXX MTX?
// entry owned by caller

/*
 * interface:
 *
 * - add known written
 * - add unwritten -> write from log thread when add to buffer
 *
 * - del: free after new log is ref'd
 */

// API
void
fellow_dle_init(struct fellow_dle entry[], unsigned n)
{
	AN(n);
	while (n-- > 0) {
		INIT_OBJ(entry, FELLOW_DLE_MAGIC);
		entry->version = 1;
		entry++;
	}
}

/*
 * fill a log entry array of size n with DLE_REG_* from a region list
 *
 * the log entry array must be init'ed
 *
 * return number of used log entries
 */

// API
unsigned
fellow_dle_reg_fill(
    struct fellow_dle *entry, unsigned nentry,
    const struct buddy_off_extent *region, unsigned nregion,
    enum dle_type dlet, const uint8_t hashpfx[DLE_REG_HASHPFXSZ]
    )
{
	unsigned used = 0, u;

	AN(entry);
	AN(nentry);
	AN(region);
	AN(nregion);
	assert(dlet == DLE_REG_ADD || dlet == DLE_REG_DEL_ALLOCED
	    || dlet == DLE_REG_DEL_FREE);

	for (u = 0; u < nregion; u++) {
		AN(region[u].off);
		AN(region[u].size);
	}

	while (nentry-- > 0 && nregion > 0) {
		CHECK_OBJ_NOTNULL(entry, FELLOW_DLE_MAGIC);
		assert(entry->version == 1);
		entry->type = DLEDSK(dlet);
		memcpy(entry->u.reg.hashpfx, hashpfx,
		    (size_t)DLE_REG_HASHPFXSZ);

		u = nregion;
		if (u > 4)
			u = 4;
		memcpy(entry->u.reg.region, region, u * sizeof *region);
		nregion -= u;
		region += u;
		used++;
		entry++;
	}
	// has to fit
	AZ(nregion);
	return (used);
}

struct buddy_ptr_extent
fellow_log_read_ban_reg(const struct fellow_fd *ffd,
    const struct fellow_dle *e)
{
	struct buddy_ptr_extent mem;
	off_t off;
	size_t sz, spc, len = 0;
	int32_t l;
	uint8_t *bans;

	assert(DLE_TYPE(e->type) == DLE_T_BAN_REG);

	sz = e->u.ban_reg.len;
	AN(sz);
	if (sz < MIN_FELLOW_BLOCK)
		sz = MIN_FELLOW_BLOCK;

	mem = buddy_alloc1_ptr_extent(ffd->membuddy, sz, 0);
	/* during FP_INIT, we are out of luck for a failing allocation */
	XXXAN(mem.ptr);
	bans = mem.ptr;
	spc = mem.size;
	DLE_BAN_REG_FOREACH(e, off, sz) {
		AN(spc);
		if (sz > spc)
			sz = spc;
		l = fellow_io_pread_sync_align(ffd, bans + len, sz, off);
		// XXX handle io error
		if (l < 0)
			INCOMPL();
		assert((uint32_t)l == sz);
		len += sz;
		spc -= sz;
	}
	assert(len >= e->u.ban_reg.len);
	return (mem);
}

//lint -e{506}
static void __attribute__((constructor))
assert_fellow_log_dle_ban_imm(void)
{
	struct fellow_dle e[1];

	// cont member is always the first byte
	assert(offsetof(struct fellow_dle_ban_im0, cont) == 0);
	assert(offsetof(struct fellow_dle_ban_imm, cont) == 0);
	assert(&e->u.ban_imm.cont == &e->u.ban_im0.cont);
	// uint16_t sufficient
	assert(FELLOW_DLE_BAN_IMM_MAX <
	    ((size_t)1 << (8 * sizeof e->u.ban_im0.len)));
	(void) e;
}

static void
fellow_log_dle_ban_imm(struct fellow_fd *ffd, enum dle_type type,
    const uint8_t *bans, uint16_t len, vtim_real t)
{
	static const unsigned nentries = FELLOW_DISK_LOG_BLOCK_ENTRIES;
	struct fellow_dle dle[nentries], *e;
	int8_t n, cont;
	uint8_t *ban;
	uint16_t l;
	size_t sz;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	assert(ffd->phase == FP_OPEN);
	assert(DLE_TYPE(type) == DLE_T_BAN_IMM);

	assert(len > 0);
	assert(len <= FELLOW_DLE_BAN_IMM_MAX);
	fellow_dle_init(dle, nentries);

	cont = 1;
	if (len > DLE_BAN_IM0_LEN) {
		sz = ((len - DLE_BAN_IM0_LEN) + (DLE_BAN_IMM_LEN - 1))
		    / DLE_BAN_IMM_LEN;
		assert(sz < INT8_MAX);
		cont += (int8_t)sz;
	}
	n = cont;

	assert(cont > 0);
	assert(cont <= FELLOW_DISK_LOG_BLOCK_ENTRIES);

	e = dle;
	l = DLE_BAN_IM0_LEN;
	e->u.ban_im0.len = len;
	e->u.ban_im0.ban_time = t;
	ban = e->u.ban_im0.ban;

	do {
		if (len < l)
			l = len;

		if (--cont == 0 && n > 1)
			e->u.ban_imm.cont = 1 - n;
		else
			e->u.ban_imm.cont = cont;

		memcpy(ban, bans, (size_t)l);
		e->type = DLEDSK(type);
		bans += l;
		len -= l;

		e++;
		l = DLE_BAN_IMM_LEN;
		ban = e->u.ban_imm.ban;
	} while (cont > 0);

	AZ(len);
	AZ(cont);
	assert(n > 0);
	assert((uint8_t)n <= nentries);
	fellow_log_dle_submit(ffd, dle, (uint8_t)n);
}

static struct buddy_off_extent *
fellow_log_ban_alloc(buddy_t *buddy,
    struct buddy_off_extent space[DLE_BAN_REG_NREGION], unsigned l,
    unsigned wait)
{
	struct buddy_off_extent e;
	unsigned u;

	memset(space, 0,
	    sizeof(struct buddy_off_extent) * DLE_BAN_REG_NREGION);

	for (u = 0; l > 0 && u < DLE_BAN_REG_NREGION - 1; u++) {
		e = buddy_alloc1_off_extent(buddy, (size_t)l, INT8_MAX);
		if (e.off == BUDDY_OFF_NIL)
			break;
		if (e.size >= l)
			l = 0;
		else
			l -= (unsigned)e.size;

		space[u] = e;
	}
	if (l == 0)
		return (space);


	if (wait)
		e = buddy_alloc1_off_extent_wait(buddy, 0, (size_t)l, 0);
	else
		e = buddy_alloc1_off_extent(buddy, (size_t)l, 0);

	if (e.off == BUDDY_OFF_NIL) {
		AZ(wait);
		/* rare, using inefficient code path */
		while (u > 0)
			buddy_return1_off_extent(buddy, &space[--u]);

		return (NULL);
	}
	assert(e.size >= l);
	assert(u < DLE_BAN_REG_NREGION);
	space[u] = e;

	return (space);
}

/* return 1 if successful */

int
fellow_log_ban(struct fellow_fd *ffd, uint8_t op,
    const uint8_t *bans, unsigned len, vtim_real t,
    struct buddy_off_extent space[DLE_BAN_REG_NREGION])
{
	struct buddy_off_extent alloc[DLE_BAN_REG_NREGION];
	struct fellow_dle dle[1];
	struct buddy_off_extent e;
	unsigned u, l = len;
	int32_t r;
	size_t sz;

	AN(ffd);
	assert(op == DLE_OP_ADD || op == DLE_OP_CHG);
	AN(bans);
	AN(len);

	//lint -e{506} constant value boolean
	assert(sizeof(struct buddy_off_extent) * DLE_BAN_REG_NREGION ==
	    sizeof dle->u.ban_reg.region);

	if (len <= FELLOW_DLE_BAN_IMM_MAX) {
		fellow_log_dle_ban_imm(ffd, (enum dle_type)(DLE_T_BAN_IMM | op),
		    bans, (uint16_t)len, t);
		return (1);
	}

	assert(ffd->phase == FP_OPEN);
	fellow_dle_init(dle, 1);
	dle->type = DLEDSK(DLE_T_BAN_REG) | op;
	dle->u.ban_reg.len = len;
	dle->u.ban_reg.ban_time = t;

	/* space is provided for ban exports except during init */

	if (space == NULL) {
		space = fellow_log_ban_alloc(ffd->dskbuddy,
		    alloc, len, op == DLE_OP_ADD ? 1 : 0);
	} else {
		assert(op == DLE_OP_CHG);

		for (u = 0, sz = 0; u < DLE_BAN_REG_NREGION; u++)
			sz += space[u].size;

		/* pre-alloc'ed space for banexports can be too small */
		if (sz < len)
			return (0);
	}

	/* allocations must not fail for new bans, we are waiting in
	 * ban_alloc()
	 */
	if (space == NULL) {
		assert(op == DLE_OP_CHG);
		return (0);
	}

	for (u = 0, sz = 0; u < DLE_BAN_REG_NREGION; u++)
		sz += space[u].size;
	assert (sz >= len);

	//lint -e{440} irregular: len tested, u modified
	for (u = 0; len > 0; u++) {
		assert(u < DLE_BAN_REG_NREGION);
		e = space[u];
		space[u] = buddy_off_extent_nil;

		if (e.size > len)
			buddy_trim1_off_extent(ffd->dskbuddy, &e, (size_t)len);

		if (e.size < len)
			l = (typeof(l))e.size;
		else
			l = len;

		dle->u.ban_reg.region[u] = e;

		r = fellow_io_pwrite_sync_align(ffd, bans, (size_t)l, e.off);
		// XXX error handling
		if (r < 0)
			INCOMPL();
		assert((uint32_t)r == l);
		bans += l;
		len -= l;
	}
	for (; u < DLE_BAN_REG_NREGION; u++) {
		if (space[u].size == 0)
			continue;
		buddy_return1_off_extent(ffd->dskbuddy, &space[u]);
	}

	fellow_log_dle_submit(ffd, dle, 1);
	return (1);
}

/*
 * we turn DEL_ALLOC DLEs and free them via regions_to_free when the log is
 * referenced
 *
 * must be called with sufficient space in prep->tofree->arr
 */

static void
fellow_log_entries_mutate(struct fellow_log_prep *prep,
    struct fellow_dle *e, unsigned n)
{
	off_t off;
	size_t sz;

	CHECK_OBJ_NOTNULL(prep, FELLOW_LOG_PREP_MAGIC);
	AZ(prep->tofree.regionlist);

#ifdef DEBUG
	/* test with and without mutate */
	return;
#endif

	while (n--) {
		switch (e->type) {
		case DLE_OBJ_DEL_ALLOCED:
			off = fdb_off(e->u.obj.start);
			sz = fdb_size(e->u.obj.start);
#ifdef BUDDY_WITNESS
			buddy_assert_alloced_off_extent(hack_dskbuddy,
			    &BUDDY_OFF_EXTENT(off, sz));
#endif
			AZ((size_t)off & FELLOW_BLOCK_ALIGN);
			regionlist_onlystk_add(prep->tofree, off, sz);
			e->type = DLEDSK(DLE_OBJ_DEL_FREE);
			break;
		case DLE_REG_DEL_ALLOCED:
			DLE_REG_FOREACH(e, off, sz) {
				AZ((size_t)off & FELLOW_BLOCK_ALIGN);
#ifdef BUDDY_WITNESS
				buddy_assert_alloced_off_extent(hack_dskbuddy,
				    &BUDDY_OFF_EXTENT(off, sz));
#endif
				regionlist_onlystk_add(prep->tofree, off, sz);
			}
			e->type = DLEDSK(DLE_REG_DEL_FREE);
			break;
		default:
			break;
		}
		e++;
	}
}

/*
 * prep log entries for addition, can be called unlocked
 *
 * space needs to be provided by caller
 *
 * returns number of entries prep'd
 */
static unsigned
fellow_log_entries_prep(struct fellow_log_prep *prep,
    struct fellow_dle *entry, unsigned n)
{
	uint8_t hashpfx[DLE_REG_HASHPFXSZ], dle_op = 0;
	const struct fellow_dle *e = NULL;
	unsigned reg, nn;
	int8_t cont, c;

	AN(prep);
	AN(entry);
	AN(n);
	if (n > fellow_log_prep_max_dles)
		n = fellow_log_canfit(entry, fellow_log_prep_max_dles);
	assert(n <= fellow_log_prep_max_dles);

	memset(hashpfx, 0, sizeof hashpfx);
	INIT_OBJ(prep, FELLOW_LOG_PREP_MAGIC);
	prep->tofree.space = fellow_log_prep_max_regions;

	fellow_log_entries_mutate(prep, entry, n);

	cont = c = 0;
	for (reg = 0, nn = 0; nn < n; nn++) {
		e = &entry[nn];
		CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);
		assert(e->version == 1);

		if (cont)
			assert(DLE_TYPE(e->type) == DLE_T_BAN_IMM);

		if (DLE_TYPE(e->type))
			assert(! DLECHG_IS_OBJ_DEL_THIN(&e->u.obj));

		// check REG/OBJ hash consistency
		switch (e->type) {
		case DLE_OBJ_DEL_ALLOCED:
#ifndef DEBUG
			WRONG("DLE_OBJ_DEL_ALLOCED should have been mutated");
#endif
		case DLE_OBJ_ADD:
		case DLE_OBJ_DEL_FREE:
			if (reg) {
				assert(DLE_OP(e->type) == dle_op);
				AZ(memcmp(hashpfx, e->u.obj.hash,
					sizeof hashpfx));
			}
			reg = 0;
			break;
		case DLE_OBJ_CHG:
		case DLE_OBJ_DEL_THIN:
			break;
		case DLE_REG_DEL_ALLOCED:
#ifndef DEBUG
			WRONG("DLE_REG_DEL_ALLOCED should have been mutated");
#endif
		case DLE_REG_ADD:
		case DLE_REG_DEL_FREE:
			// for subsequent DLE_REGs, the hashpfx must not change
			if (reg) {
				assert(DLE_OP(e->type) == dle_op);
				AZ(memcmp(hashpfx, e->u.reg.hashpfx,
					sizeof hashpfx));
			} else {
				dle_op = DLE_OP(e->type);
				memcpy(hashpfx, e->u.reg.hashpfx,
					sizeof hashpfx);
			}
			reg = 1;
			break;
		case DLE_BAN_ADD_REG:
		case DLE_BAN_EXP_REG:
			break;
		case DLE_BAN_ADD_IMM:
		case DLE_BAN_EXP_IMM:
			if (c--) {
				if (c == 0) {
					assert(e->u.ban_imm.cont == 0 - cont);
					cont = 0;
				}
				else {
					assert(e->u.ban_imm.cont == c);
				}
			}
			else {
				cont = c = e->u.ban_imm.cont;
			}
			break;
		default:
			WRONG("logblk entry type (fellow_log_entries_prep)");
		}

		dle_stats_inc(&prep->dle_stats, (enum dle_type)e->type);
	}

	AZ(c);
	assert(nn == n);
	AN(e);
	assert(fellow_log_block_valid_last(e));
	prep->entry = entry;
	prep->n = n;
	return (n);
}

/* for concurrent logbuffer, needs to be called under lock */
static void
fellow_log_entries_add(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, struct fellow_log_prep *prep)
{
	struct fellow_disk_log_block *logblk;
	const struct fellow_dle *entry;
	unsigned n, nn, u;
	uint8_t seq;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	CHECK_OBJ_NOTNULL(lbuf, FELLOW_LOGBUFFER_MAGIC);

	CHECK_OBJ_NOTNULL(prep, FELLOW_LOG_PREP_MAGIC);
	dle_stats_add(&lbuf->dle_stats, &prep->dle_stats);

	n = prep->n;
	entry = prep->entry;

	AN(n);
	AN(entry);

	if (prep->tofree.n) {
		logbuffer_need_regions_to_free(lbuf);
		prep->tofree.regionlist = lbuf->regions_to_free;
		regionlist_stk_flush(prep->tofree);
	}
	AZ(prep->tofree.n);

	while (n) {
		logblk = logbuffer_getblk(ffd, lbuf, GET_USED);

		assert(logblk->nentries < FELLOW_DISK_LOG_BLOCK_ENTRIES);
		nn = FELLOW_DISK_LOG_BLOCK_ENTRIES - logblk->nentries;

		AN(nn);

		if (n < nn)
			nn = n;

		nn = fellow_log_canfit(entry, nn);
		if (nn == 0) {
			logblk = logbuffer_getblk(ffd, lbuf, GET_NEW);
			AZ(logblk->nentries);
			nn = FELLOW_DISK_LOG_BLOCK_ENTRIES;
			if (n < nn)
				nn = n;
			nn = fellow_log_canfit(entry, nn);
			AN(nn);
		}

		memcpy(&logblk->entry[logblk->nentries], entry,
		    nn * sizeof *entry);

		seq = lbuf->seq;
		//lint -e{679} suspicios truncation with pointer
		for (u = 0; u < nn; u++)
			logblk->entry[logblk->nentries + u].seq = seq_inc(seq);
		lbuf->seq = seq;

		entry += nn;
		u = logblk->nentries + nn;
		assert(u <= FELLOW_DISK_LOG_BLOCK_ENTRIES);
		logblk->nentries = (typeof(logblk->nentries))u;
		n -= nn;
	}
}

/* convenience wrapper for prep + add - only to be used for owned
 * logbuffer
 */
static void
fellow_privatelog_submit(struct fellow_fd *ffd,
    struct fellow_logbuffer *lbuf, struct fellow_dle *entry, unsigned n)
{
	struct fellow_log_prep prep[1];
	unsigned nn;

	/*
	 * private logs do only really exist during FP_OPEN.  During FP_INIT,
	 * the caller usually needs to hold the logmtx
	 */

	while (n > 0) {
		nn = fellow_log_entries_prep(prep, entry, n);
		assert(nn <= n);
		n -= nn;
		entry += nn;

		switch (ffd->phase) {
		case FP_INIT:
			assert(lbuf == ffd->logbuf);
			/* FALLTHROUGH */
		case FP_OPEN:
			fellow_log_entries_add(ffd, lbuf, prep);
			AZ(prep->tofree.n);
			break;
		default:
			WRONG("privatelog_submit can only be called during "
			    "FP_INIT and FP_OPEN");
		}
	}
}


#define logwrong(x) return("logblk wrong " x)

static const char *
fellow_logblk_check(const struct fellow_disk_log_block *logblk, uint8_t *id)
{

	AN(id);

	if (logblk->magic != FELLOW_DISK_LOG_BLOCK_MAGIC)
		logwrong("magic");
	if (logblk->version != 1)
		logwrong("version");
	if (logblk->fht >= FH_LIM)
		logwrong("hash type (>= FH_LIM)");
	if (! fh_name[logblk->fht])
		logwrong("hash type (support missing)");
	if (*id == 0)
		*id = logblk->id;
	if (logblk->id != *id)
		logwrong("id");
	if (fhcmp(logblk->fht, logblk->fh,
	      (char *)logblk + FELLOW_DISK_LOG_BLOCK_CHK_START,
	      FELLOW_DISK_LOG_BLOCK_CHK_LEN))
		logwrong("chksum");
	return (NULL);
}

/* remember the export time so we can skip earlier bans */
static inline void
fellow_log_ban_exp_time(struct flivs *flivs, vtim_real t)
{
	AN(t);
	AZ(flivs->ban_export_time);
	flivs->ban_export_time = t;
}

/*
 * keep a ban export or add:
 *
 * - export: if ban_dles is full, submit to the existing to have the banexport
 *   last (first when read)

 * - add: if ban_dles is full, keep it because it can contain a ban export,
 *   which we want to see early when reading.
 *
 * we use a local iter_out to save duplication of the locking logic
 */
static void
fellow_log_ban_save(const struct flics *flics, struct flivs *flivs,
    const struct fellow_dle *e, uint16_t n)
{
	struct iter_out ban_add[1];

	if (iter_out_save(flivs->ban_dles, e, n))
		return;

	if (DLE_OP(e->type) == DLE_OP_CHG) {
		// EXP
		iter_out_submit(flivs->ban_dles, flivs->lbuf);
		AN(iter_out_save(flivs->ban_dles, e, n));
		return;
	}

	assert(DLE_OP(e->type) == DLE_OP_ADD);

	iter_out_init(ban_add, flics);
	AN(iter_out_save(ban_add, e, n));
	iter_out_submit(ban_add, flivs->lbuf);
}

/*
 * decision function whether to keep a ban DLE
 *
 * also manages the exp time
 */

// we use bit 0 to mark a seen ban export
#define BAN_EXP_BIT (size_t)0

static unsigned
fellow_log_ban_keep(struct flivs *flivs, const struct fellow_dle *e,
    vtim_real t)
{
	unsigned u;

	if (DLE_OP(e->type) == DLE_OP_ADD)
		return (t > flivs->ban_export_time);

	// EXP
	assert(DLE_OP(e->type) == DLE_OP_CHG);
	u = bitfs_set(flivs->bitfs, BAN_EXP_BIT);
	FDBG(D_LOGS_ITER_BLOCK, "  u %u = bitf_set(%zu)", u, BAN_EXP_BIT);
	if (u) {
		fellow_log_ban_exp_time(flivs, t);
		return (1);
	}
	AN(flivs->ban_export_time);
	return (0);
}

#if defined(DEBUG) && FDBGL & D_LOGS_ITER_BLOCK
static inline void
printf_ban_times(const uint8_t *banspec, unsigned len, const char *fmt)
{
	unsigned l;
	vtim_real t;
	uint64_t u;
	/* XXX uses inside knowledge of varnishd */

	AN(banspec);
	assert(len >= 16);	// HEAD_LEN
	while (len) {
		assert(len >= 16);	// HEAD_LEN
		u = vbe64dec(banspec);
		memcpy(&t, &u, sizeof t);
		AN(t);
		fprintf(stderr, fmt, t);

		l = vbe32dec(banspec + 8);
		assert(l <= len);
		len -= l;
		banspec += l;
	}
}
#endif

#define OOB_PRINT 5

/* bit representing a block at offset in the "check if already seen"
 * bitmask
 */
static inline size_t
off_bit(off_t off)
{
	size_t sz;
	assert(off >= 0);
	sz = (typeof(sz))off;
	AZ(sz & FELLOW_BLOCK_ALIGN);
	return (sz >> MIN_FELLOW_BITS);
}

/*
 * for DEL_THIN, we turn the original DLE_REG_ADDs into DLE_REG_DEL_ALLOCs
 *
 * if we used this on an lbuf which we preserve, we would need to update
 * dle_stats accordingly, but we are only called for a rewrite and the new
 * lbuf has fresh stats based on kept DLEs
 */

static void
mutate_del_thin(struct fellow_dle *e, int8_t i)
{
	assert(e->type == DLEDSK(DLE_OBJ_DEL_ALLOCED));
	while (i-- > 0) {
		e--;
		if (DLE_TYPE(e->type) != DLE_T_REG)
			return;
		/* hash consistency etc will be checked in
		 * iter_block loop */
		assert(e->type == DLEDSK(DLE_REG_ADD));

		FDBG(D_LOGS_ITER_BLOCK,
		    "mutate version 0x%x type 0x%x (%s)",
		    e->version, e->type, dle_type[e->type]);
		e->type = DLEDSK(DLE_REG_DEL_ALLOCED);
	}
}

static inline int
fellow_reg_oob(const struct flics *flics, struct flivs *flivs,
    off_t off, size_t sz)
{

	assert(off >= 0);
	if (likely((size_t)off + sz <= flics->ffd->size))
		return (0);
	assert(flics->ffd->phase == FP_INIT);
	if (flivs->oob++ < OOB_PRINT)
		fprintf(stderr, "fellow: region %zd/%zu out of bounds, "
		    "ignoring\n", off, sz);
	return (1);
}

/*
 * macro to go to the nth next DLE in fellow_logs_iter_block(),
 * checking the DLE's sequence
 */
/*lint -emacro(506, FLITER_NEXT) */
#define FLITER_NEXT(e, i, n, logblk, flivs) do {			\
		int8_t ii;						\
									\
		assert(n > 0);						\
		assert(i >= (n));					\
		assert(i <= logblk->nentries);				\
		ii = (n);						\
		while (ii--) {						\
			i--;						\
			e = &logblk->entry[i];				\
			assert(e->seq ==				\
			    seq_dec_start(flivs->seq, e->seq));		\
		}							\
	} while(0)

static void
fellow_logs_iter_block(const struct flics *flics, struct flivs *flivs,
    struct fellow_disk_log_block *logblk)
{
	struct iter_out it[1];
	struct fellow_dle *e;
	regionlist_stk_decl(tofree, FELLOW_DISK_LOG_BLOCK_ENTRIES,
	    flivs->tofree);
	int i;
	int8_t dle_op;
	off_t off;
	size_t bit, sz;
	uint8_t hashpfx[DLE_REG_HASHPFXSZ] = {0};
	unsigned u, isoob, obj_alive = 0;
	vtim_real t;

	CHECK_OBJ_NOTNULL(flics, FLICS_MAGIC);
	CHECK_OBJ_NOTNULL(flivs, FLIVS_MAGIC);

	// no tofree for FP_INIT, but otherwise
	//lint -e(731)
	assert((flics->ffd->phase == FP_INIT) == (flivs->tofree == NULL));

	// lbuf is passed only, not checked here

	// logblk to be checked by caller
	CHECK_OBJ_NOTNULL(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);

	FDBG_LOGBLK(logblk);

	assert(logblk->version == 1);

	assert(flics->resur_f != NULL || flics->ffd->phase != FP_INIT);
	assert(flics->resur_priv == NULL || flics->resur_f != NULL);

	assert(logblk->nentries <= FELLOW_DISK_LOG_BLOCK_ENTRIES);
	i = (typeof(i))logblk->nentries;

	if (i > 0) {
		// a log block can not end in a DLE_REG_*
		e = &logblk->entry[i - 1];
		assert(fellow_log_block_valid_last(e));
	}

	iter_out_init(it, flics);

	dle_op = 0;
  next_entry:
	while (i > 0) {
		FLITER_NEXT(e, i, 1, logblk, flivs);
	  again:
		FDBG(D_LOGS_ITER_BLOCK,
		    "entry version 0x%x seq 0x%02x type 0x%x (%s)",
		    e->version, e->seq, e->type, dle_type[e->type]);

		/* the outgoing transaction is complete whenever
		 * we see not a T_REG
		 */
		if (DLE_TYPE(e->type) != DLE_T_REG)
			iter_out_commit(it);

		switch (DLE_TYPE(e->type)) {
		case DLE_T_BAN_IMM:
			if (e->u.ban_imm.cont != 0) {
				/*
				 * last of continuation is negative of the
				 * number of DLEs in front
				 *
				 * skip to first
				 */
				assert(e->u.ban_imm.cont < 0);
				int cont_abs = abs(e->u.ban_imm.cont);
				assert(cont_abs <= i);
				assert(cont_abs > 0);

				FLITER_NEXT(e, i,
				    (int8_t)cont_abs, logblk, flivs);

				assert(DLE_TYPE(e->type) == DLE_T_BAN_IMM);
				assert(e->u.ban_imm.cont == cont_abs);
			}

			if (flics->ffd->phase == FP_FINI)
				continue;

			assert(flics->ffd->phase == FP_INIT ||
			    flics->ffd->phase == FP_OPEN);

			t = e->u.ban_im0.ban_time;
			u = fellow_log_ban_keep(flivs, e, t);

			FDBG(D_LOGS_ITER_BLOCK,
			    "  ban cont %d len %d time %f keep %u",
			    e->u.ban_im0.cont, e->u.ban_im0.len, t, u);

			if (! u)
				continue;

			// submit non-ban log before we might submit bans
			iter_out_submit(it, flivs->lbuf);

			if (flics->ffd->phase == FP_INIT &&
			    flics->resur_f != NULL)
				(void)flics->resur_f(flics->resur_priv, e);

			assert(e->u.ban_im0.cont >= 0);
			fellow_log_ban_save(flics, flivs, e,
			    (uint8_t)e->u.ban_im0.cont + 1);

			continue;
		case DLE_T_BAN_REG:
			AN(e->u.ban_reg.len);

			t = e->u.ban_reg.ban_time;
			u = fellow_log_ban_keep(flivs, e, t);

			FDBG(D_LOGS_ITER_BLOCK, "  ban time %f keep %u", t, u);
#if defined(DEBUG) && FDBGL & D_LOGS_ITER_BLOCK
			struct buddy_ptr_extent mem;
			mem = fellow_log_read_ban_reg(flics->ffd, e);
			printf_ban_times(mem.ptr, e->u.ban_reg.len,
			    "D_LOGS_ITER_BLOCK   ban time %f\n");
			buddy_return1_ptr_extent(flics->ffd->membuddy, &mem);
#endif

			DLE_BAN_REG_FOREACH(e, off, sz) {
				FDBG(D_LOGS_ITER_BLOCK,
				    "  ban reg %zu sz %zu", off, sz);
				if (fellow_reg_oob(flics, flivs, off, sz)) {
					u = 0;
					continue;
				}
				bit = off_bit(off);
				AN(bitfs_set(flivs->bitfs, bit));
			}

			if (flics->ffd->phase == FP_FINI ||
			    (flics->ffd->phase == FP_OPEN && !u))
				DLE_BAN_REG_FOREACH(e, off, sz)
					regionlist_stk_add(tofree, off, sz);

			if (flics->ffd->phase == FP_FINI || ! u)
				continue;

			AN(u);
			assert(flics->ffd->phase == FP_INIT ||
			       flics->ffd->phase == FP_OPEN);

			// submit non-ban log before we might submit bans
			iter_out_submit(it, flivs->lbuf);

			if (flics->ffd->phase == FP_INIT) {
				struct buddy_off_extent
				    totake[DLE_BAN_REG_NREGION];

				memset(totake, 0, sizeof totake);

				u = 0;
				DLE_BAN_REG_FOREACH(e, off, sz) {
					assert(u < DLE_BAN_REG_NREGION);
					totake[u++] = BUDDY_OFF_EXTENT(off, sz);
				}
				buddy_take_off_extent(flics->ffd->dskbuddy,
				    totake, u);
			}

			if (flics->ffd->phase == FP_INIT &&
			    flics->resur_f != NULL)
				(void)flics->resur_f(flics->resur_priv, e);

			fellow_log_ban_save(flics, flivs, e, 1);

			continue;

		/* we read T_OBJ before T_REG, check that the operation
		 * and checksum match
		 */
		case DLE_T_OBJ:
			memcpy(hashpfx, e->u.obj.hash,
			    (size_t)DLE_REG_HASHPFXSZ);
			dle_op = DLE_OP(e->type);

			off = fdb_off(e->u.obj.start);
			sz = fdb_size(e->u.obj.start);
			bit = off_bit(off);

			FDBG(D_LOGS_ITER_BLOCK,
			    "  obj hash %02x%02x%02x%02x off %zu sz %zu",
			    hashpfx[0], hashpfx[1], hashpfx[2], hashpfx[3],
			    off, sz);
			FDBG(D_LOGS_ITER_BLOCK,
			    "  obj ban %f t_ori %f ttl %f grace %f keep %f",
			    e->u.obj.ban, e->u.obj.t_origin,
			    e->u.obj.ttl, e->u.obj.grace, e->u.obj.keep);
			FDBG(D_LOGS_ITER_BLOCK,
			    "  obj dlechg %p",
			    fellow_dlechg_find(flivs->fdct, e));
			if (fellow_reg_oob(flics, flivs, off, sz)) {
				obj_alive = 0;
				continue;
			}
			break;
		case DLE_T_REG:
			assert(DLE_TYPE(e->type) == DLE_T_REG);
			AZ(memcmp(hashpfx, e->u.reg.hashpfx,
				(size_t)DLE_REG_HASHPFXSZ));
			assert(DLE_OP(e->type) == dle_op);

			FDBG(D_LOGS_ITER_BLOCK, "  reg hash %02x%02x%02x%02x",
			    hashpfx[0], hashpfx[1], hashpfx[2], hashpfx[3]);

			isoob = 0;
			DLE_REG_FOREACH(e, off, sz) {
				FDBG(D_LOGS_ITER_BLOCK, "  reg %zu sz %zu",
				    off, sz);
				if (fellow_reg_oob(flics, flivs, off, sz))
				    isoob = 1;
			}
			if (likely(isoob == 0)) {
				off = 0;
				sz = 0;
				bit = SIZE_MAX;
				break;
			}

			obj_alive = 0;
			// disregard the respective object
			iter_out_clear(it);
			DLE_REG_FOREACH(e, off, sz) {
				bit = off_bit(off);

				FDBG(D_LOGS_ITER_BLOCK,
				    "  reg oob off %zu sz %zu bit %zu",
				    off, sz, bit);

				if ((size_t)off + sz > flics->ffd->size)
					continue;

				FDBG(D_LOGS_ITER_BLOCK,
				    "  (void) bitf_set(%zu)", bit);
				(void) bitfs_set(flivs->bitfs, bit);
			}
			goto next_entry;
		default:
			WRONG("DLE_TYPE fellow_logs_iter_block");
		}

		switch (e->type) {

		/* DLE_BAN_* */

		case DLE_BAN_EXP_IMM:
		case DLE_BAN_ADD_IMM:
		case DLE_BAN_EXP_REG:
		case DLE_BAN_ADD_REG:
			WRONG("DLE_BAN_* handled in first switch/case");
			break;

		/* DLE_OBJ_* */

		case DLE_OBJ_DEL_FREE:
			FDBG(D_LOGS_ITER_BLOCK, "  (void) bitf_set(%zu)", bit);
			(void) bitfs_set(flivs->bitfs, bit);
			// no CHG seen after DEL_FREE
#ifndef DEBUG
			AZ(fellow_dlechg_find(flivs->fdct, e));
#else
			{
				struct fellow_dlechg *c;
				c = fellow_dlechg_find(flivs->fdct, e);
				if (c == NULL)
					break;
				DBGDLECHG(c, "WRONG");
			}
#endif
			break;
		case DLE_OBJ_DEL_ALLOCED:
			AN(bitfs_set(flivs->bitfs, bit));
			// no CHG seen after DEL_FREE
			AZ(fellow_dlechg_find(flivs->fdct, e));
			FDBG(D_LOGS_ITER_BLOCK, "  AN(bitf_set(%zu))", bit);
			switch (flics->ffd->phase) {
			case FP_INIT:
				break;
			case FP_OPEN:
			case FP_FINI:
				regionlist_stk_add(tofree, off, sz);
				break;
			default:
				WRONG("iter block phase");
			}
			break;
		case DLE_OBJ_DEL_THIN:
			// THIN is like DEL_ALLOCED, space still reserved
			FDBG(D_LOGS_ITER_BLOCK, "  AN(bitf_set(%zu))", bit);
			AN(bitfs_set(flivs->bitfs, bit));
			/* no CHG after DEL_THIN
			 * AZ(fellow_dlechg_find(flivs->fdct, e));
			 * ^^^ is implicit in fellow_dlechg_add()
			 */
			if (fellow_dlechg_add(flivs->fdct, e))
				break;
			WRONG("Need to handle ENOMEM for DLECHG with DEL_THIN");
			break;
		case DLE_OBJ_CHG:
			u = bitfs_set(flivs->bitfs, bit);
			FDBG(D_LOGS_ITER_BLOCK,
			    "  u %u = bitf_set(%zu)", u, bit);
			if (! u)
				break;
			if (fellow_dlechg_add(flivs->fdct, e))
				break;
			fprintf(stderr,
			    "fellow: had to drop DLE_OBJ_CHG errno %d\n",
			    errno);
			FDBG(D_LOGS_ITER_BLOCK, "  AN(bitf_clr(%zu))", bit);
			AN(bitfs_clr(flivs->bitfs, bit));
			break;
		case DLE_OBJ_ADD:
			obj_alive = bitfs_set(flivs->bitfs, bit);
			FDBG(D_LOGS_ITER_BLOCK,
			    "  obj alive %u = bitf_set(%zu)",
			    obj_alive, bit);
			if (obj_alive) {
				AZ(fellow_dlechg_find(flivs->fdct, e));
			} else {
				assert(e == &logblk->entry[i]);
				e = fellow_dlechg_get(flivs->fdct,
				    &logblk->entry[i]);
				if (e == NULL)
					break;
				FDBG(D_LOGS_ITER_BLOCK,
				    "  chg version 0x%x type 0x%x (%s)",
				    e->version, e->type, dle_type[e->type]);
				FDBG(D_LOGS_ITER_BLOCK,
				    "  chg ban %f t_ori %f ttl %f grace %f",
				    e->u.obj.ban, e->u.obj.t_origin,
				    e->u.obj.ttl, e->u.obj.grace);
				/* implicit: start fdb is the key */
				assert(fdb_off(e->u.obj.start) == off);
				assert(fdb_size(e->u.obj.start) == sz);
				assert(bit == off_bit(off));
				switch (e->type) {
				case DLE_OBJ_ADD:
					obj_alive = 1;
					break;
				case DLE_OBJ_DEL_ALLOCED:
					mutate_del_thin(&logblk->entry[i],
					    (int8_t)i);
					FDBG(D_LOGS_ITER_BLOCK,
					    "  AN(bitf_clr(%zu))", bit);
					AN(bitfs_clr(flivs->bitfs, bit));
					goto again;
				default:
					WRONG("dle type for dlechg");
				}
			}
			switch (flics->ffd->phase) {
			case FP_INIT:
			case FP_OPEN:
				iter_out_start(it, e);
				break;
			case FP_FINI:
				regionlist_stk_add(tofree, off, sz);
				break;
			default:
				WRONG("iter block phase");
			}
			break;

		/* DLE_REG_* */

		case DLE_REG_DEL_FREE:
			DLE_REG_FOREACH(e, off, sz) {
				bit = off_bit(off);
				FDBG(D_LOGS_ITER_BLOCK,
				    "  (void) bitf_set(%zu)", bit);
				(void) bitfs_set(flivs->bitfs, bit);
			}
			break;
		case DLE_REG_DEL_ALLOCED:
			DLE_REG_FOREACH(e, off, sz) {
				bit = off_bit(off);
				// must be the first time we see this offset
				u = bitfs_set(flivs->bitfs, bit);
				FDBG(D_LOGS_ITER_BLOCK,
				    "  u %u = bitf_set(%zu)", u, bit);
				assert(flivs->oob || u);

				switch (flics->ffd->phase) {
				case FP_INIT:
					break;
				case FP_OPEN:
				case FP_FINI:
					regionlist_stk_add(tofree, off, sz);
					break;
				default:
					WRONG("iter block phase");
				}
			}
			break;
		case DLE_REG_ADD:
			DLE_REG_FOREACH(e, off, sz) {
				bit = off_bit(off);
				/* mark any regions not already marked because
				 * they can not be valid for an earlier object.
				 *
				 * We can only assert that they were free if the
				 * object is alive, it is not, they might have
				 * been reused or not.
				 */

				u = bitfs_set(flivs->bitfs, bit);
				FDBG(D_LOGS_ITER_BLOCK,
				    "  u %u = bitf_set(%zu)", u, bit);

				// semantically a break, but sanity
				// check the other regions too
				if (! obj_alive)
					continue;

				AN(u);

				switch (flics->ffd->phase) {
				case FP_INIT:
				case FP_OPEN:
					// add entry outsite REG_FOREACH
					break;
				case FP_FINI:
					regionlist_stk_add(tofree, off, sz);
					break;
				default:
					WRONG("iter block phase");
				}
			}
			if (obj_alive && flics->ffd->phase != FP_FINI)
				iter_out_add_reg(it, e);
			break;
		default:
			WRONG("logblk entry type (fellow_logs_iter_block)");
		}
	}

	if (flivs->tofree != NULL) {
		regionlist_stk_flush(tofree);
	}

	iter_out_commit(it);
	FDBG(D_LOGS_ITER_BLOCK, "%u entries to log", it->nused);
	iter_out_submit(it, flivs->lbuf);
}

static int
fellow_logs_iter(const struct flics *flics, struct flivs *flivs,
    const struct buddy_off_extent *active_logregion,
    const struct buddy_off_extent *empty_logregion,
    off_t off)
{
	struct fellow_disk_log_block *logblk;
	struct fellow_logcache flc[1];
	struct fellow_logcache_r r;
	int ret, direction;
	off_t alt_off = 0;
	unsigned pre;
	regionlist_stk_decl(tofree, 256, flivs->tofree);

	CHECK_OBJ_NOTNULL(flics, FLICS_MAGIC);
	CHECK_OBJ_NOTNULL(flivs, FLIVS_MAGIC);

	fellow_logcache_init(flc, flics->ffd, flivs->mempool,
	    0, active_logregion);

	assert(flics->ffd->phase > FP_INVAL);
	assert(flics->ffd->phase < FP_LIM);

	// no tofree for FP_INIT, but otherwise
	//lint -e(731)
	assert((flics->ffd->phase == FP_INIT) == (flivs->tofree == NULL));

	direction = 0;

	pre = 4;
	/* pre-populate the backwards cache */
	r = fellow_logcache_get(flc, off, -1, pre);

	while (off > 0) {
		if ((size_t)off + sizeof *logblk > flics->ffd->size) {
			fprintf(stderr,
			    "fellow: logblk %zu out of bounds, "
			    "ending resurrection\n", off);
			assert(flics->ffd->phase == FP_INIT);
			ret = 1;
			goto out;
		}

		assert (! region_contains(empty_logregion, off));

		FDBG(D_LOGS_ITER,
		    "off %zu (%s), dir %d, phase %d", off,
		    region_contains(active_logregion, off) ? "reg" : "extra",

		    direction, flics->ffd->phase);

		r = fellow_logcache_get(flc, off, direction, pre);

		/* in FP_INIT, while we go forward, errors are non
		 * fatal and we just use what we got
		 */
		if (r.error && flics->ffd->phase == FP_INIT &&
		    direction >= 0 && alt_off > 0) {
			fprintf(stderr, "LOGBLK: %s errno %d -- "
			    "resurrecting from previous %zu\n",
			    r.error, r.u.err, alt_off);
			direction = -1;
			off = alt_off;
			continue;
		}
		else if (r.error) {
			fprintf(stderr, "LOGBLK: %s errno %d\n",
			    r.error, r.u.err);
			ret = 1;
			INCOMPL();
			//XXX goto out;
		}

		logblk = r.u.logblk;
		CHECK_OBJ_NOTNULL(logblk, FELLOW_DISK_LOG_BLOCK_MAGIC);

		if (direction >= 0 && logblk->next_off > 0) {
			direction = 1;
			alt_off = off;
			off = logblk->next_off;
			continue;
		}

		alt_off = 0;

		/* going forward, when we see the next block is 0,
		 * it is the end if the log.
		 *
		 * once going backwards past the last block, the next_off must
		 * always be set, or we are dealing with an old active block
		 * (head on second flush)
		 */
		if (direction >= 0 && logblk->next_off == 0) {
			direction = -1;
			fellow_logcache_prune_direction(flc, 1);
		}
		else
			AN(logblk->next_off);

		assert (direction == -1);

		/* once we go backwards, we can fully engage readahead because
		 * we have a limit (the beginning of the logregion)
		 */
		pre = UINT_MAX;

		if (! region_contains(active_logregion, off) &&
		    flivs->tofree != NULL) {
			regionlist_stk_add(tofree, off, sizeof *logblk);
		}

		fellow_logs_iter_block(flics, flivs, logblk);

		off = logblk->prev_off;
	}

	if (flivs->oob > OOB_PRINT) {
		flics->ffd->diag("...\n");
		flics->ffd->diag("%u oob errors total\n", flivs->oob);
	}

	ret = !!flivs->oob;

  out:
	if (flivs->tofree)
		regionlist_stk_flush(tofree);

	fellow_logcache_fini(flc);

	return (ret);
}

/*
 * struct to maintain state on incremental allocation of the bit field for log
 * rewrite
 */

#define BITFMAXSEGS 128

struct bitfalloc {
	unsigned		magic;
#define BITFALLOC_MAGIC	0x93a14c7f
	unsigned		nmem, lmem;
	buddy_t			*buddy;
	union {
		struct bitfs	bitfs[1];
		char		bitfsmem[bitfs_size(BITFMAXSEGS)];
	} u;
	struct buddy_ptr_extent	mem[BITFMAXSEGS];
};

static void
bfa_free(struct bitfalloc *bfa)
{
	struct buddy_returns *rets;
	unsigned u;

	CHECK_OBJ_NOTNULL(bfa, BITFALLOC_MAGIC);

	assert(bfa->nmem <= bfa->lmem);

	rets = BUDDY_RETURNS_STK(bfa->buddy, BUDDY_RETURNS_MAX);
	for (u = 0; u < bfa->nmem; u++)
		AN(buddy_return_ptr_extent(rets, &bfa->mem[u]));
	bfa->nmem = 0;
	buddy_return(rets);

	memset(bfa, 0, sizeof *bfa);
}

static void
bfa_alloc(struct bitfalloc *bfa, size_t need, buddy_t *membuddy)
{
	struct buddy_reqs *reqs, *hold;
	struct buddy_ptr_extent mem;
	struct bitfs *b;
	size_t sz, bits;
	int8_t cram;
	unsigned c;
	uint8_t u;

	AN(bfa);
	INIT_OBJ(bfa, BITFALLOC_MAGIC);
	b = bitfs_init(bfa->u.bitfsmem, sizeof bfa->u.bitfsmem);
	assert(b == bfa->u.bitfs);
	bfa->lmem = sizeof bfa->mem / sizeof *bfa->mem;
	bfa->buddy = membuddy;

	reqs = BUDDY_REQS_STK(bfa->buddy, 1);
	hold = BUDDY_REQS_STK(bfa->buddy, 1);
	BUDDY_REQS_PRI(reqs, FEP_MEM_REWR);
	BUDDY_REQS_PRI(hold, FEP_MEM_REWR - 1);

	sz = bitf_sz(need, BITF_NOIDX);

	AZ(bfa->nmem);
	AZ(bfa->u.bitfs->nbits);

	cram = 0;

	while (need > 0) {
		// limit by available segments
		c = log2down((size_t)bfa->lmem - bfa->nmem);
		assert(c < INT8_MAX);

		// 1/16th of the requested size should be small enough to be
		// fulfillable with a decent amount of LRU. flw
		if (c > 4)
			c = 4;
		if (cram > (int8_t)c)
			cram = (int8_t)c;

		// limit by minimal size
		cram = buddy_cramlimit_extent_minbits(sz, cram,
		    (int8_t)log2up(bitf_sz((size_t)1, BITF_NOIDX)));

		buddy_alloc_async_done(reqs);
		AN(buddy_req_extent(reqs, sz, cram));
		(void) buddy_alloc_async(reqs);

		// if the hold has memory or we have a new sz (at cram == 0),
		// return to transfer to reqs
		if (cram == 0 || buddy_alloc_async_ready(hold))
			buddy_alloc_async_done(hold);
		if (hold->n == 0) {
			AN(buddy_req_extent(hold, sz, 0));
			(void) buddy_alloc_async(hold);
		}

		u = buddy_alloc_async_ready(reqs);

		if (u == 0) {
			(void) sched_yield();
			u = buddy_alloc_async_ready(reqs);
		}

		if (u == 0 && cram == (typeof(cram))c)
			u = buddy_alloc_async_wait(reqs);

		if (u == 0) {
			cram++;
			continue;
		}

		// got an allocation
		assert(bfa->nmem < bfa->lmem);
		mem = buddy_get_ptr_extent(reqs, 0);
		AN(mem.ptr);
		bfa->mem[bfa->nmem++] = mem;

		bits = sz_nbits(mem.size, BITF_NOIDX);
		if (bits > need)
			bits = need;

		bitfs_add(bfa->u.bitfs, bitf_init(mem.ptr, bits,
		    mem.size, BITF_NOIDX));
		need -= bits;

		// next allocation might be of smaller size, reset cram
		sz = bitf_sz(need, BITF_NOIDX);
		cram = 0;
	}

	buddy_alloc_async_done(reqs);
	buddy_alloc_async_done(hold);
}

/*
 * current logbuffer must be LBUF_MEM or LBUF_OPEN
 *
 * if OPEN
 * - close it and replace with LBUF_PEND.
 *   new writes go to this logbuffer
 * - during rewrite, we use an INCOMPL logbuffer
 *
 * finally
 * if MEM: set OPEN and flush
 * if INCOMPL: set OPEN and flush. connect active and head, flush
 *
 * for FP_FINI, there is no logbuffer, it has been flushed and closed
 */

static int
fellow_logs_rewriting(const struct fellow_fd *ffd)
{
	return (ffd->rewriting > 0);
}

// under lock
static void
fellow_logs_adjust_objsize_hint(struct fellow_fd *ffd)
{
	struct stvfe_tune *tune;
	size_t spc, sz, objsize, new;
	unsigned hint, size;
	double f;

	spc = buddy_space(ffd->dskbuddy, 1);
	sz  = buddy_size(ffd->dskbuddy);

	AN(sz);
	assert(sz >= spc);

	objsize = fellow_objsize_locked(ffd, sz - spc);

	//ffd->diag("fellow: objsize %zu", objsize);

	if (objsize == 0)
		return;

	tune = ffd->tune;

	f = sz;
	f -= spc;
	f /= sz;
	f *= 100;

	//ffd->diag("fellow: approx %.2f%% full", f);

	if (f < tune->objsize_update_min_occupancy ||
	    f > tune->objsize_update_max_occupancy)
		return;

	hint = log2down(tune->objsize_hint);
	if (hint < MIN_FELLOW_BITS)
		hint = MIN_FELLOW_BITS;
	size = log2down(objsize);
	if (size < MIN_FELLOW_BITS)
		size = MIN_FELLOW_BITS;

	if (size + tune->objsize_update_min_log2_ratio <= hint) {
		new = (size_t)1 << size;
		ffd->diag("fellow: objsize %zu (2^%u) "
		    "< objsize_hint %zu (2^%u), log2_ratio %u: "
		    "lowering objsize_hint to %zu",
		    objsize, size,
		    tune->objsize_hint, hint, hint - size,
		    new);
		tune->objsize_hint = new;
	}
	else if (hint + tune->objsize_update_max_log2_ratio < size) {
		new = (size_t)1 <<
		    (size - tune->objsize_update_max_log2_ratio);
		ffd->diag("fellow: objsize %zu (2^%u) "
		    "> objsize_hint %zu (2^%u), log2_ratio %u: "
		    "raising objsize_hint to %zu",
		    objsize, size,
		    tune->objsize_hint, hint, size - hint,
		    new);
		tune->objsize_hint = new;
	}
}

static void
fellow_logs_rewrite(struct fellow_fd *ffd,
    struct buddy_off_extent *new_log_fdr,
    fellow_resurrect_f *resur_f, void *resur_priv)
{
	struct fellow_disk_log_info *log_info;
	struct fellow_logbuffer *lbuf;
	struct fellow_logbuffer lbuf_save[1], lbuf_pend[1];
	struct fellow_log_region *logreg;
	struct logblk_mempool logblk_mempool[1];
	struct buddy_off_extent fdr_tmp, *fdr;
	off_t logblk_off, pendblk_off = 0;
	struct regionlist *tofree = NULL;
	struct flw_need_why why[1] = {{"unknown"}};
	vtim_mono t0;
	unsigned u;
	int ret;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	if (fellow_logs_rewriting(ffd))
		return;

	if (ffd->logbuf->magic) {
		const struct dle_stats *s = &ffd->logbuf->dle_stats;

		(void) fellow_logwatcher_need(ffd, why);
		ffd->diag("fellow: rewrite (%s)"
#define DLE_N(x, n, v) " " #n ": %u"
#include "tbl/dle.h"
		    "\n", why->s
#define DLE_N(x, n, v) , s->n
#include "tbl/dle.h"
		    );
	}

	t0 = VTIM_mono();

	log_info = &ffd->log_info;
	CHECK_OBJ(log_info, FELLOW_DISK_LOG_INFO_MAGIC);

	assert(log_info->region < LOGREGIONS);

	const struct flics flics = (struct flics){
		.magic = FLICS_MAGIC,
		.ffd = ffd,
		.resur_f = resur_f,
		.resur_priv = resur_priv
	};
	for (u = 0; u < LOGREGIONS; u++)
		CHECK_OBJ(&ffd->logreg[u], FELLOW_LOG_REGION_MAGIC);

	//flexelint
	memset(lbuf_pend, 0, sizeof lbuf_pend);

	/* shared pool for logbuffer and logcache */
	BUDDY_POOL_INIT(logblk_mempool, ffd->membuddy, FEP_MEM_REWR,
	    logblk_mempool_fill, NULL);

	/*
	 * we put in place as the global buffer our temporary buffer
	 * backed by the pending region, which ultimately it is going to
	 * get appended to the rewritten global buffer.
	 */
	if (ffd->phase == FP_OPEN) {
		logreg = &ffd->logreg[pend_region(log_info->region)];
		logregion_reset(logreg);
		logbuffer_init(ffd, lbuf_pend, logblk_mempool, logreg);
	}

	struct bitfalloc bfa[1];
	bfa_alloc(bfa, ffd->size >> MIN_FELLOW_BITS, ffd->membuddy);

	AZ(pthread_mutex_lock(&ffd->logmtx));
	if (fellow_logs_rewriting(ffd)) {
		AZ(pthread_mutex_unlock(&ffd->logmtx));
		ffd->diag("... (raced)\n");

		bfa_free(bfa);

		assert(lbuf_pend->state == LBUF_INIT);
		lbuf_pend->state = LBUF_FINI;
		logbuffer_fini(lbuf_pend);
		BUDDY_POOL_FINI(logblk_mempool);
		return;
	}
	// IMPORTANT: post the rewriting flag only AFTER the BFA is alloc'ed
	ffd->rewriting = 1;

	switch (ffd->phase) {
	case FP_INIT:
		AZ(pthread_mutex_unlock(&ffd->logmtx));

		DBG("---- log off %zu alt %zu",
		    log_info->off.logblk, log_info->alt.logblk);
		if (log_info->alt.logblk != 0)
			logblk_off = log_info->alt.logblk;
		else
			logblk_off = log_info->off.logblk;

		DBG("---- pend off %zu alt %zu",
		    log_info->off.pendblk, log_info->alt.pendblk);
		if (log_info->alt.pendblk)
			pendblk_off = log_info->alt.pendblk;
		else
			pendblk_off = log_info->off.pendblk;

		AZ(ffd->logbuf->magic);
		logbuffer_init(ffd, ffd->logbuf, ffd->logblk_pool,
		    &ffd->logreg[empty_region(log_info->region)]);

		lbuf = ffd->logbuf;
		assert(lbuf->state == LBUF_INIT);
		lbuf->state = LBUF_LOGREG;
		break;
	case FP_OPEN:
		AZ(log_info->off.pendblk);
		AZ(pendblk_off);

		assert(lbuf_pend->state == LBUF_INIT);
		lbuf_pend->state = LBUF_PEND;

		logbuffer_take(lbuf_save, logblk_mempool, ffd->logbuf);
		logbuffer_take(ffd->logbuf, ffd->logblk_pool, lbuf_pend);
		AZ(pthread_cond_broadcast(&ffd->new_logbuf_cond));

		AZ(pthread_mutex_unlock(&ffd->logmtx));

		// close current buffer
		assert(lbuf_save->state == LBUF_OPEN);
		logbuffer_flush(ffd, lbuf_save, 1, LBUF_ALL);
		lbuf_save->state = LBUF_FINI;

		logblk_off = log_info->off.logblk;
		tofree = regionlist_alloc(ffd->membuddy);

		/* shrink logregion if too big or take new logregion
		 * if too small
		 */
		fdr = &log_info->log_region[empty_region(log_info->region)];
		logreg = &ffd->logreg[empty_region(log_info->region)];
		assert(fdr == logreg->region);
		if (new_log_fdr != NULL &&
		    new_log_fdr->size > 0 &&
		    fdr->size < new_log_fdr->size) {
			ffd->diag("fellow: take new region[%u = empty] "
			    "%zu/%zu -> %zu/%zu\n",
			    empty_region(log_info->region),
			    fdr->off, fdr->size,
			    new_log_fdr->off, new_log_fdr->size);
			regionlist_add(tofree, fdr, 1);
			*fdr = *new_log_fdr;
			*new_log_fdr = buddy_off_extent_nil;
		} else {
			const struct stvfe_tune *tune = ffd->tune;
			size_t sz;

			CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);
			sz = fellow_logregion_size(ffd, tune->objsize_hint);

			if (sz < fdr->size) {
				ffd->diag(
				    "fellow: shrink region[%u = empty] "
				    "%zu/%zu -> %zu/%zu\n",
				    empty_region(log_info->region),
				    fdr->off, fdr->size, fdr->off, sz);
				/* we need to write a new header before the trim
				 * because otherwise objects might use the space
				 * which is officially still part of the log
				 *
				 * do not think we need to logmtx here because
				 * the global buffer is PEND
				 */
				fdr_tmp = *fdr;
				fdr->size = sz;
				XXXAZ(fellow_io_write_hdr(ffd));
				buddy_trim1_off_extent(ffd->dskbuddy,
				    &fdr_tmp, sz);
				/* fellow_logregion_size() aligns */
				assert(fdr->size == fdr_tmp.size);
			}
		}

		/* init either with original or updated region */
		logregion_init(logreg, fdr);

		/* the former global buffer becomes the buffer we rewrite logs
		 * to
		 */
		logbuffer_recycle(ffd, lbuf_save, logreg);
		assert(lbuf_save->state == LBUF_INIT);
		lbuf_save->state = LBUF_INCOMPL;
		lbuf = lbuf_save;

		break;
	case FP_FINI:
		AZ(pthread_mutex_unlock(&ffd->logmtx));
		logblk_off = log_info->off.logblk;
		pendblk_off = log_info->off.pendblk;
		tofree = regionlist_alloc(ffd->membuddy);
		lbuf = NULL;
		break;
	default:
		WRONG("prep phase");
	}

	if (logblk_off == 0 && pendblk_off == 0) {
		DBG("---- no logs pending %zu log %zu",
		    pendblk_off, logblk_off);
		bfa_free(bfa);
	}
	else {
		// the actual rewrite happens outside the logmtx:
		// ffd->logbuf is usable as LBUF_PEND

#define BFA_DIAG
#ifdef BFA_DIAG_VERBOSE
		for (u = 0; u < bfa->nmem; u++) {
			ffd->diag("bitfs mem[%u].size = %zu",
			    u, bfa->mem[u].size);
		}
#endif
#ifdef BFA_DIAG
		ffd->diag("bitfs %u segments", bfa->nmem);
#endif

		struct flivs flivs = (struct flivs){
			.magic = FLIVS_MAGIC,
			.oob = 0,
			.lbuf = lbuf,
			.bitfs = bfa->u.bitfs,
			.mempool = logblk_mempool,
			.tofree = tofree,
			.ban_dles = {{0}},
			.fdct = {{0}},
			.ban_export_time = 0
		};

		iter_out_init(flivs.ban_dles, &flics);
		fellow_dlechg_top_init(flivs.fdct, ffd->membuddy);

		ret = 0;

		DBG("---- iter pending from %zu", pendblk_off);
		if (pendblk_off)
			ret += fellow_logs_iter(&flics, &flivs,
			    ffd->logreg[pend_region(log_info->region)].region,
			    ffd->logreg[empty_region(log_info->region)].region,
			    pendblk_off);

		flivs.seq = 0;
		DBG("---- iter log from %zu", logblk_off);
		if (logblk_off)
			ret += fellow_logs_iter(&flics, &flivs,
			    ffd->logreg[active_region(log_info->region)].region,
			    ffd->logreg[empty_region(log_info->region)].region,
			    logblk_off);

		bfa_free(bfa);

		// does not work for vcl mode and test cases
		//XXXAN(flivs.ban_export_time);

		if (flivs.oob)
			assert(ffd->phase == FP_INIT);

		/* do not insist on DLE changes being seen
		 * if the log was read with errors (e.g. oob)
		 */
		fellow_dlechg_top_fini(flivs.fdct,
		    ret ? FDTF_LEAK : FDTF_INSIST);

		iter_out_submit(flivs.ban_dles, lbuf);

		//lint -e{731} XXX
		XXXAN(ffd->phase == FP_INIT || ret == 0);

		// flush outside the lock to minimize work
		// under the lock
		if (ffd->phase == FP_OPEN) {
			assert(lbuf != ffd->logbuf);
			logbuffer_flush(ffd, lbuf, 0, LBUF_ALL);
		}

	}

	AZ(pthread_mutex_lock(&ffd->logmtx));

	switch (ffd->phase) {
	case FP_INIT:
		AN(lbuf);
		assert(lbuf->state == LBUF_LOGREG || lbuf->state == LBUF_MEM);
		lbuf->state = LBUF_OPEN;
		fellow_signal_open(ffd);

		ffd->log_info.region = empty_region(log_info->region);
		ffd->log_info.off.pendblk = 0;
		ffd->log_info.alt.pendblk = 0;
		break;
	case FP_OPEN:
		assert(lbuf == lbuf_save);
		assert(lbuf->mempool == logblk_mempool);
		logbuffer_append(ffd,
		    &ffd->logmtx,
		    ffd->logreg[active_region(log_info->region)].region,
		    ffd->logreg[empty_region(log_info->region)].region,
		    lbuf, ffd->logbuf, tofree);
		assert(lbuf->state == LBUF_INCOMPL);
		lbuf->state = LBUF_OPEN;

		ffd->logbuf->state = LBUF_FINI;
		logbuffer_fini(ffd->logbuf);

		/* put back the old global buffer which we saved to tmp */
		assert(lbuf == lbuf_save);
		logbuffer_take(ffd->logbuf, ffd->logblk_pool, lbuf);
		lbuf = ffd->logbuf;

		ffd->log_info.region = empty_region(log_info->region);
		ffd->log_info.off.pendblk = 0;
		ffd->log_info.alt.pendblk = 0;

		break;
	case FP_FINI:
		AZ(lbuf);
		break;
	default:
		WRONG("commit phase");
	}

	if (lbuf != NULL)
		logbuffer_flush(ffd, lbuf, 0, LBUF_ALL);

	AZ(pthread_cond_broadcast(&ffd->new_logbuf_cond));
	AZ(pthread_mutex_unlock(&ffd->logmtx));

	if (ffd->phase != FP_FINI) {
		assert(ffd->logbuf->mempool == ffd->logblk_pool);
		assert(ffd->logblk_pool->magic == BUDDY_POOL_MAGIC);
	}

	BUDDY_POOL_FINI(logblk_mempool);

	if (tofree != NULL) {
		struct fellow_fd_ioctx_lease fdil = {0};

		switch (ffd->phase) {
		case FP_OPEN:
			// assert the flush above has written a new reference
			// before discard/free blocks
			AN(lbuf);
			assert(logbuffer_can(lbuf, LBUF_CAN_REF));

			fellow_fd_ioctx_get(ffd, &fdil);

			regionlist_discard(ffd, fdil.ioctx, &tofree);
			struct buddy_off_extent d[2];
			d[0] = ffd->log_info.log_region[
			    empty_region(ffd->log_info.region)];
			d[1] = ffd->log_info.log_region[
			    pend_region(ffd->log_info.region)];
			// min=SIZE_MAX: discard whenever enabled
			fellow_io_regions_discard(ffd, fdil.ioctx, d, 2,
			    SIZE_MAX, 1);

			fellow_fd_ioctx_return(&fdil);

			break;
		case FP_FINI:
			regionlist_free(&tofree, ffd->dskbuddy);
			break;
		default:
			WRONG("free phase");
		}
	}

	AZ(pthread_mutex_lock(&ffd->logmtx));
	if (ffd->phase == FP_OPEN)
		fellow_logs_adjust_objsize_hint(ffd);
	ffd->rewriting = 0;
	if (ffd->watcher_running)
		AZ(pthread_cond_signal(&ffd->watcher_cond));
	AZ(pthread_cond_broadcast(&ffd->new_logbuf_cond));
	AZ(pthread_mutex_unlock(&ffd->logmtx));
	ffd->diag("... done: %fs\n", VTIM_mono() - t0);
}

static void
take_region(struct fellow_fd *ffd, struct buddy_off_extent *region)
{
	size_t off;

	if (region->off == 0) {
		AZ(region->size);
		return;
	}
	assert(region->off > 0);
	off = (typeof(off))region->off;
	if (off + region->size > buddy_size(ffd->dskbuddy)) {
		fprintf(stderr,
		    "fellow: region %zu/%zu out of bounds, NULLing\n",
		    off, region->size);
		memset(region, 0, sizeof *region);
		return;
	}
	AN(region->size);
	buddy_take_off_extent(ffd->dskbuddy, region, 1);
}

// free logs region in buddy for consistency check
static void
fellow_logs_close(struct fellow_fd *ffd)
{
	struct fellow_disk_log_info *log_info;
	unsigned u;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	log_info = &ffd->log_info;
	CHECK_OBJ(log_info, FELLOW_DISK_LOG_INFO_MAGIC);

	// test-read logs & free all memory for zero-check
	fellow_logs_rewrite(ffd, NULL, NULL, NULL);

	assert(log_info->region < LOGREGIONS);
	for (u = 0; u < LOGREGIONS; u++)
		buddy_return1_off_extent(ffd->dskbuddy,
		    &log_info->log_region[u]);
}

/* ------------------------------------------------------------
 * log watcher thread
 *
 * all under logmtx
 */

static inline unsigned
pct_used(unsigned fre, unsigned space)
{
	if (space == 0)
		space = 1;
	if (fre > space)
		return (0);
	return (100 * (space - fre) / space);
}

/* checks which only apply to the open ("main") log
 * (not while already rewriting)
 */
static enum flw_need_e
fellow_logwatcher_need_openlog(const struct fellow_logbuffer *lbuf,
    const struct stvfe_tune *tune, struct flw_need_why *why)
{
	const struct fellow_log_region *lreg;
	const struct dle_stats *s;
	float ratio;
	unsigned alive, n, a;
	size_t sz, sum;

	// XXX config
	const unsigned thr_logregion_rewrite_pct = 50;
	const float log_rewrite_ratio_always = 0.9F;

	/* faction of "space hog" entries compared to
	 * objects to trigger a rewrite
	 */
	const float log_rewrite_spacehog_ratio = 0.01F;

	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	lreg = lbuf->logreg;
	if (lreg != NULL && lreg->magic != FELLOW_LOG_REGION_MAGIC)
		lreg = NULL;

	s = &lbuf->dle_stats;
	n = s->obj_del_alloced +
	    s->obj_del_thin +
	    s->ban_add_reg;

	if (s->ban_exp_reg > 1)
		n += s->ban_exp_reg - 1;

	alive = s->obj_del_alloced +
	    s->obj_del_free +
	    s->obj_del_thin;
	a = s->obj_add;
	if (a > alive)
		alive = a - alive;
	else
		alive = 1;

	ratio = (float)n / (float)alive;

	if (ratio > log_rewrite_spacehog_ratio) {
		if (why) {
			bprintf(why->s, "hog (%u/%u alive)=%f > %f",
			    n, alive,
			    ratio, log_rewrite_spacehog_ratio);
		}
		return (FLW_NEED_REWRITE);
	}

	/*
	 * DLE changes (obj_chg) are a problem: For a normal object add or
	 * delete, we only need one bit in the bitmask. But for DLE changes, we
	 * need 64 *bytes* in memory. This can lead to lock ups during rewrites
	 * with small memory configurations.
	 *
	 * thus, trigger a rewrite when it looks like we might need a
	 * substantial amount of DLE changes
	 */

	sz = logbuffer_max_obj_chg(lbuf);
	sum  = s->obj_del_thin;
	sum += s->obj_chg;

	if (sum >= sz) {
		if (why) {
			bprintf(why->s, "dle_chg %zu > %zu", sum, sz);
		}
		return (FLW_NEED_REWRITE);
	}

	/* entries which take up space in the log, but not on dsk */
	n += s->obj_del_free +
	    s->ban_exp_imm;

	if (lreg == NULL ||
	    (a == 0 && n > FELLOW_DISK_LOG_BLOCK_ENTRIES)) {
		if (why)
			bprintf(why->s, "no add %u", n);
		return (FLW_NEED_REWRITE);
	}

	if (a == 0)
		a = 1;

	if (a > FELLOW_DISK_LOG_BLOCK_ENTRIES) {
		ratio = (float)n / (float)a;

		if (ratio > log_rewrite_ratio_always) {
			if (why) {
				bprintf(why->s, "always del/add "
				    "(%u/%u)=%f > %f",
				    n, a,
				    ratio, log_rewrite_ratio_always);
			}
			return (FLW_NEED_REWRITE);
		}

		if (pct_used(lreg->free_n, lreg->space) >
		      thr_logregion_rewrite_pct &&
		    ratio > tune->log_rewrite_ratio) {
			if (why) {
				bprintf(why->s, "logpct (%u/%u)=%u%% > %u%%"
				    " && del/add (%u/%u)=%f > %f",
				    lreg->free_n, lreg->space,
				    pct_used(lreg->free_n, lreg->space),
				    thr_logregion_rewrite_pct,
				    n, a,
				    ratio, tune->log_rewrite_ratio);
			}
			return (FLW_NEED_REWRITE);
		}
	}
	return (FLW_NEED_NONE);
}

static enum flw_need_e
fellow_logwatcher_need(struct fellow_fd *ffd, struct flw_need_why *why)
{
	struct fellow_logbuffer *lbuf;
	const struct stvfe_tune *tune;
	enum flw_need_e need;
	// XXX config?
	const unsigned thr_logbuffer_minfree = 2;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	lbuf = ffd->logbuf;

	/*
	 * tolerate inconsistent read - if we return with
	 * a wrong result momentarily, the logwatcher thread
	 * will fix it with a consistent read
	 */
	if (! LBUF_OK(lbuf))	//lint !e774
		return (FLW_NEED_NONE);

	tune = ffd->tune;

	if (lbuf->state == LBUF_OPEN) {
		need = fellow_logwatcher_need_openlog(lbuf, tune, why);
		if (need != FLW_NEED_NONE)
			return (need);
	}

	if (lbuf->regions_to_free != NULL &&
	    regionlist_used(lbuf->regions_to_free) > 0)
		return (FLW_MAYFLUSH);

	if (lbuf->space == 0 || lbuf->n == 0)
		return (FLW_NEED_NONE);

	if (logblk_mempool_avail(lbuf->mempool) < LOGBLK_MEMPOOL_SIZE)
		return (FLW_NEED_FLUSH);

	if (lbuf->space - lbuf->n <= thr_logbuffer_minfree)
		return (FLW_NEED_FLUSH);

	return (FLW_MAYFLUSH);
}

/*
 * to enable log writes while we are flushing, we use almost the
 * same mechanics as fellow_logs_rewrite(): put in place a new
 * LBUF_PEND, flush the old one, then append
 */

static void
fellow_logwatcher_flush(struct fellow_fd *ffd, unsigned can)
{

	DBG("concurrent n=%u state=%u", ffd->logbuf->n, ffd->logbuf->state);
	if (ffd->logbuf->state != LBUF_OPEN)
		return;
	if ((ffd->logbuf->regions_to_free == NULL ||
	    regionlist_used(ffd->logbuf->regions_to_free) == 0) &&
	    ffd->logbuf->n == 0)
		return;

	// logmtx is held
	logbuffer_flush(ffd, ffd->logbuf, 0, can);
}

#define NSEC 1000000000L

static void
fellow_logwatcher_new_log_alloc(struct fellow_fd *ffd,
     struct buddy_reqs *new_log_req, struct buddy_off_extent *fdr)
{
	struct stvfe_tune *tune;
	size_t nsz, osz;
	unsigned n;

	if (fdr->size != 0)
		return;	// allocation ready to be picked up

	n = buddy_alloc_async_ready(new_log_req);
	if (n > 0) {
		assert(n == 1);
		AZ(fdr->size);
		*fdr = buddy_get_off_extent(new_log_req, 0);
		assert(fdr->off >= 0);
		AN(fdr->size);
		buddy_alloc_async_done(new_log_req);
		return;
	}

	if (new_log_req->n > 0)
		return;	// async allocation waiting

	tune = ffd->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	osz = ffd->log_info.log_region[empty_region(ffd->log_info.region)].size;
	nsz = fellow_logregion_size(ffd, tune->objsize_hint);
	if (osz >= nsz)
		return;

	// start new async allocation request
	AZ(new_log_req->n);
	AN(buddy_req_extent(new_log_req, nsz, 0));
	(void) BUDDYF(alloc_async)(new_log_req);

	fellow_logwatcher_new_log_alloc(ffd, new_log_req, fdr);
}

static void *
fellow_logwatcher_thread(void *priv)
{
	struct timespec dts, ts;
	struct fellow_fd *ffd;
	struct buddy_reqs *new_log_req;
	struct buddy_off_extent new_log_fdr = buddy_off_extent_nil;
	enum flw_need_e need;
	float dt;
	int i;

	CAST_OBJ_NOTNULL(ffd, priv, FELLOW_FD_MAGIC);

	new_log_req = BUDDY_REQS_STK(ffd->dskbuddy, 1);
	BUDDY_REQS_PRI(new_log_req, FEP_RESERVE);

	i = 0;
	AZ(pthread_mutex_lock(&ffd->logmtx));
	fellow_logwatcher_new_log_alloc(ffd, new_log_req, &new_log_fdr);
	while (ffd->watcher_running) {
		fellow_fd_update_stats(ffd);

		if (new_log_fdr.size || ffd->watcher_running == 2)
			need = FLW_NEED_REWRITE;
		else
			need = fellow_logwatcher_need(ffd, NULL);

		switch (need) {
		case FLW_NEED_NONE:
			DBG("logwatcher %s", "FLW_NEED_NONE");
			fellow_logwatcher_new_log_alloc(ffd,
			    new_log_req, &new_log_fdr);
			break;
		case FLW_MAYFLUSH:
			// most likely flush_interval hit
			DBG("logwatcher %s", "FLW_MAYFLUSH");
			ffd->watcher_state = FLW_S_FLUSHING;
			// LBUF_ALL to get a new ref
			fellow_logwatcher_flush(ffd, LBUF_ALL);
			continue;
		case FLW_NEED_FLUSH:
			DBG("logwatcher %s", "FLW_NEED_FLUSH");
			ffd->watcher_state = FLW_S_FLUSHING;
			// need space
			fellow_logwatcher_flush(ffd, 0);
			continue;
		case FLW_NEED_REWRITE:
			DBG("logwatcher %s", "FLW_NEED_REWRITE");
			ffd->watcher_state = FLW_S_FLUSHING;
			fellow_logwatcher_flush(ffd, 0);
			ffd->watcher_state = FLW_S_REWRITING;
			AZ(pthread_mutex_unlock(&ffd->logmtx));
			fellow_logs_rewrite(ffd, &new_log_fdr, NULL, NULL);
			AZ(pthread_mutex_lock(&ffd->logmtx));
			if (ffd->watcher_running == 2)
				ffd->watcher_running = 1;
			AZ(pthread_cond_signal(&ffd->watcher_cond));
			continue;
		default:
			WRONG("logwatcher need");
		}
		CHECK_OBJ_NOTNULL(ffd->tune, STVFE_TUNE_MAGIC);
		dt = ffd->tune->logbuffer_flush_interval;
		dts.tv_sec = (time_t)truncf(dt);
		dts.tv_nsec = (int)(1e9 * (dt - dts.tv_sec));

		AZ(clock_gettime(CLOCK_MONOTONIC, &ts));
		ts.tv_nsec += dts.tv_nsec;
		ts.tv_sec += dts.tv_sec + ts.tv_nsec / NSEC;
		ts.tv_nsec %= NSEC;
		ffd->watcher_state = FLW_S_WAITING;
		i = pthread_cond_timedwait(&ffd->watcher_cond, &ffd->logmtx,
		    &ts);
		assert(i == 0 || i == ETIMEDOUT);
	}

	/*
	 * close and fini the logbuffer:
	 * this implicitly waits for all IO
	 */
	assert(ffd->logbuf->state == LBUF_OPEN);
	logbuffer_flush(ffd, ffd->logbuf, 1, LBUF_ALL);
	ffd->logbuf->state = LBUF_FINI;
	logbuffer_fini(ffd->logbuf);

	AZ(pthread_mutex_unlock(&ffd->logmtx));

	if (new_log_fdr.size != 0)
		buddy_return1_off_extent(ffd->dskbuddy, &new_log_fdr);

	AZ(new_log_fdr.size);
	assert(new_log_fdr.off < 0);

	buddy_alloc_async_done(new_log_req);

	return (NULL);
}

void
fellow_logwatcher_kick_locked(struct fellow_fd *ffd)
{
	if (ffd->phase == FP_OPEN && ffd->watcher_running)
		AZ(pthread_cond_signal(&ffd->watcher_cond));
}

void
fellow_logwatcher_kick(struct fellow_fd *ffd)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AZ(pthread_mutex_lock(&ffd->logmtx));
	fellow_logwatcher_kick_locked(ffd);
	AZ(pthread_mutex_unlock(&ffd->logmtx));
}

// not unter logmtx
static void
fellow_logwatcher_init(struct fellow_fd *ffd)
{
	pthread_condattr_t ca;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	AZ(pthread_condattr_init(&ca));
	AZ(pthread_condattr_setclock(&ca, CLOCK_MONOTONIC));
	AZ(pthread_cond_init(&ffd->watcher_cond, &ca));
}

// not unter logmtx
static void
fellow_logwatcher_start(struct fellow_fd *ffd)
{
	ffd->watcher_running = 1;
	AZ(pthread_create(&ffd->watcher_thread, NULL,
		fellow_logwatcher_thread, ffd));
}

// not unter logmtx - it takes it
static void
fellow_logwatcher_fini(struct fellow_fd *ffd)
{
	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);

	AZ(pthread_mutex_lock(&ffd->logmtx));
	ffd->watcher_running = 2;
	AZ(pthread_cond_signal(&ffd->watcher_cond));
	while (ffd->watcher_running == 2)
		AZ(pthread_cond_wait(&ffd->watcher_cond, &ffd->logmtx));
	assert(ffd->watcher_running == 1);
	ffd->watcher_running = 0;
	AZ(pthread_cond_signal(&ffd->watcher_cond));
	AZ(pthread_mutex_unlock(&ffd->logmtx));

	AZ(pthread_join(ffd->watcher_thread, NULL));
	AZ(pthread_cond_destroy(&ffd->watcher_cond));
}

/*
 * ============================================================
 * API
 */

static void
fellow_log_diag_stderr(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	(void) vfprintf(stderr, fmt, ap);
	va_end(ap);
}

void
fellow_log_set_diag(struct fellow_fd *ffd, fellow_log_diag_f *diag)
{

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	AN(diag);
	ffd->diag = diag;
}

buddy_t *
fellow_dskbuddy(struct fellow_fd *ffd)
{
	fellow_wait_open(ffd);
	return (ffd->dskbuddy);
}

size_t
fellow_minsize(void)
{
	return ((N_HDR + 2) * 2 * MIN_FELLOW_BLOCK);
}

static int
fellow_skip_path_check(void)
{
	const char *e;

	e = getenv("slash_fellow_options");
	if (e == NULL)
		return (0);
	return (!! strstr(e, "skip-path-check"));
}

#define DEV_PATH(path, x)			\
	if (strncmp(path, x, strlen(x)) == 0)	\
		return (0)
/*
 * conduct a basic test to check if the path looks right for
 * a regular file
 */
int
fellow_sane_file_path(const char *path)
{
	if (fellow_skip_path_check())
		return (1);

	DEV_PATH(path, "/dev/");
	DEV_PATH(path, "/devices/");

	return (1);
}

struct fellow_fd *
fellow_log_init(const char *path, size_t wantsize, size_t objsz_hint,
    mapper *freemap_mapper, void *freemap_mapper_priv,
    buddy_t *membuddy, struct stvfe_tune *tune,
    fellow_task_run_t taskrun, struct VSC_fellow *stats)
{
	struct fellow_disk_data_hdr	hdr[N_HDR]
	    __attribute__((aligned(MIN_FELLOW_BLOCK)));
	struct fellow_disk_data_hdr	zero
	    __attribute__((aligned(MIN_FELLOW_BLOCK)));

	struct fellow_fd		*ffd;
	struct stat			st;
	unsigned			i;
	unsigned			entries;
	size_t				sz, got;
	int32_t				ssz;
	uint64_t			hdr_objsize = 0;
	char				last_err[256] = "";
	const int *fl, tryflags[] = {
#ifdef O_DIRECT
		O_DIRECT,
#endif
		O_DSYNC,
		O_SYNC,
		0
	};

	//lint --e{506,527} constant bool, unreach code

	assert(wantsize >= fellow_minsize());

	// align down by min block
	wantsize &= ~((off_t)FELLOW_BLOCK_ALIGN);

	assert (buddy_size(membuddy) >=
	    MEMBUDDY_MINSIZE(wantsize, objsz_hint) / 2);

	DBG("hdr %zu", sizeof hdr[0]);
	assert(sizeof hdr[0] == MIN_FELLOW_BLOCK);
	DBG("fellow_disk_log_block %zu", sizeof(struct fellow_disk_log_block));
	assert(sizeof(struct fellow_disk_log_block) == MIN_FELLOW_BLOCK);
	DBG("FDB_SIZE_MAX %zu", FDB_SIZE_MAX);
	DBG("fellow_dle_obj %zu", sizeof(struct fellow_dle_obj));
	DBG("fellow_dle_reg %zu", sizeof(struct fellow_dle_reg));
	DBG("fellow_dle_ban_imm %zu",
	    sizeof(struct fellow_dle_ban_imm));
	DBG("fellow_dle_ban_im0 %zu",
	    sizeof(struct fellow_dle_ban_im0));
	assert(sizeof(struct fellow_dle_ban_imm) ==
	    sizeof(struct fellow_dle_ban_im0));
	assert(sizeof(struct fellow_dle_ban_im0) ==
	    sizeof(struct fellow_dle_obj));
	assert(sizeof(struct fellow_dle_ban_im0) ==
	    sizeof(struct fellow_dle_reg));
	DBG("fellow_dle_ban_reg %zu",
	    sizeof(struct fellow_dle_ban_reg));
	DBG("fellow_dle %zu", sizeof(struct fellow_dle));
	assert(sizeof(struct fellow_dle) == 72);
	DBG("fellow_dlechg %zu", sizeof(struct fellow_dlechg));
	assert(sizeof(struct fellow_dlechg) == 64);
	{
		fellow_disk_block t;
		t = fdb((off_t)0, (size_t)FDB_SIZE_MAX);
		assert(fdb_size(t) == FDB_SIZE_MAX);
		assert(fdb_off(t) == 0);
		t = fdb(MIN_FELLOW_BLOCK, FDB_SIZE_MAX);
		assert(fdb_size(t) == FDB_SIZE_MAX);
		assert(fdb_off(t) == (off_t)MIN_FELLOW_BLOCK);
	}

	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	ALLOC_OBJ(ffd, FELLOW_FD_MAGIC);
	AN(ffd);
	ffd->phase = FP_INIT;
	ffd->membuddy = membuddy;
	ffd->tune = tune;
	ffd->taskrun = taskrun;
	ffd->diag = fellow_log_diag_stderr;
	ffd->stats = stats;
	VLIST_INIT(&ffd->ffhead);

	fl = tryflags;
	do {
		ffd->fd = open(path,
		    O_RDWR | O_CREAT | O_LARGEFILE | *fl, 0600);
		if (ffd->fd >= 0 || errno != EINVAL)
			break;
	} while (*(fl++) != 0);

	if (ffd->fd < 0) {
		ffd->diag("fellow: open(%s) failed: %s (%d)\n",
		    path, strerror(errno), errno);
		goto err_free;
	}
#define FLOCK_TRIES 3
	for (i = 0; i < FLOCK_TRIES; i++) {
		if (! flock(ffd->fd, LOCK_EX | LOCK_NB))
			break;
		assert(errno == EWOULDBLOCK);
		ffd->diag("fellow: %d/%d flock(%s) failed: %s (%d)\n",
		    i + 1, FLOCK_TRIES, path, strerror(errno), errno);
		(void) usleep(100 * 1000);	// 100ms
	}
	if (i == FLOCK_TRIES) {
		ffd->diag("a fellow file can only be used once\n");
		goto err_free;
	}
#undef FLOCK_TRIES
	ffd->cap = FFD_CAN_ANY;

	memset(hdr, 0, sizeof hdr);
	memset(&zero, 0, sizeof zero);
	ssz = fellow_io_pread_sync(ffd, hdr, sizeof hdr, (off_t)0);
	//lint -e{731} XXX
	XXXAN(ssz >= 0);
	sz = (unsigned)ssz;
	sz /= sizeof hdr[0];
	assert(sz <= N_HDR);
	ffd->active_hdr = UINT_MAX;
	for (i = 0; i < sz; i++) {
		if (hdr[i].log_info.generation <= ffd->log_info.generation)
			continue;
		if (hdr[i].magic != FELLOW_DISK_DATA_HDR_MAGIC) {
			bprintf(last_err,
			    "hdr %d magic mismatch\n", i);
			continue;
		}
		if (hdr[i].version != 1) {
			bprintf(last_err,
			    "hdr %d version %d not supported\n",
			    i, hdr[i].version);
			continue;
		}
		if (hdr[i].fht >= FH_LIM) {
			bprintf(last_err,
			    "hdr %d hash %d not supported (>= FH_LIM)\n",
			    i, hdr[i].fht);
			continue;
		}
		if (! fh_name[hdr[i].fht]) {
			bprintf(last_err,
			    "hdr %d hash %d not supported (not compiled in)\n",
			    i, hdr[i].fht);
			continue;
		}
		if (fhcmp(hdr[i].fht, hdr[i].fh,
			(char *)&hdr[i] + FELLOW_DISK_DATA_HDR_CHK_START,
			FELLOW_DISK_DATA_HDR_CHK_LEN)) {
			bprintf(last_err, "hdr %d chksum mismatch\n", i);
			continue;
		}

		ffd->active_hdr = i;
		memcpy(&ffd->log_info, &hdr[i].log_info, sizeof ffd->log_info);
		if (hdr[i].objsize > 0)
			hdr_objsize = hdr[i].objsize;
		if (hdr[i].objsize_hint > 0)
			objsz_hint = hdr[i].objsize_hint;
	}

	if (*last_err != '\0')
		ffd->diag("fellow: %s: info: %s\n", path, last_err);

	if (ffd->active_hdr == UINT_MAX) {
		if (memcmp(&hdr[0], &zero, sizeof hdr[0])) {
			ffd->diag("fellow: %s is not a fellow file\n",
			    path);
			goto err_close;
		}
		ffd->diag("fellow: %s: empty block 0, will initialize\n",
		    path);
		ffd->active_hdr = N_HDR - 1;
	} else {
		ffd->diag("fellow: %s: hdr %u generation %zu\n",
		    path, ffd->active_hdr, ffd->log_info.generation);
	}

	ffd->diag("fellow: hdr objsize %zu, objsz_hint %zu\n",
	    hdr_objsize, objsz_hint);
	tune->objsize_hint = objsz_hint;

	if (hdr_objsize < MIN_FELLOW_BLOCK)
		hdr_objsize = objsz_hint;

	sz = MEMBUDDY_MINSIZE(wantsize, hdr_objsize);

	if ((got = buddy_reserve(membuddy, sz)) < sz) {
		ffd->diag("\nfellow:\tFATAL:\tfailed reserve of "
		    "minimum memory size %zu.\n", sz);
		if (hdr_objsize < objsz_hint) {
			ffd->diag("fellow:\t\tactual objsize %zu "
			    "< objsize_hint %zu.\n",
			    (size_t)hdr_objsize, objsz_hint);
			ffd->diag("fellow:\t\tlower objsize_hint to %s or ",
			    pow2_unit(log2down(hdr_objsize)));
		}
		else
			ffd->diag("fellow:\t\t");
		ffd->diag("increase memsz by %s and restart.\n",
		    pow2_unit(log2up(sz - got)));
		goto err_close;
	}

	if (fstat(ffd->fd, &st)) {
		ffd->diag("fellow: fstat(%s) failed: %s (%d)\n",
		    path, strerror(errno), errno);
		goto err_close;
	}

	switch (st.st_mode & S_IFMT) {
	case S_IFBLK:
		// https://stackoverflow.com/a/15947809
		//
		// determining the size is platform dependent.
		// Just try to read the end
		//
		// re-using zero buffer
		if (pread(ffd->fd, &zero, sizeof zero,
		    (off_t)(wantsize - sizeof zero)) != (ssize_t)(sizeof zero)) {
			ffd->diag("fellow: %s: block device is too small"
			    " - reading %zu bytes at offset %zu failed\n",
			    path, sizeof zero, wantsize - sizeof zero);
			goto err_close;
		}
		ffd->size = wantsize;
		break;
	case S_IFREG:
		if (! fellow_sane_file_path(path)) {
			ffd->diag("fellow: %s is a file, but the path "
			    "suggests that it should be a block device. "
			    "Do you have a typo?\n", path);
			goto err_close;
		}
		if (st.st_size < (ssize_t)wantsize) {
			if (! posix_fallocate(ffd->fd,
			    (off_t)0, (off_t)wantsize)) {
				ffd->diag("fellow: %s: grown to "
				    "%zu bytes\n", path, wantsize);
				AZ(fstat(ffd->fd, &st));
			} else {
				ffd->size = wantsize;
				ffd->diag("fellow: "
				    "%s: warning, fallocate failed for "
				    "%zu bytes: %s (%d)\n",
				    path, wantsize, strerror(errno), errno);
				break;
			}
		}

		// XXX if log is in truncated part, all is lost
		// XXX TODO: move objects to smaller size
		if (st.st_size > (ssize_t)wantsize) {
			if (ftruncate(ffd->fd, (off_t)wantsize) == 0) {
				ffd->diag("fellow: %s: shrunk to "
				    "%zu bytes\n", path, wantsize);
				AZ(fstat(ffd->fd, &st));
			} else {
				ffd->size = wantsize;
				ffd->diag("fellow: "
				    "%s: warning, ftruncate failed to "
				    "%zu bytes: %s (%d)\n",
				    path, wantsize, strerror(errno), errno);
				break;
			}
		}

		assert(st.st_size >= 0);
		ffd->size = (size_t)st.st_size;
		break;
	default:
		ffd->diag("fellow: %s is neither a file "
		    "nor a block device\n", path);
		goto err_close;
	}

	entries = fellow_io_ring_size("fellow_log_io_entries");
	DBG("log io ring entries: %u", entries);

	if (fellow_fd_ioctx_init(ffd->fdio, ffd->fd, entries,
	    membuddy->area, membuddy->map->size, taskrun))
		goto err_close;

	// XXX varnish vsm mapper
	buddy_init(ffd->dskbuddy, MIN_FELLOW_BITS, ffd->size,
	    NULL, NULL, freemap_mapper, freemap_mapper_priv);

#ifdef BUDDY_WITNESS
	// XXX HACK
	hack_dskbuddy = ffd->dskbuddy;
#endif

	buddy_take_off_extent(ffd->dskbuddy,
	    &BUDDY_OFF_EXTENT(0, sizeof hdr), 1);
	for (i = 0; i < LOGREGIONS; i++)
		take_region(ffd, &ffd->log_info.log_region[i]);

	fellow_logregion_init_resize(ffd, objsz_hint);

	for (i = 0; i < LOGREGIONS; i++)
		logregion_init(&ffd->logreg[i], &ffd->log_info.log_region[i]);

	AZ(pthread_mutex_init(&ffd->phase_mtx, NULL));
	AZ(pthread_cond_init(&ffd->phase_cond, NULL));

	assert(ffd->phase == FP_INIT);

	return (ffd);
  err_close:
	DBG("ERROR close %p", ffd);
	close(ffd->fd);
  err_free:
	FREE_OBJ(ffd);
	return (NULL);
}

void
fellow_log_discardctl(struct fellow_fd *ffd, size_t min)
{
	unsigned enable;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	enable = min < ffd->size;

	//lint -e{731}
	if (!enable == !(ffd->cap & FFD_CAN_ANY))
		return;

	if (enable)
		ffd->cap |= FFD_CAN_ANY;
	else
		ffd->cap = 0;
}

/*
 * during logs rewrite, we have no LRU, so we need to fail allocation requests
 * when out of memory
 *
 */

struct fellow_log_memfail {
	unsigned			magic;
#define FELLOW_LOG_MEMFAIL_MAGIC	0x483bbfed
	unsigned			phase;
	buddy_t			*membuddy;
};

static void *
fellow_log_memfail(void *priv)
{
	struct fellow_log_memfail *mf;
	int i;

	CAST_OBJ_NOTNULL(mf, priv, FELLOW_LOG_MEMFAIL_MAGIC);

	while (mf->phase == 1) {
		buddy_wait_needspace(mf->membuddy);
		i = 3;
		while (i-- && mf->membuddy->waiting)
			(void) usleep(1000 * 1000);
		if (mf->membuddy->waiting)
			buddy_wait_fail(mf->membuddy);
	}
	assert(mf->phase == 2);
	mf->phase = 3;
	return (NULL);
}

void
fellow_log_open(struct fellow_fd *ffd,
    fellow_resurrect_f *resur_f, void *resur_priv)
{
	struct fellow_log_memfail mf[1];
	pthread_t mf_thread;

	AZ(pthread_mutex_init(&ffd->logmtx, NULL));
	AZ(pthread_cond_init(&ffd->new_logbuf_cond, NULL));
	fellow_logwatcher_init(ffd);

	BUDDY_POOL_INIT(ffd->logblk_pool, ffd->membuddy, FEP_MEM_LOG,
	    logblk_mempool_fill, NULL);

	INIT_OBJ(mf, FELLOW_LOG_MEMFAIL_MAGIC);
	mf->membuddy = ffd->membuddy;
	mf->phase = 1;
	AZ(pthread_create(&mf_thread, NULL, fellow_log_memfail, mf));

	MEM_SET(buddy_size(ffd->membuddy));
	fellow_logs_rewrite(ffd, NULL, resur_f, resur_priv);
	assert(ffd->phase == FP_OPEN);

	mf->phase = 2;
	while (mf->phase != 3) {
		buddy_wait_kick(mf->membuddy);
		(void) usleep(1);
	}
	AZ(pthread_join(mf_thread, NULL));

	fellow_logwatcher_start(ffd);
}

/* looks like _destroy might hit an outdated value */
static void
fellow_mutex_destroy(pthread_mutex_t *mtx)
{
	int err;

	err = pthread_mutex_destroy(mtx);
	if (err == 0)
		return;

	assert(err == EBUSY);
	AZ(pthread_mutex_lock(mtx));
	AZ(pthread_mutex_unlock(mtx));
	AZ(pthread_mutex_destroy(mtx));
}

void
fellow_log_close(struct fellow_fd **ffdp)
{
	struct buddy_off_extent e;
	struct fellow_fd	*ffd;
	buddy_t *bp;

	TAKE_OBJ_NOTNULL(ffd, ffdp, FELLOW_FD_MAGIC);

	fellow_logwatcher_fini(ffd);

	ffd->phase = FP_FINI;
	fellow_fd_update_stats(ffd);
	fellow_logs_close(ffd);

	BUDDY_POOL_FINI(ffd->logblk_pool);

	AZ(pthread_mutex_lock(&ffd->phase_mtx));
	while (ffd->nff > 0)
		AZ(pthread_cond_wait(&ffd->phase_cond, &ffd->phase_mtx));
	AZ(pthread_mutex_unlock(&ffd->phase_mtx));

	fellow_mutex_destroy(&ffd->phase_mtx);
	AZ(pthread_cond_destroy(&ffd->phase_cond));
	AZ(pthread_cond_destroy(&ffd->new_logbuf_cond));

	fellow_mutex_destroy(&ffd->logmtx);

	fellow_fd_ioctx_fini(ffd->fdio);

	bp = ffd->dskbuddy;
	e = BUDDY_OFF_EXTENT(0, N_HDR * MIN_FELLOW_BLOCK);
	buddy_return1_off_extent(bp, &e);
	BWIT_ISEMPTY(bp->witness);
	buddy_fini(&bp, NULL, NULL, NULL, NULL);
	AZ(bp);
	AZ(flock(ffd->fd, LOCK_UN));
	close(ffd->fd);
	FREE_OBJ(ffd);
}

#define CHECK_FFD_USABLE(ffd)					\
	do {							\
		CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);	\
		assert(ffd->phase == FP_OPEN);			\
	} while(0)

void
fellow_log_dle_submit(struct fellow_fd *ffd,
    struct fellow_dle *entry, unsigned n)
{
	struct fellow_log_prep prep[1];
	struct regionlist *prealloc = NULL;
	unsigned nn;

	while (n > 0) {
		nn = fellow_log_entries_prep(prep, entry, n);
		assert(nn <= n);
		n -= nn;
		entry += nn;

		/* pre-allocate regionlist outside the lock if it
		 * looks like we are going to need it.
		 * fellow_log_entries_add() will alloc anyway
		 */

		if (prealloc == NULL &&
		    ffd->logbuf->regions_to_free == NULL &&
		    prep->tofree.n) {
			prealloc = regionlist_alloc_nowait(ffd->membuddy);
		}

		/*
		 * for now, only a single obj_chg is submitted at a time
		 * (assertion for below)
		 */
		assert(prep->dle_stats.obj_chg <= 1);

		AZ(pthread_mutex_lock(&ffd->logmtx));
		if (ffd->logbuf->regions_to_free == NULL && prealloc != NULL)
			TAKEZN(ffd->logbuf->regions_to_free, prealloc);

		/*
		 * for now, only a single obj_chg is submitted at a time
		 *
		 * if we hit the max, we kick for a rewrite or, if rewrite is
		 * underway, wait for the new log to appear
		 */
		if (prep->dle_stats.obj_chg > 0 &&
		    ((size_t)ffd->logbuf->dle_stats.obj_chg +
		     ffd->logbuf->dle_stats.obj_del_thin)
		    >= logbuffer_max_obj_chg(ffd->logbuf)) {
			if (ffd->rewriting) {
				while (ffd->rewriting) {
					AZ(pthread_cond_wait(
						&ffd->new_logbuf_cond,
						&ffd->logmtx));
				}
			}
			else
				fellow_logwatcher_kick_locked(ffd);
		}
		fellow_log_entries_add(ffd, ffd->logbuf, prep);
		AZ(pthread_mutex_unlock(&ffd->logmtx));

		AZ(prep->tofree.n);
	}

	if (prealloc != NULL)
		regionlist_free(&prealloc, ffd->dskbuddy);
	AZ(prealloc);
}

/* emergency flush, called from nuke when we need-a-space
 * (tm Create Comforts)
 */
void
fellow_log_flush(struct fellow_fd *ffd)
{
	struct fellow_logbuffer *lbuf;

	// racy
	lbuf = ffd->logbuf;
	if (lbuf->n == 0 && lbuf->regions_to_free == NULL)
		return;

	lbuf = NULL;
	AZ(pthread_mutex_lock(&ffd->logmtx));
	/* the global logbuffer is LBUF_MEM during logwatcher flush */
	if (logbuffer_can(ffd->logbuf, LBUF_CAN_FLUSH) ||
	    logbuffer_can(ffd->logbuf, LBUF_CAN_LOGREG)) {
		lbuf = ffd->logbuf;
		logbuffer_flush(ffd, lbuf, 0, LBUF_ALL);
	}
	AZ(pthread_mutex_unlock(&ffd->logmtx));
}

void
fellow_log_flush_racy(struct fellow_fd *ffd, unsigned n)
{
	unsigned av;

	CHECK_FFD_USABLE(ffd);

	if (ffd->logbuf->space == 0)
		return;

	av = ffd->logbuf->space - ffd->logbuf->n;
	av *= FELLOW_DISK_LOG_BLOCK_ENTRIES;

	if (av > n)
		return;

	fellow_log_flush(ffd);
}

/*
 * locking handled internally, this is just a wrapper
 * for TEST_DRIVER only
 */
void
fellow_log_rewrite(struct fellow_fd *ffd)
{

	CHECK_FFD_USABLE(ffd);

	fellow_logs_rewrite(ffd, NULL, NULL, NULL);
}

size_t
fellow_rndup(const struct fellow_fd *ffd, size_t sz)
{
	return (buddy_rndup(ffd->dskbuddy, sz));
}

// XXX SHOULD NOT BE NEEDED
int
fellow_fd(const struct fellow_fd *ffd)
{
	return (ffd->fd);
}
