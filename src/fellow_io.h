/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "fellow_const.h"

#ifndef O_LARGEFILE
#define O_LARGEFILE	0
#endif

/*
 * fdb encodes offset and size in a single off_t
 *
 * low 12 bits encode for a multiple of 4 KB (as in: number of blocks)
 *
 * 12 + 12 = 24 bits = 16MB - 4K
 */

typedef struct {
	uint64_t fdb;
} fellow_disk_block;

#define FDB_SIZE_MAX ((MIN_FELLOW_BLOCK - 1) << MIN_FELLOW_BITS)
#define FDB_MASK(t) (((t)1 << MIN_FELLOW_BITS) - 1)

// disk block to real offset
static inline off_t
fdb_off(fellow_disk_block fdb)
{
	return (off_t)(fdb.fdb & ~FDB_MASK(off_t));
}

static inline size_t
fdb_size(fellow_disk_block fdb)
{
	return ((fdb.fdb & FDB_MASK(size_t)) << MIN_FELLOW_BITS);
}

static inline fellow_disk_block
fdb(off_t off, size_t size)
{
	fellow_disk_block r;

	assert(off >= 0);
	AZ(off & FDB_MASK(off_t));
	AZ(size & FDB_MASK(size_t));
	assert(size <= FDB_SIZE_MAX);
	r.fdb = (uint64_t)off | (size >> MIN_FELLOW_BITS);
	return (r);
}

static inline fellow_disk_block
region_fdb(const struct buddy_off_extent *reg)
{
	return (fdb(reg->off, reg->size));
}

struct fellow_fd;
int32_t fellow_io_pread_sync(const struct fellow_fd *, void *, size_t, off_t);
int32_t fellow_io_pwrite_sync(const struct fellow_fd *,
    const void *, size_t, off_t);

#define TAKE(to, from) do {						\
		(to) = (from);						\
		memset(&(from), 0, sizeof from);			\
	} while (0)

#define TAKEZN(to, from) do {						\
		AZ(to);							\
		AN(from);						\
		TAKE(to, from);						\
	} while (0)

// async IO info so we know what it is and the next action
enum fellow_aio_info_type {
	FAIOT_NONE = 0,
	// a disk lock block which needs membuddy freeing
	FAIOT_DLB_FINISH,
	FAIOT_DISCARD,
	FAIOT_CACHE_READ,
	FAIOT_CACHE_WRITE,
	FAIOT_LIM
};

static inline uint64_t
faio_ptr_info(enum fellow_aio_info_type t, void *ptr)
{
	uintptr_t p = (uintptr_t)ptr;
	AZ(p & 7);
	assert(t < FAIOT_LIM);
	return (p | (int)t);
}

static inline enum fellow_aio_info_type
faio_info_type(uint64_t info)
{
	enum fellow_aio_info_type t;

	t = (enum fellow_aio_info_type)(info & 7);
	assert(t < FAIOT_LIM);
	return (t);
}

static inline void *
faio_info_ptr(uint64_t info)
{
	return ((void *)(info & ~((uintptr_t)7)));
}
void fellow_io_regions_discard(struct fellow_fd *ffd, void *ioctx,
    struct buddy_off_extent *fdr, unsigned n,
    size_t min, unsigned wait);

#define FAIO_SYNC  1
#define FAIO_CLOSE (1<<1)
