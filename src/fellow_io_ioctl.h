/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

struct fellow_io_ioctl_blkdiscard_ret {
	unsigned	ndiscards;
	unsigned	nslots;
};

struct fellow_io_ioctl;

struct fellow_io_ioctl *
fellow_io_ioctl_init(int fd, fellow_task_run_t *taskrun, unsigned entries);
void
fellow_io_ioctl_fini(struct fellow_io_ioctl **fuioctlp);
unsigned
fellow_io_ioctl_wait(struct fellow_io_ioctl *fuioctl);
unsigned
fellow_io_ioctl_peek(struct fellow_io_ioctl *fuioctl,
struct fellow_io_status *results, unsigned spacea);
struct fellow_io_ioctl_blkdiscard_ret
fellow_io_ioctl_blkdiscard_enq(struct fellow_io_ioctl *fuioctl, uint64_t info,
const struct fellow_io_discard *discards, unsigned n);
