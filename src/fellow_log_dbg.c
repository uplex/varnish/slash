/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#ifdef HAVE_LINUX_FS_H
#include <linux/fs.h>
#elif HAVE_SYS_DKIO_H
#include <sys/types.h>
#include <sys/dkio.h>
#include <sys/vtoc.h>
#else
#error No support to determine device size
#endif

#include <vdef.h>
#include <vas.h>
#include <miniobj.h>

#ifdef DEBUG
#include "debug.h"
#endif
#include "buddy.h"
#include "fellow_io.h"	// XXX needed for buddy_off_extent etc
#include "fellow_log_storage.h"
#include "fellow_log.h"
#include "fellow_tune.h"
#include "fellow_hash.h"
#include "fellow_testenv.h"

static void
usage(const char *n)
{
	fprintf(stderr, "Usage: %s <fellow file>\n\n"
	    "Get debug output of the fellow's log\n", n);
}

static size_t
getsize(const char *filename)
{
	struct stat st;
	size_t r;
	int fd;

	// XXX should be RDONLY
	fd = open(filename, O_RDWR);
	if (fd < 0)
		goto err;
	if (fstat(fd, &st))
		goto err;

	switch (st.st_mode & S_IFMT) {
	case S_IFBLK:
#ifdef HAVE_LINUX_FS_H
		if (ioctl(fd, BLKGETSIZE, &r))
			goto err;
		r *= 512;
#elif HAVE_SYS_DKIO_H
		{
		struct dk_minfo dkmp;
		if (ioctl(fd, DKIOCGMEDIAINFO, &dkmp))
			goto err;
		r = dkmp.dki_capacity * dkmp.dki_lbsize;
		}
#else
#error TODO
#endif
		break;
	case S_IFREG:
		assert(st.st_size >= 0);
		r = (typeof(r))st.st_size;
		break;
	default:
		errno = EINVAL;
		goto err;
	}

	(void)close(fd);
	return (r);

  err:
	if (fd >= 0)
		(void)close(fd);
	perror(filename);
	return (0);
}

static int v_matchproto_(fellow_resurrect_f)
resurrect_keep(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (1);
}

int
main(int argc, char **argv)
{
	struct fellow_fd *ffd;
	buddy_t mb, *membuddy = &mb;
	struct stvfe_tune tune[1];
	const size_t objsz_hint = 10 * 1024 * 1024;
	const char *filename;
	size_t dsksz, memsz = 1024 * 1024 * 1024;

	if (argc != 2) {
		usage(argv[0]);
		return(1);
	}

	filename = argv[1];
	dsksz = getsize(filename);
	if (dsksz == 0)
		return (errno);
	if (memsz > dsksz)
		dsksz = memsz;

	fprintf(stderr, "path %s size %zu\n\n", filename, dsksz);
	AZ(stvfe_tune_init(tune, memsz, dsksz, objsz_hint));

	buddy_init(membuddy, MIN_BUDDY_BITS, memsz,
	    BUDDYF(mmap), NULL, NULL, NULL);
	ffd = fellow_log_init(filename, dsksz, objsz_hint,
	    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
	if (! ffd)
		return (errno);

	fellow_log_open(ffd, resurrect_keep, NULL);
	fellow_log_close(&ffd);
	AZ(ffd);
	BWIT_ISEMPTY(membuddy->witness);
	buddy_fini(&membuddy, BUDDYF(unmap), NULL, NULL, NULL);
	return (0);

}
