/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include "cache/cache_varnishd.h"

#include "debug.h"
#include "compiler.h"
#include "buddy.h"
#include "fellow_pri.h"
#include "fellow_io.h"
#include "fellow_io_backend.h"
#include "fellow_log_storage.h"
#include "fellow_log.h"
#include "fellow_hash.h"
#include "fellow_cache.h"
#include "fellow_cache_storage.h"
#include "fellow_tune.h"
#include "fellow_task.h"
#include "VSC_fellow.h"

#include "foreign/vend.h"
#include "foreign/qdef.h"	// VLIST_* __containerof()

#ifdef TEST_DRIVER
#define FCERR_INJECT
#endif
#include "fellow_inject.h"

// pretty useless, but also harmless to help Flexelint
static pthread_mutex_t wrong_mtx = PTHREAD_MUTEX_INITIALIZER;
static char wrongbuf[1024];

#define FC_WRONG(...) do {						\
		PTOK(pthread_mutex_lock(&wrong_mtx));			\
		bprintf(wrongbuf, __VA_ARGS__);			\
		WRONG(wrongbuf);					\
		PTOK(pthread_mutex_unlock(&wrong_mtx));			\
	} while (0)

static pthread_mutexattr_t fc_mtxattr_errorcheck;
static void __attribute__((constructor))
init_mutexattr(void)
{
	PTOK(pthread_mutexattr_init(&fc_mtxattr_errorcheck));
	PTOK(pthread_mutexattr_settype(&fc_mtxattr_errorcheck,
		PTHREAD_MUTEX_ERRORCHECK));
}

struct fellow_cache_obj;
// look up objects by fdb
VRBT_HEAD(fellow_cache_fdb_head, fellow_cache_obj);

struct fellow_cache_seg;
VTAILQ_HEAD(fellow_cache_lru_head, fellow_cache_seg);

/* ============================================================
 * LRU
 */

struct fellow_cache_lru {
	unsigned			magic;
#define FELLOW_CACHE_LRU_MAGIC		0x5fd80809
	// informational: how many entries on list
	unsigned			n;
	struct fellow_cache		*fc;
	pthread_mutex_t			lru_mtx;
	pthread_cond_t			lru_cond;
	struct fellow_cache_lru_head	lru_head;
	pthread_t			lru_thread;
	struct fellow_cache_seg		*resume;
	// this is only to get information on the current object for panics
	struct busyobj			panic_bo;
};

static struct fellow_cache_lru *
fellow_cache_lru_new(struct fellow_cache *fc)
{
	struct fellow_cache_lru *lru;

	AN(fc);

	ALLOC_OBJ(lru, FELLOW_CACHE_LRU_MAGIC);
	AN(lru);
	lru->fc = fc;
	AZ(pthread_mutex_init(&lru->lru_mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_cond_init(&lru->lru_cond, NULL));
	VTAILQ_INIT(&lru->lru_head);

	return (lru);
}

static void
fellow_cache_lru_fini(struct fellow_cache_lru **lrup)
{
	struct fellow_cache_lru *lru;
	void *r;

	TAKE_OBJ_NOTNULL(lru, lrup, FELLOW_CACHE_LRU_MAGIC);
	if (lru->lru_thread != 0) {
		AZ(pthread_mutex_lock(&lru->lru_mtx));
		AZ(pthread_cond_signal(&lru->lru_cond));
		AZ(pthread_mutex_unlock(&lru->lru_mtx));
		AZ(pthread_join(lru->lru_thread, &r));
		AZ(r);
	}
	assert(VTAILQ_EMPTY(&lru->lru_head));
	AZ(lru->n);
	AZ(pthread_cond_destroy(&lru->lru_cond));
	AZ(pthread_mutex_destroy(&lru->lru_mtx));
}

/*
 * lifetime of a fellow_obj in cache
 *
 * presence in fdb tree is reliable
 * priv PTR is hint
 *
 * 1 create fellow_cache_obj and ref in fdb tree
 * 2 fetch obj
 *
 *
 * remove from cache:
 * - nuke  : varnish-cache forgets about the object
 * - evict : object still exists, but removing from memory
 *
 * eviction:
 * - remove stobj priv ptr
 * - remove from fdb tree
 * - remove from lru
 * when refcnt == 0:
 * - free
 *
 * refcnt'ing obj (fco) and body (fcs)
 *
 * IF body is needed:
 * - for fco INCORE, grab ref on all body fcs
 *   for fco FETCHING -> INCORE this happens with the transition
 *
 * - fco ref must be held if any body fcs ref is held
 */

enum fcos {
	FCOS_INIT = 1,
	FCOS_USABLE,
	FCOS_EMBED,
	FCOS_EMBED_BUSY,
	FCOS_BUSY,
	FCOS_WRITING,
	FCOS_DISK,
	FCOS_MEM,
	FCOS_READING,
	FCOS_CHECK,
	FCOS_REDUNDANT,
	FCOS_INCORE,
	FCOS_READFAIL,
	FCOS_EVICT,
	FCOS_MAX
} __attribute__ ((__packed__));

#define FCOS_SHIFT 4
#define FCOS_MASK ((1 << FCOS_SHIFT) - 1)
#define FCAA_HIGH 0
#define FCL_HIGH (1 << FCOS_SHIFT)
#define FCO_HIGH (2 << FCOS_SHIFT)
#define FCS_HIGH (3 << FCOS_SHIFT)
#define FCOS_HIGH(x) (x & FCS_HIGH)
#define FCOS_MUT(from, to) ((enum fcos_state)(FCOS_HIGH((from)->state) | to))

#define FCAA(fcos) (FCAA_HIGH | FCOS_ ## fcos)
#define FCL(fcos)  (FCL_HIGH  | FCOS_ ## fcos)
#define FCO(fcos)  (FCO_HIGH  | FCOS_ ## fcos)
#define FCS(fcos)  (FCS_HIGH  | FCOS_ ## fcos)
#define FCOS(x) (x & FCOS_MASK)

#define FCOSD(x, y, c, lru, fdb, mem, dsk, paref) x ## _ ## y = x(y),

//lint -e{123} macro FCO
enum fcos_state {
	FCOS_INVAL = 0,
#include "tbl/fcaa_states.h"
	FCAA_MAX,
#include "tbl/fcl_states.h"
	FCL_MAX,
#include "tbl/fco_states.h"
	FCO_MAX,
#include "tbl/fcs_states.h"
	FCS_MAX,
	FCOS_STATE_MAX = FCS_MAX
} __attribute__ ((__packed__));
#undef FCOSD
//lint -e{786} string concat
//lint -e{785} too few
static const char * const fcos_state_s[FCOS_STATE_MAX] = {
#define FCOSD(x, y, c, lru, fdb, mem, dsk, paref)  [x ## _ ## y] = #x "_" #y,
#include "tbl/fcaa_states.h"
#include "tbl/fcl_states.h"
#include "tbl/fco_states.h"
#include "tbl/fcs_states.h"
};
#undef FCOSD

#define FCOS_IS(state, fcos) (FCOS(state) == FCOS_ ## fcos)

static void __attribute__((constructor))
assert_fcos(void)
{
	//lint --e{506} constant boolean
	assert(FCOS_MAX <= FCOS_MASK);
	assert(FCO_READFAIL == FCO(READFAIL));
	assert(FCS_READFAIL == FCS(READFAIL));
	assert(FCOS_IS(FCS_INCORE, INCORE));
	assert(FCOS_IS(FCO_INCORE, INCORE));
	assert(! FCOS_IS(FCO_INCORE, DISK));
}

#define FCOS_BIT(fcos) ((uint16_t)1 << (fcos - 1))

// this is enum fcos_state -> FCOS_BIT(enum fcos)
static uint16_t fcos_transition[FCOS_STATE_MAX];

static void __attribute__((constructor))
init_fcos_transitions(void)
{

	memset(fcos_transition, 0, sizeof fcos_transition);
#define FCAAT(f, t, c)						\
	fcos_transition[FCAA_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fcaa_transition.h"
#define FCLT(f, t, c)						\
	fcos_transition[FCL_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fcl_transition.h"
#define FCOT(f, t, c)						\
	fcos_transition[FCO_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fco_transition.h"
#define FCST(f, t, c)						\
	fcos_transition[FCS_ ## f] |= FCOS_BIT(FCOS_ ## t);
#include "tbl/fcs_transition.h"
}

// whether or not to reference the parent fcs in this state
//lint -e{785} too few
static const uint8_t fcos_parent_ref[FCOS_STATE_MAX] = {
#define FCOSA_NOPAREF 0
#define FCOSA_PAREF 1
#define FCOSD(type, state, comment, lru, fdb, ptrsz, diskseg, refparent) \
	[type ## _ ## state] = refparent,
#include "tbl/fcl_states.h"
#include "tbl/fco_states.h"
#include "tbl/fcs_states.h"
#undef FCOSD
#undef FCOSA_NOPAREF
#undef FCOSA_PAREF
};

static inline void
assert_fcos_transition(enum fcos_state f, enum fcos_state t)
{

	assert(FCOS_HIGH(f) == FCOS_HIGH(t));
	if (! (fcos_transition[f] & FCOS_BIT(FCOS(t))))
		FC_WRONG("transition %s -> %s",
		    fcos_state_s[f], fcos_state_s[t]);
}

/* batch of changes to be applied on fcses */
struct fellow_lru_chgbatch {
	unsigned			magic;
#define FELLOW_LRU_CHGBATCH_MAGIC	0xaab452d9
	unsigned			n_add;
	unsigned			l_rem, n_rem;
	struct fellow_cache_obj		*fco;
	struct fellow_cache_lru_head	add_head, add_tail;
	struct fellow_cache_seg		**fcs;
};

static void
lcb_cleanup(const struct fellow_lru_chgbatch (*lcbp)[1])
{
	const struct fellow_lru_chgbatch *lcb = *lcbp;

	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);
	AZ(lcb->n_add);
	AZ(lcb->n_rem);
	assert(VTAILQ_EMPTY(&lcb->add_head));
	assert(VTAILQ_EMPTY(&lcb->add_tail));
}

#define FELLOW_LRU_CHGBATCH(name, fcoa, size) \
    __attribute__((cleanup(lcb_cleanup))) struct fellow_lru_chgbatch name[1] = {{	\
	.magic = FELLOW_LRU_CHGBATCH_MAGIC,					\
	.n_add = 0,								\
	.l_rem = (size),							\
	.n_rem = 0,								\
	.fco = (fcoa),								\
	.add_head = VTAILQ_HEAD_INITIALIZER((name)->add_head),			\
	.add_tail = VTAILQ_HEAD_INITIALIZER((name)->add_tail),			\
	.fcs = (struct fellow_cache_seg*[size + 1]){0}				\
}}

static inline void
fellow_cache_seg_transition_locked_notincore(struct fellow_cache_seg *fcs,
    enum fcos_state to);

static inline void
fellow_cache_seg_transition_locked(
    struct fellow_lru_chgbatch *lcb, struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to);

struct fellow_disk_seg {
	uint16_t			magic;
#define FELLOW_DISK_SEG_MAGIC		0xf93d
	uint8_t				_reserved;
	uint8_t				fht;
	uint32_t			segnum;
	/* seg.size is the net size used by data. The size
	 * of the disk allocation is the roundup to the disk block
	 */
	struct buddy_off_extent		seg;

	// checksum of data referred to by seg
	// EXCEPT for fellow_disk_obj where it is only
	// the bottom of fellow_disk_obj + va_data
	union fh			fh[1];
};

struct fellow_disk_seglist {
	uint32_t			magic;
#define FELLOW_DISK_SEGLIST_MAGIC	0x06bbf521
	uint8_t				version;
	uint8_t				_reserved[2];
	uint8_t				fht;

	// off = 8 - checksum of data below
	union fh			fh[1];

	// vvv CHECKSUMMED

#define FELLOW_DISK_SEGLIST_CHK_START			\
	offsetof(struct fellow_disk_seglist, next)
#define FELLOW_DISK_SEGLIST_LEN(fdsl)				\
	(sizeof *fdsl + (fdsl)->nsegs * sizeof *(fdsl)->segs)
#define FELLOW_DISK_SEGLIST_CHK_LEN(fdsl)			\
	(FELLOW_DISK_SEGLIST_LEN(fdsl) - FELLOW_DISK_SEGLIST_CHK_START)

	struct buddy_off_extent	next;
	uint16_t			nsegs;
	uint16_t			lsegs;
	uint16_t			idx;
	uint8_t				_reserved2[2];
	struct fellow_disk_seg		segs[];
};

/* A good maximum number of segments per seglist is _just_ under
 * 1<<16
 *
 * formula:
 * size of 1<<16 seg plus the seglist
 * rounded down to a fellow block
 * minus the seglist
 * devided by size of the seg
 *
 * see t_seglist_sizes() output
 */

#define FELLOW_DISK_SEGLIST_MAX_SEGS	/* 65534 */			\
	(unsigned)(((((sizeof(struct fellow_disk_seg) << 16) +		\
	sizeof(struct fellow_disk_seglist)) & ~FELLOW_BLOCK_ALIGN) -	\
	sizeof(struct fellow_disk_seglist)) /				\
	sizeof(struct fellow_disk_seg))

#define SEGLIST_SIZE(xdsl, lsegs)		\
	(sizeof *xdsl + lsegs * sizeof *xdsl->segs)

#define SEGLIST_FIT_FUNC(what)						\
static inline uint16_t							\
	fellow_ ## what ## _seglist_fit(size_t sz)			\
{									\
	assert(sz >= sizeof(struct fellow_ ## what ## _seglist));	\
	sz -= sizeof(struct fellow_ ## what ## _seglist);		\
	sz /= sizeof(struct fellow_ ## what ## _seg);			\
	if (sz > UINT16_MAX)						\
		sz = UINT16_MAX;					\
	return (uint16_t)sz;						\
}

/* type specific union elements of cache segs */
struct fellow_cache_seg_fcl {
	struct buddy_reqs		*reqs;	// allocation during tentative
};

struct fellow_cache_seg_fcs {
	size_t				len;	// FCS_* used size
};

struct fellow_cache_seg_fco {
	struct fellow_disk_obj		*fdo;	// FCO_* ptr to fdo
};

union fellow_cache_seg_ext {
	struct fellow_cache_seg_fcs	fcs;
	struct fellow_cache_seg_fco	fco;
	struct fellow_cache_seg_fcl	fcl;
};

SEGLIST_FIT_FUNC(disk)

#define FCS_HDR(fcs) ((struct fellow_cache_seghdr *)(void*)((fcs) - (fcs)->idx) - 1)
#define FCS_FCO(fcs) (FCS_HDR(fcs)->fco)
#define FCS_PARENT(fcs) (FCS_HDR(fcs)->parent_fcs)

struct fellow_cache_obj;
struct fellow_cache_seg {
	uint16_t			magic;
#define FELLOW_CACHE_SEG_MAGIC		0x6279
	enum fcos_state			state;
	unsigned			fcs_onlru:1;
	unsigned			fco_infdb:1;
	unsigned			lcb_add_head:1;
	unsigned			lcb_add_tail:1;
	unsigned			lcb_remove:1;
	/*
	 * during LRU (fco mutate path): the FCO is already
	 * removed from the LRU, but fcs_onlru is still 1
	 * for fcs consistency. If mutation fails, the FCO
	 * is put back on LRU for real
	 */
	unsigned			fco_lru_mutate:1;
	/*
	 * select if this object should go to the tail (0) or
	 * head (1) of lru. head is for segements which are suspected not to be
	 * needed any more
	 */
	unsigned			lru_to_head:1;

	/*
	 * for FCO, protected by fdb_mtx
	 * for FCS, protected by fco mtx
	 */
	unsigned			refcnt;		//0x4

	unsigned			idx;		//0x8
	// HOLE 0x04

	VTAILQ_ENTRY(fellow_cache_seg)	lru_list;	//0x10
	struct buddy_ptr_extent		alloc;

	union fellow_cache_seg_ext	u;
};

struct fellow_cache_seghdr {
	// XXX make the next two a single pointer?
	struct fellow_cache_obj		*fco;
	// we ref the parent_fcs whenever an FCS needs a container
	struct fellow_cache_seg		*parent_fcs;

	struct fellow_disk_seg		*disk_segs;
};

struct fellow_disk_seg;
// XXX rename
static inline struct fellow_disk_seg *
FCS_FDS(const struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg *fds = FCS_HDR(fcs)->disk_segs;
	return (fds != NULL) ? fds + fcs->idx : NULL;
}


/*
 * marks an fcsl which is being created. iterators over fcsls need to
 * wait on fco->cond while this value is being read
 *
 * it is no longer stored directly, but rather returned by fcsl_next()
 * when:
 * - the current seglist is busy
 * - fdsl has a successor, but not fcsl
 *
 * todo:
 * - the next seglist has been LRU'd (next index != current + 1)
 */
static struct fellow_cache_seglist * const fcsl_pending =
    (void *)(uintptr_t)0x3ead;

struct fellow_cache_seglist {
	uint32_t				magic;
#define FELLOW_CACHE_SEGLIST_MAGIC		0xcad6e9db
	uint16_t				lsegs;		// how many av.
	uint16_t				idx;

	size_t					fcsl_sz;	// 0 for embedded

	struct fellow_cache_seghdr		fcshdr;
	struct fellow_cache_seg			fcs[1];
	VLIST_ENTRY(fellow_cache_seglist)	list;
	struct fellow_cache_seghdr		segshdr;
	struct fellow_cache_seg			segs[];
};

#define FCSL_FCO(fcsl) ((fcsl)->fcshdr.fco)
#define FCSL_FDSL(fcsl) ((struct fellow_disk_seglist *)(fcsl)->fcs->alloc.ptr)
#define FCSL_FDSLSZ(fcsl) ((fcsl)->fcs->alloc.size)

SEGLIST_FIT_FUNC(cache)

#define fellow_cache_seglist_size(n) (sizeof(struct fellow_cache_seglist) + \
	(n) * sizeof(struct fellow_cache_seg))

struct fellow_disk_obj_attr {
	uint32_t	aoff;	// from struct fellow_disk_obj
	uint32_t	alen;
};

/*
 * fdo attribues we want to keep our own enum and, consequently, bitfield
 * stable.  so we define the enum manually and then generate code from the table
 * files to ensure that all values in the tables are defined
 */

enum fdo_attr {
	FDOA_FLAGS = 0,
	FDOA_LEN,
	FDOA_VXID,
	FDOA_LASTMODIFIED,
	FDOA_GZIPBITS,
	FDOA_VARY,
	FDOA_HEADERS,
	FDOA_ESIDATA,
	FDOA__MAX
};

/*
 * the sl and sr functions are to avoid shift left/right by negative amount
 * errors. the compiler emits these before eliminating dead code
 */

static inline uint16_t
sl(uint16_t v, uint8_t b) {
	//lint -e{701} shift left of signed -- WEIRD
	//lint -e{734} loss of precision
	return (v << b);
}

static inline uint16_t
sr(uint16_t v, uint8_t b) {
	return (v >> b);
}

static uint16_t
fdoa2oa_present(const uint16_t fdoa) {
	uint16_t oa = 0;

	//lint --e{506} constant value boolean
	//lint --e{774} boolean always false
	//lint --e{570} loss if sign
	//lint --e{778} constant 0 in -
#define HANDLE_FDOA(x) do {						\
		assert((int)OA_##x < UINT8_MAX);			\
		assert((int)FDOA_##x < UINT8_MAX);			\
		if ((uint8_t)OA_##x == (uint8_t)FDOA_##x)		\
			oa |= fdoa & (1 << (uint8_t)FDOA_##x);		\
		else if ((uint8_t)OA_##x > (uint8_t)FDOA_##x) {		\
			oa |= sl(fdoa & (1 << (uint8_t)FDOA_##x),	\
			    (uint8_t)OA_##x - (uint8_t)FDOA_##x);	\
		}							\
		else {							\
			oa |= sr(fdoa & (1 << (uint8_t)FDOA_##x),	\
			    (uint8_t)FDOA_##x - (uint8_t)OA_##x);	\
		}							\
	} while(0)

#define FDO_FIXATTR(U, l, s, ss) HANDLE_FDOA(U);
#define FDO_VARATTR(U, l)	 HANDLE_FDOA(U);
#define FDO_AUXATTR(U, l)	 HANDLE_FDOA(U);
#include "tbl/fellow_obj_attr.h"
#undef HANDLE_FDOA

	return (oa);
}

static uint16_t
oa2fdoa_present(const uint16_t oa) {
	uint16_t fdoa = 0;

#define HANDLE_OA(x) do {						\
	    assert((int)OA_##x < UINT8_MAX);				\
	    assert((int)FDOA_##x < UINT8_MAX);				\
	    if ((uint8_t)OA_##x == (uint8_t)FDOA_##x)			\
		    fdoa |= oa & (1 << (uint8_t)OA_##x);		\
	    else if ((uint8_t)OA_##x > (uint8_t)FDOA_##x) {		\
		    fdoa |= sr(oa & (1 << (uint8_t)OA_##x),		\
			(uint8_t)OA_##x - (uint8_t)FDOA_##x);		\
	    }								\
	    else {							\
		    fdoa |= sl(oa & (1 << (uint8_t)OA_##x),		\
			(uint8_t)FDOA_##x - (uint8_t)OA_##x);		\
	    }								\
} while(0)

#define OBJ_FIXATTR(U, l, s)	HANDLE_OA(U);
#define OBJ_VARATTR(U, l)	HANDLE_OA(U);
#define OBJ_AUXATTR(U, l)	HANDLE_OA(U);
#include "tbl/obj_attr.h"
#undef HANDLE_OA

	return (fdoa);
}

static void __attribute__((constructor))
assert_oa_fdoa(void)
{
	assert(oa2fdoa_present(1 << (uint8_t)OA_LEN | 1 << (uint8_t)OA_VXID) ==
	    (1 << (uint8_t)FDOA_LEN | 1 << (uint8_t)FDOA_VXID));
#define HANDLE_OA(x)							\
	assert(oa2fdoa_present(1 << (uint8_t)OA_##x) == (1 << (uint8_t)FDOA_##x));	\
	assert(fdoa2oa_present(1 << (uint8_t)FDOA_##x) == (1 << (uint8_t)OA_##x));	\

#define OBJ_FIXATTR(U, l, s)	HANDLE_OA(U);
#define OBJ_VARATTR(U, l)	HANDLE_OA(U);
#define OBJ_AUXATTR(U, l)	HANDLE_OA(U);
#include "tbl/obj_attr.h"
#undef HANDLE_OA
}

// fdo_flags
#define FDO_F_INLOG	1
#define FDO_F_VXID64	2	// vxid uses 8 bytes

/*
 * disk layout
 *
 * struct fellow_disk_obj	disk_obj
 * uint8_t			varattr_data[variable]
 * fellow_disk_seglist		seglist
 *
 * new memory allocation layout IF FITS IN POW2
 *
 * struct fellow_disk_obj	disk_obj
 * uint8_t			varattr_data[variable]
 * fellow_disk_seglist		seglist
 * ... variable length disk segs
 * fellow_cache_seglist
 * ... variable length cache segs
 * fellow_cache_obj
 */


struct fellow_disk_obj {
	uint32_t			magic;
#define FELLOW_DISK_OBJ_MAGIC		0x50728fbd
	uint8_t				version;
	uint8_t				_reserved[2];

	/* flags only set for an FCO_INCORE, never written */
	uint8_t				incore_flags;
#define FDO_INCORE_F_SLIM		1

	// segment off/sz is for fellow_disk_obj + seglist
	// checksum is only for data below
	struct fellow_disk_seg		fdo_fds;
#define FELLOW_DISK_OBJ_CHK_START		\
	offsetof(struct fellow_disk_obj, va_data_len)
#define FELLOW_DISK_OBJ_CHK_LEN(fdo)					\
	((sizeof(struct fellow_disk_obj) - FELLOW_DISK_OBJ_CHK_START) +	\
	(fdo)->va_data_len)

	/* FDO_FIXATTR is 2, 8, 8, 8, 32
	 * so we use 4, 1, 1 to pad to 2x 32 */
	uint32_t			va_data_len;
	uint8_t				fdo_flags;
	uint8_t				fdo_reserve;

	/* attributes */
#define FDO_FIXATTR(U, l, s, vs)			\
	uint8_t				fa_##l[s];
#include "tbl/fellow_obj_attr.h"
	uint8_t				fa_reserve[62];

	uint16_t			fdoa_present;
#define FDO_VARATTR(U, l)			\
	struct fellow_disk_obj_attr	va_##l;
#include "tbl/fellow_obj_attr.h"
	struct fellow_disk_obj_attr	va_reserve[4];

#define FDO_AUXATTR(U, l)			\
	struct fellow_disk_seg		aa_##l##_seg;
#include "tbl/fellow_obj_attr.h"
	// no need for aa_reserve, va offset is variable

	uint8_t				va_data[];
};

// pointer to the embedded fdsl
#define fellow_disk_obj_fdsl(fdo)					\
	(void *)((uint8_t *)fdo + sizeof *fdo + fdo->va_data_len)

static size_t
fellow_disk_obj_size(const struct fellow_disk_obj *fdo)
{
	const struct fellow_disk_seglist *fdsl;

	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	AZ(fdo->incore_flags & FDO_INCORE_F_SLIM);
	fdsl = fellow_disk_obj_fdsl(fdo);
	CHECK_OBJ(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	return (sizeof *fdo + fdo->va_data_len +
	    SEGLIST_SIZE(fdsl, fdsl->lsegs));
}

static size_t
fellow_disk_obj_slimmed_size(const struct fellow_disk_obj *fdo)
{
	const struct fellow_disk_seglist *fdsl = NULL;
	size_t sz;

	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	/* fellow_busy_obj_alloc() assures alignment via PRNDUP(wsl),
	 * but we do not check it to avoid potential failures because of disk
	 * format issues.
	 */
	//PAOK(fdo->va_data_len);
	sz = sizeof *fdo + fdo->va_data_len;
	if ((fdo->incore_flags & FDO_INCORE_F_SLIM) == 0)
		fdsl = fellow_disk_obj_fdsl(fdo);
	if (fdsl != NULL && fdsl->lsegs > 0)
		sz += SEGLIST_SIZE(fdsl, fdsl->lsegs);
	return (sz);
}

// shaved off one pointer...
#define FCO_FCS(fco)	((fco)->fdo_fcs)
#define FCO_FDO(fco)	fellow_disk_obj(FCO_FCS(fco))
#define FCO_REFCNT(fco)	(FCO_FCS(fco)->refcnt)
#define FCO_STATE(fco)	(FCO_FCS(fco)->state)

/*
 * dunno -> wantlog
 * dunno -> nolog
 * dunno -> toolate
 * dunno -> inlog	// read from disk
 * wantlog -> toolate
 * wantlog -> inlog
 * inlog -> deleted
 */
enum fcol_state {
	FCOL_DUNNO = 0,	// not yet decided
	FCOL_WANTLOG,
	FCOL_NOLOG,	// private object
	FCOL_TOOLATE,	// already dead
	FCOL_INLOG,
	FCOL_DELETED,
	FCOL__MAX
} __attribute__ ((__packed__));

VLIST_HEAD(fellow_cache_seglist_head, fellow_cache_seglist);

struct fellow_cache_obj {
	unsigned			magic;
#define FELLOW_CACHE_OBJ_MAGIC		0x837d555f
	enum fcol_state			logstate;

	// ^^^ protected by mtx - do not add <64bit values

	struct fellow_cache_lru		*lru;
	struct fellow_cache_res		fcr;
	uint8_t				ntouched;

	struct buddy_ptr_extent		fco_mem;
	struct buddy_ptr_page		fco_dowry;

	pthread_mutex_t			mtx;
	pthread_cond_t			cond;	// signal ready

	struct objcore			*oc;

	fellow_disk_block		fdb;	// *1) below
	VRBT_ENTRY(fellow_cache_obj)	fdb_entry;

	struct fellow_cache_seghdr	fdo_seghdr;
	struct fellow_cache_seg		fdo_fcs[1];

	struct fellow_cache_seghdr	aa_seghdr;
#define FDO_AUXATTR(U, l)				\
	struct fellow_cache_seg		aa_##l##_seg[1];
#include "tbl/fellow_obj_attr.h"

	struct fellow_cache_seglist_head fcslhead;
	// *2) below
};

/* comments on the above:
 *
 * *1) this is redundant to region_fdb(&fdo->fdo_fds.seg) for efficiency of the
 *     fdb lookup (see fco_cmp())
 *
 * *2) fcsl can be a separate allocation or immediately follow the fco. Also,
 *     the fellow_disk_obj may be copied into the same allocation. So the
 *     structure is
 *
 *     fellow_cache_obj
 *     [fellow_cache_seglist + n * fellow_cache_seg]
 *     [fellow_disk_obj]
 *
 *     we write fellow_disk_object(s) with the embedded fellow_disk_seglist such
 *     that, when reading the object, the fco allocation can also hold all of
 *     the fellow_cache_seglist required for the fellow_disk_seglist.
 *
 *     the numbers on linux:
 *
 *     (gdb) p ((1<<12) - sizeof(struct fellow_cache_obj) - sizeof(struct fellow_cache_seglist)) / sizeof(struct fellow_cache_seg)
 *     $1 = 58
 *     (gdb) p sizeof(struct fellow_cache_obj) + sizeof(struct fellow_cache_seglist) + 58 * sizeof(struct fellow_cache_seg)
 *     $2 = 4072
 *
 *     -> 24 bytes left
 *
 *     mutex_t and cond_t can be bigger on other platforms and might change in
 *     size, so this optimization may always fail when transporting storage to
 *     other platforms. no problem if it does, we might just waste some space
 *     when reading back objects
 */

#define fellow_cache_obj_with_seglist_size(n)				\
	(sizeof(struct fellow_cache_obj) + fellow_cache_seglist_size(n))

#define FDO_MAX_EMBED_SEGS \
	(unsigned)(((MIN_FELLOW_BLOCK - sizeof(struct fellow_cache_obj)) \
	   - sizeof(struct fellow_cache_seglist))			\
	 / sizeof(struct fellow_cache_seg))

#define FCR_OK(p)\
	(struct fellow_cache_res){.r.ptr = (p), .status = fcr_ok}
#define FCR_ALLOCERR(str)\
	(struct fellow_cache_res){.r.err = (str), .status = fcr_allocerr}
#define FCR_IOERR(str)\
	(struct fellow_cache_res){.r.err = (str), .status = fcr_ioerr}
#define FCR_ALLOCFAIL(str) FCR_ALLOCERR(FC_ERRSTR(str))
#define FCR_IOFAIL(str) FCR_IOERR(FC_ERRSTR(str))
#define FCR_IOFAILHOW(str, have, want)					\
	((have) < 0 ? errno = (int)(-(have)), FCR_IOFAIL(str " error")	\
	: ((have) == 0 ? FCR_IOFAIL(str " zero")			\
	: ((have) < (typeof(have))(want) ? FCR_IOFAIL(str " short")	\
	: FCR_IOFAIL(str " other"))))

const char * const fellow_cache_res_s[FCR_LIM] = {
	[fcr_ok] = "ok",
	[fcr_allocerr] = "allocation",
	[fcr_ioerr] = "io"
};

/* ============================================================
 * fwd decl
 */

static inline unsigned
fellow_cache_seg_ref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs);
static inline unsigned
fellow_cache_seg_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs);
static void
fellow_cache_lru_chgbatch_apply(struct fellow_lru_chgbatch *lcb);
static unsigned
fellow_cache_obj_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache *fc, struct fellow_cache_obj *fco);
static void
fellow_cache_obj_free(const struct fellow_cache *fc,
    struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_obj **fcop);

/* ============================================================
 * fco locking wrapped in fellow_lru_chgbatch for safety
 */

//lint -sem(fellow_cache_obj_lock, thread_lock)
static inline void
fellow_cache_obj_lock(const struct fellow_lru_chgbatch *lcb)
{
	AZ(lcb->n_add);
	AZ(lcb->n_rem);
	AZ(pthread_mutex_lock(&lcb->fco->mtx));
}

//lint -sem(fellow_cache_obj_unlock, thread_unlock)
static inline void
fellow_cache_obj_unlock(struct fellow_lru_chgbatch *lcb)
{
	fellow_cache_lru_chgbatch_apply(lcb);
	AZ(pthread_mutex_unlock(&lcb->fco->mtx));
}


/*
 * ============================================================
 * cursor IO
 *
 * cursors can share this structure. It does not itself relate to the actual IO,
 * it only collects IOs to be issued and provides data structures required for
 * IO.
 *
 * collect an array of FCSes for read - used for FCS and FCL
 *
 * on tentative*:
 *
 * tentative_fds is a disk_seg which exists for the sole purpose of using our
 * I/O functions on segments. So why does the previews fdsl not contain an fds
 * to the next? Because we basically would create a hash chain where we could
 * never write fdsls incrementally, because the last fdsl detemines the checksum
 * in the first fdsl...
 *
 * tentative is a cache_seglist which gets inserted into the VLIST until we have
 * the disk_seglist, know the length of the cache_seglist, and allocated it.
 */

BUDDY_REQS(fcsc_mem,1);

static inline void
fcsc_mem_init(struct fcsc_mem *mem, buddy_t *buddy)
{
	AZ(mem->reqs.magic);
	BUDDY_REQS_INIT(mem, buddy);
	BUDDY_REQS_PRI(&mem->reqs, FEP_META);
}

struct fellow_cursor_io {
	unsigned				magic;
#define FELLOW_CURSOR_IO_MAGIC			0x5cc89d60
	uint16_t				l, n;
	struct fellow_cache_seg	** const	fcs;

	struct fellow_cache			*fc;
	// async allocation for new fcsls
	struct fcsc_mem				mem;
	// placeholder while we read the segment
	// allocation to read an fcsl
	struct fellow_disk_seg			tentative_fds;
	struct fellow_cache_seglist		tentative;
};

static void
fcio_cleanup(const struct fellow_cursor_io *fcio)
{

	CHECK_OBJ_NOTNULL(fcio, FELLOW_CURSOR_IO_MAGIC);
	AZ(fcio->n);
	AZ(fcio->mem.reqs.n);
	AZ(fcio->tentative_fds.magic);
	AZ(fcio->tentative.magic);
}

#define FCIO __attribute__((cleanup(fcio_cleanup))) struct fellow_cursor_io

#define FELLOW_CURSOR_IO_(name, uniq, size, fcache)			\
	struct fellow_cache_seg	*uniq[size];				\
	/*lint -e{651, 785}*/						\
	FCIO name = {							\
		.magic = FELLOW_CURSOR_IO_MAGIC,			\
		.l = (size), .n = 0,					\
		.fcs = uniq,						\
		.fc = (fcache),						\
		.mem = {{}},						\
		.tentative_fds = {0},					\
		.tentative = {0}					\
	};								\
	fcsc_mem_init(&(name).mem, (fcache)->membuddy)
#define FELLOW_CURSOR_IO(name, size, fcache)				\
	FELLOW_CURSOR_IO_(name, VUNIQ_NAME(ioseg), size, fcache)

static inline void
fcio_add(struct fellow_cursor_io *fcio, struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcio, FELLOW_CURSOR_IO_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	// read IO takes a ref on the segment and the fco
	// see fellow_cache_read_complete()
	(void) fellow_cache_seg_ref_locked(lcb, fcs);
	(void) fellow_cache_seg_ref_locked(lcb, FCO_FCS(FCS_FCO(fcs)));
	fellow_cache_seg_transition_locked(lcb, fcs, fcs->state,
	    FCOS_MUT(fcs, FCOS_READING));
	assert(fcio->n < fcio->l);
	fcio->fcs[fcio->n++] = fcs;
}

struct fcoi_deref;

struct fcscursor {
	unsigned magic;
#define FCSC_MAGIC				0xded03b2f
	// points to next
	unsigned u;
	struct fellow_cache_seglist *old, *fcsl, *next;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cursor_io *fcio;
	struct fcoi_deref *fcoid;
};

static struct fellow_cache_seglist *
fcsc_fcsl_next_locked(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait);

static void
fcsc_init_locked(struct fcscursor *c, struct fellow_cache_seglist *fcsl,
    struct fellow_cursor_io *fcio, struct fcoi_deref *fcoid,
    struct fellow_lru_chgbatch *lcb)
{

	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	INIT_OBJ(c, FCSC_MAGIC);
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	(void) fellow_cache_seg_ref_locked(lcb, fcsl->fcs);
	c->fcsl = fcsl;
	c->fdsl = FCSL_FDSL(fcsl);
	c->fcio = fcio;
	CHECK_OBJ_NOTNULL(c->fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	c->u = 0;
	c->next = fcsc_fcsl_next_locked(c, lcb, 0);
	c->fcoid = fcoid;
}

static void
fcsc_init(struct fcscursor *c, struct fellow_cursor_io *fcio,
    struct fcoi_deref *fcoid, struct fellow_cache_seglist *fcsl)
{
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fco = FCSL_FCO(fcsl);
	FELLOW_LRU_CHGBATCH(lcb, fco, 3);
	fellow_cache_obj_lock(lcb);
	(void) fellow_cache_seg_ref_locked(lcb, FCO_FCS(fco));
	fcsc_init_locked(c, fcsl, fcio, fcoid, lcb);
	fellow_cache_obj_unlock(lcb);
}

static struct fellow_cache_seglist *
fcsc_fcsl_next_finish(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait);

static void
fcsc_fini_locked(struct fellow_lru_chgbatch *lcb, struct fcscursor *c)
{
	struct fellow_cache_seglist *next;

	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);
	if (c->old == NULL && c->fcsl == NULL && c->next == NULL)
		goto out;

	CHECK_OBJ_NOTNULL(c->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	next = VLIST_NEXT(c->fcsl, list);
	if (c->next == fcsl_pending && next != NULL &&
	    next->idx == c->fcsl->idx + 1)
		c->next = fcsc_fcsl_next_finish(c, lcb, 1);
	else if (c-> next == fcsl_pending)
		c->next = NULL;
	assert(c->next != fcsl_pending);

	if (c->next != NULL)
		(void) fellow_cache_seg_deref_locked(lcb, c->next->fcs);

	CHECK_OBJ_NOTNULL(c->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	(void) fellow_cache_seg_deref_locked(lcb, c->fcsl->fcs);
	/*
	 * special case: if we hold onto the last, empty seglist, we need to
	 * coordinate with fellow_busy_obj_trimstore to not free before we
	 * drop our reference
	 */
	if (UNLIKELY(c->fdsl->nsegs == 0 && c->fcsl->fcs->refcnt == 0))
		AZ(pthread_cond_broadcast(&lcb->fco->cond));
	if (c->old)
		(void) fellow_cache_seg_deref_locked(lcb, c->old->fcs);
	c->fcsl = NULL;
	c->old = NULL;
	c->fdsl = NULL;
    out:

	c->magic++;
}

static void
fcsc_fini(struct fcscursor *c)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache *fc;
	unsigned refcount;

	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);
	fc = c->fcio->fc;
	CHECK_OBJ_NOTNULL(c->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fco = FCSL_FCO(c->fcsl);

	FELLOW_LRU_CHGBATCH(lcb, fco, 3);
	fellow_cache_obj_lock(lcb);

	fcsc_fini_locked(lcb, c);

	refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
	if (refcount == 0)
		fellow_cache_obj_free(fc, lcb, &fco);
	else
		fellow_cache_obj_unlock(lcb);
}

static void
fcsc_cpy_locked(struct fellow_lru_chgbatch *lcb,
    struct fcscursor *dst, const struct fcscursor *src)
{

	CHECK_OBJ_NOTNULL(src, FCSC_MAGIC);
	*dst = *src;

	CHECK_OBJ_NOTNULL(dst->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	dst->old = NULL;

	(void) fellow_cache_seg_ref_locked(lcb, dst->fcsl->fcs);
	if (dst->next != NULL && dst->next != fcsl_pending)
		(void) fellow_cache_seg_ref_locked(lcb, dst->next->fcs);
}

static void
fcsc_cpy(struct fcscursor *dst, const struct fcscursor *src)
{
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(src->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fco = FCSL_FCO(src->fcsl);
	FELLOW_LRU_CHGBATCH(lcb, fco, 2);
	fellow_cache_obj_lock(lcb);
	fcsc_cpy_locked(lcb, dst, src);
	(void) fellow_cache_seg_ref_locked(lcb, FCO_FCS(lcb->fco));
	fellow_cache_obj_unlock(lcb);
}

/*
 * rather than running _fini as the cleanup, we only use the cleanup to check
 * that _fini has run
 */
static void
fcsc_cleanup(struct fcscursor *c)
{

	CHECK_OBJ_NOTNULL(c, (FCSC_MAGIC + 1));

	(void)c;
}

#define FCSC __attribute__((cleanup(fcsc_cleanup))) struct fcscursor

static struct fellow_cache_seg *
fcsc_next_locked(struct fellow_lru_chgbatch *lcb, struct fcscursor *c, unsigned wait)
{
	struct fellow_disk_seglist *fdsl;

	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);

	if (c->u == c->fdsl->nsegs) {
		if (c->next == fcsl_pending)
			c->next = fcsc_fcsl_next_locked(c, lcb, wait);
		if (wait)
			assert(c->next != fcsl_pending);

		if (c->next == fcsl_pending || c->next == NULL) {
			DBG("%p null ret", c->next);
			return (NULL);
		}

		CHECK_OBJ(c->next, FELLOW_CACHE_SEGLIST_MAGIC);
		fdsl = FCSL_FDSL(c->next);
		CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
		assert(c->next->idx == c->fcsl->idx + 1);
		if (fdsl->nsegs == 0) {
			DBG("nsegs %u null ret", fdsl->nsegs);
			return (NULL);
		}
		if (c->old != NULL)
			(void) fellow_cache_seg_deref_locked(lcb, c->old->fcs);
		c->old = c->fcsl;
		c->fcsl = c->next;
		c->fdsl = fdsl;
		c->u = 0;
		c->next = fcsl_pending;
	}
	if (c->next == fcsl_pending)
		c->next = fcsc_fcsl_next_locked(c, lcb, 0);
	assert(c->u < c->fdsl->nsegs);
	return (&c->fcsl->segs[c->u++]);
}

static struct fellow_cache_seg *
fcsc_next(struct fcscursor *c, unsigned wait)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *r;

	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);
	CHECK_OBJ_NOTNULL(c->fcsl, FELLOW_CACHE_SEGLIST_MAGIC);

	// unlocked iteration over this fcsl first
	if (c->next != fcsl_pending && c->u < c->fdsl->nsegs)
		return (&c->fcsl->segs[c->u++]);

	fco = FCSL_FCO(c->fcsl);
	FELLOW_LRU_CHGBATCH(lcb, fco, 2);
	fellow_cache_obj_lock(lcb);
	r = fcsc_next_locked(lcb, c, wait);
	fellow_cache_obj_unlock(lcb);

	return (r);
}

// look at the next element, but do not change the cursor
static struct fellow_cache_seg *
fcsc_peek_wait_expensive(const struct fcscursor *ca)
{
	struct fellow_cache_seg *r;
	FCSC c;

	fcsc_cpy(&c, ca);
	r = fcsc_next(&c, 1);
	fcsc_fini(&c);
	DBG("r = %p r->status = %s", r, r ? fcos_state_s[r->state] : "n/a");

	return (r);
}

static inline struct fellow_cache_seg *
fcsc_peek_wait(const struct fcscursor *ca)
{

	DBG("%u %u", ca->u, ca->fdsl->nsegs);
	if (ca->u < ca->fdsl->nsegs)
		return (&ca->fcsl->segs[ca->u]);
	return (fcsc_peek_wait_expensive(ca));
}

/* ============================================================
 * read ahead seglist cursor
 *
 * this is a light version which stays within the segment lists
 * of the actual cursor (next only if available)
 */

struct fcsracursor {
	unsigned magic;
#define FCSRAC_MAGIC				0x23c10282
	unsigned u;
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	const struct fcscursor *fcsc;
};

static inline void
fcsrac_init(struct fcsracursor *rc, const struct fcscursor *fcsc)
{

	CHECK_OBJ_NOTNULL(fcsc, FCSC_MAGIC);
	INIT_OBJ(rc, FCSRAC_MAGIC);
	rc->fcsl = fcsc->fcsl;
	rc->fdsl = fcsc->fdsl;
	rc->fcsc = fcsc;
}

static struct fellow_cache_seg *
fcsrac_next(struct fcsracursor *rc)
{

	assert(rc->fcsl != fcsl_pending);

	if (rc->fcsl != NULL && rc->u == rc->fdsl->nsegs) {
		if (rc->fcsl == rc->fcsc->old) {
			rc->fcsl = rc->fcsc->fcsl;
			rc->fdsl = rc->fcsc->fdsl;
			rc->u = 0;
		}
		else if (rc->fcsl == rc->fcsc->fcsl &&
		    rc->fcsc->next != fcsl_pending) {
			rc->fcsl = rc->fcsc->next;
			if (rc->fcsl != NULL)
				rc->fdsl = FCSL_FDSL(rc->fcsl);
			else
				rc->fdsl = NULL;
			rc->u = 0;
		}
		else
			return (0);
	}

	if (rc->fcsl == NULL) {
		AZ(rc->fdsl);
		AZ(rc->u);
		return (NULL);
	}

	assert(rc->u < rc->fdsl->nsegs);
	return (&rc->fcsl->segs[rc->u++]);
}

/* ============================================================
 * seglist cursor
 *
 */

struct fellow_body_region {
	struct buddy_off_extent	*reg;
	// used space
	size_t				len;
	// counting allocations from the same region
	unsigned			segnum;
};

/* info struct for busy writes */
enum fellow_busy_io_e {
	FBIO_INVAL = 0,
	FBIO_SEG,
	FBIO_SEGLIST
} __attribute__ ((__packed__));

enum fellow_busy_iosync_e {
	FBIOS_INVAL = 0,
	FBIOS_SYNC,
	FBIOS_ASYNC
} __attribute__ ((__packed__));

// XXX tunable?
#define FBIO_MAX_RETRIES 3

struct fellow_busy_io_seglist {
	struct fellow_cache_seglist *fcsl;
	struct buddy_off_extent reg;
};

// pool for segment memory
BUDDY_POOL(fbo_segmem, 1);
BUDDY_POOL_GET_FUNC(fbo_segmem, static)

struct fellow_busy_io {
	uint16_t				magic;
#define FELLOW_BUSY_IO_MAGIC			0x0bcb
	uint16_t				retries;
	enum fellow_busy_io_e			type;
	enum fellow_busy_iosync_e		sync;
	struct fellow_busy			*fbo;
	union {
		// for FBIO_SEG
		struct fellow_cache_seg		*fcs;
		// for FBIO_SEGLIST
		struct fellow_busy_io_seglist	seglist;
	} u;
};

// Busy Body Region Requests
BUDDY_REQS(fellow_bbrr, 1);

struct fellow_layout_limits {
	size_t		disk_size;
	int8_t		disk_cram;

	unsigned	chunk_exponent;
};

// stored in stevedore priv
struct fellow_busy {
	unsigned			magic;
#define FELLOW_BUSY_MAGIC		0x8504a132

	// for fellow_busy_setattr()
	uint32_t			va_data_len;
	uint8_t				*va_data;

	struct buddy_ptr_extent		fbo_mem;
	size_t				sz_estimate;

	// sum of getspace returned
	size_t				sz_returned;
	// we add more whenever we exceed estimate
	size_t				sz_increment;
	// how much we already alloced on dsk
	size_t				sz_dskalloc;

	struct fellow_cache		*fc;
	struct fellow_cache_obj		*fco;
	struct fellow_cache_seglist	*body_seglist;
	struct fellow_cache_seg		*body_seg;
	struct fellow_cache_seg		*unbusy_seg;
	struct fellow_cache_seglist	*unbusy_seglist;
	struct fellow_body_region	body_region;
	struct fellow_bbrr		bbrr;
	struct buddy_off_extent		segdskdowry;

	struct buddy_ptr_page		segdowry;
	struct fbo_segmem		segmem[1];

#define FCO_MAX_REGIONS ((FELLOW_DISK_LOG_BLOCK_ENTRIES - 1) * DLE_REG_NREGION)
// for fdsl and busy region last chance
#define FCO_REGIONS_RESERVE 3
	struct buddy_off_extent		region[FCO_MAX_REGIONS];
	unsigned			nregion;
	uint8_t				growing;
	struct fellow_layout_limits	fll;
#define FBO_NIO 91
	/* protected by fco mtx */
	uint8_t				io_idx;
	uint8_t				io_outstanding;
	struct fellow_busy_io		io[FBO_NIO];
};

#define MAX_NLRU_EXPONENT 6

struct fellow_cache_lrus {
	unsigned			magic;
#define FELLOW_CACHE_LRUS_MAGIC	0xadad56fb
	pthread_mutex_t			mtx;
	struct fellow_cache_lru		*lru[1 << MAX_NLRU_EXPONENT];
};

struct fellow_cache {
	unsigned			magic;
#define FELLOW_CACHE_MAGIC		0xe2f2243e
	unsigned			running;

	buddy_t				*membuddy;
	struct fellow_fd		*ffd;
	struct stvfe_tune		*tune;
	fellow_task_run_t		*taskrun;

	pthread_mutex_t			fdb_mtx;
	struct fellow_cache_fdb_head	fdb_head;

	struct VSC_fellow		*stats;

	struct fellow_cache_lrus	lrus[1];

	// for re-use of buddy_reqs during cache_obj_iter
	pthread_key_t			iter_reqs_key;

	pthread_mutex_t			async_mtx;
	pthread_cond_t			async_cond;
	void				*async_ioctx;
	pthread_t			async_thread;
	unsigned			async_idle;
	// to get panic info from async_thread
	struct busyobj			panic_bo;
};

/* ============================================================
 * multi-LRU
 */

static void * fellow_cache_lru_thread(struct worker *wrk, void *priv);

static void
fellow_cache_lrus_init(struct fellow_cache_lrus *lrus)
{

	INIT_OBJ(lrus, FELLOW_CACHE_LRUS_MAGIC);
	AZ(pthread_mutex_init(&lrus->mtx, &fc_mtxattr_errorcheck));
}

static void
fellow_cache_lrus_fini(struct fellow_cache_lrus *lrus)
{
	unsigned u;

	for (u = 0; u < 1 << MAX_NLRU_EXPONENT; u++) {
		if (lrus->lru[u] == NULL)
			continue;
		fellow_cache_lru_fini(&lrus->lru[u]);
	}
	AZ(pthread_mutex_destroy(&lrus->mtx));
}

static struct fellow_cache_lru *
fellow_cache_get_lru(struct fellow_cache *fc, uint64_t n)
{
	struct fellow_cache_lrus *lrus;
	struct fellow_cache_lru *lru;
	struct stvfe_tune *tune;
	uint8_t exponent;
	pthread_t thr;
	size_t i;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	lrus = fc->lrus;
	CHECK_OBJ_NOTNULL(lrus, FELLOW_CACHE_LRUS_MAGIC);
	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	exponent = tune->lru_exponent;
	assert(exponent <= MAX_NLRU_EXPONENT);
	i = exponent ? fib(n, exponent) : 0;

	lru = lrus->lru[i];
	if (lru != NULL && lru->lru_thread != 0)
		return (lru);

	AZ(pthread_mutex_lock(&lrus->mtx));
	lru = lrus->lru[i];
	if (lru == NULL) {
		lru = lrus->lru[i] = fellow_cache_lru_new(fc);
		WRK_BgThread(&thr, "sfe-mem-lru", fellow_cache_lru_thread, lru);
		AN(thr);
	}
	AZ(pthread_mutex_unlock(&lrus->mtx));
	AN(lru);
	return (lru);
}

/* ============================================================
 * util
 */

static void
fellow_cache_res_check(const struct fellow_cache *fc,
    const struct fellow_cache_res fcr)
{
	struct stvfe_tune *tune;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	switch (fcr.status) {
	case fcr_allocerr:
		if (tune->allocerr_obj)
			return;
		break;
	case fcr_ioerr:
		if (tune->ioerr_obj)
			return;
		break;
	case fcr_ok:
		return;
	default:
		FC_WRONG("fcr.status %d", fcr.status);
	}

	FC_WRONG("fcr.status = %d, fcr.r.err = %s",
	    fcr.status, fcr.r.err);
}

// check with latch
static struct fellow_cache_res
fellow_cache_obj_res(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco, struct fellow_cache_res fcr)
{

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	fellow_cache_res_check(fc, fcr);
	if (fco->fcr.status != fcr_ok)
		return (fco->fcr);
	/*
	 * the latched ok value must be the fco, but we do return other values,
	 * depending on context
	 *
	 * XXX this is racy for IO callbacks
	 */
	//assert(fco->fcr.status == fcr_ok);
	//assert(fco->fcr.r.ptr == fco);
	if (fcr.status != fcr_ok)
		fco->fcr = fcr;
	else if (fco->fcr.r.ptr == NULL)
		fco->fcr.r.ptr = fco;
	return (fcr);
}

/* ============================================================
 * fwd decl
 */

static void
fellow_cache_obj_fini(const struct fellow_cache_obj *fco);
static void
fellow_cache_obj_redundant(const struct fellow_cache *fc,
    struct fellow_cache_obj **fcop);

static inline unsigned
fellow_cache_seg_ref_n_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned nref);
static inline unsigned
fellow_cache_seg_deref_n_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned nref);
static void
fellow_cache_async_write_complete(struct fellow_cache *fc,
    void *fbio, int32_t result);
static void
fellow_cache_seg_async_read(struct fellow_cursor_io *fcio);

static void
fellow_busy_log_submit(const struct fellow_busy *);
static void
fellow_busy_free(struct fellow_busy **fbop);

static void
fellow_disk_seglist_fini(struct fellow_disk_seglist *fdsl);
static void
fellow_cache_seg_fini(const struct fellow_cache_seg *fcs);
/* ============================================================
 * busy io
 */

/* under fco lock */
static struct fellow_busy_io *
fellow_busy_io_get(struct fellow_busy *fbo, struct fellow_busy_io *stk)
{
	struct fellow_busy_io *fbio;
	uint8_t u;

	AN(stk);
	ASSERT_MUTEX_OWNED(fbo->fco->mtx);

	for (u = 0; u < FBO_NIO; u++) {
		fbio = &fbo->io[fbo->io_idx];
		fbo->io_idx++;
		fbo->io_idx %= FBO_NIO;
		if (fbio->magic == 0 && fbio->type == FBIO_INVAL) {
			INIT_OBJ(fbio, FELLOW_BUSY_IO_MAGIC);
			fbio->fbo = fbo;
			fbio->sync = FBIOS_ASYNC;
			return (fbio);
		}
	}

	fbio = stk;
	INIT_OBJ(fbio, FELLOW_BUSY_IO_MAGIC);
	fbio->fbo = fbo;
	fbio->sync = FBIOS_SYNC;
	return (fbio);
}

struct fbios {
	enum fellow_busy_iosync_e	sync;
	uint64_t			info;
	void				*ptr;
	size_t				size;
	off_t				off;
};

// Avoiding VLA
#define FBIOS_MAX 4

//lint -e{662} XXX better way?
static void
fellow_busy_io_submit(struct fellow_cache *fc, struct fellow_busy_io *fbioa[], unsigned n)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	struct fellow_busy_io *fbio, **fbiop;
	struct fbios *fbios, fbiosa[FBIOS_MAX];

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	AN(n);
	assert (n <= FBIOS_MAX);

	// prep phase, gather into local array
	for (fbiop = fbioa, fbios = fbiosa;
	    fbiop < fbioa + n;
	    fbiop++, fbios++) {
		fbio = *fbiop;
		CHECK_OBJ_NOTNULL(fbio, FELLOW_BUSY_IO_MAGIC);
		fbios->info = faio_ptr_info(FAIOT_CACHE_WRITE, fbio);
		switch (fbio->type) {
		case FBIO_SEG:
			fcs = fbio->u.fcs;
			CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
			fds = FCS_FDS(fcs);
			CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

			switch (fcs->state) {
			case FCS_WRITING:
			case FCAA_WRITING:
				fellow_cache_seg_fini(fcs);
				break;
			case FCO_WRITING:
				// FCO gets finalized outside here
				break;
			default:
				WRONG("fcs->state in busy_io_submit");
			}

			fbios->ptr  = fcs->alloc.ptr;
			fbios->size = fellow_rndup(fc->ffd, fds->seg.size);
			fbios->off  = fds->seg.off;
			assert(fcs->alloc.size >= fbios->size);
			break;
		case FBIO_SEGLIST:
			fcsl = fbio->u.seglist.fcsl;
			CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
			fdsl = FCSL_FDSL(fcsl);
			CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
			fellow_disk_seglist_fini(fdsl);
			fbios->ptr  = fdsl;
			fbios->size = fbio->u.seglist.reg.size;
			fbios->off  = fbio->u.seglist.reg.off;
			break;
		default:
			WRONG("fbio->type");
		}
		switch (fbio->sync) {
		case FBIOS_ASYNC:
		case FBIOS_SYNC:
			fbios->sync = fbio->sync;
			break;
		default:
			WRONG("fbio->sync");
		}
	}

	// async submit
	AZ(pthread_mutex_lock(&fc->async_mtx));
	for (fbios = fbiosa; fbios < fbiosa + n; fbios++) {
		if (UNLIKELY(fbios->sync != FBIOS_ASYNC))
			continue;
		if (LIKELY(fellow_io_write_async_enq(fc->async_ioctx,
		    fbios->info, fbios->ptr, fbios->size, fbios->off)))
			continue;
		fbios->sync = FBIOS_SYNC;
	}
	if (fc->async_idle)
		AZ(pthread_cond_signal(&fc->async_cond));
	AZ(pthread_mutex_unlock(&fc->async_mtx));

	// sync submit
	for (fbios = fbiosa; fbios < fbiosa + n; fbios++) {
		if (fbios->sync != FBIOS_SYNC)
			continue;
		fellow_cache_async_write_complete(fc,
		    faio_info_ptr(fbios->info),
		    fellow_io_pwrite_sync(fc->ffd, fbios->ptr,
			fbios->size, fbios->off));
	}
}


/* ============================================================
 * disk obj
 */

#define fdowrong(x) do {					\
		return (FC_ERRSTR("disk object wrong " x));	\
	} while(0)

static inline struct fellow_disk_obj *
fellow_disk_obj(const struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert(FCOS_HIGH(fcs->state) == FCO_HIGH);
	if (fcs->alloc.ptr != NULL)
		assert(fcs->alloc.ptr == fcs->u.fco.fdo);
	return (fcs->u.fco.fdo);
}

static const char *
fellow_disk_obj_check(const struct fellow_disk_obj *fdo,
    fellow_disk_block fdba)
{
	const struct fellow_disk_seg *fds;
	size_t off, len;

	AN(fdo);
	/*
	 * FC_INJ: Because the error handling path in the caller is identical we
	 * do not inject at all possible points
	 */
	if (FC_INJ || fdo->magic != FELLOW_DISK_OBJ_MAGIC)
		fdowrong("magic");
	if (fdo->version != 1)
		fdowrong("version");
	if (fdo->incore_flags != 0)
		fdowrong("incore_flags");
	if (fdo->va_data_len + sizeof *fdo > fdb_size(fdba))
		fdowrong("va_data_len out of bounds");
	fds = &fdo->fdo_fds;
	if (FC_INJ || fds->magic != FELLOW_DISK_SEG_MAGIC)
		fdowrong("fds magic");
	if (fds->fht >= FH_LIM)
		fdowrong("hash type (>= FH_LIM)");
	if (! fh_name[fds->fht])
		fdowrong("hash type (support missing)");
	if (fds->segnum != 0)
		fdowrong("fds segnum");
	if (fds->seg.off != fdb_off(fdba))
		fdowrong("fds off mismatch");
	if (fds->seg.size != fdb_size(fdba))
		fdowrong("fds size mismatch");
	if (FC_INJ || fhcmp(fds->fht, fds->fh,
		(uint8_t *)fdo + FELLOW_DISK_OBJ_CHK_START,
		FELLOW_DISK_OBJ_CHK_LEN(fdo)))
		fdowrong("chksum");
#define FDO_VARATTR(U, l)						\
	len = fdo->va_##l.alen;						\
	if (len > 0) {							\
		off = fdo->va_##l.aoff;					\
		if (off < offsetof(struct fellow_disk_obj, va_data))	\
			fdowrong(#l " offset");				\
		if (off + len > sizeof(*fdo) + fdo->va_data_len)	\
			fdowrong(#l " length");				\
	}
#include "tbl/fellow_obj_attr.h"

	return (NULL);
}

/*
 * convert old formats to current
 */
static void
fellow_disk_obj_compat(struct fellow_disk_obj *fdo)
{
	uint32_t vxid;

	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	if (fdo->fdo_flags & FDO_F_VXID64)
		return;

	vxid = vbe32dec(fdo->fa_vxid);
	vbe64enc(fdo->fa_vxid, (uint64_t)vxid);
	fdo->fdo_flags |= FDO_F_VXID64;
}

/*
 * when we trim or move the object, the fdsl might get lost
 */
static void
fellow_disk_obj_mark_slim(struct fellow_disk_obj *fdo)
{
	struct fellow_disk_seglist *fdsl;

	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	AZ(fdo->incore_flags & FDO_INCORE_F_SLIM);
	fdsl = fellow_disk_obj_fdsl(fdo);
	if (fdsl->lsegs == 0)
		fdo->incore_flags |= FDO_INCORE_F_SLIM;
}

static struct fellow_disk_obj *
fellow_disk_obj_trim(const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seglist *fdsl;
	struct fellow_disk_obj *fdo;
	struct fellow_cache_obj *fco;
	struct buddy_ptr_extent mem, ret;
	size_t trim_sz;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	fdo = fellow_disk_obj(fcs);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	// not if already relocated
	assert(fdo == fcs->alloc.ptr);

	fdsl = fellow_disk_obj_fdsl(fdo);
	CHECK_OBJ(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fdsl->lsegs >= fdsl->nsegs);
	fdsl->lsegs = fdsl->nsegs;

	/*
	 * Objects which are already potentially accessed asynchronously need to
	 * keep their address (fellow_cache_async_write_complete() call), and we
	 * can not remove the fellow_disk_seglist, because it might already be
	 * read concurrently.
	 *
	 * But objects which we are reading and which thus do not yet have
	 * concurrent access can be relocated. We need to copy, but we gain a
	 * larger page.
	 */

	switch (fcs->state) {
	case FCO_READING:
		trim_sz = fellow_disk_obj_slimmed_size(fdo);
		fellow_disk_obj_mark_slim(fdo);
		if (log2up(trim_sz) < log2up(fcs->alloc.size)) {
			mem = buddy_alloc1_ptr_extent_wait(fc->membuddy,
			    FEP_SPCPRI, trim_sz, 0);
		}
		else
			mem = buddy_ptr_extent_nil;

		if (mem.ptr) {
			memcpy(mem.ptr, fdo, trim_sz);
			fco = FCS_FCO(fcs);

			AZ(pthread_mutex_lock(&fco->mtx));
			ret = fcs->alloc;
			fcs->alloc = mem;
			fcs->u.fco.fdo = mem.ptr;
			AZ(pthread_mutex_unlock(&fco->mtx));

			buddy_return1_ptr_extent(fc->membuddy, &ret);
			break;
		}
		goto trim;
	case FCO_WRITING:
		trim_sz = fellow_disk_obj_size(fdo);
	    trim:
		buddy_trim1_ptr_extent(fc->membuddy, &fcs->alloc, trim_sz);
		assert(fdo == fcs->alloc.ptr);
		break;
	default:
		WRONG("fcs->state to call fellow_disk_obj_trim()");
	}
	return (fellow_disk_obj(fcs));
}

/* ============================================================
 * cache obj
 */

static inline int
fco_cmp(const struct fellow_cache_obj *o1, const struct fellow_cache_obj *o2)
{
	uint64_t b1, b2;

	AN(o1);
	AN(o2);
	b1 = o1->fdb.fdb;
	b2 = o2->fdb.fdb;
	// busy objects may not be added to the fdb tree
	AN(b1);
	AN(b2);

	if (b1 < b2)
		return (-1);
	if (b1 > b2)
		return (1);
	return (0);
}

VRBT_GENERATE_INSERT_COLOR(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_FIND(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, fco_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
#endif
VRBT_GENERATE_INSERT(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, fco_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
VRBT_GENERATE_REMOVE(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_NEXT(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)
//VRBT_GENERATE_MINMAX(fellow_cache_fdb_head, fellow_cache_obj, fdb_entry, static)

static inline int
fellow_cache_shouldlru(enum fcos_state state, const struct objcore *oc,
    unsigned refcnt)
{
	if (FCOS(state) != FCOS_INCORE)
		return (0);
	if (FCOS_HIGH(state) == FCO_HIGH)
		return (oc != NULL && refcnt == 1);
	return (refcnt == 0);
}

#define lcb_assert(fcs, ah, at, r) do {							\
	if (UNLIKELY(! (fcs->lcb_add_head == ah &&					\
			fcs->lcb_add_tail == at &&					\
			fcs->lcb_remove == r))) {					\
		assert(fcs->lcb_add_head == ah);					\
		assert(fcs->lcb_add_tail == at);					\
		assert(fcs->lcb_remove == r);						\
	}										\
} while(0)

static int
fellow_cache_lru_chgbatch_apply_prep(const struct fellow_lru_chgbatch *lcb)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	unsigned n;

	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);
	ASSERT_MUTEX_OWNED(lcb->fco->mtx);

	//DBG("%u/%u", lcb->n, !VTAILQ_EMPTY(&lcb->add));

	if (lcb->n_rem == 0 && lcb->n_add == 0) {
		AZ(lcb->n_rem);
		AZ(lcb->n_add);
		assert(VTAILQ_EMPTY(&lcb->add_head));
		assert(VTAILQ_EMPTY(&lcb->add_tail));
		return (0);
	}

	fco = lcb->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	n = lcb->n_rem;
	while (n) {
		n--;
		fcs = lcb->fcs[n];
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);
		//		ah at r
		lcb_assert(fcs, 0, 0, 1);
		fcs->lcb_remove = 0;
	}
	n = 0;
	VTAILQ_FOREACH(fcs, &lcb->add_head, lru_list) {
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);
		//		ah at r
		lcb_assert(fcs, 1, 0, 0);
		fcs->lcb_add_head = 0;
		n++;
	}
	VTAILQ_FOREACH(fcs, &lcb->add_tail, lru_list) {
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);
		//		ah at r
		lcb_assert(fcs, 0, 1, 0);
		fcs->lcb_add_tail = 0;
		n++;
	}
	assert(n == lcb->n_add);
	return (1);
}

// under fco and lru mtx
static inline void
fellow_cache_lru_chgbatch_apply_work(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_lru *lru, const struct fellow_cache_obj *fco)
{
	struct fellow_cache_seg *fcs;

	lru->n += lcb->n_add;
	lru->n -= lcb->n_rem;
	while (lcb->n_rem) {
		lcb->n_rem--;
		TAKE_OBJ_NOTNULL(fcs, &lcb->fcs[lcb->n_rem],
		    FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);
		VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
		DBG("lru rem fcs %p", fcs);
	}
	if (! VTAILQ_EMPTY(&lcb->add_head) || ! VTAILQ_EMPTY(&lcb->add_tail))
		AZ(pthread_cond_signal(&lru->lru_cond));

	if (! VTAILQ_EMPTY(&lcb->add_head)) {
		VTAILQ_CONCAT(&lcb->add_head, &lru->lru_head, lru_list);
		VTAILQ_SWAP(&lcb->add_head, &lru->lru_head, fellow_cache_seg, lru_list);
	}
	VTAILQ_CONCAT(&lru->lru_head, &lcb->add_tail, lru_list);
}

static inline void
fellow_cache_lru_chgbatch_apply_finish(struct fellow_lru_chgbatch *lcb)
{

	lcb->n_add = 0;
	assert(VTAILQ_EMPTY(&lcb->add_head));
	assert(VTAILQ_EMPTY(&lcb->add_tail));
	AZ(lcb->n_rem);
}

static void
fellow_cache_lru_chgbatch_apply(struct fellow_lru_chgbatch *lcb)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_lru *lru;

	if (fellow_cache_lru_chgbatch_apply_prep(lcb) == 0)
		return;

	fco = lcb->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);

	AZ(pthread_mutex_lock(&lru->lru_mtx));
	fellow_cache_lru_chgbatch_apply_work(lcb, lru, fco);
	AZ(pthread_mutex_unlock(&lru->lru_mtx));
	fellow_cache_lru_chgbatch_apply_finish(lcb);
}

static void
fellow_cache_lru_chgbatch_apply_locked(struct fellow_lru_chgbatch *lcb)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_lru *lru;

	if (fellow_cache_lru_chgbatch_apply_prep(lcb) == 0)
		return;

	fco = lcb->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);

	fellow_cache_lru_chgbatch_apply_work(lcb, lru, fco);
	fellow_cache_lru_chgbatch_apply_finish(lcb);
}

/* chg is fellow_cache_shouldlru(new) - fellow_cache_shouldlru(old)
 *
 * iow: 0 -> noop, 1 -> add, -1 remove
 *
 * to be called after the change
 *
 * the lcb can be null if the caller knows that always chg == 0
 */
static inline void
fellow_cache_lru_chg(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, int chg)
{
	uint16_t i;

	if (chg == 0)
		return;

	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);
	ASSERT_MUTEX_OWNED(lcb->fco->mtx);
	AN(lcb->fcs);

	assert(lcb->fco == FCS_FCO(fcs));

	unsigned add = chg > 0;

#ifdef EXTRA_ASSERTIONS
	assert(add ==
	    fellow_cache_shouldlru(fcs->state, FCS_FCO(fcs)->oc, fcs->refcnt));
#endif
	AZ(fcs->fco_lru_mutate);
	assert(fcs->fcs_onlru == (unsigned)!add);
	fcs->fcs_onlru = add ? 1 : 0;

	if (add && fcs->lcb_remove) {
		//		ah at r
		lcb_assert(fcs, 0, 0, 1);

		//DBG("%p -rem", fcs);
		// remove the remove
		AN(lcb->n_rem);
		for (i = 0; i < lcb->n_rem; i++) {
			if (lcb->fcs[i] != fcs)
				continue;
			lcb->fcs[i] = NULL;
			break;
		}
		assert(i < lcb->n_rem);
		if (i + 1 < lcb->n_rem) {
			memmove(&lcb->fcs[i], &lcb->fcs[i + 1],
			    sizeof lcb->fcs[0] * (lcb->n_rem - (i + 1)));
		}
		lcb->n_rem--;
		fcs->lcb_remove = 0;
	}
	else if (add) {
		//DBG("%p +add", fcs);
		//		ah at r
		lcb_assert(fcs, 0, 0, 0);
		if (fcs->lru_to_head) {
			fcs->lcb_add_head = 1;
			VTAILQ_INSERT_HEAD(&lcb->add_head, fcs, lru_list);
		}
		else {
			fcs->lcb_add_tail = 1;
			VTAILQ_INSERT_TAIL(&lcb->add_tail, fcs, lru_list);
		}
		lcb->n_add++;
	}
	else if (fcs->lcb_add_head) {
		//DBG("%p -add", fcs);
		//		ah at r
		lcb_assert(fcs, 1, 0, 0);
		VTAILQ_REMOVE(&lcb->add_head, fcs, lru_list);
		fcs->lcb_add_head = 0;
		AN(lcb->n_add);
		lcb->n_add--;
	}
	else if (fcs->lcb_add_tail) {
		//DBG("%p -add", fcs);
		//		ah at r
		lcb_assert(fcs, 0, 1, 0);
		VTAILQ_REMOVE(&lcb->add_tail, fcs, lru_list);
		fcs->lcb_add_tail = 0;
		AN(lcb->n_add);
		lcb->n_add--;
	}
	else {
		//DBG("%p +rem", fcs);
		//		ah at r
		lcb_assert(fcs, 0, 0, 0);
		if (lcb->n_rem == lcb->l_rem) {
			fellow_cache_lru_chgbatch_apply(lcb);
			AZ(lcb->n_rem);
		}
		fcs->lcb_remove = 1;
		lcb->fcs[lcb->n_rem++] = fcs;
	}
}

static inline void
assert_cache_seg_consistency(const struct fellow_cache_seg *fcs)
{
#define FCOSA_NOLRU AZ(fcs->fcs_onlru)
#define FCOSA_MAYLRU assert(fcs->fcs_onlru == \
	fellow_cache_shouldlru(fcs->state, FCS_FCO(fcs)->oc, fcs->refcnt))
#define FCOSA_NOFDB AZ(fcs->fco_infdb)
#define FCOSA_MAYFDB assert(fcs->fco_infdb == (unsigned)(fcs->refcnt > 0))
#define FCOSA_NOMEM do {				\
		AZ(fcs->alloc.ptr);			\
		AZ(fcs->alloc.size);			\
		AZ(fcs->u.fcs.len);				\
	} while(0)
#define FCSA_MEM do {					\
		AN(fcs->alloc.ptr);			\
		AN(fcs->alloc.size);			\
	} while(0)
#define FCLA_MEM FCSA_MEM
#define FCOA_MEM do {						\
	    /* can be NULL for embedded fdo */			\
	    if (fcs->alloc.ptr != NULL) {			\
		AN(fcs->alloc.size);				\
		assert(fcs->alloc.ptr == fcs->u.fco.fdo);	\
	    }							\
	} while(0)
#define FCLA_PTR do {					\
		AN(fcs->alloc.ptr);			\
		AZ(fcs->alloc.size);			\
	} while(0)
/* conceptually, FDSA_NULL means there is no disk segment,
 * but since we infer the fds pointer via the seglist header,
 * it might alrealy be available
 */
#define FDSA_NULL FDSA_MAY
#define FDSA_MAY do {						\
	    struct fellow_disk_seg *fds = FCS_FDS(fcs);		\
	    CHECK_OBJ_ORNULL(fds, FELLOW_DISK_SEG_MAGIC);	\
	} while(0)
#define FDSA_UN do {						\
	    struct fellow_disk_seg *fds = FCS_FDS(fcs);		\
	    CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);	\
	    AZ(fds->seg.off);					\
	    AZ(fds->seg.size);					\
	} while(0)
#define FDSA_ALLOC do {						\
	    struct fellow_disk_seg *fds = FCS_FDS(fcs);		\
	    CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);	\
	    AN(fds->seg.off);					\
	    AN(fds->seg.size);					\
	} while(0)
#define FCOSA_PAREF
#define FCOSA_NOPAREF

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	switch (fcs->state) {
//lint --e{525} Negative indentation
//lint --e{665} Unparan ... as expression
#define FCOSD(what, state, comment, lru, fdb, mem, fds, paref)	\
		case what ## _ ## state:			\
			lru;					\
			fdb;					\
			mem;					\
			fds;					\
			paref;					\
			break;
#include "tbl/fcaa_states.h"
#include "tbl/fcl_states.h"
#include "tbl/fco_states.h"
#include "tbl/fcs_states.h"
	default:
		WRONG("fcs->state");
	}
#undef FCOSA_NOLRU
#undef FCOSA_MAYLRU
#undef FCOSA_NOFDB
#undef FCOSA_FDB
#undef FCOSA_NOMEM
#undef FCOA_MEM
#undef FCSA_MEM
#undef FCLA_MEM
#undef FDSA_NULL
#undef FDSA_UN
#undef FDSA_ALLOC
}

static inline void
fellow_cache_seg_wait_locked(const struct fellow_cache_seg *fcs)
{
	struct fellow_cache_obj *fco = FCS_FCO(fcs);

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	ASSERT_MUTEX_OWNED(fco->mtx);
	AN(fcs->refcnt);
//	DBG("%p", fcs);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_cond_wait(&fco->cond, &fco->mtx));
}

/*
 * free the cached segment, not the fellow_(cache|disk)_seg
 *
 * called holding the fco lock
 */
static void
fellow_cache_seg_free(struct buddy_returns *memret,
    struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned deref)
{
	struct buddy_ptr_extent mem;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	// lru removal is already done when freeing fco
	if (lcb != NULL)
		ASSERT_MUTEX_OWNED(lcb->fco->mtx);

	DBG("%p deref %u", fcs, deref);

	//lint --e{454} mutex locked but not unlocked
	while (FCOS(fcs->state) == FCOS_READING ||
	       FCOS(fcs->state) == FCOS_WRITING) {
		// can't happen for fco free
		AN(lcb);
		(void) fellow_cache_seg_ref_locked(NULL, fcs);
		deref++;
		fellow_cache_lru_chgbatch_apply(lcb);
		if (memret->n) {
			//lint -e{455} not locked mutex unlocked
			AZ(pthread_mutex_unlock(&lcb->fco->mtx));
			buddy_return(memret);
			AZ(pthread_mutex_lock(&lcb->fco->mtx));
			//lint -e{456} two execution paths...
			continue;
		}
		fellow_cache_seg_wait_locked(fcs);
	}
	assert_cache_seg_consistency(fcs);
	if (FCOS(fcs->state) == FCOS_CHECK) {
		fellow_cache_seg_transition_locked(lcb, fcs,
		    fcs->state, FCOS_MUT(fcs, FCOS_INCORE));
	}
	if (FCOS(fcs->state) == FCOS_INCORE) {
		fellow_cache_seg_transition_locked(lcb, fcs,
		    fcs->state, FCOS_MUT(fcs, FCOS_DISK));
	}
	if (fcs->state == FCL_EMBED || fcs->state == FCL_EMBED_BUSY) {
		AZ(fcs->fcs_onlru);
		assert(fcs->refcnt == deref);
		return;
	}

	assert(
	    fcs->state == FCO_EVICT ||
	    fcs->state == FCO_REDUNDANT ||

	    fcs->state == FCS_INIT ||
	    fcs->state == FCS_USABLE ||
	    fcs->state == FCS_DISK ||
	    fcs->state == FCS_READFAIL ||

	    fcs->state == FCAA_INIT ||
	    fcs->state == FCAA_USABLE ||
	    fcs->state == FCAA_DISK ||
	    fcs->state == FCAA_READFAIL ||

	    fcs->state == FCL_INIT ||
	    fcs->state == FCL_DISK ||
	    fcs->state == FCL_READFAIL ||
	    fcs->state == FCL_REDUNDANT		// surplus from trim_seglists
	);
	AZ(fcos_parent_ref[fcs->state]);
	TAKE(mem, fcs->alloc);
	AZ(fcs->fcs_onlru);
	assert(fcs->refcnt == deref);
	fcs->refcnt = 0;
	fcs->u.fcs.len = 0;
	/*
	 * at this point, the fcs is not consistent in all cases, e.g. FCS_EVICT
	 * has no memory - but this is the point where it does no longer exist
	 */
	if (mem.size)
		AN(buddy_return_ptr_extent(memret, &mem));
}

/*
 * references on AUXATTR are never returned after access
 * through fellow_cache_obj_getattr()
 *
 * fco->mtx held
 */
static void
fellow_cache_seg_auxattr_free(struct buddy_returns *memret,
    struct fellow_cache_seg *fcs)
{
	struct fellow_cache_obj *fco;

	fco = FCS_FCO(fcs);
	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	ASSERT_MUTEX_OWNED(fco->mtx);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert_cache_seg_consistency(fcs);
	fellow_cache_seg_free(memret, lcb, fcs, fcs->refcnt);
	fellow_cache_lru_chgbatch_apply(lcb);
}

/* fco->mtx held */
static void
fellow_cache_seglist_free(struct buddy_returns *memret,
    struct fellow_cache_seglist *fcsl, unsigned free_lists)
{
	struct fellow_cache_seglist *next, *resume = NULL;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;
	struct buddy_ptr_extent e;
	unsigned u, segs, skip, ref, waitio = 0;

	if (fcsl == NULL)
		return;

	CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fco = FCSL_FCO(fcsl);
	ASSERT_MUTEX_OWNED(fco->mtx);
	next = fcsl;

	FELLOW_LRU_CHGBATCH(lcb, fco, 64);

	while ((fcsl = next) != NULL || (fcsl = resume) != NULL) {
		if (fcsl == resume) {
			waitio = 1;
			resume = NULL;
		}

		CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
		DBG("%p waitio %u", fcsl, waitio);

		skip = ! free_lists;
		next = VLIST_NEXT(fcsl, list);

		segs = 0;
		if (FCSL_FDSL(fcsl))
			segs = FCSL_FDSL(fcsl)->nsegs;

		// we take segs additional references to avoid the seglist going
		// to lru
		if (segs && skip == 0) {
			ref = 1;
			(void) fellow_cache_seg_ref_locked(lcb, fcsl->fcs);
		} else
			ref = 0;

		u = 0;
		fcs = fcsl->segs;

		for (; u < segs; u++, fcs++) {
			if (waitio == 0 &&
			    (FCOS(fcs->state) == FCOS_READING ||
			     FCOS(fcs->state) == FCOS_WRITING)) {
				if (resume == NULL)
					resume = fcsl;
				skip = 1;
				continue;
			}
			fellow_cache_seg_free(memret, lcb, fcs, 0);
		}
		for (; u < fcsl->lsegs; u++, fcs++) {
			assert(FCOS_IS(fcs->state, INIT) ||
			    FCOS_IS(fcs->state, USABLE));
			AZ(fcs->alloc.ptr);
		}

		if (skip) {
			if (ref)
				(void) fellow_cache_seg_deref_locked(lcb, fcsl->fcs);
			continue;
		}

		VLIST_REMOVE(fcsl, list);

		DBG("fcsl free seg=%p", fcsl->fcs);
		// off lru, frees the fdsl, not the fcsl
		fellow_cache_seg_free(memret, lcb, fcsl->fcs, ref);
		fellow_cache_lru_chgbatch_apply(lcb);

		if (fcsl->fcsl_sz > 0) {
			e = BUDDY_PTR_EXTENT(fcsl, fcsl->fcsl_sz);
			fcsl->fcsl_sz = 0;
			AN(buddy_return_ptr_extent(memret, &e));
		}
	}
	fellow_cache_lru_chgbatch_apply(lcb);
}

/*
 * get back region list from segments
 */

static inline void
fellow_seg_regions(const struct fellow_fd *ffd,
    const struct fellow_cache_seg *fcs,
    struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned *na)
{
	const struct fellow_disk_seg *fds;
	unsigned u, n = *na;
	buddyoff_t off;
	size_t sz;

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	sz = fellow_rndup(ffd, fds->seg.size);
	if (fds->segnum > 0) {
		for (u = 0; u < n; u++) {
			off = region[u].off + (buddyoff_t)region[u].size;
			if (off == fds->seg.off) {
				region[u].size += sz;
				assert (*na == n);
				return;
			}
		}
		WRONG("segnum > 0 needs to follow previous region");
	}
	region[n].off = fds->seg.off;
	region[n].size = sz;
	AN(fds->seg.off);
	AN(sz);
	n++;
	assert(n <= FCO_MAX_REGIONS);
	*na = n;
}

static unsigned
fellow_seglist_regions(struct fellow_cache *fc,
    const struct fellow_fd *ffd,
    const struct fellow_cache_obj *fco,
    struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache_seglist *ofcsl;
	struct buddy_off_extent reg;

	if (VLIST_FIRST(&fco->fcslhead) == NULL)
		return (0);

	// only need single IO to get the region
	FELLOW_CURSOR_IO(fcio, 1, fc);
	FCSC c;

	fcsc_init(&c, &fcio, NULL, VLIST_FIRST(&fco->fcslhead));
	ofcsl = NULL;
	while ((fcs = fcsc_next(&c, 1)) != NULL) {
		if (c.fcsl != ofcsl) {
			reg = FCSL_FDSL(c.fcsl)->next;
			if (reg.size)
				region[n++] = reg;
			ofcsl = c.fcsl;
		}
		fellow_seg_regions(ffd, fcs, region, &n);
	}
	fcsc_fini(&c);
	return (n);
}

static unsigned
fellow_obj_regions(struct fellow_cache *fc,
    const struct fellow_cache_obj *fco,
    struct buddy_off_extent region[FCO_MAX_REGIONS])
{
	const struct fellow_fd *ffd;
	unsigned n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	ffd = fc->ffd;

	n = fellow_seglist_regions(fc, ffd, fco, region, 0);
	assert(n <= FCO_MAX_REGIONS);
	DBG("seglist_regions %u", n);
#define FDO_AUXATTR(U, l)						\
	assert(FCOS_HIGH(fco->aa_##l##_seg->state) == FCAA_HIGH);	\
	if (fco->aa_##l##_seg->state != FCAA_USABLE)			\
		fellow_seg_regions(ffd, fco->aa_##l##_seg, region, &n);
#include "tbl/fellow_obj_attr.h"
	DBG("+auxattr %u", n);
	assert(n <= FCO_MAX_REGIONS);
	return (n);
}

/* fco mtx must be held, will be unlocked */
static void
fellow_cache_obj_free(const struct fellow_cache *fc,
    struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_obj **fcop)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct buddy_ptr_extent mem;
	struct buddy_returns *memret;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(lcb, FELLOW_LRU_CHGBATCH_MAGIC);
	TAKE_OBJ_NOTNULL(fco, fcop, FELLOW_CACHE_OBJ_MAGIC);
	assert(lcb->fco == fco);

	memret = BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);

	fcs = FCO_FCS(fco);
	AZ(FCO_REFCNT(fco));
	assert_cache_seg_consistency(fcs);
	fellow_cache_seg_transition_locked(lcb, FCO_FCS(fco),
	    FCO_STATE(fco), FCO_EVICT);
	fellow_cache_lru_chgbatch_apply(lcb);

	DBG("fco %p", fco);
	fellow_cache_seglist_free(memret, VLIST_FIRST(&fco->fcslhead), 1);

#define FDO_AUXATTR(U, l)				\
	fellow_cache_seg_auxattr_free(memret, fco->aa_##l##_seg);
#include "tbl/fellow_obj_attr.h"

	AZ(fcs->fco_infdb);
	AZ(fcs->fcs_onlru);

	AZ(pthread_mutex_unlock(&fco->mtx));
	AZ(pthread_mutex_destroy(&fco->mtx));
	AZ(pthread_cond_destroy(&fco->cond));

	if (fco->fco_dowry.bits)
		AN(buddy_return_ptr_page(memret, &fco->fco_dowry));
	TAKE(mem, fco->fco_mem);
	fellow_cache_seg_free(memret, NULL, fcs, 0);
	AN(buddy_return_ptr_extent(memret, &mem));
	buddy_return(memret);
}

/*
 * evict from stvfe_mutate():
 *
 * state must be FCO_INCORE, object is on LRU, must be referenced
 *
 * fco mtx is held
 *
 * similar in structure to fellow_cache_obj_deref(), but
 * - keep reference
 * - remove exp
 */

void
fellow_cache_obj_evict_mutate(const struct fellow_cache_lru *lru,
    struct fellow_cache_obj *fco)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache *fc;

	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	fc = lru->fc;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	assert(lru == fco->lru);

	fcs = FCO_FCS(fco);

	assert_cache_seg_consistency(fcs);
	assert(fco->logstate == FCOL_INLOG);

	// fdb
	AN(fcs->fco_infdb);
	fcs->fco_infdb = 0;
	AN(FCO_REFCNT(fco));
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	(void) VRBT_REMOVE(fellow_cache_fdb_head, &fc->fdb_head, fco);
	AN(fc->stats->g_mem_obj);
	fc->stats->g_mem_obj--;
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	// state, oc (relevant for lru)
	assert(FCO_STATE(fco) == FCO_INCORE);
	FCO_STATE(fco) = FCO_EVICT;
	AN(fco->oc);
	fco->oc = NULL;

	/* finish lru transaction which has been started in
	 * fellow_cache_lru_work(): the fcs is already removed
	 * from LRU, we just finish the flags
	 */

	AN(fcs->fco_lru_mutate);
	AN(fcs->fcs_onlru);
	fcs->fco_lru_mutate = 0;
	fcs->fcs_onlru = 0;

	assert_cache_seg_consistency(fcs);
}

static inline void
fellow_disk_seg_init(struct fellow_disk_seg *fds, uint8_t fht)
{

	assert(fht > FH_NONE);
	assert(fht < FH_LIM);
	AN(fh_name[fht]);
	INIT_OBJ(fds, FELLOW_DISK_SEG_MAGIC);
	fds->fht = fht;
}

static struct fellow_disk_seglist *
fellow_disk_seglist_init(void *ptr, uint16_t ldsegs, uint8_t fht)
{
	struct fellow_disk_seglist *fdsl = ptr;
	unsigned u;

	AN(ptr);
	assert(PAOK(ptr));
	INIT_OBJ(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	fdsl->version = 1;
	fdsl->lsegs = ldsegs;
	fdsl->fht = fht;
	for (u = 0; u < ldsegs; u++)
		fellow_disk_seg_init(&fdsl->segs[u], fht);
	return (fdsl);
}

static inline void
fellow_cache_seg_init(struct fellow_cache_seg *fcs, unsigned idx, enum fcos_state state)
{

	INIT_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);
	fcs->state = state;
	fcs->idx = idx;
}

// unlocked variant, can not take a parent ref
static inline void
fellow_cache_seg_associate(struct fellow_cache_seg *fcs,
    const struct fellow_disk_seg *fds, enum fcos_state state)
{

	assert(FCOS(fcs->state) == FCOS_INIT);
	assert_cache_seg_consistency(fcs);
	assert(FCS_FDS(fcs) == fds);
	fellow_cache_seg_transition_locked_notincore(fcs, state);
}

// optimized for batch change from FCOS_INIT, locked
static inline void
fellow_cache_seg_associate_n(
    struct fellow_cache_seg *fcs, const struct fellow_disk_seg *fds,
    unsigned n, enum fcos_state to)
{
	struct fellow_cache_seg *parent_fcs;
	enum fcos_state from;
	struct objcore *oc;
	int d;

	// checks only for first seg: mtx parent and transition dont change
	assert(FCS_FDS(fcs) == fds);
	AN(n);

	from = fcs->state;
	assert(FCOS(from) == FCOS_INIT);
	assert_fcos_transition(from, to);

	AZ(fcs->refcnt);

	// optimized for no lru change
	oc = FCS_FCO(fcs)->oc;
	AZ(fellow_cache_shouldlru(from, oc, fcs->refcnt));
	AZ(fellow_cache_shouldlru(to, oc, fcs->refcnt));

	parent_fcs = FCS_PARENT(fcs);

	// get/release all n parent refs if needed
	d = fcos_parent_ref[to] - fcos_parent_ref[from];
	if (d) {
		struct fellow_cache_obj *fco;

		AN(parent_fcs);
		fco = FCS_FCO(parent_fcs);

		FELLOW_LRU_CHGBATCH(lcb, fco, 1);

		fellow_cache_obj_lock(lcb);
		if (d == 1)
			(void) fellow_cache_seg_ref_n_locked(lcb, parent_fcs, n);
		else if (d == -1)
			(void) fellow_cache_seg_deref_n_locked(lcb, parent_fcs, n);
		else
			WRONG("fcos_parent_ref tbl");
		fellow_cache_obj_unlock(lcb);
	}

	while (n--) {
		AZ(fcs->refcnt);
		assert(fcs->state == from);
		assert_cache_seg_consistency(fcs);
		fcs->state = to;
		assert_cache_seg_consistency(fcs);

		fcs++;
	}
}

/*
 * cache seglist must be able to hold all disk seglist
 */
static void
fellow_cache_seglist_associate(
    struct fellow_cache_seglist *fcsl,
    struct fellow_disk_seglist *fdsl, enum fcos_state state)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	unsigned n;

	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fcsl->lsegs >= fdsl->nsegs);

	// no references taken
	assert(state == FCS_USABLE || state == FCS_DISK);

	if (FCSL_FDSL(fcsl) == NULL)
		fcsl->fcs->alloc.ptr = fdsl;
	else
		assert(FCSL_FDSL(fcsl) == fdsl);

	if (fdsl->idx == 0)
		fdsl->idx = fcsl->idx;
	else
		assert(fdsl->idx == fcsl->idx);

	AZ(fcsl->segshdr.disk_segs);
	fcsl->segshdr.disk_segs = fdsl->segs;

	fcs = fcsl->segs;
	fds = fdsl->segs;
	n = fdsl->nsegs;
	if (n)
	    fellow_cache_seg_associate_n(fcs, fds, n, state);
	fcs += n;
	fds += n;

	if (fcsl->lsegs < fdsl->lsegs)
		n = fcsl->lsegs - n;
	else
		n = fdsl->lsegs - n;

	if (n)
	    fellow_cache_seg_associate_n(fcs, fds, n, FCS_USABLE);
}

static void
fellow_cache_segarr_init(struct fellow_cache_seghdr *hdr,
    struct fellow_cache_obj *fco, struct fellow_cache_seg *parent_fcs,
    struct fellow_cache_seg *segs, unsigned n, enum fcos_state state)
{
	const struct fellow_cache_seg *seg0;
	unsigned idx;

	AN(hdr);
	AZ(hdr->fco);
	AZ(hdr->disk_segs);
	hdr->fco = fco;
	hdr->parent_fcs = parent_fcs;

	// check that the hdr immediately preceeds the segment array
	seg0 = (void *)(hdr + 1);
	assert(seg0 == segs);

	for (idx = 0; idx < n; idx++)
		fellow_cache_seg_init(segs + idx, idx, state);
	assert_cache_seg_consistency(segs);
}

static inline void
fellow_cache_seglist_init_stub(struct fellow_cache_seglist *fcsl,
    struct fellow_cache_obj *fco, uint16_t idx)
{

	INIT_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fcsl->idx = idx;
	fellow_cache_segarr_init(&fcsl->fcshdr, fco, NULL, fcsl->fcs, 1, FCL_INIT);
}

static struct fellow_cache_seglist *
fellow_cache_seglist_init(void *ptr, size_t sz, struct fellow_cache_obj *fco, uint16_t idx)
{
	struct fellow_cache_seglist *fcsl = ptr;
	size_t lsegs;

	AN(ptr);
	assert(PAOK(ptr));
	assert(sz > sizeof *fcsl);

	sz -= sizeof *fcsl;
	lsegs = sz / sizeof *fcsl->segs;
	assert(lsegs > 0);
	assert(lsegs <= UINT16_MAX);
	fellow_cache_seglist_init_stub(fcsl, fco, idx);
	fcsl->lsegs = (uint16_t)lsegs;
	fellow_cache_segarr_init(&fcsl->segshdr, fco, fcsl->fcs, fcsl->segs, fcsl->lsegs, FCS_INIT);
	return (fcsl);
}

static void
fellow_disk_seglist_fini(struct fellow_disk_seglist *fdsl)
{
	unsigned u;

	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	assert(fdsl->version == 1);
	// AN(fdsl->nsegs); // not possible for the embedded seglist
	assert(fdsl->nsegs <= fdsl->lsegs);
	// no zero segments active
	for (u = 0; u < fdsl->nsegs; u++) {
		AN(fdsl->segs[u].seg.off);
		AN(fdsl->segs[u].seg.size);
	}
	fh(fdsl->fht, fdsl->fh,
	    (char *)fdsl + FELLOW_DISK_SEGLIST_CHK_START,
	    FELLOW_DISK_SEGLIST_CHK_LEN(fdsl));
}

#define fdslwrong(x) return(FC_ERRSTR("disk seglist wrong " x));

static const char *
fellow_disk_seglist_check(const struct fellow_disk_seglist *fdsl)
{

	AN(fdsl);
	if (FC_INJ || fdsl->magic != FELLOW_DISK_SEGLIST_MAGIC)
		fdslwrong("magic");
	if (fdsl->version != 1)
		fdslwrong("version");
	if (fdsl->nsegs > fdsl->lsegs)
		fdslwrong("nsegs");
	if (fdsl->fht >= FH_LIM)
		fdslwrong("hash type (>= FH_LIM)");
	if (! fh_name[fdsl->fht])
		fdslwrong("hash type (support missing)");
	if (FC_INJ || fhcmp(fdsl->fht, fdsl->fh,
		(char *)fdsl + FELLOW_DISK_SEGLIST_CHK_START,
		FELLOW_DISK_SEGLIST_CHK_LEN(fdsl)))
		fdslwrong("chksum");
	return (NULL);
}

/*
 * seglist async load
 *
 * (ITER): Iterator
 * (ASYN): Async thread
 *
 * x		-> (ITER) insert tentative, start mem allocation -> FCL_INIT
 * FCL_INIT	-> (ITER) get mem -> FCL_DISK
 * FCL_DISK	-> (ITER) issue IO -> FCL_READING
 * FCL_READING	-> (ASYN) when done -> FCL_CHECK
 * FCL_CHECK	-> (ITER) check fdsl, request memory, init fcsl, replace tentative -> FCL_INCORE
 *
 * FCL_INIT has sub-state marked by otherwise invalid fcsl_sz values
 * - 1 already waiting for mem
 *
 * FCL_CHECK has sub-states marked by otherwise invalid fcsl_sz values
 * - 1 req_mem & checking fdsl (unlocked)
 * - 2 fdsl ok, waiting for mem
 *
 * - memory requested and
 */

#define FCL_INIT_NIL		(size_t)0
#define FCL_INIT_WAITMEM	(size_t)1

#define FCL_CHECK_NIL		(size_t)0
#define FCL_CHECK_WORKING	(size_t)0x11
#define FCL_CHECK_WAITMEM	(size_t)0x12
#define FCL_CHECK_INSERT	(size_t)0x13


static inline struct fellow_cache_seglist *
fcsc_tfcsl_insert(struct fellow_cache_seglist *fcsl,
    struct fellow_cache_seglist *next)
{

	AN(next);

	AZ(next->magic);
	fellow_cache_seglist_init_stub(next, FCSL_FCO(fcsl), fcsl->idx + 1);
	VLIST_INSERT_AFTER(fcsl, next, list);
	return (next);
}

static inline void
fcsc_tfcsl_fds(struct fellow_cache_seglist *tfcsl, struct fellow_disk_seg *tfds,
    struct buddy_off_extent reg)
{

	CHECK_OBJ_NOTNULL(tfcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	AZ(tfds->magic);
	INIT_OBJ(tfds, FELLOW_DISK_SEG_MAGIC);
	tfds->seg = reg;
	tfcsl->fcshdr.disk_segs = tfds;
}

// zero out the tentative fcsl to signal we are done with it
static void
fcsc_tfcsl_zero(struct fellow_cache_seglist *tfcsl)
{
	memset(tfcsl->fcshdr.disk_segs, 0, sizeof *tfcsl->fcshdr.disk_segs);
	memset(tfcsl, 0, sizeof *tfcsl);
}

static struct fellow_cache_seglist *
fcsc_fcsl_next1_locked(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait);
static int
fcoid_need_deref(const struct fcoi_deref *fcoid);
static int
fellow_cache_obj_iter_flush_deref(struct fcoi_deref *fcoid);

/*
 * before we send a cursor iterator to wait on some other event
 * (IO, memory allocation, segment unbusy), ensure that we submit
 * all pending IO, flush data to the client and return all memory
 * we still hold onto
 *
 * XXX this probably needs some restructuring and cleanup
 */
static inline int
fcsc_chores_to_do(const struct fcscursor *c)
{
	return (c->fcio->n ||
	    (c->fcoid != NULL && fcoid_need_deref(c->fcoid)));
}

static void
fcsc_chores_unlocked(const struct fcscursor *c)
{
	if (! fcsc_chores_to_do(c))
		return;
	if (c->fcio->n)
		fellow_cache_seg_async_read(c->fcio);
	if (c->fcoid != NULL && fcoid_need_deref(c->fcoid))
		(void) fellow_cache_obj_iter_flush_deref(c->fcoid);
}

//lint -sem(fcsc_chores_locked, thread_protected, thread_lock, thread_unlock)
static void
fcsc_chores_locked(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb)
{
	if (! fcsc_chores_to_do(c))
		return;

	fellow_cache_obj_unlock(lcb);
	fcsc_chores_unlocked(c);
	fellow_cache_obj_lock(lcb);
}

/* return values:
 *
 * NULL:	 final
 * fcsl_pending: needs to be called again
 * other:	 final, with reference gained
 */
static struct fellow_cache_seglist *
fcsc_fcsl_next_locked(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait)
{
	struct fellow_cache_seglist *fcsl, *next;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cursor_io *fcio;
	struct buddy_reqs *reqs;

    again:
	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);
	fcsl = c->fcsl;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = FCSL_FDSL(fcsl);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	fcio = c->fcio;
	CHECK_OBJ_NOTNULL(fcio, FELLOW_CURSOR_IO_MAGIC);

	next = VLIST_NEXT(c->fcsl, list);
	CHECK_OBJ_ORNULL(next, FELLOW_CACHE_SEGLIST_MAGIC);
	/*
	 * as long as our current segment is FCL_BUSY, we must not even try to
	 * read the next, because it might not have been created yet
	 *
	 */
	if (next == NULL &&
	    (fcsl->fcs->state == FCL_BUSY ||
	     fcsl->fcs->state == FCL_EMBED_BUSY)) {
		if (wait)
			goto wait;
		DBG("ret current busy %p", fcsl_pending);
		return (fcsl_pending);
	}
	/*
	 * do not reference a BUSY fcsl before it has at least some data,
	 * because otherwise it can still go away (see
	 * fellow_busy_obj_trimstore() -> fellow_busy_obj_trim_seglists()
	 */
	if (next != NULL &&
	    (next->fcs->state == FCL_BUSY || next->fcs->state == FCL_EMBED_BUSY) &&
	    (FCSL_FDSL(next)->nsegs == 0 || next->segs[0].u.fcs.len == 0)) {
		if (wait)
			goto wait;
		DBG("ret next busy %p", fcsl_pending);
		return (fcsl_pending);
	}

	if ((next == NULL && fdsl->next.size > 0) ||
	    (next != NULL && next->idx > fcsl->idx + 1)) {
		reqs = &fcio->mem.reqs;
		AN(buddy_req_extent(reqs, fdsl->next.size, 0));
		(void) buddy_alloc_async(reqs);
		next = fcsc_tfcsl_insert(fcsl, &fcio->tentative);
		next->fcs->u.fcl.reqs = reqs;
		fcsc_tfcsl_fds(next, &fcio->tentative_fds, fdsl->next);
	}
	if (next == NULL)
		return (NULL);
	return (fcsc_fcsl_next_finish(c, lcb, wait));
    wait:
	if (fcsc_chores_to_do(c))
		fcsc_chores_locked(c, lcb);
	else {
		fellow_cache_lru_chgbatch_apply(lcb);
		fellow_cache_seg_wait_locked(fcsl->fcs);
	}
	goto again;
}

//lint -sem(fcsc_fcsl_next_finish, thread_protected)
static struct fellow_cache_seglist *
fcsc_fcsl_next_finish(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait)
{
	struct fellow_cache_seglist *fcsl, *next;

	fcsl = c->fcsl;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);

	next = fcsc_fcsl_next1_locked(c, lcb, wait);
	while (wait && next == fcsl_pending) {
		if (fcsc_chores_to_do(c))
			fcsc_chores_locked(c, lcb);
		else {
			fellow_cache_lru_chgbatch_apply(lcb);
			fellow_cache_seg_wait_locked(fcsl->fcs);
		}
		next = fcsc_fcsl_next1_locked(c, lcb, wait);
	}
	return (next);
}

/*
 * work one fcsl_next transction.
 *
 * return values:
 *
 * NULL:	 final
 * fcsl_pending: if called with wait == 1, wait for signal, call again
 * other:	 final, with reference gained
 */
//lint -sem(fcsc_fcsl_next1_locked, thread_protected, thread_lock, thread_unlock)
static struct fellow_cache_seglist *
fcsc_fcsl_next1_locked(const struct fcscursor *c,
    struct fellow_lru_chgbatch *lcb, unsigned wait)
{
	struct fellow_cache_seglist *fcsl, *next, *final;
	struct fellow_cursor_io *fcio;
	struct buddy_ptr_extent fcsl_mem;
	struct buddy_reqs *reqs;
	const char *err;
	unsigned u;

	//lint --e{456} combined with different...

	CHECK_OBJ_NOTNULL(c, FCSC_MAGIC);
	fcsl = c->fcsl;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fcio = c->fcio;
	CHECK_OBJ_NOTNULL(fcio, FELLOW_CURSOR_IO_MAGIC);
	assert(lcb->fco == FCSL_FCO(fcsl));

	next = VLIST_NEXT(c->fcsl, list);
	if (next == NULL)
		return (NULL);
	CHECK_OBJ_NOTNULL(next, FELLOW_CACHE_SEGLIST_MAGIC);

	switch (next->fcs->state) {
	case FCL_INIT:
		reqs = next->fcs->u.fcl.reqs;
		switch (next->fcsl_sz) {
		case FCL_INIT_WAITMEM:
			return (fcsl_pending);
		case FCL_INIT_NIL:
			u = buddy_alloc_async_ready(reqs);
			if (u == 0 && wait == 0)
				return (fcsl_pending);
			if (u == 0) {
				next->fcsl_sz = FCL_INIT_WAITMEM;
				fellow_cache_obj_unlock(lcb);
				fcsc_chores_unlocked(c);
				u = buddy_alloc_async_wait(reqs);
				fellow_cache_obj_lock(lcb);
				// cursor must not change under our feet
				assert(c->fcsl == fcsl);
			}
			AN(u);
			next->fcs->alloc = buddy_get_ptr_extent(reqs, 0);
			buddy_alloc_async_done(reqs);
			fellow_cache_seg_transition_locked(NULL, next->fcs,
			    FCL_INIT, FCL_DISK);
			next->fcsl_sz = 0;
			goto fcl_disk;
		default:
			WRONG("FCL_INIT substate");
		}
	case FCL_DISK:
	    fcl_disk:
		assert(next->fcs->state == FCL_DISK);
		fcio_add(c->fcio, lcb, next->fcs);	// -> FCL_READING
		(void) fellow_cache_seg_ref_locked(NULL, next->fcs);
		// submission happens outside, so we need to return in all cases
		return (fcsl_pending);

	case FCL_READING:
		return (fcsl_pending);

	case FCL_CHECK: {
		assert(next->fcs->state == FCL_CHECK);
		switch (next->fcsl_sz) {
		case FCL_CHECK_NIL:
			next->fcsl_sz = FCL_CHECK_WORKING;
			(void) fellow_cache_seg_deref_locked(NULL, next->fcs);
			fellow_cache_obj_unlock(lcb);

			// unlocked
			err = fellow_disk_seglist_check(FCSL_FDSL(next));
			if (err) {
				fellow_cache_obj_lock(lcb);
				(void) fellow_cache_obj_res(fcio->fc, FCSL_FCO(fcsl),
				    FCR_IOFAIL(err));
				// centralize error handling in top switch
				next->fcs->state = FCL_READFAIL;
				goto fcl_readfail;
			}

			reqs = next->fcs->u.fcl.reqs;
			AN(buddy_req_extent(reqs, SEGLIST_SIZE(next, FCSL_FDSL(next)->nsegs), 0));
			(void) buddy_alloc_async(reqs);
			u = buddy_alloc_async_ready(reqs);
			if (u == 0 && wait) {
				fcsc_chores_unlocked(c);
				u = buddy_alloc_async_wait(reqs);
			}
			if (u == 0) {
				fellow_cache_obj_lock(lcb);
				// no other thread must interfere (see below)
				assert(next->fcs->state == FCL_CHECK);
				assert(next->fcsl_sz == FCL_CHECK_WORKING);
				next->fcsl_sz = FCL_CHECK_WAITMEM;
				goto fcl_check_waitmem;
			}
			goto insert_unlocked;

		case FCL_CHECK_WAITMEM:
		    fcl_check_waitmem:
			assert(next->fcs->state == FCL_CHECK);
			assert(next->fcsl_sz == FCL_CHECK_WAITMEM);
			reqs = next->fcs->u.fcl.reqs;
			u = buddy_alloc_async_ready(reqs);
			if (u == 0 && wait == 0)
				return (fcsl_pending);
			next->fcsl_sz = FCL_CHECK_INSERT;
			fellow_cache_obj_unlock(lcb);

			if (u == 0) {
				fcsc_chores_unlocked(c);
				u = buddy_alloc_async_wait(reqs);
			}
			AN(u);
			goto insert_unlocked;

		case FCL_CHECK_WORKING:
		case FCL_CHECK_INSERT:
			/* another thread is at it */
			return (fcsl_pending);

		    insert_unlocked:
			reqs = next->fcs->u.fcl.reqs;
			fcsl_mem = buddy_get_ptr_extent(reqs, 0);
			buddy_alloc_async_done(reqs);
			final = fellow_cache_seglist_init(fcsl_mem.ptr, fcsl_mem.size,
			    FCSL_FCO(fcsl), fcsl->idx + 1);
			final->fcsl_sz = fcsl_mem.size;
			// fdsl memory
			final->fcs->alloc = next->fcs->alloc;
			fellow_cache_seglist_associate(final, FCSL_FDSL(next), FCS_DISK);

			// locked
			fellow_cache_obj_lock(lcb);
			VLIST_REMOVE(next, list);

			AZ(next->fcs->refcnt);
			fcsc_tfcsl_zero(next);

			next = final;
			VLIST_INSERT_AFTER(fcsl, next, list);
			next->fcs->state = FCL_CHECK;
			(void) fellow_cache_seg_ref_locked(NULL, next->fcs);
			fellow_cache_seg_transition_locked(NULL, next->fcs, FCL_CHECK, FCL_INCORE);
			AZ(pthread_cond_broadcast(&FCSL_FCO(next)->cond));
			return (next);
		default:
			WRONG("FCL_CHECK substate (fcsl_sz)");
		}
		//WRONG("fcl_check break not allowed");
	}
	case FCL_BUSY:
	case FCL_WRITING:
	case FCL_MEM:
	case FCL_INCORE:
		(void) fellow_cache_seg_ref_locked(lcb, next->fcs);
		return (next);
	case FCL_EMBED_BUSY:
	case FCL_EMBED:
		WRONG("the embedded fcsl can not be the next fcsl");
	case FCL_READFAIL:
	    fcl_readfail:
		assert(next->fcs->state == FCL_READFAIL);
		buddy_return1_ptr_extent(fcio->fc->membuddy,
		    &next->fcs->alloc);
		VLIST_REMOVE(next, list);
		AZ(pthread_cond_broadcast(&FCSL_FCO(next)->cond));
		fcsc_tfcsl_zero(next);
		return (NULL);
	case FCL_REDUNDANT:
		return (NULL);
	default:
		WRONG("fcl state");
	}
	//WRONG("break not allowed");

}

/*
 * MEM LAYOUT
 *
 *     fellow_disk_obj
 * ? x va_data
 *     fellow_disk_seglist
 * ? x fellow_disk_seg
 * dsk_sz = sum(^^)
 *
 * mem_sz = sum(vv)
 *     fellow_cache_obj
 *     [fellow_cache_seglist]	embedded in cache_obj
 * n x fellow_cache_seg
 *
 * for a call from fellow_busy_obj_alloc, we know how many disk_seg
 * for a call from fellow_cache_obj_prepread, we will guess (0 argument)
 *
 * we make one or two allocations, depending on whether the
 * second part fits in the same LOG2UP or not
 *
 * XXX
 * - collapse allocation with fellow_budy_obj_alloc?
 */

static struct fellow_cache_res
fellow_cache_obj_new(
    struct fellow_cache *fc,
    size_t dsk_sz, unsigned nseg_guess,
    struct buddy_ptr_extent *fbo_mem,
    struct buddy_ptr_page *dowry,
    uint8_t pri)
{
	struct fellow_disk_obj *fdo;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct buddy_reqs *reqs;
	struct buddy_ptr_extent fco_mem;
	struct buddy_ptr_extent fdo_mem;
	size_t mem_sz;
	unsigned u;

	DBG("arg dsk_sz %zu nseg_guess %u", dsk_sz, nseg_guess);

	AN(nseg_guess);

	// for busy objects, mem_sz may be less than MIN_FELLOW_BLOCK
	mem_sz = (size_t)1 << log2up(
	    fellow_cache_obj_with_seglist_size(nseg_guess));
	assert(mem_sz <= MIN_FELLOW_BLOCK);

	// dowry is largest, so we allocate it first
	reqs = BUDDY_REQS_STK(fc->membuddy, 4);
	BUDDY_REQS_PRI(reqs, pri);
	if (dowry != NULL)
		AN(buddy_req_page(reqs, fc->tune->chunk_exponent, 0));
	if (fbo_mem != NULL)
		AN(buddy_req_extent(reqs, sizeof(struct fellow_busy), 0));

	assert(dsk_sz == fellow_rndup(fc->ffd, dsk_sz));
	assert(dsk_sz >= mem_sz);
	AN(buddy_req_extent(reqs, dsk_sz, 0));
	AN(buddy_req_extent(reqs, mem_sz, 0));

	u = buddy_alloc_wait(reqs);
	if (FC_INJ || u != 2 + (dowry ? 1 : 0) + (fbo_mem ? 1 : 0)) {
		buddy_alloc_wait_done(reqs);
		return (FCR_ALLOCFAIL("fellow_cache_obj_new alloc failed"));
	}

	if (dowry != NULL) {
		assert(u >= 3);
		*dowry = buddy_get_next_ptr_page(reqs);
	}
	if (fbo_mem != NULL)
		*fbo_mem = buddy_get_next_ptr_extent(reqs);

	fdo_mem = buddy_get_next_ptr_extent(reqs);
	fco_mem = buddy_get_next_ptr_extent(reqs);

	buddy_alloc_wait_done(reqs);

	fdo = fdo_mem.ptr;
	AN(fdo);

	fco = fco_mem.ptr;
	AN(fco);

	INIT_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	DBG("fco %p", fco);
	fco->fco_mem = fco_mem;
	fco->lru = fellow_cache_get_lru(fc, (uintptr_t)fco);

	AZ(pthread_mutex_init(&fco->mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_cond_init(&fco->cond, NULL));

	fcs = FCO_FCS(fco);
	fellow_cache_segarr_init(&fco->fdo_seghdr, fco, NULL, fcs, 1, FCO_INIT);
	fcs->alloc = fdo_mem;
	fcs->u.fco.fdo = fdo;

	// XXX replace 1 with count of AAs once we have more than 1
#define FDO_AUXATTR(U, l)						\
	fellow_cache_segarr_init(&fco->aa_seghdr, fco, fcs, fco->aa_##l##_seg, 1, FCAA_INIT);
#include "tbl/fellow_obj_attr.h"


	return (fellow_cache_obj_res(fc, fco, FCR_OK(fco)));
}

static void
fellow_busy_fill_segmem(struct buddy_reqs *reqs, const void *priv)
{
	const struct fellow_busy *fbo;
	const struct fellow_cache *fc;
	const struct stvfe_tune *tune;
	unsigned u, bits;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	CAST_OBJ_NOTNULL(fbo, priv, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	bits = fbo->fll.chunk_exponent;
	if (bits == 0)
		bits = tune->chunk_exponent;
	AN(bits);

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_page(reqs, bits, 0));
}

static uint16_t
fellow_cache_seglist_capacity(unsigned bits)
{
	uint16_t segs = fellow_cache_seglist_fit((size_t)1 << bits);
	if (segs > FELLOW_DISK_SEGLIST_MAX_SEGS)
		segs = FELLOW_DISK_SEGLIST_MAX_SEGS;
	return (segs);
}

struct fellow_cache_res
fellow_busy_obj_alloc(struct fellow_cache *fc,
    struct fellow_cache_obj **fcop, uintptr_t *priv2,
    unsigned wsl)
{
	struct fellow_disk_obj *fdo;
	struct fellow_disk_seg *fds;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_res fcr;
	struct fellow_busy *fbo;
	struct buddy *dskbuddy;
	struct buddy_reqs *reqs;
	struct buddy_ptr_extent fbo_mem;
	struct buddy_ptr_page fbo_dowry;
	struct buddy_off_extent fds_seg, dskdowry;
	size_t sz, asz, dsk_sz;
	uint16_t ldsegs;
	unsigned u;

	// XXX peek for known content-length=

	// round up to disk block such that there
	// is space for at least one disk_seg
	wsl = (unsigned)PRNDUP(wsl);
	sz = asz = sizeof(struct fellow_disk_obj) + wsl;
	assert(PAOK(asz));
	sz += sizeof(struct fellow_disk_seglist);
	sz += sizeof(struct fellow_disk_seg);
	assert(PAOK(sz));

	dsk_sz = fellow_rndup(fc->ffd, sz);
	assert(PAOK(dsk_sz));
	assert(dsk_sz >= sz);

	ldsegs = fellow_disk_seglist_fit(dsk_sz - asz);
	if (ldsegs > FDO_MAX_EMBED_SEGS)
		ldsegs = FDO_MAX_EMBED_SEGS;

//	DBG("-> %u embedded segs", ldsegs);

	// uring has signed 32bit length
	assert(dsk_sz <= FIO_MAX);

	// XXX delay to allow new objects during FP_INIT ?
	dskbuddy = fellow_dskbuddy(fc->ffd);

	/*
	 * cram == 0:
	 * the dskdowry acts as an admission ticket to creating an
	 * expensive busy object: We need to motivate dsk LRU to
	 * start makeing room if it needs to before we allocate
	 * memory which LRU would need
	 */
	reqs = BUDDY_REQS_STK(dskbuddy, 2);
	BUDDY_REQS_PRI(reqs, FEP_NEW);

	sz = (size_t)1 << (fc->tune->chunk_exponent);
	AN(buddy_req_extent(reqs, sz, 0));
	AN(buddy_req_extent(reqs, dsk_sz, 0));

	u = buddy_alloc_wait(reqs);
	if (u == 0) {
		buddy_alloc_wait_done(reqs);
		fcr = FCR_ALLOCFAIL("dsk dowry and fds");
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}
	assert(u == 2);

	fds_seg = FC_INJ ? buddy_off_extent_nil :
	    buddy_get_off_extent(reqs, 1);

	dskdowry = FC_INJ ? buddy_off_extent_nil :
	    buddy_get_off_extent(reqs, 0);
	buddy_alloc_wait_done(reqs);

	if (fds_seg.off < 0) {
		if (dskdowry.off >= 0)
			buddy_return1_off_extent(dskbuddy, &dskdowry);
		fcr = FCR_ALLOCFAIL("fds region");
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}

	fcr = fellow_cache_obj_new(fc, dsk_sz, ldsegs, &fbo_mem, &fbo_dowry,
	    FEP_NEW);
	if (fcr.status != fcr_ok) {
		fellow_cache_res_check(fc, fcr);
		buddy_return1_off_extent(dskbuddy, &fds_seg);
		if (dskdowry.off >= 0)
			buddy_return1_off_extent(dskbuddy, &dskdowry);
		return (fcr);
	}
	fco = fcr.r.ptr;

	CHECK_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	fcs = FCO_FCS(fco);
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	fbo = fbo_mem.ptr;
	AN(fbo);
	INIT_OBJ(fbo, FELLOW_BUSY_MAGIC);
	fbo->fbo_mem = fbo_mem;
	fbo->segdowry = fbo_dowry;
	BUDDY_REQS_INIT(&fbo->bbrr, dskbuddy);
	fbo->segdskdowry = dskdowry;

	// disk object in memory
	fdo = fellow_disk_obj(fcs);
	memset(fdo, 0, fcs->alloc.size);
	fdo->magic = FELLOW_DISK_OBJ_MAGIC;
	fdo->version = 1;
	fdo->va_data_len = wsl;
	fdo->fdo_flags = FDO_F_VXID64;

	// disk obj init (allocation happened before)
	fds = &fdo->fdo_fds;
	fellow_disk_seg_init(fds, fc->tune->hash_obj);

	fds->seg = fds_seg;

	fco->fdo_seghdr.disk_segs = fds;
	fco->fdb = region_fdb(&fds->seg);
	AN(fcop);
	*fcop = fco;
	AN(priv2);
	*priv2 = fco->fdb.fdb;

	fbo->va_data = fdo->va_data;

	// XXX adust for multiple aa segments
#define FDO_AUXATTR(U, l)						\
	fco->aa_seghdr.disk_segs = &fdo->aa_##l##_seg;			\
	fellow_disk_seg_init(&fdo->aa_##l##_seg, fc->tune->hash_obj);	\
	fellow_cache_seg_associate(fco->aa_##l##_seg,			\
	    &fdo->aa_##l##_seg, FCAA_USABLE);
#include "tbl/fellow_obj_attr.h"

	// we prepare the state the object will have
	// when inserted -- 2nd ref is for write fellow_cache_async_complete()
	fcs->refcnt = 2;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_BUSY);

	fdsl = fellow_disk_seglist_init(fellow_disk_obj_fdsl(fdo),
	    ldsegs, fc->tune->hash_obj);
	assert(PAOK(fdsl));

	// FDO_MAX_EMBED_SEGS ensures this
	sz = fellow_cache_seglist_size(ldsegs);
	assert(sizeof *fco + sz <= fco->fco_mem.size);
	fcsl = fellow_cache_seglist_init((void *)(fco + 1), sz, fco, 0);
	AN(fcsl);
	VLIST_INSERT_HEAD(&fco->fcslhead, fcsl, list);
	fellow_cache_seglist_associate(fcsl, fdsl, FCS_USABLE);
	fellow_cache_seg_transition_locked_notincore(fcsl->fcs, FCL_EMBED_BUSY);
	(void) fellow_cache_seg_ref_locked(NULL, fcsl->fcs);
//	DBG("fdsl lsegs %u fcsl lsegs %u", fdsl->lsegs, fco->fcsl->lsegs);

	fbo->fc = fc;
	fbo->fco = fco;
	fbo->body_seglist = fcsl;
	AZ(fbo->body_seg);

	AN(fbo->body_seglist);
	AN(fbo->body_seglist->lsegs);
	AN(FCSL_FDSL(fbo->body_seglist)->lsegs);
	AZ(FCSL_FDSL(fbo->body_seglist)->nsegs);

	/* keep one io outstanding until unbusy is called
	 * to prevent the object from completing to FCO_INCORE
	 *
	 * fellow_cache_obj_unbusy does not take an IO, it owns this one
	 */
	fbo->io_outstanding = 1;

	BUDDY_POOL_INIT(fbo->segmem, fc->membuddy, FEP_SPC,
	    fellow_busy_fill_segmem, fbo);
	buddy_return1_ptr_page(fc->membuddy, &fbo->segdowry);

	return (FCR_OK(fbo));
}

/* commit a region to the busy object */

static struct buddy_off_extent *
fellow_busy_region_commit(struct fellow_busy *fbo, struct buddy_off_extent reg)
{
	struct buddy_off_extent *fdr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	if (reg.off < 0) {
		return (NULL);
	}

	AN(reg.size);
	AZ(reg.size & FELLOW_BLOCK_ALIGN);

	assert(fbo->nregion < FCO_MAX_REGIONS);
	fdr = &fbo->region[fbo->nregion++];
	*fdr = reg;

	return (fdr);
}

/* allocate a new region for body, aux attr or seglist
 *
 * aux attr use: We do not yet coalesce allocations because
 * there only is a single aux attr, so the one segment gets
 * its own region
 */
static struct buddy_off_extent *
fellow_busy_region_alloc(struct fellow_busy *fbo, size_t size, int8_t cram)
{
	const struct fellow_cache *fc;
	struct buddy_off_extent *r;
	struct buddy_reqs *reqs;
	struct buddy *dskbuddy;
	unsigned u;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	dskbuddy = fellow_dskbuddy(fc->ffd);
	reqs = BUDDY_REQS_STK(dskbuddy, 1);
	BUDDY_REQS_PRI(reqs, FEP_SPC);

	AN(size);
	AN(buddy_req_extent(reqs, size, cram));

	if (FC_INJ)
		return (NULL);

	u = buddy_alloc_wait(reqs);
	assert(u == 1);

	r = fellow_busy_region_commit(fbo, buddy_get_off_extent(reqs, 0));
	buddy_alloc_wait_done(reqs);

	return (r);
}

/*
 * return an unused region for seglist trim
 */
static void
fellow_busy_region_free(struct fellow_busy *fbo, struct buddy_off_extent *fdr)
{
	struct fellow_cache *fc;
	ssize_t u;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	u = fdr - &fbo->region[0];
	if (! (u >= 0 && u < fbo->nregion)) {
		for (u = 0; u < fbo->nregion; u++)
			if (fbo->region[u].off == fdr->off)
				break;
		assert(u < fbo->nregion);
		assert(fbo->region[u].size == fdr->size);
		fbo->region[u] = buddy_off_extent_nil;
	}
	else
		assert(&fbo->region[u] == fdr);

	buddy_return1_off_extent(fellow_dskbuddy(fc->ffd), fdr);

	if (u == --fbo->nregion)
		return;

	memmove(&fbo->region[u], &fbo->region[u + 1],
	    (fbo->nregion - (size_t)u) * sizeof *fdr);
}

/*
 * ============================================================
 * segment plan
 *
 * PLANNING GOALs
 * - what is the minimum disk region pow2 (bits)
 * - what is the minimum segment pow2 (chunk_bits) -> determines fdsl needed
 *
 * calc minimum number of disk regions:
 * c-l     : number of 1 bits over 12
 * chunked : number of bits over 12
 * remaining is number of seglists
 * determine chunk_bits to fit this number of seglists
 * number of seglists determines disk regions available
 * this determines minumum disk region pow2
 *
 * track number of regions available and (hypothetical) number of segments
 * available, limit cram accordingly
 */

/*
 * plan which chunk_exponent for segments we need to fit the
 * available regions (one segment list uses one region)
 *
 * segment lists should use the same size as segments
 *
 * avail is the number of already available segments (from the embedded
 * segment list)
 *
 */
#ifdef TEST_DRIVER
unsigned long long n_plan_seg = 0;
#endif

static unsigned
fellow_plan_seg(size_t sz, unsigned bits, unsigned avail, unsigned *reg_remaining)
{
	size_t nseg, nsegl;
	uint16_t cap;

#ifdef TEST_DRIVER
	n_plan_seg++;
#endif

	AN(reg_remaining);
	AN(*reg_remaining + avail);

	assert(bits >= MIN_FELLOW_BITS);
	do {
		nseg = rup_min(sz, bits) >> bits;
		if (avail > nseg)
			nseg = 0;
		else
			nseg -= avail;
		cap = fellow_cache_seglist_capacity(bits);
		AN(cap);
		nsegl = (nseg + (cap - 1)) / cap;
		bits++;
	} while (nseg > 0 && nsegl > *reg_remaining);

	bits--;

	// XXX should double check that this can not happen
	assert(bits <= FIO_MAX_BITS);

	DBG("sz %zu avail %u reg_remaining %u -> bits %u nsegl %zu "
	    "(nseg %zu cap %u) -> remaining %zu",
	    sz, avail, *reg_remaining, bits, nsegl,
	    nseg, cap, *reg_remaining - nsegl);

	assert(nsegl <= UINT_MAX);
	assert(*reg_remaining >= nsegl);
	*reg_remaining -= (unsigned)nsegl;
	return (bits);
}

#ifdef TEST_DRIVER
unsigned long long n_plan_layout = 0;
#endif

static struct fellow_layout_limits
fellow_plan_layout(size_t sz, unsigned chunk_bits, int8_t cram, unsigned segs_avail,
    unsigned reg_remaining, unsigned growing)
{
	struct fellow_layout_limits fll = {0};
	unsigned c, bits, cancram;
	int8_t bc, cc;

#ifdef TEST_DRIVER
	n_plan_layout++;
#endif

	AN(sz);
	sz = FELLOW_BLOCK_RNDUP(sz);
	AZ(sz & FELLOW_BLOCK_ALIGN);

	assert(chunk_bits >= MIN_FELLOW_BITS);
	assert(reg_remaining > 0);
	assert(segs_avail > 0 || reg_remaining > 1);

	bits = log2up(sz);

	if (reg_remaining <= FCO_REGIONS_RESERVE) {
		c = 1;
		cram = 0;
	}
	else if (growing)
		c = (1 + bits) - MIN_FELLOW_BITS;
	else
		c = popcount(sz);

	if (c >= reg_remaining) {
		if (segs_avail > 0)
			c = reg_remaining;
		else
			c = reg_remaining - 1;
	}

	AN(c);
	DBG("sz 0x%zx popcount %d", sz, c);

	// c is now an upper bound for needed body regions
	assert(c <= reg_remaining);
	reg_remaining -= c;
	assert(segs_avail > 0 || reg_remaining > 0);

	// fit seglists
	fll.chunk_exponent = fellow_plan_seg(sz, chunk_bits, segs_avail,
	    &reg_remaining);

	if (fll.chunk_exponent > chunk_bits)
		chunk_bits = fll.chunk_exponent;

	// for growing, there is no last segment for which we could have
	// a smaller allocation. If we can't cram, we round up becuase
	// this is what buddy does anyway - a superfluous allocation will
	// be trimmed later
	// if not growing and sz is small, it's the last segment
	if (growing)
		sz = rup_min(sz, chunk_bits);
	else if (sz <= (size_t)1 << chunk_bits) {
		fll.disk_size = sz;
		return (fll);
	}


	if (cram == 0 || sz >> FIO_MAX_BITS) {
		bits = log2up(sz);
		sz = (size_t)1 << bits;
		fll.disk_size = sz;
		return (fll);
	}

	AN(cram);

	bits = log2down(sz);
	if ((size_t)1 << bits != sz) {
		// allocating by log2down() implies that we already cram by one,
		cram += cram > 0 ? -1 : 1;
	}

	fll.disk_size = (size_t)1 << bits;
	DBG("final disk_size log2down %zu", fll.disk_size);

	// determine cram limit by regions remaining
	reg_remaining += c;

	sz = rup_min(sz, chunk_bits);
	c = popcount(sz);
	DBG("sz 0x%zx reg_remainig %u popcount2 %d", sz, reg_remaining, c);

	if (reg_remaining <= c + 1)
		return (fll);

	// cram rules:
	// - not below chunk_exponent
	// - only within cram parameter

	cancram = log2down((size_t)reg_remaining - c);
	assert(cancram < INT8_MAX);
	cc = (int8_t)cancram;
	if (cc < abs(cram))
	    cram = (cram < 0 ? 0 - cc : cc);

	assert(bits < INT8_MAX);
	bc = (int8_t)bits;
	assert(chunk_bits < INT8_MAX);
	cc = (int8_t)chunk_bits;

	fll.disk_cram = buddy_cramlimit_page_minbits(bc, cram, cc);

	DBG("final disk_cram %d", fll.disk_cram);

	return (fll);
}


#ifdef TEST_DRIVER
static uint16_t TEST_LIMIT_LDSEGS = 0;
static unsigned TEST_LIMIT_SEG_BITS = 0;
#endif

static struct fellow_cache_res
fellow_disk_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl, uint16_t ldsegs, uint8_t fht);

static struct fellow_cache_res
fellow_busy_body_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl)
{
	struct fellow_cache *fc;
	unsigned chunk_exponent;
	uint16_t ldsegs;
	size_t sz;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	chunk_exponent = fbo->fll.chunk_exponent;
	if (chunk_exponent == 0)
		chunk_exponent = fc->tune->chunk_exponent;
	ldsegs = fellow_cache_seglist_capacity(chunk_exponent);
	if (fbo->growing == 0) {
		sz = fbo->sz_estimate - fbo->sz_returned;
		sz += (((size_t)1 << chunk_exponent) - 1);
		sz >>= chunk_exponent;
		if (sz < ldsegs)
			ldsegs = (uint16_t)sz;
	}

#ifdef TEST_DRIVER
	if (TEST_LIMIT_LDSEGS > 0)
		ldsegs = TEST_LIMIT_LDSEGS;
#endif

	return (
	    fellow_disk_seglist_alloc(fbo, ofcsl,
	      ldsegs, fc->tune->hash_obj));
}

// returns a busy io to submit outside the lock, if any
static struct fellow_busy_io *
fellow_cache_seglist_unbusy_locked(struct fellow_busy *fbo,
    struct fellow_busy_io *fbiostk, struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seglist *fcsl)
{
	struct fellow_cache_seglist *prev;
	struct fellow_busy_io *fbio = NULL;

	if (fcsl->fcs->state == FCL_EMBED_BUSY) {
		fellow_cache_seg_transition_locked(lcb, fcsl->fcs, FCL_EMBED_BUSY, FCL_EMBED);
		if (fellow_cache_seg_deref_locked(lcb, fcsl->fcs))
			AZ(pthread_cond_broadcast(&fbo->fco->cond));
	}
	else {
		fbo->io_outstanding++;
		fbio = fellow_busy_io_get(fbo, fbiostk);

		// can access because parent_fcs
		prev = VLIST_PREV(fcsl, &fbo->fco->fcslhead, fellow_cache_seglist, list);
		AN(prev);
		assert(prev->fcs == fcsl->fcshdr.parent_fcs);
		assert(prev->idx == fcsl->idx - 1);

		AN(fbio);
		fbio->type = FBIO_SEGLIST;
		fbio->u.seglist.reg = FCSL_FDSL(prev)->next;
		fbio->u.seglist.fcsl = fcsl;

		fellow_cache_seg_transition_locked(lcb, fcsl->fcs, FCL_BUSY, FCL_WRITING);
		fcsl->fcshdr.parent_fcs = NULL;

		AN(fcsl->fcs->refcnt);
		if (fcsl->fcs->refcnt > 1)
			AZ(pthread_cond_broadcast(&fbo->fco->cond));
	}
	return (fbio);
}

static struct fellow_cache_res
fellow_disk_seglist_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seglist *ofcsl, uint16_t ldsegs, uint8_t fht)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *ofdsl, *fdsl;
	struct buddy_off_extent *fdr;
	struct buddy_ptr_extent fdsl_mem, fcsl_mem;
	struct buddy_reqs *reqs;
	unsigned u;
	size_t sz;

	FELLOW_LRU_CHGBATCH(lcb, fbo->fco, 2);

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	CHECK_OBJ_NOTNULL(ofcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	AZ(VLIST_NEXT(ofcsl, list));

	ofdsl = FCSL_FDSL(ofcsl);
	AZ(ofdsl->next.off);
	AZ(ofdsl->next.size);

	AN(ldsegs);
	sz = SEGLIST_SIZE(fdsl, ldsegs);

	fdr = fellow_busy_region_alloc(fbo, sz, 0);
	if (fdr == NULL)
		return (FCR_ALLOCFAIL("seglist disk region"));

	assert(fellow_disk_seglist_fit(fdr->size) >= ldsegs);

	reqs = BUDDY_REQS_STK(fbo->fc->membuddy, 2);
	BUDDY_REQS_PRI(reqs, FEP_META);
	AN(buddy_req_extent(reqs, fdr->size, 0));
	AN(buddy_req_extent(reqs, SEGLIST_SIZE(fcsl, ldsegs), 0));
	u = buddy_alloc_wait(reqs);
	if (FC_INJ || u != 2) {
		buddy_alloc_wait_done(reqs);
		fellow_busy_region_free(fbo, fdr);
		return (FCR_ALLOCFAIL("seglist memory"));
	}

	fdsl_mem = buddy_get_ptr_extent(reqs, 0);
	fcsl_mem = buddy_get_ptr_extent(reqs, 1);

	buddy_alloc_wait_done(reqs);

	assert(fdsl_mem.size == fdr->size);
	fdsl = fellow_disk_seglist_init(fdsl_mem.ptr, ldsegs, fht);

	AN(fcsl_mem.ptr);
	fcsl = fellow_cache_seglist_init(fcsl_mem.ptr, fcsl_mem.size, fbo->fco,
	    ofcsl->idx + 1);
	fcsl->fcs->alloc = fdsl_mem;

	fellow_cache_seglist_associate(fcsl, fdsl, FCS_USABLE);

	fcsl->fcsl_sz = fcsl_mem.size;

	struct fellow_busy_io *fbio = NULL, fbiostk[1];

	AZ(pthread_mutex_lock(&fbo->fco->mtx));

	ofdsl->next = *fdr;

	// FCL_BUSY references the previous because it needs the disk region
	fcsl->fcshdr.parent_fcs = ofcsl->fcs;
	fellow_cache_seg_transition_locked(lcb, fcsl->fcs, FCL_INIT, FCL_BUSY);
	(void) fellow_cache_seg_ref_locked(lcb, fcsl->fcs);

	VLIST_INSERT_AFTER(ofcsl, fcsl, list);

	if (fbo->unbusy_seglist != NULL)
		fbio = fellow_cache_seglist_unbusy_locked(fbo, fbiostk, lcb, fbo->unbusy_seglist);
	fbo->unbusy_seglist = ofcsl;

	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(ofcsl->fcs);
	assert_cache_seg_consistency(fcsl->fcs);
	AZ(pthread_mutex_unlock(&fbo->fco->mtx));

	if (fbio == NULL)
		return (FCR_OK(fcsl));

	fellow_disk_seglist_fini(FCSL_FDSL(fbio->u.seglist.fcsl));
	fellow_busy_io_submit(fbo->fc, &fbio, 1);

	return (FCR_OK(fcsl));
}

/*
 * Return a size to allocate next for the object body
 */
static struct fellow_layout_limits
fellow_busy_body_size_strategy(const struct fellow_busy *fbo)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fcsl = fbo->body_seglist;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = FCSL_FDSL(fcsl);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fdsl->nsegs <= fdsl->lsegs);

	assert(fbo->sz_estimate > fbo->sz_dskalloc);

	return(fellow_plan_layout(
	    fbo->sz_estimate - fbo->sz_dskalloc,
	    fbo->fc->tune->chunk_exponent,
	    fbo->fc->tune->cram,
	    fdsl->lsegs - fdsl->nsegs,
	    FCO_MAX_REGIONS - fbo->nregion,
	    fbo->growing ? 1 : 0));
}

/* allocate a new region for body data */
static size_t
fellow_busy_body_seg_alloc(struct fellow_busy *fbo, struct fellow_disk_seg *fds)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent reg, *fdr;
	struct fellow_layout_limits fll;
	struct buddy_reqs *reqs;
	struct fellow_cache *fc;
	size_t spc, sz;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	fbr = &fbo->body_region;
	AZ(fds->seg.off);
	AZ(fds->seg.size);

	fdr = fbr->reg;
	if (fdr == NULL || fbr->len == fdr->size) {
		if (FC_INJ)
			return (0);

		memset(fbr, 0, sizeof *fbr);

		reqs = &fbo->bbrr.reqs;
		reg = fbo->segdskdowry;
		fbo->segdskdowry = buddy_off_extent_nil;

		// must return a size, or sz_estimate is wrong
		fbo->fll = fellow_busy_body_size_strategy(fbo);
		AN(fbo->fll.disk_size);

	  wait_dskalloc:
		BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
		if (reg.off < 0 && buddy_alloc_async_wait(reqs)) {
			reg = buddy_get_next_off_extent(reqs);
			assert(reg.off >= 0);
		}

		buddy_alloc_async_done(reqs);

		// return and realloc if one power of two too big
		if (reg.off < 0 ||
		    (log2up(reg.size) > MIN_FELLOW_BITS &&
		     log2up(reg.size) > log2up(fbo->fll.disk_size))) {
			AN(buddy_req_extent(reqs, fbo->fll.disk_size, fbo->fll.disk_cram));
			BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
			(void) buddy_alloc_async(reqs);
			if (reg.off >= 0)
				buddy_return1_off_extent(reqs->buddy, &reg);
			goto wait_dskalloc;
		}

		// Can only fail if reg.off < 0, which can't happen
		fdr = fellow_busy_region_commit(fbo, reg);
		AN(fdr);

		fbr->reg = fdr;
		AZ(fbr->len);
		fbo->sz_dskalloc += fdr->size;

		if (fbo->growing == 0 && fbo->sz_estimate > fbo->sz_dskalloc) {
			fll = fellow_busy_body_size_strategy(fbo);
			AN(buddy_req_extent(reqs, fll.disk_size, fll.disk_cram));
			BUDDY_REQS_PRI(reqs, FEP_SPC);
			(void) buddy_alloc_async(reqs);
		}
	}

	assert(fbr->len < fdr->size);

	spc = fdr->size - fbr->len;
	AZ(spc & FELLOW_BLOCK_ALIGN);

	AN(fbo->fll.chunk_exponent);
	sz = (size_t)1 << fbo->fll.chunk_exponent;
	if (spc < sz)
		sz = spc;

	fds->seg.off = fdr->off + (buddyoff_t)fbr->len;
	fds->seg.size = sz;
	fds->segnum = fbr->segnum++;

	fbr->len += sz;

	return (sz);
}

/* return an fds to the body seg (for failed memalloc) */
static void
fellow_busy_body_seg_return(struct fellow_busy *fbo,
    struct fellow_disk_seg *fds)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent *fdr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	AN(fds->seg.off);
	AN(fds->seg.size);

	fbr = &fbo->body_region;
	fdr = fbr->reg;
	AN(fdr);

	assert(fbr->len >= fds->seg.size);
	fbr->len -= fds->seg.size;
	assert(fds->seg.off == fdr->off + (buddyoff_t)fbr->len);
	fbr->segnum--;

	fds->seg.off = 0;
	fds->seg.size = 0;
}


static void
fellow_cache_seg_fini(const struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg	*fds;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	AN(fcs->alloc.ptr);
	AN(fcs->u.fcs.len);
	assert(fcs->u.fcs.len <= fds->seg.size);
	fds->seg.size = fcs->u.fcs.len;
	fh(fds->fht, fds->fh, fcs->alloc.ptr, fcs->u.fcs.len);
}

static inline void
fellow_cache_seg_transition_parentref(
    struct fellow_lru_chgbatch *lcb, const struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to)
{
	struct fellow_cache_seg *parent_fcs;
	int o, n, d;

	o = fcos_parent_ref[from];
	n = fcos_parent_ref[to];
	if ((o | n) == 0)
		return;
	parent_fcs = FCS_PARENT(fcs);
	AN(parent_fcs);
	d = n - o;
	if (d == 1)
		(void) fellow_cache_seg_ref_locked(lcb, parent_fcs);
	else if (d == -1)
		(void) fellow_cache_seg_deref_locked(lcb, parent_fcs);
	else if (d != 0)
		WRONG("fcos_parent table");
}

static inline void
fellow_cache_seg_transition_locked_notincore(struct fellow_cache_seg *fcs,
    enum fcos_state to)
{

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	/*
	 * to/from INCORE requires lru operations
	 * see fellow_cache_seg_transition*
	 */
	assert(! FCOS_IS(fcs->state, INCORE));
	assert(! FCOS_IS(to, INCORE));

	DBG("%p %s %s", fcs, fcos_state_s[fcs->state], fcos_state_s[to]);

	assert(fcs->state != to);
	assert_fcos_transition(fcs->state, to);
	fellow_cache_seg_transition_parentref(NULL, fcs, fcs->state, to);
	fcs->state = to;
	assert_cache_seg_consistency(fcs);
}

static inline void
fellow_cache_seg_transition_locked(
    struct fellow_lru_chgbatch *lcb, struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to)
{
	struct objcore *oc = FCS_FCO(fcs)->oc;
	int o, n;

	ASSERT_MUTEX_OWNED(FCS_FCO(fcs)->mtx);
#ifdef DEBUG
	assert_fcos_transition(from, to);
#endif

	DBG("%p %s %s", fcs, fcos_state_s[from], fcos_state_s[to]);
	o = fellow_cache_shouldlru(from, oc, fcs->refcnt);
	n = fellow_cache_shouldlru(to, oc, fcs->refcnt);

	assert(fcs->state == from);
	fcs->state = to;

	fellow_cache_lru_chg(lcb, fcs, n - o);
	fellow_cache_seg_transition_parentref(lcb, fcs, from, to);
}

static void
fellow_cache_seg_transition(
    struct fellow_cache_seg *fcs,
    enum fcos_state from, enum fcos_state to)
{
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fco = FCS_FCO(fcs);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	assert(from != to);

	assert_fcos_transition(from, to);

	AZ(pthread_mutex_lock(&fco->mtx));
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
	if (fcs->refcnt)
		AZ(pthread_cond_broadcast(&fco->cond));
	AZ(pthread_mutex_unlock(&fco->mtx));
}

/* return old refcnt */
static inline unsigned
fellow_cache_seg_ref_n_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned nref)
{
	struct objcore *oc = FCS_FCO(fcs)->oc;
	unsigned refcnt;
	int o, n;

	AN(nref);

	refcnt = fcs->refcnt;
	fcs->refcnt += nref;

	o = fellow_cache_shouldlru(fcs->state, oc, refcnt);
	n = fellow_cache_shouldlru(fcs->state, oc, fcs->refcnt);

	fellow_cache_lru_chg(lcb, fcs, n - o);
	return (refcnt);
}

/* return old refcnt */
static inline unsigned
fellow_cache_seg_ref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{
	return (fellow_cache_seg_ref_n_locked(lcb, fcs, 1));
}

static inline unsigned
fellow_cache_seg_deref_n_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, unsigned nref)
{
	struct objcore *oc = FCS_FCO(fcs)->oc;
	unsigned refcnt;
	int o, n;

	ASSERT_MUTEX_OWNED(FCS_FCO(fcs)->mtx);

	assert(fcs->refcnt >= nref);
	fcs->refcnt -= nref;
	refcnt = fcs->refcnt;

	o = fellow_cache_shouldlru(fcs->state, oc, refcnt + nref);
	n = fellow_cache_shouldlru(fcs->state, oc, refcnt);

	fellow_cache_lru_chg(lcb, fcs, n - o);
	return (refcnt);
}

/* return new refcnt */
static inline unsigned
fellow_cache_seg_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{

	return (fellow_cache_seg_deref_n_locked(lcb, fcs, 1));
}

static struct fellow_busy_io *
fellow_cache_seg_unbusy_locked(struct fellow_busy *fbo,
    struct fellow_busy_io *fbiostk, struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs)
{
	struct fellow_busy_io *fbio;
	enum fcos_state from, to;

	from = FCOS_MUT(fcs, FCOS_BUSY);
	to = FCOS_MUT(fcs, FCOS_WRITING);

	fbo->io_outstanding++;
	fbio = fellow_busy_io_get(fbo, fbiostk);
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_cond_broadcast(&lcb->fco->cond));

	AN(fbio);
	fbio->type = FBIO_SEG;
	fbio->u.fcs = fcs;

	return (fbio);
}


//lint -sem(fellow_cache_seg_unbusy_locked_finish, thread_unlock)
static void
fellow_cache_seg_unbusy_locked_finish(struct fellow_busy *fbo, struct fellow_cache_seg *fcs,
    struct fellow_lru_chgbatch *lcb)
{
	struct fellow_busy_io *fbio, fbiostk[1];

	fbio = fellow_cache_seg_unbusy_locked(fbo, fbiostk, lcb, fcs);
	fellow_cache_obj_unlock(lcb);

	fellow_busy_io_submit(fbo->fc, &fbio, 1);
}

static void
fellow_cache_seg_unbusy(struct fellow_busy *fbo, struct fellow_cache_seg *fcs)
{
	struct fellow_busy_io *fbio, fbiostk[1];
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	CHECK_OBJ_NOTNULL(fbo->fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fco = FCS_FCO(fcs);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	assert(fco == fbo->fco);
	AN(fcs->refcnt);
	AN(fcs->u.fcs.len);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	AZ(pthread_mutex_lock(&lcb->fco->mtx));
	fbio = fellow_cache_seg_unbusy_locked(fbo, fbiostk, lcb, fcs);
	fellow_cache_obj_unlock(lcb);

	fellow_busy_io_submit(fbo->fc, &fbio, 1);
}

static void
fellow_cache_obj_trim(const struct fellow_cache *fc,
    struct fellow_cache_obj *fco)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	uint16_t n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	// no trim if fdo is embedded - XXX cleaner way?
	assert(FCO_FDO(fco) == FCO_FCS(fco)->alloc.ptr);

	fcsl = VLIST_FIRST(&fco->fcslhead);

	if (fcsl == NULL) {
		buddy_trim1_ptr_extent(fc->membuddy, &fco->fco_mem, sizeof(*fco));
		return;
	}
	CHECK_OBJ(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = FCSL_FDSL(fcsl);
	CHECK_OBJ_ORNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	if (fdsl == NULL)
		n = 0;
	else
		n = fdsl->nsegs;

	// for a busy object, fcsl and fdsl are embedded in fco/fdo
	AZ(fcsl->fcsl_sz);
	AZ(FCSL_FDSLSZ(fcsl));

	assert(fcsl->lsegs >= n);
	fcsl->lsegs = n;
	buddy_trim1_ptr_extent(fc->membuddy, &fco->fco_mem,
	    fellow_cache_obj_with_seglist_size(n));
}

/* XXX can save some io for inlog == 0 */

static void
fellow_cache_obj_unbusy(struct fellow_busy *fbo, enum fcol_state wantlog)
{
	const enum fcos_state from = FCO_BUSY, to = FCO_WRITING;
	struct fellow_cache_obj *fco, *ofco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache *fc;
	struct fellow_busy_io *fbio, fbiostk[1];

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	fcs = FCO_FCS(fco);
	assert_fcos_transition(from, to);

	assert(wantlog == FCOL_WANTLOG || wantlog == FCOL_NOLOG);

	AZ(pthread_mutex_lock(&fco->mtx));
	AZ(fcs->fco_infdb);
	fcs->fco_infdb = 1;
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	ofco = VRBT_INSERT(fellow_cache_fdb_head, &fc->fdb_head, fco);
	AZ(ofco);
	fc->stats->g_mem_obj++;
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	if (likely(fco->logstate == FCOL_DUNNO))
		fco->logstate = wantlog;
	else
		assert(fco->logstate == FCOL_TOOLATE);

	/*
	 * fco has its io_outstanding from fellow_busy_obj_alloc()
	 * fbo->io_outstanding++;
	 */
	fbio = fellow_busy_io_get(fbo, fbiostk);
	fellow_cache_seg_transition_locked(lcb, fcs, from, to);
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_mutex_unlock(&fco->mtx));

	fellow_cache_obj_fini(fco);
	fellow_cache_obj_trim(fc, fco);

	AN(fbio);
	fbio->type = FBIO_SEG;
	fbio->u.fcs = fcs;

	fellow_busy_io_submit(fbo->fc, &fbio, 1);
}

/*
 * ============================================================
 * ASYNC
 *
 */

// handle completed io based on info
static void
fellow_cache_read_complete(struct fellow_cache *fc, void *ptr, int32_t result)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	enum fcos_state fcos_next;
	unsigned refcount;

	CAST_OBJ_NOTNULL(fcs, ptr, FELLOW_CACHE_SEG_MAGIC);
	assert(FCOS(fcs->state) == FCOS_READING);

	fco = FCS_FCO(fcs);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fc->panic_bo.fetch_objcore = fco->oc;

	assert(fcs->alloc.size <= INT32_MAX);
	if (FC_INJ || result < (int32_t)fcs->alloc.size) {
		fcr = FCR_IOFAILHOW("fcs read",
		    result, (int32_t)fcs->alloc.size);
		fcos_next = (typeof(fcos_next))FCOS_READFAIL;
	}
	else {
		fcr = FCR_OK(fco);
		fcos_next = (typeof(fcos_next))FCOS_CHECK;
	}

	//lint -e{655} bit-wise operation uses (compatible) enum's
	fcos_next |= (typeof(fcos_next))FCOS_HIGH(fcs->state);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	assert_fcos_transition(fcs->state, fcos_next);

	fellow_cache_obj_lock(lcb);
	(void) fellow_cache_obj_res(fc, fco, fcr);
	fellow_cache_seg_transition_locked(lcb, fcs, fcs->state, fcos_next);
	// io holds a ref on the seg and the fco
	if (fellow_cache_seg_deref_locked(lcb, fcs))
		AZ(pthread_cond_broadcast(&fco->cond));
	refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
	if (refcount == 0)
		fellow_cache_obj_free(fc, lcb, &fco);
	else
		fellow_cache_obj_unlock(lcb);
}

// handle completed io based on info
static void
fellow_cache_async_write_complete(struct fellow_cache *fc,
    void *ptr, int32_t result)
{
	struct fellow_busy_io *fbio;
	struct fellow_busy *fbo;
	struct fellow_cache_seg *fcs = NULL;
	struct fellow_disk_seg *fds;
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	struct fellow_disk_obj *fdo, *fdo2;
	enum fcos_state fcos_next = FCOS_INVAL;
	enum fellow_busy_io_e type;
	uint8_t io_outstanding;
	unsigned refcount;
	size_t sz;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	CAST_OBJ_NOTNULL(fbio, ptr, FELLOW_BUSY_IO_MAGIC);

	if (result == -EAGAIN && fbio->retries++ < FBIO_MAX_RETRIES) {
		fellow_busy_io_submit(fc, &fbio, 1);
		return;
	}

	type = fbio->type;

	fbo = fbio->fbo;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fc->panic_bo.fetch_objcore = fco->oc;

	// can potentially change the object and a segment
	FELLOW_LRU_CHGBATCH(lcb, fco, 2);

	if (type == FBIO_SEG) {
		fcs = fbio->u.fcs;
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);
		fds = FCS_FDS(fcs);
		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

		sz = fellow_rndup(fc->ffd, fds->seg.size);
		assert(sz < INT32_MAX);

		if (FC_INJ || result < (int32_t)sz)
			fcr = FCR_IOFAILHOW("fcs write", result, sz);
		else
			fcr = FCR_OK(fco);

		assert(FCOS(fcs->state) == FCOS_WRITING);
		fcos_next = FCOS_MUT(fcs, FCOS_INCORE);
		assert_fcos_transition(fcs->state, fcos_next);

		if (fcs->state == FCO_WRITING) {
			fdo = fellow_disk_obj(fcs);
			fdo2 = fellow_disk_obj_trim(fc, fcs);
			assert(fdo == fdo2);
		}
	} else {
		assert(type == FBIO_SEGLIST);
		if (FC_INJ || result < (int32_t)fbio->u.seglist.reg.size) {
			fcr = FCR_IOFAILHOW("seglist write",
			    result, (int32_t)fbio->u.seglist.reg.size);
		}
		else
			fcr = FCR_OK(fco);

		CHECK_OBJ_NOTNULL(fbio->u.seglist.fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
		fcs = fbio->u.seglist.fcsl->fcs;
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		assert(FCS_FCO(fcs) == fco);

		assert(FCOS(fcs->state) == FCOS_WRITING);
		fcos_next = FCOS_MUT(fcs, FCOS_INCORE);
		assert_fcos_transition(fcs->state, fcos_next);
	}

	/* we keep the FCO writing until all else is written
	 * to avoid a race with evict, which removes the oc, which
	 * we need for writing the log
	 *
	 * the writer acquired a ref, so we deref by one
	 *
	 */
	fellow_cache_obj_lock(lcb);
	memset(fbio, 0, sizeof *fbio);
	AN(fbo->io_outstanding);
	io_outstanding = --fbo->io_outstanding;

	AN(fcs);
	if (FCOS_HIGH(fcs->state) == FCS_HIGH ||
	    FCOS_HIGH(fcs->state) == FCAA_HIGH ||
	    FCOS_HIGH(fcs->state) == FCL_HIGH) {
		/* transition a segment */
		assert(fcos_next != FCOS_INVAL);
		fellow_cache_seg_transition_locked(lcb, fcs,
		    fcs->state, fcos_next);
		if (fellow_cache_seg_deref_locked(lcb, fcs))
			AZ(pthread_cond_broadcast(&fco->cond));
	}

	/*
	 * We latch an error, but _still_ add the object to the log, such that
	 * fellow_cache_obj_delete() can do a thin delete
	 */
	(void) fellow_cache_obj_res(fc, fco, fcr);

	//lint --e{456} Two execution paths are being combined...
	//lint --e{454} A thread mutex has been locked...
	if (io_outstanding == 0) {
		switch (fco->logstate) {
		case FCOL_WANTLOG:
			/* fellow_cache_obj_delete() can not race
			 * us because of wait for FCO_WRITING.
			 *
			 * unlock during busy_log_submit because
			 * of LRU and waiting allocs
			 *
			 * sfe_oc_event DOES race us and we may
			 * lose events between the time we write
			 * the log here and call stvfe_oc_log_submitted()
			 *
			 * the whole event thing is racy anyway, not sure
			 * how relevant...
			 */
			fellow_cache_obj_unlock(lcb);

			fellow_busy_log_submit(fbo);
			stvfe_oc_log_submitted(fco->oc);

			fellow_cache_obj_lock(lcb);
			assert(fco->logstate == FCOL_WANTLOG);
			fco->logstate = FCOL_INLOG;
			break;
		case FCOL_NOLOG:
		case FCOL_TOOLATE:
			break;
		case FCOL_DUNNO:
		case FCOL_INLOG:
		case FCOL_DELETED:
			FC_WRONG("fellow_cache_async_write_complete "
			    "wrong logstate %d", fco->logstate);
			break;
		default:
			WRONG("fellow_cache_async_write_complete "
			    "invalid logstate");
		}

		fcs = FCO_FCS(fco);
		assert(fcs->state == FCO_WRITING);
		fcos_next = FCO_INCORE;

		/* transition the FCO */
		fellow_cache_seg_transition_locked(lcb, fcs,
		    fcs->state, fcos_next);
		refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
		if (refcount == 0)
			fellow_cache_obj_free(fc, lcb, &fco);
		else {
			AZ(pthread_cond_broadcast(&fco->cond));
			fellow_cache_obj_unlock(lcb);
		}
	} else
		fellow_cache_obj_unlock(lcb);

	if (io_outstanding)
		return;

	fellow_busy_free(&fbo);
}

static void
fellow_cache_async_cb(void *priv,
    /*lint -e{818}*/ struct fellow_io_status *status, unsigned n)
{
	struct fellow_cache *fc;
	unsigned u;

	CAST_OBJ_NOTNULL(fc, priv, FELLOW_CACHE_MAGIC);
	for (u = 0; u < n; u++) {
		switch (faio_info_type(status[u].info)) {
		case FAIOT_CACHE_READ:
			fellow_cache_read_complete(fc,
			    faio_info_ptr(status[u].info),
			    status[u].result);
			break;
		case FAIOT_CACHE_WRITE:
			fellow_cache_async_write_complete(fc,
			    faio_info_ptr(status[u].info),
			    status[u].result);
			break;
		default:
			WRONG("faio_info_type in fellow_cache");
		}
	}
}

//lint -sem(fellow_cache_async_cb_locked, thread_protected, thread_lock, thread_unlock)
static void
fellow_cache_async_cb_locked(void *priv,
    /*lint -e{818}*/ struct fellow_io_status *status, unsigned n)
{
	struct fellow_cache *fc;

	CAST_OBJ_NOTNULL(fc, priv, FELLOW_CACHE_MAGIC);
	AZ(pthread_mutex_unlock(&fc->async_mtx));
	fellow_cache_async_cb(fc, status, n);
	AZ(pthread_mutex_lock(&fc->async_mtx));
}

static inline struct fellow_disk_seg *
fellow_cache_seg_io_check(const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg *fds;
	size_t asz;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	asz = fellow_rndup(fc->ffd, fds->seg.size);

	assert(fcs->alloc.size == asz);
	AN(fds->seg.off);
	AN(fds->seg.size);
	assert(fds->seg.size <= asz);
	switch (fcs->state) {
	case FCAA_READING:
	case FCS_READING:
		assert(fds->seg.size >= fcs->u.fcs.len);
		fcs->u.fcs.len = fds->seg.size;
		break;
	case FCL_READING:
		assert(fds->seg.size == asz);
		break;
	default:
		WRONG("fcs->state in seg_io_check");
	}

	// O_DIRECT alignment on 4K
	AZ((uintptr_t) fcs->alloc.ptr & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t) fcs->alloc.size & FELLOW_BLOCK_ALIGN);
	AZ((uintptr_t)fds->seg.off & FELLOW_BLOCK_ALIGN);

	return (fds);
}

static void
fellow_cache_seg_sync_read(struct fellow_cache *fc,
    struct fellow_cache_seg * const *segs, unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;

	while (n > 0) {
		fcs = *segs;
		fds = fellow_cache_seg_io_check(fc, fcs);

		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
		assert(FCOS_IS(fcs->state, READING));
		assert(fds->seg.off >= 0);
		fellow_cache_read_complete(fc, fcs,
		    fellow_io_pread_sync(fc->ffd,
			fcs->alloc.ptr, fcs->alloc.size, fds->seg.off));
		n--;
		segs++;
	}
}

/* no fcs locking required, it needs to be referenced and have the
 * reading/writing state, so can't go away
 */
static void
fellow_cache_seg_async_read(struct fellow_cursor_io *fcio)
{
	struct fellow_cache_seg * const *segs;
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	struct fellow_cache *fc;
	uint64_t info;
	uint16_t n;
	int r;

	CHECK_OBJ_NOTNULL(fcio, FELLOW_CURSOR_IO_MAGIC);
	segs = fcio->fcs;
	fc = fcio->fc;
	n = fcio->n;
	fcio->n = 0;

	AZ(pthread_mutex_lock(&fc->async_mtx));
	while (n > 0) {
		fcs = *segs;
		info = faio_ptr_info(FAIOT_CACHE_READ, fcs);
		fds = fellow_cache_seg_io_check(fc, fcs);

		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
		assert(FCOS_IS(fcs->state, READING));
		r = fellow_io_read_async_enq(fc->async_ioctx,
		    info, fcs->alloc.ptr, fcs->alloc.size, fds->seg.off);
		if (! r)
			break;
		n--;
		segs++;
	}

	if (fc->async_idle)
		AZ(pthread_cond_signal(&fc->async_cond));
	AZ(pthread_mutex_unlock(&fc->async_mtx));

	if (n == 0)
		return;

	// could not submit, fall back to sync
	DBG("ASYNC FULL; sync fallback %u", n);
	fellow_cache_seg_sync_read(fc, segs, n);
}

static void *
fellow_cache_async_thread(void *priv)
{
	struct fellow_cache *fc;
	unsigned n, entries;

	CAST_OBJ_NOTNULL(fc, priv, FELLOW_CACHE_MAGIC);
	entries = fellow_io_entries(fc->async_ioctx);
	struct fellow_io_status status[entries];
	AZ(fc->panic_bo.magic);
	fc->panic_bo.magic = BUSYOBJ_MAGIC;
	stvfe_setbusyobj(&fc->panic_bo);

	AZ(pthread_mutex_lock(&fc->async_mtx));
	while (fc->running) {
		n = 0;
		while (fellow_io_unsubmitted(fc->async_ioctx) > 0) {
			n = fellow_io_submit_and_wait(fc->async_ioctx,
			    status, entries, 0, fellow_cache_async_cb_locked, fc);
		}
		if (fellow_io_outstanding(fc->async_ioctx)) {
			AZ(fellow_io_unsubmitted(fc->async_ioctx));
			AZ(pthread_mutex_unlock(&fc->async_mtx));
			(void) fellow_io_wait_completions_only(fc->async_ioctx,
			    status, entries, 1, fellow_cache_async_cb, fc);
			AZ(pthread_mutex_lock(&fc->async_mtx));
		}
		else if (n == 0) {
			fc->async_idle = 1;
			AZ(pthread_cond_wait(&fc->async_cond, &fc->async_mtx));
			fc->async_idle = 0;
		}
	}
	// drain
	do {
		n = fellow_io_submit_and_wait(fc->async_ioctx,
		    status, entries, INT_MAX, fellow_cache_async_cb_locked, fc);
	} while (n > 0);
	AZ(pthread_mutex_unlock(&fc->async_mtx));
	stvfe_setbusyobj(NULL);
	return (NULL);
}

static inline void
fellow_cache_seg_evict_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, struct buddy_returns *rets)
{
	//		ah at r
	lcb_assert(fcs, 0, 0, 0);
	AZ(fcs->refcnt);
	AZ(fcs->fcs_onlru);
	assert(fcs->state == FCS_INCORE);
	fellow_cache_seg_transition_parentref(lcb, fcs, fcs->state, FCS_DISK);
	AN(buddy_return_ptr_extent(rets, &fcs->alloc));
	fcs->u.fcs.len = 0;
	fcs->state = FCS_DISK;
}

static inline void
fellow_cache_seglist_evict_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, struct buddy_returns *rets)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_seghdr *hdr;
	struct buddy_ptr_extent e;

	//		ah at r
	lcb_assert(fcs, 0, 0, 0);
	AZ(fcs->refcnt);
	AZ(fcs->fcs_onlru);
	assert(fcs->state == FCL_INCORE);
	fellow_cache_seg_transition_parentref(lcb, fcs, fcs->state, FCL_DISK);
	fcs->state = FCL_DISK;

	// fdsl
	AN(buddy_return_ptr_extent(rets, &fcs->alloc));

	// XXX extra complicated because of Flexelint complaint
	hdr = FCS_HDR(fcs);
	fcsl = __containerof(hdr, struct fellow_cache_seglist, fcshdr);
	VLIST_REMOVE(fcsl, list);

	if (fcsl->fcsl_sz > 0) {
		e = BUDDY_PTR_EXTENT(fcsl, fcsl->fcsl_sz);
		fcsl->fcsl_sz = 0;
		AN(buddy_return_ptr_extent(rets, &e));
	}
}

static inline void
fellow_cache_lru_seg_evict_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache_seg *fcs, struct buddy_returns *rets,
    struct fellow_cache_lru *lru)
{
	AN(fcs->fcs_onlru);
	fcs->fcs_onlru = 0;
	AN(lru->n);
	lru->n--;
	VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
	DBG("lru rem fcs %p", fcs);
	switch (fcs->state) {
	case FCS_INCORE:
		fellow_cache_seg_evict_locked(lcb, fcs, rets);
		break;
	case FCL_INCORE:
		fellow_cache_seglist_evict_locked(lcb, fcs, rets);
		break;
	default:
		WRONG("fcs->state in lru_seg_evict_locked");
	}
}

static void
fellow_busy_slim(const struct fellow_cache *fc, struct fellow_busy *fbo)
{
	struct fellow_cache_obj *fco;
	struct fellow_disk_obj *fdo;
	struct buddy *dskbuddy;
	struct buddy_returns *dskrets;
	unsigned u;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	dskbuddy = fellow_dskbuddy(fc->ffd);
	dskrets = BUDDY_RETURNS_STK(dskbuddy, BUDDY_RETURNS_MAX);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	AZ(fbo->io_outstanding);

#define FDO_AUXATTR(U, l)								\
	if (fdo->aa_##l##_seg.seg.size)							\
		AN(buddy_return_off_extent(dskrets, &fdo->aa_##l##_seg.seg));
#include "tbl/fellow_obj_attr.h"
	for (u = 0; u < fbo->nregion; u++)
		AN(buddy_return_off_extent(dskrets, &fbo->region[u]));
	fbo->nregion = 0;
	fbo->body_seglist = NULL;
	fbo->body_seg = NULL;
	buddy_return(dskrets);
}

/* evict all segments of an object */
void
fellow_cache_obj_slim(struct fellow_cache *fc,
    struct fellow_cache_obj *fco, struct fellow_busy *fbo)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);
	struct fellow_cache_seglist_head fcslhead = VLIST_HEAD_INITIALIZER(&fcslhead);

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	fellow_cache_obj_lock(lcb);
	if (VLIST_FIRST(&fco->fcslhead) == NULL) {
		fellow_cache_obj_unlock(lcb);
		return;
	}

	if (fbo) {
		(void) fellow_cache_seg_ref_locked(lcb, FCO_FCS(fco));
		fellow_cache_lru_chgbatch_apply(lcb);
		while (fbo->io_outstanding)
			fellow_cache_seg_wait_locked(FCO_FCS(fco));
		(void) fellow_cache_obj_deref_locked(lcb, fc, fco);
	}

	// XXX unclear: can we ignore concurrent reads here?
	// can return fco lock for wait on io, so make the head private
	if (fbo)
		VLIST_SWAP(&fco->fcslhead, &fcslhead, fellow_cache_seglist, list);
	else
		fcslhead = fco->fcslhead;
	fellow_cache_seglist_free(rets, VLIST_FIRST(&fcslhead), fbo ? 1 : 0);
	fellow_cache_obj_unlock(lcb);

	buddy_return(rets);

	if (fbo)
		fellow_busy_slim(fc, fbo);
}

/*
 * returns the number of things (FCS, FCL, FCO) evicted
 */
static unsigned
fellow_cache_lru_work(struct worker *wrk, struct fellow_cache_lru *lru)
{
	struct fellow_cache_seg *fcs, *fcss, *resume;
	struct fellow_cache_obj *fco, *fco2;
	struct objcore *oc;
	struct buddy_ptr_extent alloc;
	unsigned evicted = 0;
	int count = 0, r;

	/*lint --e{454,455,456}
	 *
	 * flexelint does not understand the locking structure here:
	 *
	 * - it does not understand trylock
	 * - we leave the loop either with break
	 *   - mtx unlocked
	 *   - fcs != NULL
	 * - or foreach finishes, then fcs == NULL and the mtx
	 *   needs to be unlocked
	 *
	 * this is safe because the oc holds a ref on the fco, so
	 * the LRU state of the fco does not change
	 */

	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	resume = lru->resume;
	CHECK_OBJ(resume, FELLOW_CACHE_SEG_MAGIC);

	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(lru->fc->membuddy, BUDDY_RETURNS_MAX);

	alloc = buddy_ptr_extent_nil;
	oc = NULL;
	fco = NULL;
	AZ(pthread_mutex_lock(&lru->lru_mtx));
	if (resume->fcs_onlru) {
		VTAILQ_REMOVE(&lru->lru_head, resume, lru_list);
		resume->fcs_onlru = 0;
	}

	while (VTAILQ_EMPTY(&lru->lru_head) && lru->fc->running)
		AZ(pthread_cond_wait(&lru->lru_cond, &lru->lru_mtx));

	//lint -e{850} loop variable modified in body
    again:
	VTAILQ_FOREACH_SAFE(fcs, &lru->lru_head, lru_list, fcss) {
		assert(fcs != fcss);
		// no use trying the same object again and again
		fco2 = FCS_FCO(fcs);
		if (fco2 == fco)
			continue;
		fco = fco2;
		CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
		lru->panic_bo.fetch_objcore = fco->oc;
		assert(fco->lru == lru);
		r = pthread_mutex_trylock(&fco->mtx);
		if (r != 0) {
			assert(r == EBUSY);
			continue;
		}
		if (fcs->state == FCS_INCORE || fcs->state == FCL_INCORE) {
			// small lcb, because we only ever add to lru (see
			// below)
			FELLOW_LRU_CHGBATCH(lcb, fco, 1);
			while (1) {
				AZ(lcb->n_rem);

				fellow_cache_lru_seg_evict_locked(lcb, fcs,
				    rets, lru);
				evicted++;

				if (rets->size < lru->fc->membuddy->deficit &&
				    rets->n + 2 < rets->space &&
				    fcss != NULL &&
				    (fcss->state == FCS_INCORE || fcss->state == FCL_INCORE) &&
				    FCS_FCO(fcss) == fco) {
					fcs = fcss;
					fcss = VTAILQ_NEXT(fcs, lru_list);
					continue;
				}
				break;
			}

			/*
			 * an eviction can cause an lru addition of the parent,
			 * but not a deletion
			 */
			AZ(lcb->n_rem);
			fellow_cache_lru_chgbatch_apply_locked(lcb);

			AZ(pthread_mutex_unlock(&fco->mtx));
			fco = NULL;
			if (rets->size < lru->fc->membuddy->deficit)
				continue;
			AZ(pthread_mutex_unlock(&lru->lru_mtx));
			break;
		}
		if (fcs->state == FCO_INCORE) {
			oc = fco->oc;
			CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
			assert(oc->stobj->priv == fco);
			AZ(fcs->fco_lru_mutate);
			fcs->fco_lru_mutate = 1;

			AN(lru->n);
			lru->n--;

			AZ(resume->fcs_onlru);
			VTAILQ_INSERT_AFTER(&lru->lru_head,
			    fcs, resume, lru_list);
			resume->fcs_onlru = 1;
			VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
			AZ(pthread_mutex_unlock(&lru->lru_mtx));

			r = stvfe_mutate(wrk, lru, oc);
			if (r) {
				// mutate was successful
				evicted++;
				AZ(fcs->fco_lru_mutate);
				AZ(pthread_mutex_unlock(&fco->mtx));
				break;
			}

			/* mutate has failed
			 *
			 * FCO will be put back on LRU below when we have the
			 * lru lock again
			 */

			AZ(pthread_mutex_lock(&lru->lru_mtx));

			AN(fcs->fcs_onlru);
			AN(fcs->fco_lru_mutate);
			fcs->fco_lru_mutate = 0;

			AN(resume->fcs_onlru);
			VTAILQ_INSERT_BEFORE(resume, fcs, lru_list);
			lru->n++;

			AZ(pthread_mutex_unlock(&fco->mtx));

			fcss = VTAILQ_NEXT(resume, lru_list);
			VTAILQ_REMOVE(&lru->lru_head, resume, lru_list);
			resume->fcs_onlru = 0;

			fco = NULL;
			oc = NULL;
			continue;
		}
		AZ(pthread_mutex_unlock(&fco->mtx));
		WRONG("fcs state in lru");
	}

	lru->panic_bo.fetch_objcore = NULL;

	/*
	 * for a break from the foreach, fcs != NULL
	 *
	 * so for fcs == NULL, we know we have iterated over
	 * all of the LRU
	 */
	if (fcs == NULL) {
		if (lru->fc->running && rets->n == 0 && count++ == 0) {
			AZ(pthread_cond_wait(&lru->lru_cond, &lru->lru_mtx));
			goto again;
		}
		else
			AZ(pthread_mutex_unlock(&lru->lru_mtx));
	}

	AZ(alloc.ptr);
	buddy_return(rets);

	if (fcs == NULL) {
		AZ(oc);
		return (evicted);
	}

	/*
	 * at this point, we know we terminated the foreach
	 * with a break. If we have an fco, we need to deref it.
	 */
	if (fco != NULL) {
		AN(oc);
		// we removed the oc's reference on fco in stvfe_mutate()
		fellow_cache_obj_deref(lru->fc, fco);
	}
	return (evicted);
}

static void
reserve_free(buddy_t *buddy, struct buddy_ptr_page *r, unsigned n)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(buddy, BUDDY_RETURNS_MAX);

	if (n == 0)
		return;

	AN(r);

	while (n--)
		AN(buddy_return_ptr_page(rets, r++));

	buddy_return(rets);
}

static void
reserve_req(struct buddy_reqs *reqs, unsigned n, unsigned bits)
{
	if (n > BUDDY_REQS_MAX)
		n = BUDDY_REQS_MAX;
	while (n--)
		AN(buddy_req_page(reqs, bits, 0));
}

static void
reserve_fill(struct buddy_ptr_page *r, const struct buddy_reqs *reqs, uint8_t n)
{
	uint8_t u;

	for (u = 0; u < n; u++) {
		AZ(r->ptr);
		*r++ = buddy_get_ptr_page(reqs, u);
	}
}

static void *
fellow_cache_lru_thread(struct worker *wrk, void *priv)
{
	struct fellow_cache_lru *lru;
#ifdef LRU_NOISE
	struct vsl_log vsl;
#endif
	struct buddy_reqs *reqs;
	const struct fellow_cache *fc;
	struct fellow_cache_seg resume[1];
	buddy_t *buddy;
	struct buddy_ptr_page *r = NULL;
	unsigned e, evicted, i, filled = 0, nr = 0, rc, cb;
	uint8_t n;
	size_t sz;

	CAST_OBJ_NOTNULL(lru, priv, FELLOW_CACHE_LRU_MAGIC);
	fc = lru->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	buddy = fc->membuddy;
	CHECK_OBJ(buddy, BUDDY_MAGIC);

	AZ(lru->lru_thread);
	lru->lru_thread = pthread_self();

	AZ(lru->panic_bo.magic);
	lru->panic_bo.magic = BUSYOBJ_MAGIC;
	stvfe_setbusyobj(&lru->panic_bo);

	// fellow_cache_lru_seg_evict_locked()
	assert_fcos_transition(FCS_INCORE, FCS_DISK);

	reqs = BUDDY_REQS_STK(buddy, BUDDY_REQS_MAX);
	BUDDY_REQS_PRI(reqs, FEP_RESERVE);

	INIT_OBJ(resume, FELLOW_CACHE_SEG_MAGIC);
	AZ(lru->resume);
	lru->resume = resume;

#ifdef LRU_NOISE
	AZ(wrk->vsl);
	wrk->vsl = &vsl;
	VSL_Setup(wrk->vsl, NULL, (size_t)0);
#endif

	while (fc->running) {
		cb = fc->tune->chunk_exponent;
		rc = fc->tune->mem_reserve_chunks;

		// reserve pointers
		while (nr != rc) {
			if (nr != 0)
				AN(r);
			reserve_free(buddy, r, filled);
			filled = 0;
			nr = rc;
#ifdef LRU_NOISE
			VSLb(wrk->vsl, SLT_Debug,
			    "sfedsk reserve config cb=%u nr=%u",
			    cb, nr);
#endif
			if (nr == 0)
				break;

			sz = nr * sizeof *r;
			r = realloc(r, sz);
			AN(r);
			memset(r, 0, sz);
			break;
		};

		// fill reserve
		n = 1;
		evicted = 0;
		while (filled < nr && buddy->waiting == 0 && n > 0) {
			AN(r);
			reserve_req(reqs, nr - filled, cb);
			(void) BUDDYF(alloc_async)(reqs);

			while (buddy->waiting) {
				e = fellow_cache_lru_work(wrk, lru);
				if (e == 0)
					break;
				evicted += e;
			}

			n = BUDDYF(alloc_async_ready)(reqs);
			assert(filled + n <= nr);
			reserve_fill(r + filled, reqs, n);
			filled += n;
			BUDDYF(alloc_async_done)(reqs);
		}
		fc->stats->c_mem_lru_evict_fill_reserve += evicted;

#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sfemem reserve fill: %u", filled);

		VSL_Flush(wrk->vsl, 0);
#endif
		stvfe_sumstat(wrk);
		buddy_wait_needspace(buddy);
		fc->stats->c_mem_lru_wakeups++;

		// drain reserve
		while (filled > 0 && buddy->waiting > 0) {
			i = --filled;

			AN(r);
			AN(r[i].ptr);
			fc->stats->c_mem_lru_reserve_used_bytes =
			    (size_t)1 << r[i].bits;
			BUDDYF(return1_ptr_page)(buddy, &r[i]);
		}

#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sfemem reserve use : %u left", filled);
#endif

		evicted = 0;
		while (buddy->waiting)
			evicted += fellow_cache_lru_work(wrk, lru);
		fc->stats->c_mem_lru_evict_reserve_drained += evicted;
	}
	stvfe_setbusyobj(NULL);
	AN(lru->resume);
	AZ(pthread_mutex_lock(&lru->lru_mtx));
	if (lru->resume->fcs_onlru) {
		VTAILQ_REMOVE(&lru->lru_head, lru->resume, lru_list);
		lru->resume->fcs_onlru = 0;
	}
	AZ(pthread_mutex_unlock(&lru->lru_mtx));
	lru->resume = NULL;
	reserve_free(buddy, r, filled);
	free(r);
	return (NULL);
}

/* returns if moved */
int
fellow_cache_obj_lru_touch(struct fellow_cache_obj *fco)
{
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	struct fellow_cache_lru *lru;
	struct fellow_cache_seg *fcs;
	int r = 0;

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	lru = fco->lru;
	CHECK_OBJ_NOTNULL(lru, FELLOW_CACHE_LRU_MAGIC);
	fcs = FCO_FCS(fco);
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	if (! fcs->fcs_onlru)
		goto out;

	r = 1;
	// mutate should be failing, so it will do the lru move
	if (fcs->fco_lru_mutate)
		goto out;

	r = pthread_mutex_trylock(&fco->mtx);
	if (r != 0) {
		assert(r == EBUSY);
		return (0);
	}

	r = pthread_mutex_trylock(&lru->lru_mtx);
	if (r != 0) {
		assert(r == EBUSY);
		//lint -e{455} flexelint does not grok trylock
		AZ(pthread_mutex_unlock(&fco->mtx));
		return (0);
	}
	if (fcs->fcs_onlru && ! fcs->fco_lru_mutate) {
		assert(fellow_cache_shouldlru(fcs->state, fco->oc, fcs->refcnt));
		VTAILQ_REMOVE(&lru->lru_head, fcs, lru_list);
		VTAILQ_INSERT_TAIL(&lru->lru_head, fcs, lru_list);
	}
	//lint -e{455} flexelint does not grok trylock
	AZ(pthread_mutex_unlock(&lru->lru_mtx));
	//lint -e{455} flexelint does not grok trylock
	AZ(pthread_mutex_unlock(&fco->mtx));

	r = 1;
  out:
	// deliberately unlocked, not critical if update is missed
	if (fco->ntouched++ > 3 && fco->fco_dowry.bits) {
		/* if the object has been touched 4 times and
		 * still has the dowry, it probably only receives
		 * HEAD requests, so we should use the memory for
		 * some better purpose
		 */
		AZ(pthread_mutex_lock(&fco->mtx));
		TAKE(dowry, fco->fco_dowry);
		AZ(pthread_mutex_unlock(&fco->mtx));
		if (dowry.bits) {
			CHECK_OBJ_NOTNULL(lru->fc, FELLOW_CACHE_MAGIC);
			buddy_return1_ptr_page(lru->fc->membuddy, &dowry);
		}
	}

	return (r);
}

// not unter logmtx
static void
fellow_cache_async_init(struct fellow_cache *fc, fellow_task_run_t taskrun)
{
	unsigned entries;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	entries = fellow_io_ring_size("fellow_cache_io_entries");
	DBG("io entries %u", entries);

	AZ(pthread_mutex_init(&fc->async_mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_cond_init(&fc->async_cond, NULL));
	fc->async_ioctx = fellow_io_init(fellow_fd(fc->ffd),
	    entries, fc->membuddy->area, fc->membuddy->map->size,
	    taskrun);
	AN(fc->async_ioctx);
	AZ(pthread_create(&fc->async_thread, NULL,
		fellow_cache_async_thread, fc));
}

static void
fellow_cache_async_fini(struct fellow_cache *fc)
{
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	AZ(pthread_mutex_lock(&fc->async_mtx));
	AZ(pthread_cond_signal(&fc->async_cond));
	AZ(pthread_mutex_unlock(&fc->async_mtx));
	AZ(pthread_join(fc->async_thread, NULL));
	AZ(pthread_cond_destroy(&fc->async_cond));
	AZ(pthread_mutex_destroy(&fc->async_mtx));
}


/* .
 * .
 * .
 * ASYNC END
 * ============================================================
 */

static void
fellow_cache_seg_deref(struct fellow_cache_seg * const *segs, unsigned n)
{
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;

	CHECK_OBJ_NOTNULL(*segs, FELLOW_CACHE_SEG_MAGIC);
	AN(n);
	fco = FCS_FCO(*segs);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	FELLOW_LRU_CHGBATCH(lcb, fco, 64);

	AZ(pthread_mutex_lock(&fco->mtx));
	while (n--) {
		fcs = *segs++;
		assert(FCS_FCO(fcs) == fco);
		CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
		(void) fellow_cache_seg_deref_locked(lcb, fcs);
		/* special case: if we are reading the last trimmed segment,
		 * we need to notify fellow_busy_obj_trimstore()
		 * that it can go ahead
		 */
		if (UNLIKELY(fcs->state == FCS_USABLE && fcs->refcnt == 0))
			AZ(pthread_cond_broadcast(&fco->cond));
	}
	fellow_cache_obj_unlock(lcb);
}

struct fcoi_deref {
	unsigned		magic;
#define FCOI_DEREF_MAGIC	0x2a16ec74
	unsigned		n, max;
	struct fellow_cache_seg **segs;
	objiterate_f		*func;
	void			*priv;
	struct buddy_returns	*memret;
};

static inline void
fcoi_add(struct fcoi_deref *fcoid, struct fellow_cache_seg *fcs)
{

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);
	AN(fcoid->segs);
	assert(fcoid->n < fcoid->max);

	fcoid->segs[fcoid->n++] = fcs;
}

/*
 * deref when we know a flush has just happened or not needed
 */
static inline void
fcoi_deref(struct fcoi_deref *fcoid)
{

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);

	buddy_return(fcoid->memret);

	if (fcoid->n == 0)
		return;

	AN(fcoid->segs);
	assert(fcoid->n <= fcoid->max);

	fellow_cache_seg_deref(fcoid->segs, fcoid->n);
	memset(fcoid->segs, 0, sizeof *fcoid->segs * fcoid->n);
	fcoid->n = 0;
}

static int
fcoid_need_deref(const struct fcoi_deref *fcoid)
{
	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);

	return (fcoid->memret->n || fcoid->n);
}

/*
 * when deref'ing from from _obj_iter(), we need to flush first.
 * this function wraps the deref'ing in a struct
 */
static int
fellow_cache_obj_iter_flush_deref(struct fcoi_deref *fcoid)
{
	int r;

	CHECK_OBJ_NOTNULL(fcoid, FCOI_DEREF_MAGIC);

	buddy_return(fcoid->memret);

	if (fcoid->n == 0)
		return (0);

	AN(fcoid->func);
	r = fcoid->func(fcoid->priv, OBJ_ITER_FLUSH, NULL, (size_t)0);

	fcoi_deref(fcoid);

	return (r);
}

static const char *
fellow_cache_seg_check(struct fellow_cache_seg *fcs)
{
	const struct fellow_disk_seg *fds;
	struct fellow_cache_obj *fco;
	const char *err = NULL;
	enum fcos_state to;
	enum fcos fcos;
	int type;

	type = FCOS_HIGH(fcs->state);
	assert(type == FCS_HIGH || type == FCAA_HIGH);

	fcos = (typeof(fcos))FCOS(fcs->state);

	to = (typeof(to))(type | FCOS_INCORE);

	switch (fcos) {
	case FCOS_READFAIL:
		return ("segment FCS_READFAIL");
	case FCOS_BUSY:
	case FCOS_WRITING:
	case FCOS_INCORE:
		return (NULL);
	case FCOS_CHECK:
		break;
	default:
		WRONG("segment state in _check");
	}

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	if ((fds->segnum % 16 == 0 && FC_INJ) ||
	    fhcmp(fds->fht, fds->fh, fcs->alloc.ptr, fcs->u.fcs.len)) {
		err = "segment checksum error";
		to = (typeof(to))(type | FCOS_READFAIL);
	}

	fco = FCS_FCO(fcs);
	AZ(pthread_mutex_lock(&fco->mtx));
	AN(fcs->refcnt);
	if (FCOS(fcs->state) == FCOS_CHECK) {
		// re-check, could have raced
		fellow_cache_seg_transition_locked(NULL, fcs,
		    fcs->state, to);
		AZ(pthread_cond_broadcast(&fco->cond));
	}
	fcs->lru_to_head = 0;
	AZ(pthread_mutex_unlock(&fco->mtx));

	return (err);
}


/*
 * buddy_reqs shared for all iterations of this call stack
 */
struct iter_reqs {
	unsigned		magic;
#define ITER_REQS_MAGIC		0xb966d721
	unsigned		new;

	// 56 bytes per i_reqalloc + 176 bytes per reqs
	struct buddy_reqs	reqs[2];
};

#undef NREQS
#undef OREQS

#define NREQS(ir) (ir->reqs[ir->new ? 1 : 0])
#define OREQS(ir) (ir->reqs[ir->new ? 0 : 1])

/* return 1 if allocation has been assigned */
static inline int
fellow_cache_obj_readahead_assign_or_request(
    const struct fellow_cache *fc,
    struct fellow_cache_seg *fcs,
    struct buddy_returns *rets,
    struct iter_reqs *ir)
{
	struct fellow_disk_seg *fds;
	struct buddy_ptr_extent mem;
	size_t sz;
	int r;

	assert(fcs->state == FCS_DISK);
	AZ(fcs->alloc.ptr);

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	sz = fellow_rndup(fc->ffd, fds->seg.size);
	mem = buddy_get_next_ptr_extent(&OREQS(ir));
	while (mem.ptr != NULL && mem.size != sz) {
		DBG("%zu != %zu", mem.size, sz);
		AN(buddy_return_ptr_extent(rets, &mem));
		mem = buddy_get_next_ptr_extent(&OREQS(ir));
	}
	if (mem.ptr != NULL) {
		DBG("success %p", mem.ptr);
		fcs->alloc = mem;
		return (1);
	}
	DBG("fail %u", 0);
	r = buddy_req_extent(&NREQS(ir), sz, 0);
	if (r == 0)
		assert(errno == ENOSPC);
	return (0);
}

static __attribute__ ((noinline)) void
fellow_cache_obj_readahead(
    const struct fellow_cache *fc,
    struct fcsracursor *rac, struct fellow_cache_obj *fco,
    struct fellow_cache_seg *ra[], const unsigned mod /* length of ra */,
    unsigned *ranp, const unsigned ranto, unsigned need,
    struct fcoi_deref *fcoid,
    struct iter_reqs *ir)
{
	struct fellow_cache_seg *fcs, *fcs2;
	unsigned ran, ckp, u, needdisk;
	struct buddy_ptr_page dowry;
	struct fcsracursor c;

	/* jump target for need == 1 and sync allocation */
  again:
	needdisk = 0;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	AN(rac);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	AN(ra);
	AN(mod);
	AN(ranp);
	ran = *ranp;
	assert(need <= 1);
	AN(fcoid);
	AN(ir);
	assert(ir->new <= 1);

	FELLOW_LRU_CHGBATCH(lcb, fco, 64);
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(fc->membuddy, BUDDY_RETURNS_MAX);

	assert(ranto >= ran);

	c = *rac;

	// update available allocations
	(void) buddy_alloc_async_ready(&OREQS(ir));

	BUDDY_REQS_PRI(&NREQS(ir), FEP_MEM_ITER);

	ckp = ranto;
	fcs = NULL;
	AZ(pthread_mutex_lock(&fco->mtx));

	/* with need, we enter the loop also for FCOS_BUSY, but only once
	 *
	 * after the loop, needdisk signifies if we need the radisk
	 */
	while (ran < ranto && ((fcs = fcsrac_next(&c)) != NULL) &&
	    (need || FCOS(fcs->state) > FCOS_BUSY)) {
		if (need)
			assert(FCOS(fcs->state) >= FCOS_BUSY);
		else
			assert(FCOS(fcs->state) > FCOS_BUSY);
		assert(FCS_FCO(fcs) == fco);

		/* ref all incore-ish, remember _DISK */
		switch (fcs->state) {
		case FCS_READFAIL:
			goto err;
		case FCS_BUSY:
		case FCS_READING:
		case FCS_WRITING:
		case FCS_CHECK:
		case FCS_INCORE:
			goto ref;
		case FCS_DISK:
			break;
		default:
			WRONG("_readahead fcs->state");
		}

		assert(fcs->state == FCS_DISK);
		if (fellow_cache_obj_readahead_assign_or_request(fc, fcs,
			rets, ir) == 0) {
			if (ran < ckp)
				ckp = ran;
			// end the loop to sync wait for single allocation
			if (need > needdisk) {
				needdisk = need;
				need = 0;
				// because sync wait, raise pri
				BUDDY_REQS_PRI(&NREQS(ir), FEP_MEM_ITERPRI);
				break;
			}
			goto ref;
		}

		fcio_add(c.fcsc->fcio, lcb, fcs);

	  ref:
		need = 0;

		/*
		 * for goto again, references are already taken
		 */

		if (ra[ran % mod] == fcs)
			AN(fcs->refcnt);
		else {
			(void) fellow_cache_seg_ref_locked(lcb, fcs);
			AZ(ra[ran % mod]);
			ra[ran % mod] = fcs;
		}
		ran++;
	}
  err:
	TAKE(dowry, fco->fco_dowry);
	fellow_cache_obj_unlock(lcb);

	if (ckp < ran)
		ran = ckp;

	fellow_cache_seg_async_read(c.fcsc->fcio);

	// final assertions & advance read-ahead cursor
	for (u = *ranp; u < ran; u++) {
		fcs2 = fcsrac_next(rac);
		assert(fcs2 == ra[u % mod]);
		CHECK_OBJ_NOTNULL(fcs2, FELLOW_CACHE_SEG_MAGIC);
		AN(fcs2->refcnt);
	}
	*ranp = ran;

	(void) buddy_alloc_async(&NREQS(ir));
	if (dowry.bits)
		AN(buddy_return_ptr_page(rets, &dowry));
	buddy_alloc_async_done(&OREQS(ir));
	buddy_return(rets);
	uint8_t rdy = buddy_alloc_async_ready(&NREQS(ir));

	// swap OREQS <=> NREQS
	ir->new = ! ir->new;

	if (fcs != NULL && fcs->state == FCS_READFAIL)
		return;

	// if allocation succeeded immediately, kick off I/O
	if (rdy)
		goto again;

	if (needdisk) {
		AZ(need);
		(void) fellow_cache_obj_iter_flush_deref(fcoid);
		AN(buddy_alloc_async_wait(&OREQS(ir)));
		goto again;
	}
}

/*
 * ra[] is a ring of pointers to fcses which we (potentially)
 * read ahead.
 *
 * ra[n % mod] is the current fcs, anything after that are read
 * aheads.
 * ran the next element to be read ahead
 *
 * c is a cursor tracking the current fcs
 * rac is a cursor tracking the read ahead
 *
 */

//static __attribute__ ((noinline)) struct fellow_cache_res
static struct fellow_cache_res
fellow_cache_obj_iter_work(struct fellow_cache *fc, struct iter_reqs *ir,
    struct fellow_cache_obj *fco,
    void *priv, objiterate_f func, int final)
{
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fc->tune, STVFE_TUNE_MAGIC);

	struct fellow_cache_res fcr;
	unsigned readahead = fc->tune->readahead;
	unsigned mod = readahead + 1;
	struct fellow_cache_seg *fcsnext, *fcs, *ra[mod], *deref[mod];
	unsigned need, n = 0, ran = 0, flags, flush;
	struct fcoi_deref fcoid[1];
	const char *err;
	ssize_t sz;
	int ret2;

	// stack usage
	assert(readahead <= 31);

	if (fco->fcr.status != fcr_ok)
		return (fco->fcr);

	// fcr_ok is also returned if func() != 0
	memset(&fcr, 0, sizeof fcr);
	fcr.status = fcr_ok;

	if (VLIST_FIRST(&fco->fcslhead) == NULL)
		return (fcr);

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	memset(ra, 0, sizeof ra);
	memset(deref, 0, sizeof deref);

	INIT_OBJ(fcoid, FCOI_DEREF_MAGIC);
	fcoid->max = mod;
	fcoid->segs = deref;
	fcoid->func = func;
	fcoid->priv = priv;
	fcoid->memret = BUDDY_RETURNS_STK(fc->membuddy, 1);

	// readahead + 1 current + 1 fcsl
	FELLOW_CURSOR_IO(fcio, (uint16_t)readahead + 2, fc);
	FCSC c;
	struct fcsracursor rac;

	fcsc_init(&c, &fcio, fcoid, VLIST_FIRST(&fco->fcslhead));
	fcsrac_init(&rac, &c);

	flags = final ? OBJ_ITER_FLUSH : 0;
	flush = 0;
	while ((fcs = fcsc_next(&c, 1)) != NULL) {
		/*
		 * fellow_stream_f/test_iter_f ensure
		 * that we do not read past the last busy segment
		 */
		assert(FCOS(fcs->state) >= FCOS_BUSY);
		assert(FCS_FCO(fcs) == fco);

		need = ran == n ? 1 : 0;

		assert(ran >= n);
		if (ran - n <= readahead / 2 + need) {
			DBG("(ran - n) %u <= %u + %u", ran - n,
			    readahead / 2, need);
			fellow_cache_obj_readahead(fc, &rac, fco, ra, mod, &ran,
			    n + readahead + need, need, fcoid, ir);
		}
		DBG("ran %u", ran);
		assert(ran > n);
		assert(ra[n % mod] == fcs);

		// aborted busy segment
		if (fcs->state == FCS_USABLE)
			break;

		if (fcs->state == FCS_READING) {
			/* before we wait, do some useful work
			 * and free memory
			 */
			fcr.r.integer =
			    fellow_cache_obj_iter_flush_deref(fcoid);
			if (fcr.r.integer)
				break;
			AZ(pthread_mutex_lock(&fco->mtx));
			while (fcs->state == FCS_READING)
				fellow_cache_seg_wait_locked(fcs);
			AZ(pthread_mutex_unlock(&fco->mtx));
		}

		err = fellow_cache_seg_check(fcs);
		if (err != NULL) {
			fcr = FCR_IOFAIL(err);
			break;
		}

		assert(fcs->state != FCS_READFAIL);

		/* peek at next segment for END
		 * flush if next segment not in ram or no deref space left
		 *
		 * for readahead == 0, this implies flushing always because
		 * derefn == mod - 1 == 0
		 */
		AZ(flags & OBJ_ITER_END);
		if (fcs->state == FCS_BUSY)
			flush = OBJ_ITER_FLUSH;
		else if (((fcsnext = fcsc_peek_wait(&c)) == NULL) ||
		    fcsnext->state == FCS_USABLE) {
			flags |= OBJ_ITER_END;
			flush = OBJ_ITER_FLUSH;
		}
		else if (fcoid->n == fcoid->max - 1 ||
		    fcsnext->state != FCS_INCORE)
			flush = OBJ_ITER_FLUSH;
		else
			flush = 0;

		assert(ra[n % mod] == fcs);
		ra[n % mod] = NULL;

		// for BUSY, we pass the maximum segment size
		// and our func-wrapper will look after the amount
		// to write
		if (fcs->state == FCS_BUSY)
			sz = (ssize_t)fcs->alloc.size;
		else {
			struct fellow_disk_seg *fds = FCS_FDS(fcs);
			CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
			sz = (ssize_t)fds->seg.size;
		}

		fcr.r.integer = func(priv, flags | flush, fcs->alloc.ptr, sz);

		if (final) {
			AZ(fcoid->n);
			AN(flags & OBJ_ITER_FLUSH);
			/* if the opportunistic free fails, the segment will
			 * get deleted when the object is */
			AZ(pthread_mutex_lock(&fco->mtx));
			if (fcs->refcnt == 1) {
				FELLOW_LRU_CHGBATCH(lcb, fco, 1);
				fellow_cache_seg_free(fcoid->memret, lcb, fcs, 1);
				fellow_cache_lru_chgbatch_apply(lcb);
			}
			else {
				// NULL: refcount must be > 1, so cant be on LRU
				AN(fellow_cache_seg_deref_locked(NULL, fcs));
			}
			AZ(pthread_mutex_unlock(&fco->mtx));
		} else {
			fcoi_add(fcoid, fcs);
			if (flush)
				fcoi_deref(fcoid);
		}
		assert(fcr.status == fcr_ok);
		if (fcr.r.integer)
			break;
		n++;
	}

	if (fcr.status != fcr_ok || fcr.r.integer != 0)
		while (mod--) {
			if (ra[mod] != NULL)
				fellow_cache_seg_deref(&ra[mod], 1);
		}
	else
		while (mod--)
			AZ(ra[mod]);

	fcoi_deref(fcoid);



	if ((flags & OBJ_ITER_END) == 0 &&
	    (fcr.status == fcr_ok && fcr.r.integer == 0)) {
		ret2 = func(priv, OBJ_ITER_END, NULL, (size_t)0);
		if (fcr.status == fcr_ok && fcr.r.integer == 0)
			fcr.r.integer = ret2;
	}

	fcsc_fini(&c);

	return (fellow_cache_obj_res(fc, fco, fcr));
}

static __attribute__ ((noinline)) struct fellow_cache_res
fellow_cache_obj_iter_ir(struct fellow_cache *fc, struct fellow_cache_obj *fco,
    void *priv, objiterate_f func, int final)
{
	struct fellow_cache_res r;
	struct iter_reqs ir;

	INIT_OBJ(&ir, ITER_REQS_MAGIC);

	ir.reqs[0] = (struct buddy_reqs)BUDDY_REQS_LIT(fc->membuddy, 3);
	ir.reqs[1] = (struct buddy_reqs)BUDDY_REQS_LIT(fc->membuddy, 4);

	AZ(pthread_setspecific(fc->iter_reqs_key, &ir));

	r = fellow_cache_obj_iter_work(fc, &ir, fco, priv, func, final);

	AZ(pthread_setspecific(fc->iter_reqs_key, NULL));

	buddy_alloc_async_done(&ir.reqs[0]);
	buddy_alloc_async_done(&ir.reqs[1]);

	return (r);
}

struct fellow_cache_res
fellow_cache_obj_iter(struct fellow_cache *fc, struct fellow_cache_obj *fco,
    void *priv, objiterate_f func, int final)
{
	struct iter_reqs *ir;

	ir = pthread_getspecific(fc->iter_reqs_key);

	if (ir == NULL)
		return (fellow_cache_obj_iter_ir(fc, fco, priv, func, final));
	else
		return (fellow_cache_obj_iter_work(fc, ir, fco, priv, func, final));
}
/* Auxiliary attributes
 * ref is never returned and just ignored by
 * fellow_cache_seg_auxattr_free()
 *
 */
static void
fellow_cache_seg_auxattr_ref_in(
    struct fellow_cache *fc,
    struct fellow_cache_seg *fcs)
{
	struct fellow_cache_obj *fco;
	struct buddy_ptr_extent mem;
	struct fellow_disk_seg *fds;
	unsigned io = 0;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);

	if (fcs->state == FCAA_INCORE && fcs->refcnt >= 3)
		return;

	fco = FCS_FCO(fcs);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	/* racy memory allocation */
	mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
	    fellow_rndup(fc->ffd, fds->seg.size), 0);
	AN(mem.ptr);

	AZ(pthread_mutex_lock(&fco->mtx));
	(void) fellow_cache_seg_ref_locked(lcb, fcs);

	while (fcs->state == FCAA_BUSY || fcs->state == FCAA_READING) {
		fellow_cache_lru_chgbatch_apply(lcb);
		fellow_cache_seg_wait_locked(fcs);
	}
	switch (fcs->state) {
	case FCAA_BUSY:
	case FCAA_READING:
		WRONG("auxattr ref_in: state can't be BUSY or READING");
		break;
	case FCAA_READFAIL:
		// will fail in fellow_cache_seg_check()
		break;
	case FCAA_WRITING:
	case FCAA_CHECK:
	case FCAA_INCORE:
		break;
	case FCAA_DISK:
		// reference for io
		(void) fellow_cache_seg_ref_locked(lcb, fcs);

		fcs->alloc = mem;
		mem = buddy_ptr_extent_nil;

		fellow_cache_seg_transition_locked_notincore(fcs, FCAA_READING);
		io = 1;
		break;
	default:
		WRONG("cache_seg_in state");
	}
	AN(FCO_REFCNT(fco));
	if (io)
		(void) fellow_cache_seg_ref_n_locked(lcb, FCO_FCS(fco), io);
	fellow_cache_obj_unlock(lcb);

	if (mem.ptr)
		buddy_return1_ptr_extent(fc->membuddy, &mem);

	if (io)
		fellow_cache_seg_sync_read(fc, &fcs, 1);
}

static struct fellow_cache_res
fellow_busy_body_seg_next(struct fellow_busy *fbo)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_res fcr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fcsl = fbo->body_seglist;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fdsl = FCSL_FDSL(fcsl);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

	assert(fdsl->nsegs <= fdsl->lsegs);
	if (fdsl->nsegs == fdsl->lsegs) {
		fcr = fellow_busy_body_seglist_alloc(fbo, fcsl);
		if (fcr.status != fcr_ok)
			return (fcr);
		CAST_OBJ_NOTNULL(fcsl, fcr.r.ptr, FELLOW_CACHE_SEGLIST_MAGIC);
		fbo->body_seglist = fcsl;
		fdsl = FCSL_FDSL(fcsl);
		AZ(fdsl->nsegs);
		AN(fdsl->lsegs);
	}

	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	assert(fdsl->nsegs < fcsl->lsegs);

	fcs = &fcsl->segs[fdsl->nsegs];
	fbo->body_seg = fcs;
	assert(fcs->state == FCS_USABLE);
	assert(FCS_FCO(fcs) == fbo->fco);
	AZ(fcs->refcnt);
	return (FCR_OK(fcs));
}

/*
 * segment already has disk space allocated, allocate memory
 * half way between FCS_USABLE and FCS_BUSY, returns BUSY
 *
 * used for body and auxattr
 */
static size_t
fellow_busy_seg_memalloc(struct fellow_busy *fbo, struct fellow_cache_seg *fcs)
{
	struct fellow_disk_seg *fds;
	struct buddy_ptr_page mem;
	struct fellow_cache *fc;
	unsigned bits;
	size_t sz;

	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	assert (fcs->state == FCS_USABLE || fcs->state == FCAA_USABLE);
	AN(fds->seg.off);
	AN(fds->seg.size);

	AZ(fcs->alloc.ptr);
	AZ(fcs->alloc.size);

	if (FC_INJ)
		return (0);

	bits = log2up(fds->seg.size);
#ifdef TEST_DRIVER
	if (TEST_LIMIT_SEG_BITS > 0 && bits > TEST_LIMIT_SEG_BITS)
		bits = TEST_LIMIT_SEG_BITS;
#endif
	assert(bits >= MIN_FELLOW_BITS);
	assert(bits <= INT8_MAX);

	mem = buddy_get_next_ptr_page(fbo_segmem_get(fbo->segmem, fbo));

	AN(mem.ptr);
	sz = (size_t)1 << mem.bits;

	/*
	 * if the fbo_segmem pool gave us too large/small a page,
	 * trade it in for a new allocation
	 */
	if (bits != mem.bits) {
		struct buddy_reqs *reqs =
		    BUDDY_REQS_STK(fc->membuddy, 1);

		BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
		AN(buddy_req_extent(reqs, fds->seg.size, 0));
		buddy_return1_ptr_page(fc->membuddy, &mem);
		AN(buddy_alloc_wait(reqs));
		fcs->alloc = buddy_get_ptr_extent(reqs, 0);
		buddy_alloc_wait_done(reqs);
	}
	else {
		fcs->alloc.ptr = mem.ptr;
		fcs->alloc.size = sz;
	}

	if (fcs->alloc.ptr == NULL)
		return (0);

	if (fds->seg.size < fcs->alloc.size) {
		buddy_trim1_ptr_extent(fc->membuddy, &fcs->alloc,
		    fds->seg.size);
	}

	assert(fcs->alloc.size >= MIN_FELLOW_BLOCK);
	assert((fcs->alloc.size & FELLOW_BLOCK_ALIGN) == 0);
	// membuddy min < dskbuddy min
	assert(fcs->alloc.size <= fds->seg.size);

	memset(fcs->alloc.ptr, 0, fcs->alloc.size);

	AZ(fcs->refcnt);
	fcs->refcnt = 1;
	fellow_cache_seg_transition(fcs, fcs->state, FCOS_MUT(fcs, FCOS_BUSY));

	return (fcs->alloc.size);
}

/*
 * segment for auxattr
 */
static int
fellow_busy_seg_alloc(struct fellow_busy *fbo,
    struct fellow_cache_seg *fcs, size_t size)
{
	struct fellow_cache *fc;
	struct buddy_off_extent *fdr;
	struct fellow_disk_seg *fds;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	assert (fcs->state == FCAA_USABLE);
	assert_cache_seg_consistency(fcs);

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	fdr = fellow_busy_region_alloc(fbo, size, 0);
	if (fdr == NULL)
		return (0);
	fds->seg = *fdr;
	if (fellow_busy_seg_memalloc(fbo, fcs) == 0)
		// we do not need to free because we use the busy region
		return (0);
	return (1);
}


// varnish-cache will ask for content-length if it is known.
// otherwise it will ask for cache_param->fetch_chunksize
// so we only need to limit that for mem
// for disk, we need to multiply by two each time we extend

struct fellow_cache_res
fellow_busy_obj_getspace(struct fellow_busy *fbo, size_t *sz, uint8_t **ptr)
{
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	size_t chunksize, spc, delta;
	struct fellow_cache_res fcr;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	AN(sz);
	AN(ptr);

	// #33 https://github.com/varnishcache/varnish-cache/pull/4013
	if (fbo->body_seglist == NULL)
		WRONG("_getspace() called after _trimstore()");

	CHECK_OBJ_NOTNULL(fbo->fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fbo->fc->tune, STVFE_TUNE_MAGIC);
	chunksize = (size_t)1 << fbo->fc->tune->chunk_exponent;
	assert(chunksize <= FIO_MAX);

	if (*sz == 0)
		*sz = (size_t)1 << log2up(fbo->fc->tune->objsize_hint);
	if (*sz == 0)
		*sz = chunksize;

	if (fbo->sz_returned >= fbo->fc->tune->objsize_max)
		return (FCR_ALLOCFAIL("objsize_max reached"));

	fcs = fbo->body_seg;
	if (fcs != NULL && fcs->state == FCS_BUSY) {
		AN(fcs->alloc.size);
		if (fcs->u.fcs.len < fcs->alloc.size) {
			spc = fcs->alloc.size - fcs->u.fcs.len;
			assert(spc > 0);
			if (spc < *sz)
				*sz = spc;
			*ptr = (uint8_t *)fcs->alloc.ptr + fcs->u.fcs.len;
			return (FCR_OK(fbo));
		}

		assert(fcs->u.fcs.len == fcs->alloc.size);
		AZ(fbo->unbusy_seg);
		fbo->unbusy_seg = fcs;
		fcs = NULL;
	}

	/*
	 * for content-length, varnish-cache will ask for less and less.
	 *
	 * if it keeps on asking for more, we
	 * increment each time we exceed the previous estimate
	 */

	if (fbo->sz_estimate == 0) {
		fbo->sz_estimate = *sz;
		fbo->sz_increment = *sz;
	}
	else if (*sz + fbo->sz_returned > fbo->sz_estimate) {
		fbo->growing++;

		delta = *sz + fbo->sz_returned - fbo->sz_estimate;

		if (fbo->sz_increment < delta)
			fbo->sz_increment = delta;
		else
			fbo->sz_increment <<= 1;

		fbo->sz_estimate = fbo->sz_dskalloc + fbo->sz_increment;

		if (fbo->sz_estimate > fbo->fc->tune->objsize_max) {
			fbo->sz_estimate = fbo->fc->tune->objsize_max;
			fbo->sz_increment = 0;
			fbo->growing = 0;
		}
	}
	if (fcs == NULL) {
		fcr = fellow_busy_body_seg_next(fbo);
		if (fcr.status != fcr_ok)
			return (fcr);
		CAST_OBJ_NOTNULL(fcs, fcr.r.ptr, FELLOW_CACHE_SEG_MAGIC);
	}

	assert(fcs->state == FCS_USABLE);
	assert_cache_seg_consistency(fcs);

	/* we set the state of the next segment to BUSY before we unbusy the
	 * previous to signal to _iter() that more data is to come. unbusy
	 * happens in _extend() such that the previous segment only gets
	 * unbusied once the next has data
	 */

	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
	spc = fellow_busy_body_seg_alloc(fbo, fds);
	if (spc == 0)
		return (FCR_ALLOCFAIL("body disk seg alloc"));

	spc = fellow_busy_seg_memalloc(fbo, fcs);
	if (spc == 0) {
		fellow_busy_body_seg_return(fbo, fds);
		return (FCR_ALLOCFAIL("body memory seg alloc"));
	}

	assert(fcs->state == FCS_BUSY);
	AN(fbo->body_seglist);
	AN(FCSL_FDSL(fbo->body_seglist));
	FCSL_FDSL(fbo->body_seglist)->nsegs++;
	assert(FCSL_FDSL(fbo->body_seglist)->nsegs <=
	       FCSL_FDSL(fbo->body_seglist)->lsegs);

#ifdef GETSPACE_PRINTF_DBG
	fprintf(stderr, "%p cur disk 0x%zx mem 0x%zx\n",
	    fbo, fbo->body_region.reg->size, fcs->alloc.size);
	fprintf(stderr, "%p sz %zu/0x%zx spc 0x%zx est 0x%zx inc 0x%zx grow %u "
	    "fll.disk_size 0x%zx cram %d chunk %u\n",
	    fbo, *sz, *sz, spc, fbo->sz_estimate, fbo->sz_increment, fbo->growing,
	    fbo->fll.disk_size, fbo->fll.disk_cram, fbo->fll.chunk_exponent);
#endif

	*sz = spc;
	*ptr = fcs->alloc.ptr;

	return (FCR_OK(fbo));
}

// caller commits used space
void
fellow_busy_obj_extend(struct fellow_busy *fbo, size_t l)
{
	struct fellow_cache_seg *fcs;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	assert(l > 0);

	fcs = fbo->body_seg;
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	assert(fcs->state == FCS_BUSY);
	fcs->u.fcs.len += l;
	fbo->sz_returned += l;
	assert(fcs->u.fcs.len <= fcs->alloc.size);

	if (fbo->unbusy_seg == NULL && fbo->unbusy_seglist == NULL)
		return;

#define FBOE_NIO 2
	unsigned nio = 0;
	struct fellow_busy_io *fbio[FBOE_NIO] = {NULL}, fbiostk[FBOE_NIO];
	FELLOW_LRU_CHGBATCH(lcb, fbo->fco, FBOE_NIO);

	fellow_cache_obj_lock(lcb);
	// while the first segment is busy, we may still relocate the region
	if (fbo->unbusy_seglist != NULL && fcs->idx > 1) {
		fbio[nio] = fellow_cache_seglist_unbusy_locked(fbo, &fbiostk[nio], lcb, fbo->unbusy_seglist);
		if (fbio[nio])
			nio++;
		fbo->unbusy_seglist = NULL;
	}

	if (fbo->unbusy_seg != NULL) {
		fbio[nio] = fellow_cache_seg_unbusy_locked(fbo, &fbiostk[nio], lcb, fbo->unbusy_seg);
		if (fbio[nio])
			nio++;
	}
	fbo->unbusy_seg = NULL;

	fellow_cache_obj_unlock(lcb);

	if (nio)
		fellow_busy_io_submit(fbo->fc, fbio, nio);
}

// remove the current (active, last) seglist from fbo
static void
fellow_busy_obj_trim_seglists(struct fellow_busy *fbo,
    struct fellow_cache_seglist *empty)
{
	struct fellow_cache_seglist *prev;
	struct fellow_disk_seglist *fdsl;
	struct fellow_cache_obj *fco;
	struct buddy_returns *memret;
	struct buddy_off_extent reg;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	CHECK_OBJ_NOTNULL(empty, FELLOW_CACHE_SEGLIST_MAGIC);
	assert(empty->fcs->state == FCL_BUSY || empty->fcs->state == FCL_REDUNDANT);
	fdsl = FCSL_FDSL(empty);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	// only to be called if anything to trim at all
	AZ(fdsl->nsegs);

	memret = BUDDY_RETURNS_STK(fbo->fc->membuddy, BUDDY_RETURNS_MAX);

	AZ(pthread_mutex_lock(&fco->mtx));
	// our disk region is in the previous fdsl
	prev = VLIST_PREV(empty, &fco->fcslhead, fellow_cache_seglist, list);
	// also implicitly asserts that we are not calling it on the first
	// seglist
	CHECK_OBJ_NOTNULL(prev, FELLOW_CACHE_SEGLIST_MAGIC);
	assert(prev->fcs->state == FCL_BUSY || prev->fcs->state == FCL_EMBED_BUSY);
	fdsl = FCSL_FDSL(prev);
	CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);
	VLIST_REMOVE(empty, list);
	reg = fdsl->next;
	// we do not use -1 consistently for "unset offset"
	fdsl->next.off = 0;
	fdsl->next.size = 0;
	fellow_cache_seglist_free(memret, empty, 1);
	AZ(pthread_mutex_unlock(&fco->mtx));

	buddy_return(memret);
	fellow_busy_region_free(fbo, &reg);
}

static inline int
fdr_contains(
    const struct buddy_off_extent *outer,
    const struct buddy_off_extent *inner)
{

	assert(outer->off > 0);
	assert(inner->off > 0);

	return (
	    inner->off >= outer->off &&
	    (typeof(inner->size))inner->off + inner->size <=
	    (typeof(outer->size))outer->off + outer->size);
}

static void
fellow_busy_obj_realloc_seg_mem_unbusy(struct fellow_busy *fbo, struct fellow_cache_seg *fcs,
    buddy_t *membuddy, size_t sz)
{
	struct buddy_ptr_extent mem, ret;
	FELLOW_LRU_CHGBATCH(lcb, fbo->fco, 1);
	unsigned obits, nbits;

	obits = log2up(fcs->alloc.size);
	nbits = log2up(sz);

	assert(nbits <= obits);
	if (nbits == obits) {
		// trim if we don't save a pow2
		buddy_trim1_ptr_extent(membuddy, &fcs->alloc, sz);
		AZ(pthread_mutex_lock(&lcb->fco->mtx));
		fellow_cache_seg_unbusy_locked_finish(fbo, fcs, lcb);
		return;
	}

	mem = buddy_alloc1_ptr_extent_wait(membuddy, FEP_META, sz, 0);
	AN(mem.ptr);
	ret = mem;

	memset(mem.ptr, 0, mem.size);
	memcpy(mem.ptr, fcs->alloc.ptr, fcs->u.fcs.len);

	AZ(pthread_mutex_lock(&lcb->fco->mtx));
	if (fcs->refcnt == 1) {
		ret = fcs->alloc;
		fcs->alloc = mem;
	}
	else
		fcs->lru_to_head = 1;
	fellow_cache_seg_unbusy_locked_finish(fbo, fcs, lcb);
	buddy_return1_ptr_extent(membuddy, &ret);

	/* note: if we did not swap out the memory allocation (so ret ==
	 * mem), the fcs' memory allocation now is too big. But we do
	 * not trim it, because it is going to get LRU'd at the next
	 * opportunity and trimming it could cause an allocation in the
	 * trimmed part, which is bad for fragmentation
	 */
}

/*
 * we need to write full disk blocks and if the membuddy minsize is less than
 * the dskbuddy minsize, we could trim to under the blocksize
 *
 * so we make this a two-step process: Determine surplus, trim disk allocation,
 * trim mem allocation if there is an FCS_BUSY
 */

void
fellow_busy_obj_trimstore(struct fellow_busy *fbo)
{
	struct fellow_body_region *fbr;
	struct buddy_off_extent *fdr;
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_seg *fcs;
	struct fellow_disk_seg *fds;
	struct fellow_cache_obj *fco;
	struct fellow_cache *fc;
	size_t nsz, base, surplus = 0;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fcsl = fbo->body_seglist;
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	fcs = fbo->body_seg;
	if (fcs == NULL)
		goto finish;
	CHECK_OBJ_NOTNULL(fcs, FELLOW_CACHE_SEG_MAGIC);
	fds = FCS_FDS(fcs);
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	fbo->body_seg = NULL;
	fbo->body_seglist = NULL;

	fbr = &fbo->body_region;
	fdr = fbr->reg;

	assert(fcs->state == FCS_USABLE || fcs->state == FCS_BUSY);
	assert_cache_seg_consistency(fcs);

	/* checks */
	if (fcs->state == FCS_BUSY) {
		assert(fcs->u.fcs.len <= fcs->alloc.size);
		AN(fdr);

		/*
		 * fcs must be contained in the fdr, there can
		 * be a free region behind it
		 */

		fds = FCS_FDS(fcs);
		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);
		assert(fds->seg.off > 0);
		assert(fds->seg.size > 0);
		assert(fdr_contains(fdr, &fds->seg));
	} else {
		assert(fcs->state == FCS_USABLE);
		AZ(fcs->alloc.size);
		AZ(fcs->u.fcs.len);
	}

	/* opportunistic: if the busy FCS is the first in the region, we can
	 * still reallocate the region.
	 *
	 * if we hold the only reference, we can also reallocate the memory
	 *
	 * XXX we could also re-write all previous segments if they are still
	 * FCS_INCORE
	 */
	if (fcs->state == FCS_BUSY && fds->segnum == 0 &&
	    fcs->u.fcs.len > 0 &&
	    log2up(fdr->size) > log2up(fellow_rndup(fc->ffd, fcs->u.fcs.len))) {
		struct buddy_reqs *reqs;

		// ensured by fdr_contains above
		assert(fdr->off == fds->seg.off);

		reqs = &fbo->bbrr.reqs;

		buddy_alloc_async_done(reqs);
		BUDDY_REQS_PRI(reqs, FEP_SPCPRI);
		AN(buddy_req_extent(reqs, fcs->u.fcs.len, 0));
		buddy_return1_off_extent(reqs->buddy, fdr);
		AN(buddy_alloc_wait(reqs));
		*fdr = fds->seg = buddy_get_off_extent(reqs, 0);
		buddy_alloc_wait_done(reqs);

		fellow_busy_obj_realloc_seg_mem_unbusy(fbo, fcs, fc->membuddy, fdr->size);

		AN(FCSL_FDSL(fcsl)->nsegs);
		goto finish;
	}

	/* the remainder of the function handles two cases for more than one
	 * segment on the segment list
	 * - body region is unused
	 * - body region partily used and gets trimmed
	 */

	if (fdr) {
		assert(fbr->len >= fcs->alloc.size);

		// fbr offset where fcs starts
		base = fbr->len - fcs->alloc.size;
		nsz = base + fcs->u.fcs.len;

		if (nsz == 0)
			fellow_busy_region_free(fbo, fdr);
		else {
			buddy_trim1_off_extent(fellow_dskbuddy(fbo->fc->ffd),
			    fdr, nsz);
			AZ(fdr->size & FELLOW_BLOCK_ALIGN);
			nsz = fdr->size;
			AN(nsz);
		}

		assert(fcs->alloc.size >= (nsz - base));
		surplus = fcs->alloc.size - (nsz - base);
	}

//	DBG("surplus %zu", surplus);

	if (fcs->state == FCS_BUSY) {
		assert(fcs->alloc.size >= surplus);
		nsz = fcs->alloc.size - surplus;

		if (nsz == 0) {
			AZ(fcs->u.fcs.len);
			fds->seg.off = 0;
			fds->seg.size = 0;
			buddy_return1_ptr_extent(fc->membuddy, &fcs->alloc);

			fco = FCS_FCO(fcs);

			FELLOW_LRU_CHGBATCH(lcb, fco, 1);

			AZ(pthread_mutex_lock(&fco->mtx));
			fellow_cache_seg_transition_locked(lcb, fcs,
			    FCS_BUSY, FCS_USABLE);
			(void) fellow_cache_seg_deref_locked(lcb, fcs);
			FCSL_FDSL(fcsl)->nsegs--;
			fellow_cache_lru_chgbatch_apply(lcb);
			if (fcs->refcnt)
				AZ(pthread_cond_broadcast(&fbo->fco->cond));
			// see fellow_cache_seg_deref()
			while (fcs->refcnt)
				AZ(pthread_cond_wait(&fco->cond, &fco->mtx));
			AZ(pthread_mutex_unlock(&fco->mtx));
			assert(fcs == &fcsl->segs[FCSL_FDSL(fcsl)->nsegs]);
		}
		else
			fellow_busy_obj_realloc_seg_mem_unbusy(fbo, fcs, fc->membuddy, nsz);
	} else {
		AZ(surplus);
		assert(fcs->state == FCS_USABLE);
	}

    finish:
	fco = FCSL_FCO(fcsl);

	#define FBOTRIM_NIO 3
	unsigned nio = 0;
	struct fellow_busy_io *fbio[FBOTRIM_NIO] = {NULL}, fbiostk[FBOTRIM_NIO];
	FELLOW_LRU_CHGBATCH(lcb, fco, 3);

	fellow_cache_obj_lock(lcb);

	if (fcsl->fcs->state == FCL_BUSY && FCSL_FDSL(fcsl)->nsegs == 0) {
		fellow_cache_seg_transition_locked(lcb, fcsl->fcs,
		    FCL_BUSY, FCL_REDUNDANT);
		(void) fellow_cache_seg_deref_locked(lcb, fcsl->fcs);
		// signal under all circumstances, because a reader must
		// not yet ref an empty fcsl (see explanation in
		// fcsc_fcsl_next_locked())
		AZ(pthread_cond_broadcast(&fbo->fco->cond));
		while (fcsl->fcs->refcnt)
			AZ(pthread_cond_wait(&fco->cond, &fco->mtx));
		fellow_cache_obj_unlock(lcb);

		fellow_busy_obj_trim_seglists(fbo, fcsl);

		fellow_cache_obj_lock(lcb);
	}
	else {
		fbio[nio] = fellow_cache_seglist_unbusy_locked(fbo, &fbiostk[nio], lcb, fcsl);
		if (fbio[nio])
			nio++;
	}

	if (fbo->unbusy_seglist != NULL) {
		fbio[nio] = fellow_cache_seglist_unbusy_locked(fbo, &fbiostk[nio], lcb, fbo->unbusy_seglist);
		if (fbio[nio])
			nio++;
	}
	fbo->unbusy_seglist = NULL;

	if (fbo->unbusy_seg != NULL) {
		fbio[nio] = fellow_cache_seg_unbusy_locked(fbo, &fbiostk[nio], lcb, fbo->unbusy_seg);
		if (fbio[nio])
			nio++;
	}
	fbo->unbusy_seg = NULL;

	fellow_cache_obj_unlock(lcb);

	if (nio)
		fellow_busy_io_submit(fc, fbio, nio);
}

#ifdef TEST_DRIVER
static int
fdr_compar(const void *aa, const void *bb)
{
	const struct buddy_off_extent *a = aa;
	const struct buddy_off_extent *b = bb;

	if (a->off < b->off)
		return (-1);
	if (a->off > b->off)
		return (1);
	if (a->size < b->size)
		return (-1);
	if (a->size > b->size)
		return (1);
	return (0);
}
#endif

/* NOT fco mtx */
static void
fellow_busy_log_submit(const struct fellow_busy *fbo)
{
	unsigned r = 0, ndle = DLE_N_REG(fbo->nregion) + 1;
	struct fellow_dle *e, dle_obj[1], dles[ndle];
	struct fellow_cache_obj *fco;
	const struct fellow_disk_obj *fdo;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	AN(fdo->fdo_flags & FDO_F_INLOG);

	fellow_dle_init(dle_obj, 1);
	stvfe_oc_dle_obj(fco->oc, dle_obj);

	fellow_dle_init(dles, ndle);
	if (fbo->nregion)
		r = fellow_dle_reg_fill(dles, ndle, fbo->region, fbo->nregion,
		    DLE_REG_ADD, dle_obj->u.obj.hash);

	assert(r < ndle);
	e = &dles[r++];
	e->type = DLEDSK(DLE_OBJ_ADD);
	memcpy(&e->u.obj, &dle_obj->u.obj, sizeof e->u.obj);
	e->u.obj.start = fbo->fco->fdb;

	assert(r <= ndle);
	fellow_log_dle_submit(fbo->fc->ffd, dles, r);

#ifdef TEST_DRIVER
	/*
	 * additional sanity check that we get back the same regions via
	 * fellow_obj_regions which we have created originally
	 */
	struct buddy_off_extent region[FCO_MAX_REGIONS];
	unsigned n;

	n = fellow_obj_regions(fbo->fc, fbo->fco, region);
	assert(n == fbo->nregion);
	qsort(((struct fellow_busy *)fbo)->region,
	    (size_t)n, sizeof(struct buddy_off_extent), fdr_compar);
	qsort(region,
	    (size_t)n, sizeof(struct buddy_off_extent), fdr_compar);
	for (r = 0; r < n; r++) {
		assert(region[r].off == fbo->region[r].off);
		assert(region[r].size == fbo->region[r].size);
	}
#endif
}

/*
 * write additional seglists
 * write object + embedded seglist
 * wait for all segments to be written (async)
 * - body
 * - aux
 *
 * XXX can save some io for inlog == 0
 */

void
fellow_busy_done(struct fellow_busy *fbo, struct objcore *oc, unsigned inlog)
{
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_obj *fco;
	struct fellow_disk_obj *fdo;
	struct fellow_cache *fc;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	if (fbo->segdowry.ptr)
		buddy_return1_ptr_page(fc->membuddy, &fbo->segdowry);
	if (fbo->segdskdowry.off >= 0) {
		buddy_return1_off_extent(fellow_dskbuddy(fc->ffd),
		    &fbo->segdskdowry);
	}
	BUDDY_POOL_FINI(fbo->segmem);
	buddy_alloc_wait_done(&fbo->bbrr.reqs);

	// nicer way?
	if (fbo->body_seglist != NULL)
		fellow_busy_obj_trimstore(fbo);

	AZ(fbo->unbusy_seg);

	// the first disk seglist is always embedded
	fcsl = VLIST_FIRST(&fco->fcslhead);
	if (fcsl)
		fellow_disk_seglist_fini(FCSL_FDSL(fcsl));

	AZ(fdo->fdo_flags & FDO_F_INLOG);
	AZ(fco->oc);
	// XXX still needed with logstate?
	if (inlog) {
		fdo->fdo_flags |= FDO_F_INLOG;
		fco->oc = oc;
		fdo->fdoa_present = oa2fdoa_present(oc->oa_present);
	}
	fellow_cache_obj_unbusy(fbo, inlog ? FCOL_WANTLOG : FCOL_NOLOG);
}

static void
fellow_busy_free(struct fellow_busy **fbop)
{
	struct fellow_busy *fbo;
	struct fellow_cache *fc;

	TAKE_OBJ_NOTNULL(fbo, fbop, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);

	buddy_return1_ptr_extent(fc->membuddy, &fbo->fbo_mem);
}

/*
 * data is written to the last cache_seglist
 * when done, nsegs is incremented
 *
 * to write segments:
 * - mem alloc space for fdsl
 * - mem alloc space for fcsl & point fcs'es to fds
 * - dsk alloc space for data (disk allocation)
 * - dsk alloc space for fdsl (n x fds)
 *
 * remaining space into first fds
 *
 * disk segment != disk allocation
 * disk segment = checksum, size to write/read
 * disk allocation -> alloc'ed in log
 *
 * rules for writing
 * - always from start to end
 * - if size is known, make one disk allocation
 * - if size is unknown, next alloc must be current + 1
 *
 *
 * the trouble with disk vs. mem segments
 *
 * the log assumes we got max FCO_MAX_REGIONS = 220 regions per obj
 * our memory segments must not be larger than 1/16th - 1/8 of mem
 *
 * so: alloc one big chunk of disk, chop into smaller, individually
 * checksummed segments
 *
 * XXX CHANGE:
 * for chunked: next pow2 with every chunk, so we never get above ~50
 * disk segments
 *
 * Ballpark
 * - disk/mem = 10k
 * min:
 * - mem =  1GB -> max seg 128MB
 * - dsk = 10TB -> max obj 1.28TB
 * max:
 * - max seg because I/O 1<<30 = 1 GB
 * - dsk = 16EB -> max obj 1<<61 2EB
 * - region sz -> 2EB / 220 = 9PB (16PB/8PB)
 * - seg/obj -> 1<<31 = 2g
 * - sizeof(fds) = 56
 * - size of all segments in seglists: 112GB
 *
 * TODO:
 * - region != segment
 * - fbo.io independent of FCO_MAX_REGIONS
 * - introduce max object size depending on dsk size
 * - change region allocation such that max object will always fit
 */

// XXX rearrange seglists ?

static void
fellow_cache_obj_fini(const struct fellow_cache_obj *fco)
{
	struct fellow_disk_seg	*fds;
	struct fellow_disk_obj	*fdo;

	fds = FCS_FDS(FCO_FCS(fco));
	CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);

	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	assert(fdo == (void *)FCO_FCS(fco)->alloc.ptr);

	fh(fds->fht, fds->fh, (char *)fdo + FELLOW_DISK_OBJ_CHK_START,
	    FELLOW_DISK_OBJ_CHK_LEN(fdo));
}

static struct fellow_cache_res
fellow_cache_obj_prepread(struct fellow_cache *fc, fellow_disk_block fdba,
    unsigned crit)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_obj *fco;
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	size_t sz;

	sz = fdb_size(fdba);
	if (! PAOK(sz))
		return (FCR_IOFAIL("bad size"));

	fcr = fellow_cache_obj_new(fc, sz, FDO_MAX_EMBED_SEGS, NULL,
	    crit ? NULL : &dowry,
	    crit ? FEP_OHLCK : FEP_GET);
	if (fcr.status != fcr_ok)
		return (fcr);

	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	fco->fdb = fdba;
	fco->fco_dowry = dowry;

	assert(fco->logstate == FCOL_DUNNO);
	fco->logstate = FCOL_INLOG;

	fcs = FCO_FCS(fco);
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	// we prepare the state the object will have
	// when inserted
	fcs->refcnt = 1;
	fcs->fco_infdb = 1;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_READING);

	return (fcr);
}

// undo fellow_cache_obj_prepread()
static void
fellow_cache_obj_redundant(const struct fellow_cache *fc,
    struct fellow_cache_obj **fcop)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;

	TAKE_OBJ_NOTNULL(fco, fcop, FELLOW_CACHE_OBJ_MAGIC);
	fcs = FCO_FCS(fco);
	CHECK_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);

	assert(fcs->refcnt == 1);
	assert(fcs->fco_infdb);
	assert(fcs->state == FCO_READING);

	fcs->refcnt = 0;
	fcs->fco_infdb = 0;
	fellow_cache_seg_transition_locked_notincore(fcs, FCO_REDUNDANT);

	/* holding mutex not necessary here, but this call site is the exception
	 * and the other call side requires it
	 */

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);
	fellow_cache_obj_lock(lcb);
	fellow_cache_obj_free(fc, lcb, &fco);
}

/*
 * if returns 0, fellow_cache_obj_free() must be called under the lock
 */
static unsigned
fellow_cache_obj_deref_locked(struct fellow_lru_chgbatch *lcb,
    struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	struct fellow_cache_seg *fcs;
	unsigned last, refcnt;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	fcs = FCO_FCS(fco);
	AN(fcs->refcnt);

	assert_cache_seg_consistency(fcs);

	last = fcs->refcnt == 1 && fcs->fco_infdb;
	refcnt = fellow_cache_seg_deref_locked(lcb, fcs);
	if (last) {
		fcs->fco_infdb = 0;
		AZ(refcnt);
		/* REF_FDB_REMOVE */
		AZ(pthread_mutex_lock(&fc->fdb_mtx));
		(void) VRBT_REMOVE(fellow_cache_fdb_head, &fc->fdb_head, fco);
		AN(fc->stats->g_mem_obj);
		fc->stats->g_mem_obj--;
		AZ(pthread_mutex_unlock(&fc->fdb_mtx));
	}
	return (refcnt);
}

void
fellow_cache_obj_deref(struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	unsigned refcount;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	AZ(pthread_mutex_lock(&fco->mtx));
	refcount = fellow_cache_obj_deref_locked(lcb, fc, fco);
	DBG("fco %p refcount %u", fco, refcount);

	if (refcount == 0)
		fellow_cache_obj_free(fc, lcb, &fco);
	else
		fellow_cache_obj_unlock(lcb);
}

/*
 * return with an additional reference
 *
 * if an ocp argument is present and this is the first reference, it gets taken
 *
 */
struct fellow_cache_res
fellow_cache_obj_get(struct fellow_cache *fc,
struct objcore **ocp, uintptr_t priv2, unsigned crit)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco, *nfco;
	struct fellow_cache_seg *fcs;
	struct fellow_cache_seglist *fcsl;
	struct fellow_disk_obj *fdo;
	struct fellow_disk_seglist *fdsl;
	struct buddy_ptr_extent fcsl_mem = buddy_ptr_extent_nil;
	struct buddy_ptr_page dowry = buddy_ptr_page_nil;
	fellow_disk_block fdba;
	unsigned oref = 0;
	const char *err;
	char *ptr;
	size_t spc, sz, nsz;
	int r;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fdba.fdb = priv2;

	struct buddy_returns *rets = BUDDY_RETURNS_STK(fc->membuddy, 2);

	/*
	 * init a new object, then insert it atomically
	 *
	 * for a race, insert might result in an existing object, in which case
	 * we wasted some work and free the extra object again
	 *
	 */
	fcr =  fellow_cache_obj_prepread(fc, fdba, crit);
	if (fcr.status != fcr_ok) {
		fellow_cache_res_check(fc, fcr);
		return (fcr);
	}
	CAST_OBJ_NOTNULL(nfco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);

  again:
	AZ(oref);
	//lint --e{456} flexelint does not grok trylock
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	fco = VRBT_INSERT(fellow_cache_fdb_head, &fc->fdb_head, nfco);
	if (fco != NULL) {
		// the fco might be about to be destroyed see REF_FDB_REMOVE
		r = pthread_mutex_trylock(&fco->mtx);
		if (r != 0) {
			assert(r == EBUSY);
		} else {
			FELLOW_LRU_CHGBATCH(lcb, fco, 1);
			oref = fellow_cache_seg_ref_locked(lcb, FCO_FCS(fco));
			fellow_cache_obj_unlock(lcb);
			AN(oref);
		}
	}
	else if (ocp) {
		AZ(nfco->oc);
		TAKE_OBJ_NOTNULL(nfco->oc, ocp, OBJCORE_MAGIC);
		fc->stats->g_mem_obj++;
	}
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	if (fco != NULL && oref == 0) {
		/* we did not manage to grab a reference on the
		 * object
		 */
		goto again;
	}

	if (fco != NULL) {
		// entries on the fdb need at least one reference
		AN(oref);
		// clean up over-allocated new fco
		fellow_cache_obj_redundant(fc, &nfco);

		if (FCO_STATE(fco) == FCO_READING) {
			AZ(pthread_mutex_lock(&fco->mtx));
			while (FCO_STATE(fco) == FCO_READING)
				fellow_cache_seg_wait_locked(FCO_FCS(fco));
			AZ(pthread_mutex_unlock(&fco->mtx));
		}

		switch (FCO_STATE(fco)) {
		case FCO_WRITING:
		case FCO_INCORE:
			break;
		case FCO_READFAIL:
			fellow_cache_obj_deref(fc, fco);
			(void) fellow_cache_obj_res(fc, fco,
			    FCR_IOFAIL("hit READFAIL object"));
			break;
		case FCO_EVICT:
			// race, retry
			fellow_cache_obj_deref(fc, fco);
			return (fellow_cache_obj_get(fc, ocp, priv2, 0));
		default:
			WRONG("fco state");
		}

		return (fco->fcr);
	}

	TAKEZN(fco, nfco);
	assert(fco->fdb.fdb == fdba.fdb);
	fcr = FCR_OK(fco);
	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	/* not using fellow_cache_seg_read_sync()
	 * because our disk size is smaller than mem size in fcs
	 */
	fcs = FCO_FCS(fco);

	if (FC_INJ || fellow_io_pread_sync(fc->ffd, fcs->alloc.ptr,
		fdb_size(fdba), fdb_off(fdba)) != (int32_t)fdb_size(fdba)) {
		err = FC_ERRSTR("fdo read error");
		goto err;
	}

	fdo = fellow_disk_obj(fcs);
	AN(fdo);
	err = fellow_disk_obj_check(fdo, fdba);
	if (err != NULL)
		goto err;

	fellow_disk_obj_compat(fdo);
	if (fco->oc)
		fco->oc->oa_present = fdoa2oa_present(fdo->fdoa_present);

	/* the check of the fdo-embedded fdsl needs to happen before trim,
	 * because both trim and relocation change data after the used part of
	 * the fdsl
	 */
	fdsl = fellow_disk_obj_fdsl(fdo);
	err = fellow_disk_seglist_check(fdsl);
	if (err != NULL)
		goto err;

	ptr = (void *)(fco + 1);
	nsz = sizeof *fco;
	spc = fco->fco_mem.size - nsz;
	sz = fellow_cache_seglist_size(fdsl->nsegs);

	assert(PAOK(ptr));

	// embed fcsl if need to and possible
	if (fdsl->nsegs == 0) {
		AZ(VLIST_FIRST(&fco->fcslhead));
		fcsl = NULL;
	}
	else if (spc >= sz) {
		fcsl = fellow_cache_seglist_init((void *)ptr, sz, fco, 0);
		AN(fcsl);
		VLIST_INSERT_HEAD(&fco->fcslhead, fcsl, list);
		ptr += sz;
		nsz += sz;
		spc -= sz;
	}
	else {
		fcsl_mem = buddy_alloc1_ptr_extent_wait(fc->membuddy, FEP_META,
		    SEGLIST_SIZE(fcsl, fdsl->nsegs), 0);
		if (FC_INJ || fcsl_mem.ptr == NULL) {
			err = FC_ERRSTR("first disk seglist fcsl alloc failed");
			goto err;
		}
		fcsl = fellow_cache_seglist_init(fcsl_mem.ptr, fcsl_mem.size, fco, 0);
		AN(fcsl);
		fcsl->fcsl_sz = fcsl_mem.size;
		VLIST_INSERT_HEAD(&fco->fcslhead, fcsl, list);
	}
	assert(PAOK(ptr));

	fdsl->lsegs = fdsl->nsegs;
	sz = fellow_disk_obj_slimmed_size(fdo);

	if (spc >= sz) {
		struct buddy_ptr_extent mem;

		fellow_disk_obj_mark_slim(fdo);
		memcpy(ptr, fdo, sz);

		mem = fcs->alloc;

		AZ(pthread_mutex_lock(&fco->mtx));
		fdo = fcs->u.fco.fdo = (void *)ptr;
		fcs->alloc = buddy_ptr_extent_nil;
		AZ(pthread_mutex_unlock(&fco->mtx));

		AN(buddy_return_ptr_extent(rets, &mem));
		nsz += sz;
	}
	else
		fdo = fellow_disk_obj_trim(fc, fcs);

	buddy_trim1_ptr_extent(fc->membuddy, &fco->fco_mem, nsz);

	if (fcsl == NULL)
		TAKE(dowry, fco->fco_dowry);
	else {
		// fdo might have been relocated, and so might fdsl
		fdsl = fellow_disk_obj_fdsl(fdo);
		assert(PAOK(fdsl));
		CHECK_OBJ_NOTNULL(fdsl, FELLOW_DISK_SEGLIST_MAGIC);

		fellow_cache_seglist_associate(fcsl, fdsl, FCS_DISK);
		fellow_cache_seg_transition_locked_notincore(fcsl->fcs, FCL_EMBED);
	}

	// XXX adust for multiple aa segments
#define FDO_AUXATTR(U, l)						\
	fco->aa_seghdr.disk_segs = &fdo->aa_##l##_seg;			\
	fellow_cache_seg_associate(fco->aa_##l##_seg,			\
	    &fdo->aa_##l##_seg, fdo->aa_##l##_seg.seg.size == 0 ?	\
	    FCAA_USABLE : FCAA_DISK);
#include "tbl/fellow_obj_attr.h"

	AZ(pthread_mutex_lock(&fco->mtx));
	assert(fcs->state == FCO_READING);
	if (fcs->refcnt > 1)
		AZ(pthread_cond_broadcast(&fco->cond));
	assert_cache_seg_consistency(fcs);
	AN(fcs->fco_infdb);
	fco->fdo_seghdr.disk_segs = &fdo->fdo_fds;
	assert(fco->fdo_seghdr.fco == fco);
	fellow_cache_seg_transition_locked(lcb, fcs, fcs->state, FCO_INCORE);
	if (fco->oc) {
		AZ(fco->oc->stobj->priv);
		fco->oc->stobj->priv = fco;
	}
	fellow_cache_lru_chgbatch_apply(lcb);
	assert_cache_seg_consistency(fcs);
	AZ(pthread_mutex_unlock(&fco->mtx));

	if (dowry.bits)
		AN(buddy_return_ptr_page(rets, &dowry));
	buddy_return(rets);
	return (fellow_cache_obj_res(fc, fco, FCR_OK(fco)));
  err:
	if (ocp) {
		AZ(*ocp);
		TAKE_OBJ_NOTNULL(*ocp, &fco->oc, OBJCORE_MAGIC);
	}

	/* we are in the process of transitioning to FCO_READING,
	 * fcs is not yet consistent. Just set the failed state,
	 * _evict() will signal any waiters
	 */
	fellow_cache_seg_transition_locked_notincore(FCO_FCS(fco), FCO_READFAIL);
	fellow_cache_obj_deref(fc, fco);

	if (fcsl_mem.ptr)
		AN(buddy_return_ptr_extent(rets, &fcsl_mem));
	buddy_return(rets);
	if (strstr(err, "alloc"))
		fcr = FCR_ALLOCERR(err);
	else
		fcr = FCR_IOERR(err);
	return (fellow_cache_obj_res(fc, fco, fcr));
}

static void
fellow_disk_obj_delete_submit(struct fellow_fd *ffd,
    fellow_disk_block start, const uint8_t hash[DIGEST_LEN],
    const struct buddy_off_extent region[FCO_MAX_REGIONS], unsigned n)
{
	unsigned r, ndle = DLE_N_REG(n) + 1;
	struct fellow_dle *e, dles[ndle];

	fellow_dle_init(dles, ndle);
	if (n)
		r = fellow_dle_reg_fill(dles, ndle, region, n,
		    DLE_REG_DEL_ALLOCED, hash);
	else
		r = 0;

	assert(r < ndle);
	e = &dles[r++];
	e->type = DLEDSK(DLE_OBJ_DEL_ALLOCED);
	memcpy(e->u.obj.hash, hash, (size_t)DIGEST_LEN);
	e->u.obj.start = start;

	assert(r <= ndle);
	fellow_log_dle_submit(ffd, dles, r);
}

/*
 * for drain, wait until object is written before mutating
 */
void
fellow_cache_obj_wait_written(struct fellow_cache_obj *fco)
{
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);

	AZ(pthread_mutex_lock(&fco->mtx));
	/* we must not free the object's disk space while it is still writing */
	while (FCO_STATE(fco) == FCO_WRITING)
		fellow_cache_seg_wait_locked(FCO_FCS(fco));
	AZ(pthread_mutex_unlock(&fco->mtx));
}

static void
fellow_disk_obj_delete_thin(struct fellow_fd *ffd,
    fellow_disk_block start, const uint8_t hash[DIGEST_LEN])
{
	static const char ndle = 1;
	struct fellow_dle e[ndle];

	fellow_dle_init(e, ndle);
	e->type = DLEDSK(DLE_OBJ_DEL_THIN);
	memcpy(e->u.obj.hash, hash, (size_t)DIGEST_LEN);
	e->u.obj.start = start;

	fellow_log_dle_submit(ffd, e, ndle);
}

/* evict, then delete from log (this order is important to avoid
 * fdb reuse)
 *
 * will not be called if the object is still in use,
 * so we can delete all segments immediately (via the log)
 *
 * loses one reference
 */
void
fellow_cache_obj_delete(struct fellow_cache *fc,
    struct fellow_cache_obj *fco, const uint8_t hash[DIGEST_LEN])
{
	const struct fellow_disk_obj *fdo;
	/* one additional region for the object itself */
	struct buddy_off_extent region[FCO_MAX_REGIONS + 1] = {{0}};
	struct buddy_returns *rets;
	struct buddy *dskbuddy;
	struct stvfe_tune *tune;
	enum fcos_state state;
	enum fcol_state logstate;
	fellow_disk_block fdba;
	unsigned u, n;

	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	dskbuddy = fellow_dskbuddy(fc->ffd);
	rets = BUDDY_RETURNS_STK(dskbuddy, BUDDY_RETURNS_MAX);

	FELLOW_LRU_CHGBATCH(lcb, fco, 1);

	fdba = fco->fdb;

	n = fellow_obj_regions(fc, fco, region);
	assert(n <= FCO_MAX_REGIONS);

	fellow_cache_obj_lock(lcb);
	/* we must not free the object's disk space while it still
	 * has ongoing I/O
	 */
	while (FCO_REFCNT(fco) > 1)
		AZ(pthread_cond_wait(&fco->cond, &fco->mtx));

	switch (fco->logstate) {
	case FCOL_DUNNO:
		fco->logstate = FCOL_TOOLATE;
		break;
	case FCOL_WANTLOG:
		// SYNC WITH fellow_cache_async_write_complete()
		// see comment there
		WRONG("fellow_cache_obj_delete FCOL_WANTLOG - can't race");
		break;
	case FCOL_NOLOG:
		break;
	case FCOL_TOOLATE:
		WRONG("fellow_cache_obj_delete FCOL_TOOLATE");
	case FCOL_INLOG:
		fco->logstate = FCOL_DELETED;
		stvfe_oc_log_removed(fco->oc);
		break;
	case FCOL_DELETED:
		WRONG("fellow_cache_obj_delete FCOL_DELETED");
	default:
		WRONG("fellow_cache_obj_delete logstate");
	}

	logstate = fco->logstate;
	state = FCO_STATE(fco);

	// must have been the last reference
	AZ(fellow_cache_obj_deref_locked(lcb, fc, fco));
	fellow_cache_obj_free(fc, lcb, &fco);

	/* UNLOCKED because these states are all final */
	switch (logstate) {
	case FCOL_NOLOG:
	case FCOL_TOOLATE:
		break;
	case FCOL_DELETED:
		/* if read failed, we do not have all regions, but the log might
		 * still work */

		if (state == FCO_READFAIL)
			fellow_disk_obj_delete_thin(fc->ffd, fdba, hash);
		else {
			fellow_disk_obj_delete_submit(fc->ffd, fdba, hash,
			    region, n);
		}
		return;
	default:
		WRONG("fellow_cache_obj_delete logstate UNLOCKED");
	}

	/*
	 * NOTE: FOR FCO_READFAIL, we are likely to leak disk space
	 *
	 * When fellow_obj_regions() fails reading a segment list, we miss
	 * segments and, consequently, regions which the object occupies.
	 */

	/* XXX no async IO here yet: We need to free the dskbuddy space after
	 * the discard is complete. To do that, we would need to store the
	 * off/size of the discard in an additional structure of the io ring.
	 *
	 * we can not just hold the cache async io mtx because it would delay
	 * readahead
	 *
	 */

	assert(n < FCO_MAX_REGIONS + 1);
	region[n].off = fdb_off(fdba);
	region[n].size = fdb_size(fdba);
	n++;

	tune = fc->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	fellow_io_regions_discard(fc->ffd, NULL, region, n,
	    tune->discard_immediate, 1);
	for (u = 0; u < n; u++)
		AN(buddy_return_off_extent(rets, &region[u]));
	buddy_return(rets);
}

#ifndef TEST_DRIVER

#define FCPF_SEG 0x01
#define FCPF_SEGLIST 0x02

// uint16_t magic, otherwise copied from varnish-cache
static int
fc_dump_u16_struct(struct vsb *vsb, const void *ptr, uint16_t magic,
    const char *fmt, ...)
{
	va_list ap;
	const uint16_t *uptr;

	AN(vsb);
	va_start(ap, fmt);
	VSB_vprintf(vsb, fmt, ap);
	va_end(ap);
	if (ptr == NULL) {
		VSB_cat(vsb, " = NULL\n");
		return (-1);
	}
	VSB_printf(vsb, " = %p {", ptr);
	VSB_putc(vsb, '\n');
	uptr = ptr;
	if (*uptr != magic) {
		VSB_printf(vsb, "  .magic = 0x%04x", *uptr);
		VSB_printf(vsb, " EXPECTED: 0x%04x", magic);
		VSB_putc(vsb, '\n');
		VSB_cat(vsb, "}\n");
		return (-3);
	}
	VSB_indent(vsb, 2);
	return (0);
}

static void
fellow_cache_res_panic(struct vsb *vsb, const struct fellow_cache_res *res)
{
	if (res->status == fcr_ok) {
		VSB_printf(vsb, "fcr = { %s { r.integer = %d, r.ptr = %p}},\n",
		    fellow_cache_res_s[res->status],
		    res->r.integer, res->r.ptr);
	}
	else {
		VSB_printf(vsb, "fcr = { %s error: %s },\n",
		    fellow_cache_res_s[res->status], res->r.err);
	}
}

static void
fellow_cache_buddy_ptr_extent_panic(struct vsb *vsb,
    struct buddy_ptr_extent alloc, const char *name)
{
	VSB_printf(vsb, "%s = {.ptr = %p, .size = %zu},\n",
	    name, alloc.ptr, alloc.size);
}

static void
fellow_cache_buddy_off_extent_panic(struct vsb *vsb,
    struct buddy_off_extent fdr, const char *name)
{
	VSB_printf(vsb, "%s = {.off = %jd, .size = %zu},\n",
	    name, fdr.off, fdr.size);
}

static void
fellow_hash_panic(struct vsb *vsb, const union fh *h)
{
	VSB_printf(vsb, "fh[0..8] = %02x%02x%02x%02x%02x%02x%02x%02x,\n",
	    h->sha256[0],
	    h->sha256[1],
	    h->sha256[2],
	    h->sha256[3],
	    h->sha256[4],
	    h->sha256[5],
	    h->sha256[6],
	    h->sha256[7]);
}

static void
fellow_disk_seg_panic(struct vsb *vsb, const struct fellow_disk_seg *fds,
    const char *name)
{
	if (fc_dump_u16_struct(vsb, fds, FELLOW_DISK_SEG_MAGIC, "%s", name))
		return;
	// we should not assert in panic handlers, this is for flexelint
	AN(fds);
	VSB_printf(vsb, "fht = %u, segnum = %u,\n", fds->fht, fds->segnum);
	fellow_cache_buddy_off_extent_panic(vsb, fds->seg, "seg");
	fellow_hash_panic(vsb, fds->fh);
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
}

static void
fellow_cache_seg_panic(struct vsb *vsb, const struct fellow_cache_seg *fcs,
    const char *name)
{
	if (fc_dump_u16_struct(vsb, fcs, FELLOW_CACHE_SEG_MAGIC, "%s", name))
		return;
	VSB_printf(vsb, "state = %s, fcs_onlru = %d, fco_infdb = %d,\n",
	    fcos_state_s[fcs->state], fcs->fcs_onlru, fcs->fco_infdb);
	VSB_printf(vsb, "lcb_add_head = %d, lcb_add_tail = %d, lcb_remove = %d, fco_lru_mutate = %d,\n",
	    fcs->lcb_add_head, fcs->lcb_add_tail, fcs->lcb_remove, fcs->fco_lru_mutate);
	VSB_printf(vsb, "refcnt = %u, fco = %p,\n", fcs->refcnt, FCS_FCO(fcs));
	VSB_printf(vsb, "lru_list.vtqe_prev = %p, prev = %p, next = %p,\n",
	    fcs->lru_list.vtqe_prev,
	    fcs->lru_list.vtqe_prev ? VTAILQ_PREV(fcs, fellow_cache_lru_head, lru_list) : NULL,
	    VTAILQ_NEXT(fcs, lru_list));
	fellow_disk_seg_panic(vsb, FCS_FDS(fcs), "disk_seg");
	fellow_cache_buddy_ptr_extent_panic(vsb, fcs->alloc, "alloc");
	VSB_printf(vsb, "union { .fcs_len = %zu, .fco_fdo = %p },\n",
	    fcs->u.fcs.len, fcs->u.fco.fdo);
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
}

static void
fellow_disk_seglist_panic(struct vsb *vsb,
    const struct fellow_disk_seglist *fdsl, const char *name)
{
	if (PAN_dump_struct(vsb, fdsl, FELLOW_DISK_SEGLIST_MAGIC, "%s", name))
		return;
	VSB_printf(vsb, "idx = %u, version = %u, fht = %u, nsegs = %u, lsegs = %u,\n",
	    fdsl->idx, fdsl->version, fdsl->fht, fdsl->nsegs, fdsl->lsegs);
	fellow_hash_panic(vsb, fdsl->fh);
	fellow_cache_buddy_off_extent_panic(vsb, fdsl->next, "next");
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
}

static void
fellow_cache_seglist_panic_short(struct vsb *vsb,
    const struct fellow_cache_seglist *fcsl, const char *name)
{
	struct fellow_disk_seglist *fdsl;

	//lint --e{438}
next:
	if (PAN_dump_oneline(vsb, fcsl, FELLOW_CACHE_SEGLIST_MAGIC, "%s", name))
		return;

	VSB_printf(vsb, "idx = %u, lsegs = %u, fcsl_sz %zu, fdsl_sz = %zu",
	    fcsl->idx, fcsl->lsegs, fcsl->fcsl_sz, FCSL_FDSLSZ(fcsl));
	fdsl = FCSL_FDSL(fcsl);
	if (fdsl != NULL) {
		VSB_printf(vsb, ", fdsl_idx = %u, fdsl_lsegs = %u, fdsl_nsegs = %u },\n",
		    fdsl->idx, fdsl->lsegs, fdsl->nsegs);
	}
	else
		VSB_cat(vsb, " }\n");
	fcsl = VLIST_NEXT(fcsl, list);
	goto next;
}

static void
fellow_cache_seglist_panic(struct vsb *vsb,
    const struct fellow_cache_seglist *fcsl, const char *name, unsigned flags)
{
	uint16_t u;

	if (! (flags & FCPF_SEGLIST)) {
		fellow_cache_seglist_panic_short(vsb, fcsl, name);
		return;
	}
	//lint --e{438}
next:
	if (PAN_dump_struct(vsb, fcsl, FELLOW_CACHE_SEGLIST_MAGIC, "%s", name))
		return;

	VSB_printf(vsb, "idx = %u, lsegs = %u, fcsl_sz %zu, fdsl_sz = %zu\n",
	    fcsl->idx, fcsl->lsegs, fcsl->fcsl_sz, FCSL_FDSLSZ(fcsl));
	fellow_disk_seglist_panic(vsb, FCSL_FDSL(fcsl), "fdsl");
	if ((flags & FCPF_SEG) && FCSL_FDSL(fcsl)) {
		VSB_cat(vsb, "segs = [\n");
		VSB_indent(vsb, 2);
		for (u = 0; u < FCSL_FDSL(fcsl)->nsegs; u++)
			fellow_cache_seg_panic(vsb, &fcsl->segs[u], "seg[]");
		VSB_indent(vsb, -2);
	}
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
	fcsl = VLIST_NEXT(fcsl, list);
	goto next;

}
#endif

void
fellow_cache_panic(struct vsb *vsb, const struct fellow_cache_obj *fco)
{
#ifdef TEST_DRIVER
	(void) vsb;
	(void) fco;
#else
	unsigned flags = 0;

	if (PAN_dump_struct(vsb, fco, FELLOW_CACHE_OBJ_MAGIC, "fco"))
		return;
	if (fco->lru && fco->lru->fc && fco->lru->fc->tune)
		flags = fco->lru->fc->tune->panic_flags;

	VSB_printf(vsb,
	    "logstate = %d, lru = %p, ntouched = %d,\n",
	    fco->logstate, fco->lru, fco->ntouched);
	fellow_cache_res_panic(vsb, &fco->fcr);
	if (flags & FCPF_SEG)  {
		fellow_cache_seg_panic(vsb, FCO_FCS(fco), "fdo_fcs");
		fellow_cache_seg_panic(vsb, fco->aa_esidata_seg, "esidata");
	}
	fellow_cache_seglist_panic(vsb, VLIST_FIRST(&fco->fcslhead), "fcsl", flags);
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
#endif
}

void
fellow_busy_panic(struct vsb *vsb, const struct fellow_busy *fbo)
{
#ifdef TEST_DRIVER
	(void) vsb;
	(void) fbo;
#else
	unsigned u;

	if (PAN_dump_struct(vsb, fbo, FELLOW_BUSY_MAGIC, "fbo"))
		return;
	VSB_printf(vsb,
	    "sz_estimate = %zu, sz_returned = %zu, sz_increment = %zu,\n",
	    fbo->sz_estimate, fbo->sz_returned, fbo->sz_increment);
	VSB_printf(vsb, "sz_dskalloc = %zu, growing = %u, fbo_mem.size = %zu\n",
	    fbo->sz_dskalloc, fbo->growing, fbo->fbo_mem.size);
	VSB_printf(vsb, "fll disk_size = %zu, disk_cram = %d, chunk_exponent = %u\n",
	    fbo->fll.disk_size, fbo->fll.disk_cram, fbo->fll.chunk_exponent);
	VSB_printf(vsb, "region[%u] = [\n", fbo->nregion);
	VSB_indent(vsb, 2);
	for (u = 0; u < fbo->nregion; u++)
		fellow_cache_buddy_off_extent_panic(vsb, fbo->region[u], "fdr");
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "],\n");
	VSB_printf(vsb, "io_outstanding = %u,\n", fbo->io_outstanding);
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "},\n");
#endif
}

struct fellow_cache_res
fellow_cache_obj_getattr(struct fellow_cache *fc,
    struct fellow_cache_obj *fco,
    enum obj_attr attr, size_t *len)
{
	struct fellow_cache_res fcr;
	struct fellow_disk_obj *fdo;
	const char *err;

	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);
	AN(len);

	fcr = fco->fcr;
	if (fcr.status != fcr_ok)
		return (fcr);

	/* Auxiliary attributes:
	 *
	 * ref is never returned and just ignored by
	 * fellow_cache_seg_auxattr_free()
	 */

	switch (attr) {
#define FDO_FIXATTR(U, l, s, vs)					\
	case OA_##U:							\
		*len = vs;						\
		fcr = FCR_OK(fdo->fa_##l);				\
		break;
#define FDO_VARATTR(U, l)						\
	case OA_##U:							\
		if (fdo->va_##l.aoff == 0) {				\
			fcr = FCR_OK(NULL);				\
			break;						\
		}							\
		*len = fdo->va_##l.alen;				\
		fcr = FCR_OK((uint8_t *)fdo + fdo->va_##l.aoff);	\
		break;
#define FDO_AUXATTR(U, l)						\
	case OA_##U: {							\
		struct fellow_cache_seg *fcs = fco->aa_##l##_seg;	\
		struct fellow_disk_seg *fds = FCS_FDS(fcs);		\
		CHECK_OBJ_NOTNULL(fds, FELLOW_DISK_SEG_MAGIC);		\
		if (fds->seg.size == 0) {				\
			fcr = FCR_OK(NULL);				\
			break;						\
		}							\
		fellow_cache_seg_auxattr_ref_in(fc, fcs);		\
		err = fellow_cache_seg_check(fcs);			\
		if (err != NULL) {					\
			fcr = FCR_IOFAIL(err);				\
			break;						\
		}							\
		*len = fcs->u.fcs.len;					\
		fcr = FCR_OK(fcs->alloc.ptr);				\
		break;							\
	}
#include "tbl/fellow_obj_attr.h"
	default:
		WRONG("Unsupported OBJ_ATTR");
	}
	return (fellow_cache_obj_res(fc, fco, fcr));
}

void *
fellow_busy_setattr(struct fellow_busy *fbo, enum obj_attr attr,
    size_t len, const void *ptr)
{
	struct fellow_cache_obj *fco;
	struct fellow_disk_obj *fdo;
	struct fellow_cache *fc;
	void *retval = NULL;
	size_t sz;
	ssize_t diff;
	uint8_t *p;

	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc = fbo->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	fco = fbo->fco;
	CHECK_OBJ_NOTNULL(fco, FELLOW_CACHE_OBJ_MAGIC);
	fdo = FCO_FDO(fco);
	CHECK_OBJ_NOTNULL(fdo, FELLOW_DISK_OBJ_MAGIC);

	assert(fbo->va_data);
	assert(fbo->va_data >= fdo->va_data);
	assert(fbo->va_data <= fdo->va_data + fdo->va_data_len);
	assert(fbo->va_data_len <= fdo->va_data_len);

	switch (attr) {
		/* Fixed size attributes */
#define FDO_FIXATTR(U, l, s, vs)					\
	case OA_##U:							\
		assert(len <= sizeof fdo->fa_##l);			\
		retval = fdo->fa_##l;					\
		break;
#include "tbl/fellow_obj_attr.h"

		/* Variable size attributes */
#define FDO_VARATTR(U, l)						\
	case OA_##U:							\
		p = (uint8_t *)fdo;					\
		if (fdo->va_##l.alen > 0) {				\
			AN(fdo->va_##l.aoff);				\
			assert(len == fdo->va_##l.alen);		\
			retval = p + fdo->va_##l.aoff;			\
		} else if (len > 0) {					\
			assert(len <= UINT32_MAX);			\
			sz = fbo->va_data_len + len;			\
			assert(sz <= fdo->va_data_len);			\
			retval = fbo->va_data;				\
			diff = fbo->va_data - p;			\
			assert(diff > 0);				\
			assert(diff < UINT32_MAX);			\
			fdo->va_##l.aoff = (uint32_t)diff;		\
			fdo->va_##l.alen = (uint32_t)len;		\
			fbo->va_data += len;				\
		}							\
		break;
#include "tbl/fellow_obj_attr.h"

		/* Auxiliary attributes

		 * XXX: We only support writing once, this
		 * is different to varnish-cache
		 */
#define FDO_AUXATTR(U, l)						\
	case OA_##U: {							\
		struct fellow_cache_seg *fcs = fco->aa_##l##_seg;	\
		if (len == 0)						\
			break;						\
		if (! fellow_busy_seg_alloc(fbo, fcs, len))		\
			break;						\
		assert(fcs->alloc.size >= len);				\
		AZ(fcs->u.fcs.len);						\
		fcs->u.fcs.len = len;						\
		retval = fcs->alloc.ptr;				\
		memcpy(retval, ptr, len);				\
		fellow_cache_seg_unbusy(fbo, fcs);			\
		return (retval);					\
	}
#include "tbl/fellow_obj_attr.h"

	default:
		WRONG("Unsupported OBJ_ATTR");
		break;
	}

	// !! early return for AUXATTR above !
	if (retval != NULL && ptr != NULL)
		memcpy(retval, ptr, len);
	return (retval);
}

static void
fellow_cache_assert_disk_fmt(void)
{
	//lint --e{506} constant value boolean

	assert(sizeof(uintptr_t) == sizeof (fellow_disk_block));

#define S struct fellow_disk_seg
	assert(offsetof(S, segnum) == 4);
	assert(offsetof(S, seg) == 8);
	assert(offsetof(S, fh) == 24);
	assert(sizeof(S) == 24 + 32);
	assert(sizeof(S) == 7 * 8);
#undef S

#define S struct fellow_disk_seglist
	assert(offsetof(S, version) == 4);
	assert(offsetof(S, fht) == 7);
	assert(offsetof(S, fh) == 8);
	assert(offsetof(S, next) == 40);
	assert(offsetof(S, nsegs) == 56);
	assert(offsetof(S, lsegs) == 58);
	assert(offsetof(S, segs) == 64);
	assert(sizeof(S) == 64);
#undef S

#define S struct fellow_disk_obj
	assert(offsetof(S, version) == 4);
	assert(offsetof(S, fdo_fds) == 8);
	assert(offsetof(S, va_data_len) == 64);

	assert(offsetof(S, fa_flags) == 70);
	assert(offsetof(S, fa_len) == 9 * 8);
	assert(offsetof(S, fa_vxid) == 10 * 8);
	assert(offsetof(S, fa_lastmodified) == 11 * 8);
	assert(offsetof(S, fa_gzipbits) == 12 * 8);
	assert(offsetof(S, fa_reserve) == 128);

	assert(offsetof(S, va_vary) == 192);
	assert(offsetof(S, va_headers) == 200);
	assert(offsetof(S, va_reserve) == 208);

	assert(offsetof(S, aa_esidata_seg) == 240);
	assert(offsetof(S, va_data) == 296);
	assert(offsetof(S, va_data) == 37 * 8);
#undef S

}

struct fellow_cache *
fellow_cache_init(struct fellow_fd *ffd, buddy_t *membuddy,
    struct stvfe_tune *tune, fellow_task_run_t taskrun, struct VSC_fellow *stats)
{
	struct fellow_cache *fc;

	AN(ffd);
	AN(membuddy);

	fellow_cache_assert_disk_fmt();

	ALLOC_OBJ(fc, FELLOW_CACHE_MAGIC);
	AN(fc);

	fc->ffd = ffd;
	fc->membuddy = membuddy;
	fc->tune = tune;
	fc->taskrun = taskrun;
	fc->running = 1;

	fellow_cache_lrus_init(fc->lrus);

	AZ(pthread_mutex_init(&fc->fdb_mtx, &fc_mtxattr_errorcheck));
	VRBT_INIT(&fc->fdb_head);
	fc->stats = stats;

	AZ(pthread_key_create(&fc->iter_reqs_key, NULL));

	fellow_cache_async_init(fc, taskrun);

	return (fc);
}

/*
 * the LRU thread is started by the storage layer after loading
 */
void
fellow_cache_fini(struct fellow_cache **fcp)
{
	struct fellow_cache *fc;
	int i;

	TAKE_OBJ_NOTNULL(fc, fcp, FELLOW_CACHE_MAGIC);

	fc->running = 0;
	for (i = 0; i < 5; i++) {
		buddy_wait_kick(fc->membuddy);
		(void) usleep(10*1000);
	}
	fellow_cache_async_fini(fc);
	AZ(pthread_key_delete(fc->iter_reqs_key));
	fellow_cache_lrus_fini(fc->lrus);

	assert(VRBT_EMPTY(&fc->fdb_head));
	AZ(pthread_mutex_lock(&fc->fdb_mtx));
	AZ(fc->stats->g_mem_obj);
	AZ(pthread_mutex_unlock(&fc->fdb_mtx));

	AZ(pthread_mutex_destroy(&fc->fdb_mtx));

	FREE_OBJ(fc);
}

#ifdef TEST_DRIVER
#include "vsha256.h"
#include "fellow_testenv.h"

void
stvfe_sumstat(struct worker *wrk)
{
	(void) wrk;
}

int
stvfe_mutate(struct worker *wrk, struct fellow_cache_lru *lru,
    struct objcore *oc)
{
	WRONG("fellow_cache_test objects to not have an objcore");
	return (0);
}

void
stvfe_setbusyobj(const struct busyobj *bo)
{
	(void) bo;
}

#ifdef DEBUG
const char * const filename = "/tmp/fellowfile.cachetest";
#else
const char * const filename = "/tmp/fellowfile.cachetest.ndebug";
#endif

static int v_matchproto_(fellow_resurrect_f)
resurrect_keep(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (1);
}

static int v_matchproto_(fellow_resurrect_f)
resurrect_discard(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (0);
}

static int
iter_sha256(void *priv, unsigned flush, const void *ptr, ssize_t l)
{
	VSHA256_CTX *sha256ctx = priv;

	(void) flush;
	VSHA256_Update(sha256ctx, ptr, l);
	return (0);
}

static const char * const oatest[OA__MAX] = {
	[OA_LEN] = "lenlenle",
	[OA_VXID] = "vxidvxid",
	[OA_FLAGS] = "f",
	[OA_GZIPBITS] = "gzipbitsgzipbitsgzipbitsgzipbits",
	[OA_LASTMODIFIED] = "lastmodi",
	[OA_VARY] = "vary vary vary varyvaryvary vary varyvaryvary",
	[OA_HEADERS] = "headers hdrs hea",
	[OA_ESIDATA] = "This is OA_ESIDATA <EOS>"
};

void t_getattr(struct fellow_cache *fc, struct fellow_cache_obj *fco)
{
	struct fellow_cache_res fcr;
	unsigned u;
	size_t sz;

	for (u = 0; u < OA__MAX; u++) {
		fcr = fellow_cache_obj_getattr(fc, fco, u, &sz);
		assert(fcr.status == fcr_ok);
		AN(fcr.r.ptr);
		assert(sz == strlen(oatest[u]));
		AZ(strncmp(fcr.r.ptr, oatest[u], sz));
	}
}

/* mimic sfemem_bocdone() without fellow_storage */

int
stvfe_oc_inlog(struct objcore *oc)
{
	(void)oc;
	return (1);
}

void
stvfe_oc_log_removed(struct objcore *oc)
{
	(void)oc;
	return;
}

void
stvfe_oc_log_submitted(struct objcore *oc)
{
	(void)oc;
	return;
}

void
stvfe_oc_dle_obj(struct objcore *oc, struct fellow_dle *e)
{
	AN(oc);	// actually the hash
	AN(e);
	memcpy(e->u.obj.hash, oc, DIGEST_LEN);
	e->u.obj.ttl = 1;
}

static void
test_bocdone(struct fellow_busy *fbo, const uint8_t *hash, unsigned inlog)
{
	fellow_busy_done(fbo, TRUST_ME(hash), inlog);
}

enum incache_e {
	notincache = 0,
	incache = 1
};

/*
 * ======================================================================
 * test fellow cache object creation / unbusy with error injection
 * during write
 */
static void
test_fellow_cache_unbusy_inject(struct fellow_cache *fc)
{
	struct fellow_cache_obj *fco;
	struct fellow_cache_res fcr;
	struct fellow_busy *fbo;
	uint8_t hash[DIGEST_LEN];
	uintptr_t priv2;
	int injcount = -1;
	size_t sz = 1234;
	char *ptr;

	fc_inj_reset();
	while (injcount) {
		DBG("injcount=%d", injcount);
		if (injcount > 0)
			fc_inj_set(injcount);
		fcr = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234);
		if (fcr.status != fcr_ok) {
			DBG("fbo_alloc %d err %s", injcount, fcr.r.err);
			goto next;
		}

		fbo = fcr.r.ptr;
		CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);

		if (fcr.status == fcr_ok)
			fellow_busy_obj_extend(fbo, sz);
		else
			DBG("getspace inj %d err %s", injcount, fcr.r.err);

		fellow_busy_obj_trimstore(fbo);

		test_bocdone(fbo, TRUST_ME(hash), 1);
		fellow_cache_obj_deref(fc, fco);

	  next:
		if (injcount < 0)
			injcount = fc_inj_count();
		else
			injcount--;
	}
	fc_inj_reset();
}

/*
 * ======================================================================
 * test fellow cache with error injection at every possible point
 */

static struct fellow_cache_obj *
test_fellow_cache_obj_get(struct fellow_cache *fc, uintptr_t priv2,
    enum incache_e expect)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco;
	struct objcore *oc1, *toc, ocmem[1];
	int injcount;

	DBG("expect %s", expect == incache ? "incache" : "notincache");

	// baseline, no injection
	fc_inj_reset();
	INIT_OBJ(ocmem, OBJCORE_MAGIC);
	toc = ocmem;
	fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
	if (fcr.status != fcr_ok) {
		AN(toc);
		return (NULL);
	}
	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	DBG("fco1 %p ref %u", fco, FCO_REFCNT(fco));
	if (expect == incache)
		AN(toc);
	else
		AZ(toc);
	oc1 = toc;

	injcount = fc_inj_count();
	DBG("injcount %d", injcount);
	if (injcount == 0)
		return (fco);

	fellow_cache_obj_deref(fc, fco);

	/* dumb wait until seglist reads are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);

	AN(injcount);
	while (injcount) {
		fc_inj_set(injcount);
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		AN(toc);
		assert(fcr.status != fcr_ok);
		DBG("inj %d err %s", injcount, fcr.r.err);
		injcount--;
	}

	DBG("fco1e %p ref %u", fco, FCO_REFCNT(fco));

	fc_inj_reset();
	INIT_OBJ(ocmem, OBJCORE_MAGIC);
	toc = ocmem;
	fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
	DBG("fco2 %p ref %u", fco, FCO_REFCNT(fco));
	assert(toc == oc1);
	CAST_OBJ_NOTNULL(fco, fcr.r.ptr, FELLOW_CACHE_OBJ_MAGIC);
	return (fco);
}

static void test_fellow_cache_obj_iter_final(
    struct fellow_cache *fc, struct fellow_cache_obj **fcop,
    const unsigned char h1[SHA256_LEN], int final)
{
	struct fellow_cache_res fcr;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seg *fcs;
	unsigned char h2[SHA256_LEN];
	struct objcore *toc, ocmem[1];
	VSHA256_CTX sha256ctx;
	uintptr_t priv2;
	unsigned u, refcnt;
	int injcount;
	FCSC c;

	AN(fcop);
	AN(*fcop);

	// baseline, no injection
	fc_inj_reset();
	VSHA256_Init(&sha256ctx);
	fcr = fellow_cache_obj_iter(fc, *fcop, &sha256ctx, iter_sha256, final);
	assert(fcr.status == fcr_ok);
	AZ(fcr.r.integer);
	VSHA256_Final(h2, &sha256ctx);
	AZ(memcmp(h1, h2, sizeof *h1));

	FELLOW_CURSOR_IO(fcio, 33, fc);
	fcsc_init(&c, &fcio, NULL, VLIST_FIRST(&(*fcop)->fcslhead));
	if ((fcs = fcsc_next(&c, 1)) != NULL) {
		while (fcs->state == FCS_READING)
			usleep(100);
		if (fcs->state != FCS_DISK) {
			assert(fcs->state == FCS_INCORE);
			AZ(fcs->refcnt);
			AN(fcs->fcs_onlru);
		}
	} else
		WRONG("no seg");
	fcsc_fini(&c);
	fellow_cache_obj_slim(fc, *fcop, NULL);

	// remember object and deref
	priv2 = (*fcop)->fdb.fdb;
	refcnt = FCO_REFCNT(*fcop);
	for (u = 0; u < refcnt; u++)
		(void) fellow_cache_obj_deref(fc, *fcop);
	*fcop = NULL;

	fc_inj_reset();
	injcount = -1;
	AN(injcount);
	while (injcount) {
		DBG("injcount %d", injcount);

		fc_inj_set(0);
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		assert(fcr.status == fcr_ok);
		fco = fcr.r.ptr;

		fc_inj_set(injcount);
		VSHA256_Init(&sha256ctx);
		fcr = fellow_cache_obj_iter(fc, fco, &sha256ctx,
		    iter_sha256, 0);

		if (fcr.status == fcr_ok) {
			assert(injcount == -1);
			injcount = fc_inj_count();
		} else
			injcount--;

		/* dumb wait until writes are complete */
		while (FCO_REFCNT(fco) > 1)
			usleep(1000);
		assert(FCO_REFCNT(fco) == 1);
		fellow_cache_obj_deref(fc, fco);
	}

	// get back the original number of references
	for (u = 0; u < refcnt; u++) {
		INIT_OBJ(ocmem, OBJCORE_MAGIC);
		toc = ocmem;
		fcr = fellow_cache_obj_get(fc, &toc, priv2, 0);
		assert(fcr.status == fcr_ok);
		assert((toc == NULL) == (u == 0));
		fco = fcr.r.ptr;
	}
	*fcop = fco;
}

static void test_fellow_cache_obj_iter(
    struct fellow_cache *fc, struct fellow_cache_obj **fcop,
    const unsigned char h1[SHA256_LEN])
{
	int final;

	AN(fcop);
	AN(*fcop);

	for (final = 0; final < 2; final++)
		test_fellow_cache_obj_iter_final(fc, fcop, h1, final);
}

/* simulate v-c ObjExtend/ObjWaitExtend */
struct test_iter_wait_s {
	unsigned		magic;
#define TEST_ITER_WAIT_MAGIC	0x9b229927
	unsigned		done;
	unsigned		flush;
	ssize_t			r, w;
	pthread_mutex_t	mtx;
	pthread_cond_t		cond;
	// iter func
	void			*priv;
	objiterate_f		*func;
};

static void
init_test_iter_wait(struct test_iter_wait_s *tiw)
{
	INIT_OBJ(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_init(&tiw->mtx, NULL));
	AZ(pthread_cond_init(&tiw->cond, NULL));
}

static void
fini_test_iter_wait(struct test_iter_wait_s *tiw)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_destroy(&tiw->mtx));
	AZ(pthread_cond_destroy(&tiw->cond));
}

static void
test_iter_extend(struct test_iter_wait_s *tiw, ssize_t sz)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_lock(&tiw->mtx));
	tiw->w += sz;
	AZ(pthread_cond_broadcast(&tiw->cond));
	AZ(pthread_mutex_unlock(&tiw->mtx));
}

static void
test_iter_wait(struct test_iter_wait_s *tiw)
{
	CHECK_OBJ_NOTNULL(tiw, TEST_ITER_WAIT_MAGIC);
	AZ(pthread_mutex_lock(&tiw->mtx));
	assert(tiw->r <= tiw->w);
	while (! tiw->done && tiw->r == tiw->w)
		AZ(pthread_cond_wait(&tiw->cond, &tiw->mtx));
	AZ(pthread_mutex_unlock(&tiw->mtx));
}

// analogous to fellow_stream_f in fellow_storage.c
static int
test_iter_f(void *priv, unsigned flush, const void *ptr, ssize_t len)
{
	struct test_iter_wait_s *tiw;
	const char *p;
	ssize_t l;
	int r = 0;

	CAST_OBJ_NOTNULL(tiw, priv, TEST_ITER_WAIT_MAGIC);

	DBG("r=%zd w=%zd (%zd) done=%u flush=%u ptr=%p len=%zd",
	    tiw->r, tiw->w, tiw->w - tiw->r, tiw->done, flush, ptr, len);

	// can only see OBJ_ITER_END once
	if (flush & OBJ_ITER_END) {
		AZ(tiw->flush);
		tiw->flush = flush;
	}

	if (ptr == NULL || len == 0)
		return (tiw->func(tiw->priv, flush, ptr, len));

	p = ptr;
	assert(tiw->w >= tiw->r);
	l = vmin_t(ssize_t, len, tiw->w - tiw->r);

	if (flush & OBJ_ITER_END)
		assert(l <= len);

	do {
		r = tiw->func(tiw->priv, flush, p, l);
		if (r)
			return (r);

		assert(len >= l);
		tiw->r += l;
		len -= l;
		p += l;

		test_iter_wait(tiw);
		assert(tiw->w >= tiw->r);
		l = vmin_t(ssize_t, len, tiw->w - tiw->r);
	} while (tiw->done == 0 && len > 0);

	return (r);
}

/* to pass arguments to the iter thread */
struct test_iter_thread_s {
	unsigned		magic;
#define TEST_ITER_THREAD_MAGIC	0xcbcd1884
	struct fellow_cache	*fc;
	struct fellow_cache_obj	*fco;
	void			*priv;
	objiterate_f		*func;
	int			final;
	// result
	struct fellow_cache_res	res;
	// the thread itself
	pthread_t		thr;
};

/* iterate in a separate thread */
static void *
test_fellow_cache_obj_iter_thread_f(void *p)
{
	struct test_iter_thread_s *args;
	struct test_iter_wait_s *tiw;

	CAST_OBJ_NOTNULL(args, p, TEST_ITER_THREAD_MAGIC);

	// analogous to sfemem_iterator():
	// have some data ready for obj_iter
	CAST_OBJ_NOTNULL(tiw, args->priv, TEST_ITER_WAIT_MAGIC);
	test_iter_wait(tiw);

	args->res = fellow_cache_obj_iter(args->fc, args->fco,
	    args->priv, args->func, args->final);
	return (NULL);
}

#define DBGSZ(x) \
	DBG(#x "\t%3zu", sizeof(struct x))

static unsigned
lru_find(struct fellow_cache_seg *fcs)
{
	struct fellow_cache_seg *needle;
	struct fellow_cache_lru *lru;

	lru = FCS_FCO(fcs)->lru;
	VTAILQ_FOREACH(needle, &lru->lru_head, lru_list)
	    if (needle == fcs)
		    return (1);
	return (0);
}

#define LCBMAX 16
static void
t_1lcb(struct fellow_cache_seg *fcs,
    uint8_t n, uint8_t i, uint8_t j, uint8_t k)
{
	uint8_t u,v;
	FELLOW_LRU_CHGBATCH(lcb, FCS_FCO(fcs), LCBMAX);
	assert(n <= LCBMAX);

	for (u = 1; u < n + 2; u++) {	// length of remove
		AZ(lcb->n_rem);
		lcb->l_rem = u;

#define chg(from, to)							\
		DBG("%x->%x", from, to);				\
		for (v = 0; v < n; v++) {				\
		    fellow_cache_lru_chg(lcb, &fcs[v],			\
		    (int)(!!(to & 1<<v)) - (int)(!!(from & 1<<v)));	\
		}
#define apply(pos, to)							\
		if (k & (1<<pos)) {					\
			fellow_cache_lru_chgbatch_apply(lcb);		\
			for (v = 0; v < n; v++) {			\
				assert(!!(to & 1<<v) ==			\
				    lru_find(&fcs[v]));		\
			}						\
		}

		chg(0, i);
		apply(0, i);
		chg(i, j);
		apply(1, j);
		chg(j, k);
		apply(2, k);
		chg(k, i);
		apply(3, i);
		chg(i, 0);
		fellow_cache_lru_chgbatch_apply(lcb);
	}
}

#define T_LCB_NFCS 4

static void
t_lcb(struct fellow_cache *fc)
{
	uint8_t i,j,k;
	struct fellow_cache_obj fco[1];
	struct fellow_disk_seg fds[T_LCB_NFCS];
	struct {
		struct fellow_cache_seghdr seghdr;
		struct fellow_cache_seg fcs[T_LCB_NFCS];
		unsigned foo;
	} segarr;

	INIT_OBJ(fco, FELLOW_CACHE_OBJ_MAGIC);
	fco->lru = fellow_cache_lru_new(fc);


	memset(&segarr, 0, sizeof(segarr));
	fellow_cache_segarr_init(&segarr.seghdr, fco, NULL, segarr.fcs, T_LCB_NFCS, FCS_INIT);
	segarr.seghdr.disk_segs = fds;
	for (unsigned u = 0; u < T_LCB_NFCS; u++)
		fellow_disk_seg_init(fds + u, 1);
	fellow_cache_seg_associate_n(segarr.fcs, fds, T_LCB_NFCS, FCS_USABLE);

	AZ(pthread_mutex_init(&fco->mtx, &fc_mtxattr_errorcheck));
	AZ(pthread_mutex_lock(&fco->mtx));
	for (i = 0; i < 1<<T_LCB_NFCS; i++)
		for (j = 0; j < 1<<T_LCB_NFCS; j++)
			for (k = 0; k < 1<<T_LCB_NFCS; k++)
				t_1lcb(segarr.fcs, T_LCB_NFCS, i, j, k);
	AZ(pthread_mutex_unlock(&fco->mtx));
	AZ(pthread_mutex_destroy(&fco->mtx));
	DBG("done %s","---");
}

// generic cast to %p
#define PP(x) (void *)(uintptr_t)(x)
// test concurrent iter while filling object
static void
t_busyobj(unsigned chksum, struct fellow_cache *fc, uint16_t limit_ldsegs, unsigned limit_seg_bits)
{
	unsigned char h1[SHA256_LEN], h2[SHA256_LEN];
	struct fellow_cache_seglist *fcsl;
	struct fellow_cache_obj *fco;
	uint8_t hash[DIGEST_LEN];
	struct fellow_busy *fbo;
	VSHA256_CTX sha256ctx;
	uintptr_t priv2;
	unsigned u;
	char *ptr;
	size_t sz;

	VSHA256_Init(&sha256ctx);
	fbo = fellow_busy_obj_alloc(fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	// --- set up thread to iterate while we create the busy obj
	VSHA256_CTX thr_sha256;
	VSHA256_Init(&thr_sha256);

	struct test_iter_wait_s tiw[1];
	init_test_iter_wait(tiw);
	tiw->priv = &thr_sha256;
	tiw->func = iter_sha256;

	struct test_iter_thread_s iter_thr[1];
	INIT_OBJ(iter_thr, TEST_ITER_THREAD_MAGIC);
	iter_thr->fc = fc;
	iter_thr->fco = fco;
	iter_thr->priv = tiw;
	iter_thr->func = test_iter_f;

	AZ(pthread_create(&iter_thr->thr, NULL,
		test_fellow_cache_obj_iter_thread_f, iter_thr));
	DBG("concurrent test_iter thread %p", PP(iter_thr->thr));

	TEST_LIMIT_LDSEGS = limit_ldsegs;
	TEST_LIMIT_SEG_BITS = limit_seg_bits;
	fcsl = VLIST_FIRST(&fco->fcslhead);
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	for (u = 0; VLIST_NEXT(fcsl, list) == NULL; u++) {
		sz = 1234;

		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
		memset(ptr, 0, sz);
		VSHA256_Update(&sha256ctx, ptr, sz);
		fellow_busy_obj_extend(fbo, sz);
		test_iter_extend(tiw, sz);
		if (u & 1)
			(void) sched_yield();
	}
	TEST_LIMIT_LDSEGS = 0;
	TEST_LIMIT_SEG_BITS = 0;
	VSHA256_Final(h1, &sha256ctx);
	// test trim of a 0-sized busy fcs
	AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	fellow_busy_obj_trimstore(fbo);
	tiw->done = 1;
	test_iter_extend(tiw, 0);
	fcsl = VLIST_FIRST(&fco->fcslhead);
	AN(fcsl);
	fcsl = VLIST_NEXT(fcsl, list);
	AN(fcsl);

	// fixattr always return a pointer
	for (u = OA_VARY; u < OA__MAX; u++)
		AZ(fellow_cache_obj_getattr(fc, fco, u, &sz).r.ptr);
	for (u = 0; u < OA__MAX; u++)
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	t_getattr(fc, fco);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	priv2 = fco->fdb.fdb;

	// check thread status
	DBG("concurrent test_iter thread join %p", PP(iter_thr->thr));
	AZ(pthread_join(iter_thr->thr, NULL));
	assert(tiw->r == tiw->w);
	VSHA256_Final(h2, &thr_sha256);
	AZ(memcmp(h1, h2, sizeof *h1));
	DBG("concurrent test_iter hash ok %02x%02x%02x%02x...",
	    h1[0], h1[1], h1[2], h1[3]);
	fini_test_iter_wait(tiw);

	/* dumb wait until writes are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);
	//fellow_cache_obj_deref(fc, fco);
	fellow_cache_obj_delete(fc, fco, hash);
}

/*
 * ============================================================
 * infrastructure for running test cases on fellow_cache with-
 * out fellow_storage: We need a thread to watch the disk buddy
 * and trigger log flushes to make (faster) progress
 * */

struct t_fc {
	unsigned		magic;
#define TFC_MAGIC		0x8956a01b
	unsigned		shutdown;
	struct fellow_cache	*fc;
	struct VSC_fellow	stats;
	pthread_t		thr;
};

static void *
t_logwatcher_kicker(void *priv)
{
	struct fellow_cache *fc;
	struct fellow_fd *ffd;
	struct t_fc *tfc;
	buddy_t *buddy;

	CAST_OBJ_NOTNULL(tfc, priv, TFC_MAGIC);
	fc = tfc->fc;
	CHECK_OBJ_NOTNULL(fc, FELLOW_CACHE_MAGIC);
	ffd = fc->ffd;
	buddy = fellow_dskbuddy(ffd);

	while (! tfc->shutdown) {
		buddy_wait_needspace(buddy);
		fellow_logwatcher_kick(ffd);
	}

	return (NULL);
}

static struct t_fc *
t_fc_init(struct fellow_fd *ffd, buddy_t *membuddy, struct stvfe_tune *tune)
{
	struct t_fc *tfc;

	ALLOC_OBJ(tfc, TFC_MAGIC);
	AN(tfc);

	tfc->fc = fellow_cache_init(ffd, membuddy, tune, fellow_simple_task_run,
	    &tfc->stats);
	AN(tfc->fc);
	AZ(pthread_create(&tfc->thr, NULL, t_logwatcher_kicker, tfc));

	return (tfc);
}

static void
t_fc_fini(struct t_fc **tfcp)
{
	struct t_fc *tfc;

	TAKE_OBJ_NOTNULL(tfc, tfcp, TFC_MAGIC);
	AZ(tfc->shutdown);
	tfc->shutdown = 1;
	buddy_wait_kick(fellow_dskbuddy(tfc->fc->ffd));
	AZ(pthread_join(tfc->thr, NULL));
	fellow_cache_fini(&tfc->fc);
	AZ(tfc->fc);
	AZ(tfc->stats.g_mem_obj);
	FREE_OBJ(tfc);
}

static void
t_cache(const char *fn, unsigned chksum)
{
	size_t sz;
	struct fellow_fd *ffd;
	buddy_t mb, *membuddy = &mb;
	struct t_fc *tfc;
	struct fellow_cache_obj *fco;
	struct fellow_cache_seglist *fcsl;
	struct fellow_busy *fbo;
	uintptr_t priv2;
	void *resur_priv = NULL;
	unsigned u;
	uint16_t v;
	uint8_t hash[DIGEST_LEN];
	VSHA256_CTX sha256ctx;
	unsigned char h1[SHA256_LEN];
	struct stvfe_tune tune[1];
	const size_t memsz = 20 * 1024 * 1024;
	const size_t dsksz = 100 * 1024 * 1024;
	const size_t objsize_hint = 4 * 1024;
	struct fellow_cache_res fcr;
	int injcount;
	char *ptr;

	DBGSZ(fellow_disk_seg);
	DBGSZ(fellow_disk_seglist);
	DBG("FELLOW_DISK_SEGLIST_MAX_SEGS %u", FELLOW_DISK_SEGLIST_MAX_SEGS);
	assert(FELLOW_DISK_SEGLIST_MAX_SEGS <= UINT16_MAX);
	DBGSZ(fellow_disk_obj);

	DBGSZ(fellow_cache_seg);
	DBGSZ(fellow_cache_seglist);
	DBGSZ(fellow_cache_obj);

	// canary so size increase does not happen unnoticed
	sz  = sizeof(struct fellow_cache_obj);
	assert(sz <= 360);

	AZ(stvfe_tune_init(tune, memsz, dsksz, objsize_hint));
	tune->hash_obj = chksum;
	// for injection tests
	tune->ioerr_obj = 1;
	tune->ioerr_log = 1;
	tune->allocerr_obj = 1;
	tune->allocerr_log = 1;
	// for coverage - hit cramming paths
	tune->cram = -64;
	AZ(stvfe_tune_check(tune));

	buddy_init(membuddy, MIN_BUDDY_BITS, memsz,
	    BUDDYF(mmap), NULL, NULL, NULL);
	ffd = fellow_log_init(fn, dsksz, objsize_hint,
	    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
	XXXAN(ffd);
	fellow_log_open(ffd, resurrect_discard, &resur_priv);

	tfc = t_fc_init(ffd, membuddy, tune);
	AN(tfc);

	t_lcb(tfc->fc);

	// === empty obj, loop sizes
	for (sz = 0; sz < 4096 ; sz += 8) {
		fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, sz).r.ptr;
		CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
		test_bocdone(fbo, TRUST_ME(hash), 1);
		fellow_cache_obj_delete(tfc->fc, fco, hash);
	}

	// === max out region alloc
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);

	u = 0;
	while (fbo->nregion < FCO_MAX_REGIONS - 1) {
		struct fellow_cache_seglist *fcsl;
		struct fellow_body_region *fbr;
		struct buddy_off_extent *fdr;
		struct fellow_cache_seg *fcs;

		/* allocate segments until seglist is full
		 *
		 * skip body region alloc heuristic in
		 * fellow_busy_body_seg_alloc() by making sure we always have an
		 * empty region
		 */
		fcsl = fbo->body_seglist;
		fbr = &fbo->body_region;
		while (fbo->nregion < FCO_MAX_REGIONS - 1 &&
		    FCSL_FDSL(fcsl)->nsegs < FCSL_FDSL(fcsl)->lsegs) {
			fdr = fbr->reg;
			if (fdr == NULL || fbr->len == fdr->size) {
				memset(fbr, 0, sizeof *fbr);
				fdr = fellow_busy_region_alloc(fbo, 1234,
				    INT8_MAX);
				fbr->reg = fdr;
				AZ(fbr->len);
			}
			AN(fdr);
			assert(fbr->len < fdr->size);
			assert(fcsl->segs[FCSL_FDSL(fcsl)->nsegs].state ==
			       FCS_USABLE);
			fcs = &fcsl->segs[FCSL_FDSL(fcsl)->nsegs++];
			// test only
			fbo->fll.chunk_exponent = 12;
			AN(fellow_busy_body_seg_alloc(fbo, FCS_FDS(fcs)));
			// pretend - not actually written
			fcs->state = FCS_DISK;
			fbo->body_seg = fcs;
		}

		if (fbo->nregion == FCO_MAX_REGIONS - 1)
			break;

		fcsl = fbo->body_seglist;
		fcr = fellow_disk_seglist_alloc(fbo, fcsl, 64, FH_SHA256);
		assert(fcr.status == fcr_ok);
		CAST_OBJ_NOTNULL(fcsl, fcr.r.ptr, FELLOW_CACHE_SEGLIST_MAGIC);
		u++;

		fbo->body_seglist = fcsl;
		assert(FCSL_FDSL(fcsl)->lsegs >= 3);
		if (FCO_MAX_REGIONS - fbo->nregion <= 4)
			FCSL_FDSL(fcsl)->lsegs = 3;
		else
			FCSL_FDSL(fcsl)->lsegs = 1;
	}
	DBG("seglists created %u", u);

	for (u = 0; u < OA__MAX; u++) {
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	}
	t_getattr(tfc->fc, fco);

	/*
	 * #39
	 * test fellow_busy_obj_trimstore() returning
	 * the second last segment (ESI attr are last)
	 */
	AN(fbo->body_seg);
	assert(fbo->body_seg->state == FCS_DISK);
	fbo->body_seg->state = FCS_USABLE;
	AN(fellow_busy_seg_memalloc(fbo, fbo->body_seg));

	assert(fbo->nregion == FCO_MAX_REGIONS);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === alloc space, dont use
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	{
		sz = dsksz;
		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	}
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === alloc space, trim
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	{
		sz = 1234;
		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
	}
	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	(void)fellow_cache_obj_lru_touch(fco);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === alloc space error inject
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fc_inj_reset();
	injcount = -1;
	while (injcount) {
		DBG("injcount=%d", injcount);
		if (injcount > 0)
			fc_inj_set(injcount);

		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok)
			fellow_busy_obj_extend(fbo, sz);
		else
			assert(fcr.status == fcr_allocerr);
		if (injcount < 0)
			injcount = fc_inj_count();
		else
			injcount--;
	}

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === hit objsize_max
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === hit objsize_max with big chunks
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		sz = dsksz;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	// === hit objsize_max under memory pressure
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	do {
		if (fbo->nregion < FCO_MAX_REGIONS - FCO_REGIONS_RESERVE)
			FC_INJ_SZLIM_SET(4096);

		sz = 1234;
		fcr = fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr);
		if (fcr.status == fcr_ok) {
			fellow_busy_obj_extend(fbo, sz);
			continue;
		}
		assert(fcr.status == fcr_allocerr);
		AZ(strcmp(fcr.r.err, "objsize_max reached"));
		for (u = 0; u < OA__MAX; u++) {
			AN(fellow_busy_setattr(fbo, u,
				strlen(oatest[u]), oatest[u]));
		}
		t_getattr(tfc->fc, fco);
	} while (fcr.status == fcr_ok);

	fellow_busy_obj_trimstore(fbo);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	fellow_cache_obj_delete(tfc->fc, fco, hash);

	test_fellow_cache_unbusy_inject(tfc->fc);

	for (u = 0; u < 10; u++)
		for (v = MIN_FELLOW_BITS; v < MIN_FELLOW_BITS + 8; v++)
			t_busyobj(chksum, tfc->fc, u, v);

	// === alloc, then evict
	VSHA256_Init(&sha256ctx);
	fbo = fellow_busy_obj_alloc(tfc->fc, &fco, &priv2, 1234).r.ptr;
	CHECK_OBJ_NOTNULL(fbo, FELLOW_BUSY_MAGIC);
	fcsl = VLIST_FIRST(&fco->fcslhead);
	CHECK_OBJ_NOTNULL(fcsl, FELLOW_CACHE_SEGLIST_MAGIC);
	for (u = 0; VLIST_NEXT(fcsl, list) == NULL; u++) {
		void *p;
		sz = 1234;

		AZ(fellow_busy_obj_getspace(fbo, &sz, (uint8_t **)&ptr).status);
		memset(ptr, 0, sz);
		p = ptr;
		(void) snprintf(ptr, sz, "We got %zu bytes at %p", sz, p);
		if (sz > 1234)
			sz = 1234;
		VSHA256_Update(&sha256ctx, ptr, sz);
		fellow_busy_obj_extend(fbo, sz);
	}
	VSHA256_Final(h1, &sha256ctx);
	fellow_busy_obj_trimstore(fbo);
	// if this fails, u is too low
	fcsl = VLIST_FIRST(&fco->fcslhead);
	AN(fcsl);
	fcsl = VLIST_NEXT(fcsl, list);
	AN(fcsl);
	// fixattr always return a pointer
	for (u = OA_VARY; u < OA__MAX; u++)
		AZ(fellow_cache_obj_getattr(tfc->fc, fco, u, &sz).r.ptr);
	for (u = 0; u < OA__MAX; u++)
		AN(fellow_busy_setattr(fbo, u, strlen(oatest[u]), oatest[u]));
	t_getattr(tfc->fc, fco);
	test_bocdone(fbo, TRUST_ME(hash), 1);
	priv2 = fco->fdb.fdb;

	/* dumb wait until writes are complete */
	while (FCO_REFCNT(fco) > 1)
		usleep(1000);
	fellow_cache_obj_deref(tfc->fc, fco);

	/* get back object without and with close/open */
	for (u = 0; u < 2; u++) {
		fco = test_fellow_cache_obj_get(tfc->fc, priv2, notincache);
		assert_cache_seg_consistency(FCO_FCS(fco));
		DBG("fco1 %p ref %u", fco, FCO_REFCNT(fco));

		// second access does not assign the oc
		AN(fco == test_fellow_cache_obj_get(tfc->fc, priv2, incache));
		assert_cache_seg_consistency(FCO_FCS(fco));
		DBG("fco2 %p ref %u", fco, FCO_REFCNT(fco));

		// get attrs and compare
		t_getattr(tfc->fc, fco);

		// get segments and compare
		test_fellow_cache_obj_iter(tfc->fc, &fco, h1);

		assert_cache_seg_consistency(FCO_FCS(fco));

		fellow_cache_obj_deref(tfc->fc, fco);

		DBG("fco3 %p ref %u", fco, FCO_REFCNT(fco));
		if (u == 0)
			fellow_cache_obj_deref(tfc->fc, fco);
		else
			fellow_cache_obj_delete(tfc->fc, fco, hash);


		t_fc_fini(&tfc);
		AZ(tfc);

		fellow_log_close(&ffd);
		BWIT_ISEMPTY(membuddy->witness);
		buddy_fini(&membuddy, BUDDYF(unmap), NULL, NULL, NULL);
		AZ(ffd);

		if (u == 1)
			break;

		membuddy = &mb;
		buddy_init(membuddy, MIN_BUDDY_BITS, 10 * 1024 * 1024,
		    BUDDYF(mmap), NULL, NULL, NULL);
		ffd = fellow_log_init(fn, dsksz, objsize_hint,
		    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
		XXXAN(ffd);
		fellow_log_open(ffd, resurrect_keep, &resur_priv);

		tfc = t_fc_init(ffd, membuddy, tune);
		AN(tfc);
	}
}

static void
t_tailq_concat(void)
{
	struct fellow_cache_seg	fcs[1];
	struct fellow_cache_lru_head	h1[1], h2[2];

	INIT_OBJ(fcs, FELLOW_CACHE_SEG_MAGIC);
	VTAILQ_INIT(h1);
	VTAILQ_INIT(h2);
	VTAILQ_INSERT_TAIL(h2, fcs, lru_list);
	VTAILQ_CONCAT(h1, h2, lru_list);
	assert(VTAILQ_FIRST(h1) == fcs);
	AZ(VTAILQ_NEXT(fcs, lru_list));
}

#endif
#ifdef TEST_DRIVER

// based on the plan, we pretend to acutally run it for all cram
// variations until sz == 0
static void
t_plan_layout(size_t sz, struct fellow_layout_limits fll,
    size_t spc, unsigned segs_avail, unsigned regs_avail, int indent)
{
	size_t segsz, spc_n_segs, n;
	int8_t cram;

	if (sz == 0)
		return;

	if (spc == 0) {
		fll = fellow_plan_layout(sz, MIN_FELLOW_BITS, segs_avail & 1 ? 2 : -2,
		    segs_avail, regs_avail, 0);
		AN(regs_avail);
		DBG("%*sfll: sz %zu disk_size %zu disk_cram %d seg_chunk_exp %u",
		    indent, "", sz, fll.disk_size, fll.disk_cram, fll.chunk_exponent);
		indent++;
		for (cram = 0; cram <= abs(fll.disk_cram); cram++)
			t_plan_layout(sz, fll, fll.disk_size >> cram, segs_avail, regs_avail - 1, indent);
		return;
	}

	DBG("%*ssz %zu spc %zu",
	    indent, "", sz, spc);

	segsz = (size_t)1 << fll.chunk_exponent;
	spc_n_segs = spc >> fll.chunk_exponent;
	spc &= (segsz - 1);

	while (1) {
		n = sz >> fll.chunk_exponent;
		if (n > spc_n_segs)
			n = spc_n_segs;
		if (n > segs_avail)
			n = segs_avail;
		spc_n_segs -= n;
		segs_avail -= n;

		sz -= n << fll.chunk_exponent;
		if (sz == 0)
			return;
		if (segs_avail == 0) {
			// XXX can be smaller outside test
			segs_avail = fellow_cache_seglist_capacity(fll.chunk_exponent);
			AN(regs_avail);
			regs_avail--;
		}
		if (sz <= spc) {
			// last segment can be smaller
			AZ(spc_n_segs);
			AN(segs_avail);
			return;
		}
		if (spc_n_segs == 0)
			break;
		if (sz < segsz) {
			// last segment was seg-sized
			// can be > 1 if cram == 0 due to roundup
			assert(spc_n_segs >= 1);
			AN(segs_avail);
			return;
		}
	}
	AZ(spc_n_segs);
	AZ(spc);
	t_plan_layout(sz, fll, 0, segs_avail, regs_avail, indent + 1);
}

static void
t_fellow_plan_seg(double scale)
{
	unsigned u, bits, reg, plan_remaining, segs_avail, iter;
	struct fellow_layout_limits fll = {0};
	size_t sz, cap;

	// limit t_plan_layout runtime (~20 minutes for 100 with DEBUG)
#ifdef DEBUG
	iter = 3;
#else
	iter = 100;
#endif

	for (u = 0; u < iter; u++) {
		sz = drand48() * scale + 1;
		plan_remaining = reg = FCO_MAX_REGIONS;
		segs_avail = drand48() * 56;

		bits = fellow_plan_seg(sz, MIN_FELLOW_BITS, segs_avail, &plan_remaining);
		cap = fellow_cache_seglist_capacity(bits);
		/* nsegs < cap * reg */
		assert(rup_min(sz, bits) >> bits <= segs_avail + cap * (reg - plan_remaining));

		DBG("%s", "-");

		plan_remaining = reg;
		(void) fellow_plan_layout(sz, MIN_FELLOW_BITS, segs_avail & 1 ? 4 : -4,
		    segs_avail, plan_remaining, 0);
		DBG("%s", "---");

		DBG("exec sz %zu segs_avail %u", sz, segs_avail);
		t_plan_layout(sz, fll, 0, segs_avail, FCO_MAX_REGIONS, 0);
	}

}

#include <vtim.h>

static void
t_layout_plan(void)
{

	double scale = 1.1e3;
	double dt, t0 = VTIM_mono();

	while (scale < 1.1e16) {
		t_fellow_plan_seg(scale);
		scale *= 10;
	}
	dt = VTIM_mono() - t0;
	fprintf(stderr, "t_layout_plan dt = %f, n_plan_layout = %llu (%f/s), n_plan_seg = %llu (%f/s)\n",
	    dt, n_plan_layout, (double)n_plan_layout/dt, n_plan_seg, (double)n_plan_seg/dt);
	//fellow_plan_layout();
}
/*
 * if this fails, the strategy in fellow_busy_body_seglist_alloc() needs to be
 * adjusted
 */
static void
t_seglist_sizes(void)
{
	struct fellow_disk_seglist *fdsl;
	unsigned u, fcslsegs, fdslsegs, fdslcap;
	size_t fdslsz;

	for (u = 12; u < 30; u++) {
		fcslsegs = fellow_cache_seglist_fit((size_t)1 << u);
		fdslsegs = fellow_disk_seglist_fit((size_t)1 << u);
		assert(fcslsegs <= fdslsegs);
		fdslsz = rup_min(SEGLIST_SIZE(fdsl, fcslsegs),
		    MIN_FELLOW_BITS);
		fdslcap = fellow_disk_seglist_fit(fdslsz);
		fprintf(stderr, "seglist 1<<%2u fcsl %6u fdsl %6u "
		    "-> fdsl_size %7zu (%3zu blocks) cap %6u waste %6u\n",
		    u, fcslsegs, fdslsegs,
		    fdslsz, fdslsz >> MIN_FELLOW_BITS,
		    fdslcap, fdslcap - fcslsegs);
		if (fcslsegs >= FELLOW_DISK_SEGLIST_MAX_SEGS ||
		    fdslsegs >= FELLOW_DISK_SEGLIST_MAX_SEGS)
			break;
	}
}

int
main(int argc, char **argv)
{
	unsigned l, hash;
	const char *fn = filename;

	if (argc > 1)
		fn = argv[1];

	DBG("fellow_busy %zu", sizeof(struct fellow_busy));
	assert(sizeof(struct fellow_busy) <= 2 * MIN_FELLOW_BLOCK);

	t_seglist_sizes();

	t_layout_plan();

	t_tailq_concat();

#ifdef HAVE_XXHASH_H
	l = FH_LIM;
#else
	l = FH_SHA256 + 1;
#endif

	for (hash = FH_SHA256; hash < l; hash++) {
		if (fh_name[hash])
			t_cache(fn, hash);
	}
	printf("OK\n");
	return (0);
}
#endif
