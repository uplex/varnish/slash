/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>	/* 6.2 */
#include <limits.h>
#include <stdarg.h>

#define VOPT_DEFINITION
#define VOPT_INC "slashmap_options.h"

#include <vapi/voptget.h>
#include <vapi/vsl.h>
#include <vapi/vsm.h>
#include <vapi/vsig.h>

#include <vdef.h>
#include <vas.h>
#include <vut.h>
#include <miniobj.h>

#include "foreign/vcurses.h"

#include "slashmap.h"
#include "pow2_units.h"

static struct VUT *vut;

static int keep_running = 0;
static int block_sizes = 0;
static int sz_width = 14;
static int sz_percol = 0;
static int sz_cols = 0;

static unsigned zoom;
static size_t maplen = 0;
static size_t offset = 0;

static void __attribute__((__noreturn__))
usage(int status)
{
	const char **opt;

	fprintf(stderr, "Usage: %s <options>\n\n", vut->progname);
	fprintf(stderr, "Options:\n");
	for (opt = vopt_spec.vopt_usage; *opt != NULL; opt += 2)
		fprintf(stderr, " %-25s %s\n", *opt, *(opt + 1));
	exit(status);
}

static const char * const shrt =
    "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

static WINDOW *w_status = NULL;
static WINDOW *w_help = NULL;
static WINDOW *w_map = NULL;
static WINDOW *w_bar = NULL;

static void
make_windows(const struct slashmap *map);
static void
printmap(const struct slashmap *map, const char *ident)
{
	CHECK_OBJ_NOTNULL(map, BUDDY_MAP_MAGIC);

	struct bitf * const *fp;
	unsigned ll, l;
	size_t fre = 0;
	const size_t size = map->size;
	char buf[64];
	int b, w, field;

	// ascii map
	size_t msz;
	char c;
	size_t idx, page, pagesz;
	int yy, xx;

	struct mappage mp;
	// ffs cache for next_freepage
	size_t cache[map->max + 1];
	for (l = 0; l <= map->max; l++)
		cache[l] = SIZE_MAX;

	// space on screen
	getmaxyx(w_map, yy, xx);
	assert(yy >= 0);
	assert(xx >= 0);
	maplen = (size_t)(ssize_t)yy;
	maplen *= (size_t)(ssize_t)xx;

	// default zoom
	if (zoom == 0) {
		do {
			msz = bitf_nbits(map->freemap[zoom++]);
		} while (msz > maplen);
		zoom += map->min;
		zoom--;
		offset = 0;
	}

	assert(zoom >= map->min);
	assert(zoom <= map->max);

	// total map size at zoom level (rounding up to zoom)
	msz = (((size_t)1 << zoom) - 1);
	msz += size;
	msz >>= zoom;

	if (msz <= maplen) {
		maplen = msz;
		offset = 0;
	} else if (offset + maplen > msz) {
		offset = msz - maplen;
	}

	char m[maplen + 2];

	m[maplen] = '<';
	m[maplen + 1] = '\0';
	memset(m, '*', maplen);

	ll = map->max - map->min;
	size_t nset[ll + 1];
	size_t max = 0;

	for (l = 0; l <= ll; l++) {
		nset[l] = bitf_nset(map->freemap[ll - l]);
		if (nset[l] > max)
			max = nset[l];
	}

	w = snprintf(buf, sizeof buf, "%zu", max);
	// "  %5s: " 2 + 5 + 2 = 9, one additional space to separate
	w += 10;
	if (w > sz_width) {
		sz_width = w;
		make_windows(map);
	}
	field = sz_width - 10;

	// status
	AN(w_status);
	(void) werase(w_status);
	(void) werase(w_bar);
	(void) werase(w_map);

	for (l = 0, b = (int)map->max;
	     l <= ll;
	     l++, b--) {
		if (nset[l] == 0)
			continue;
		fre += nset[l] << b;
		if (block_sizes) {
			w = snprintf(buf, sizeof buf,
			    "%c)%5s: %*zu", shrt[ll - l],
			    pow2_units[b], field, nset[l]);
		} else {
			w = snprintf(buf, sizeof buf,
			    "  %5s: %*zu",
			    pow2_units[b], field, nset[l]);
		}
		assert(w > 9);
		assert(w < sizeof buf);
		(void) mvwaddnstr(w_status,
		    ((int)l % sz_percol),
		    ((int)l / sz_percol) * sz_width,
		    buf, w);
	}

	assert(sz_cols > 0);
	yy = 0;
	xx = sz_cols * sz_width;
#define pst(...) \
	(void) mvwprintw(w_status, yy++, xx, __VA_ARGS__)

	pst("%20zu bytes free", fre);
	pst("%20zu bytes alloced", size - fre);
	pst("%20zu bytes total", size);
	pst("%20.2f %% used", (1 - ((double)fre / size)) * 100);

	yy++;
	xx += 19;

	pst("* allocated");

	if (block_sizes) {
		xx -= 4;
		pst("%c - %c free", shrt[0], shrt[ll]);
		xx += 4;
	} else {
		pst("_ free");
	}
	if (zoom > map->min)
		pst("/ partially free");
#undef pst

	(void) wnoutrefresh(w_status);

	pagesz = (size_t)1 << zoom;
	if (msz <= maplen) {
		(void) wprintw(w_bar, "    %zu - %zu     - 1 char = %s",
		    (size_t)0, msz * pagesz - 1, pow2_units[zoom]);
	} else {
		(void) wprintw(w_bar, "%3s %zu - %zu %3s - 1 char = %s",
		    (offset > 0) ? "^^^" : "",
		    pagesz * offset, pagesz * (offset + maplen) - 1,
		    (offset + maplen) < msz ? "vvv" : "",
		    pow2_units[zoom]);
	}
	(void) mvwprintw(w_bar, 0, (COLS - (int)strlen(ident)) - 1,
	    "%s", ident);
	(void) wnoutrefresh(w_bar);

	/*
	 * to mark free pages at the offset, we look at all page sizes > zoom
	 * immediately left of offset
	 *
	 * there can only be one free page reaching into the offset
	 *
	 * pagesz and offset are in zoom pages
	 */


	idx = 0;
	for (l = map->max, fp = &map->freemap[l - map->min];
	     l > zoom;
	     l--, fp--) {
		page = offset >> (l - zoom);

		if (! bitf_get(*fp, page))
			continue;

		idx = page << (l - zoom);
		assert(idx <= offset);
		pagesz = (size_t)1 << (l - zoom);
		pagesz -= (offset - idx);
		idx = 0;

		if (pagesz >= maplen)
			pagesz = maplen;

		if (block_sizes)
			c = shrt[l - map->min];
		else
			c = '_';

		memset(m, c, pagesz);
		idx += pagesz;
		break;
	}

	while (idx < maplen) {
		mp.level = (int)zoom;
		mp.page = offset + idx;
		mp = next_freepage(map, mp, cache);
		if (mp.level == -1)
			break;

		if (mp.level < (int)zoom) {
			page = mp.page >> ((int)zoom - mp.level);
			pagesz = 1;
			c = '/';
		} else {
			page = mp.page << (mp.level - (int)zoom);
			pagesz = (size_t)1 << (mp.level - (int)zoom);
			b = mp.level - (int)map->min;
			if (block_sizes)
				c = shrt[b];
			else
				c = '_';
		}

		// XXX can fail, likely inconsistent read from next_freepage()
		//assert(page >= offset + idx);
		if (page < offset + idx)
			return;
		idx = page - offset;

		if (idx >= maplen)
			break;
		if (idx + pagesz > maplen)
			pagesz = maplen - idx;
		memset(&m[idx], c, pagesz);
		idx += pagesz;
	}

	(void) waddstr(w_map, m);
	(void) wnoutrefresh(w_map);
}

static const struct slashmap *
remap(const struct slashmap *map)
{
	struct slashmap *map_fantom;
	unsigned u, l;
	size_t off;

	AN(map);
	if (map->magic != BUDDY_MAP_MAGIC)
		return (NULL);

	// XXX how do we properly wait for a consistent state?
	(void) usleep(1000);

	l = map->max - map->min;
	l++;
	map_fantom = malloc(sizeof *map_fantom + sizeof(struct bitf *) * l);
	AN(map_fantom);
	INIT_OBJ(map_fantom, BUDDY_MAP_MAGIC);
	map_fantom->self = map_fantom;
	map_fantom->size = map->size;
	map_fantom->min = map->min;
	map_fantom->max = map->max;

	for (u = 0; u < l; u++) {
		off = pdiff(map->self, map->freemap[u]);
		map_fantom->freemap[u] = (void *)((char *)map + off);
	}

	return (map_fantom);
}

//from varnishstat
static void
destroy_window(WINDOW **w)
{

	AN(w);
	if (*w == NULL)
		return;
	assert(delwin(*w) != ERR);
	*w = NULL;
}

static void
make_windows(const struct slashmap *map)
{
	int sz_entries;
	int X, y;
	int x_help;
	const int overview_width = 42;
	const int status_min_y = 9;

	assert(map->max > map->min);
	sz_entries = (int)(map->max - map->min) + 1;

	X = COLS;
	X -= overview_width;

	sz_cols = (X + sz_width - 1) / sz_width;
	if (sz_cols < 1)
		sz_cols = 1;
	y = sz_entries / sz_cols;
	if (y < status_min_y)
		y = status_min_y;
	sz_cols = (sz_entries + y - 1) / y;
	assert(sz_cols >= 1);
	sz_percol = (sz_entries + sz_cols) / sz_cols;

	x_help = (sz_cols * sz_width) + overview_width;

	destroy_window(&w_help);
	destroy_window(&w_map);
	destroy_window(&w_bar);
	destroy_window(&w_status);

	w_status = newwin(y, x_help, 0, 0);
	AN(w_status);
	(void) nodelay(w_status, (bool)1);
	(void) keypad(w_status, (bool)1);
	(void) wnoutrefresh(w_status);

	w_bar = newwin(1, 0, y, 0);
	AN(w_bar);
	(void) wbkgd(w_bar, A_REVERSE);
	(void) wnoutrefresh(w_bar);

	y++;
	w_map = newwin(0, 0, y, 0);
	AN(w_map);
	(void) wnoutrefresh(w_map);

	w_help = newwin(y, 0, 0, x_help);
	if (w_help == NULL)
		return;
	AN(w_help);
	(void) waddstr(w_help, "keys:\n"
	    "q : (q)uit\n"
	    "f : show (f)ree sizes\n"
	    "+ : zoom in\n"
	    "- : zoom out\n\n"
	    "[pg](up/down): scroll map\n"
	    "left/right   : select storage");
	(void) wnoutrefresh(w_help);
}

/* find our vsm_off-th fantom
 * basically VSM_Get()
 */
static unsigned
vf_find(struct vsm *vsm, struct vsm_fantom *vf, unsigned *vsm_off)
{
	struct vsm_fantom lvf;
	unsigned u;

	AN(vsm_off);

	if (*vsm_off == 0)
		*vsm_off = 1;

	u = 0;
	memset(&lvf, 0, sizeof lvf);
	VSM_FOREACH(vf, vsm) {
		if (strcmp(vf->category, VSM_CLASS_SLASH))
			continue;
		u++;
		if (u <= *vsm_off)
			lvf = *vf;
	}
	/* too high vsm_off is ok, return last or zero */
	*vf = lvf;
	return (u);
}

#define CTRL_MOD(c) ((c) & 0x1f)

static void
mymain(struct vsm *vsm)
{
	struct vsm_fantom vf;
	const struct slashmap *map, *map_fantom = NULL;
	unsigned vsm_off, vsm_max;
	char name[80];

	vsm_off = 0;

  again:
	if (VSIG_int || VSIG_term || VSIG_hup)
		return;

	if (keep_running) {
		(void) VSM_Status(vsm);
		fprintf(stderr, ".");
		(void) sleep(1);
	}

  remap:
	if (map_fantom != NULL) {
		AZ(VSM_Unmap(vsm, &vf));
		free(TRUST_ME(map_fantom));
		map_fantom = NULL;
	}

	vsm_max = vf_find(vsm, &vf, &vsm_off);
	if (vsm_max == 0) {
		if (keep_running)
			goto again;

		(void)endwin();
		fprintf(stderr, "No slash (buddy, fellow) storage found\n");
		return;
	}

	if (VSM_Map(vsm, &vf)) {
		(void)endwin();
		fprintf(stderr, "%s\n", VSM_Error(vsm));
		goto again;
	}

	keep_running = 1;

	map = vf.b;
	if (map == NULL || (map_fantom = remap(map)) == NULL) {
		(void)endwin();
		fprintf(stderr, "Map not (yet?) initialized\n");
		goto again;
	}

	if (vsm_off > vsm_max)
		bprintf(name, "!! %u/%u %s", vsm_off, vsm_max, vf.ident);
	else
		bprintf(name, "%u/%u %s", vsm_off, vsm_max, vf.ident);

	zoom = 0;

	(void) initscr();
	(void) raw();
	(void) noecho();
	(void) nonl();
	(void) curs_set(0);

	make_windows(map);

	while (keep_running) {
		if (VSIG_int || VSIG_term || VSIG_hup) {
			keep_running = 0;
			break;
		}

		if (VSM_StillValid(vsm, &vf) != VSM_valid ||
		    VSM_Status(vsm) & (VSM_MGT_RESTARTED|VSM_WRK_RESTARTED)) {
			(void)endwin();
			fprintf(stderr, "varnishd has gone away\n");
			goto again;
		}

		printmap(map_fantom, name);
		(void) doupdate();
		wtimeout(w_status, 1000);
		int key = wgetch(w_status);
		switch (key) {
		case 'q':
		case CTRL_MOD('c'):
			keep_running = 0;
			break;
		case 'f':
			block_sizes = !block_sizes;
			break;
		case '+':
			if (zoom <= map->min) {
				zoom = map->min;
			} else {
				zoom--;
				offset <<= 1;
			}
			break;
		case '-':
			if (zoom >= map->max) {
				zoom = map->max;
			} else {
				zoom++;
				offset >>= 1;
			}
			break;
		case KEY_PPAGE:
			if (offset > maplen)
				offset -= maplen;
			else
				offset = 0;
			break;
		case KEY_NPAGE:
			offset += maplen;
			break;
#ifdef KEY_RESIZE
		case KEY_RESIZE:
			make_windows(map);
			zoom = 0;
			break;
#endif
		case KEY_LEFT:
			if (vsm_off == 0)
				break;
			vsm_off--;
			goto remap;
		case KEY_RIGHT:
			vsm_off++;
			goto remap;
		case KEY_UP:
			if (offset <= (unsigned)COLS)
				offset = 0;
			else
				offset -= (unsigned)COLS;
			break;
		case KEY_DOWN:
			offset += (unsigned)COLS;
			break;
		default:
			break;
		}
		/* we want a different storage which we do not (yet?) see */
		if (vsm_off > vsm_max)
			goto remap;
	}
	(void)endwin();
	free(TRUST_ME(map_fantom));
	map_fantom = NULL;
}

int
main(int argc, char * const *argv)
{
	int opt;

	vut = VUT_InitProg(argc, argv, &vopt_spec);
	AN(vut);

	/* XXX: parse command line */

	while ((opt = getopt(argc, argv, vopt_spec.vopt_optstring)) != -1) {
		switch (opt) {
		case 'h':
			usage(EXIT_SUCCESS);
			/* no return */
			break;
		case 'w':
			/* Write to file */
			INCOMPL();
			break;
		default:
			if (!VUT_Arg(vut, opt, optarg))
				usage(EXIT_FAILURE);
		}
	}

	if (optind != argc)
		usage(EXIT_FAILURE);

	VUT_Setup(vut);

	// XXX replace VUT because VSL not needed?
	//(void)VUT_Main(vut);
	mymain(vut->vsm);
	VUT_Fini(&vut);

	return (EXIT_SUCCESS);
}
