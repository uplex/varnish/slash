/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdint.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>	// SIGSETOPS(3C) solaris

// varnish
#include <vdef.h>
#include <vas.h>
#include <miniobj.h>
#include <vtree.h>

#ifndef TEST_DRIVER
#error TEST_DRIVER needs to be defined
#endif

#include "debug.h"
#include "buddy.h"
#include "fellow_io.h"	// XXX needed for buddy_off_extent etc
#include "fellow_log_storage.h"
#include "fellow_log.h"
#include "fellow_sha256.h"
#include "fellow_tune.h"
#include "fellow_hash.h"
#include "fellow_testenv.h"

typedef void child_f(void);
typedef void t_f(struct fellow_fd *ffd, void *t_priv);

typedef void t_resur_init_f(void **priv);
typedef void t_resur_fini_f(void **priv);

struct tc {
	const char * const	name;
	child_f		*child_f;
	t_f			*t_f;
	fellow_resurrect_f	*resur_f;
	t_resur_init_f		*resur_init_f;
	t_resur_fini_f		*resur_fini_f;
};

static struct buddy_off_extent
randalloc(buddy_t *buddy, double max)
{
	struct buddy_off_extent e;
	size_t sz;

	do {
		do {
			sz = (drand48() * max);
		} while (sz == 0);
		e = buddy_alloc1_off_extent(buddy, sz, 0);
	} while (e.off < 0);
	return (e);
}

/* ----------------------------------------------------------------------
 * simply exit from child
 */
static void v_matchproto_(child_f)
t_exit(void)
{
	exit(0);
}

const struct tc tc_SIGCHLD = {
	.name		 = "tc_SIGCHLD - just exit, test for forkrun()",
	.child_f	 = t_exit
};

/* ----------------------------------------------------------------------
 * test log append
 */

// fwd decl
static void v_matchproto_(t_f)
t_log_adddel_netzero(struct fellow_fd *ffd, void *priv);

#define BANCOUNT (100 * FELLOW_DISK_LOG_BLOCK_ENTRIES)

static void v_matchproto_(t_f)
t_log_append(struct fellow_fd *ffd, void *priv)
{
	unsigned u, n;
	int r;

	(void) ffd;
	(void) priv;

	for (u = 0; u < 5; u++) {
		// produce log traffic to force rewrite
		t_log_adddel_netzero(ffd, priv);
		n = 0;
		r = fellow_log_ban(ffd, DLE_OP_CHG,
		    (unsigned char *)&n, sizeof(n),
		    42.42, NULL);

		for (n = 1; n < BANCOUNT; n++) {
			r = fellow_log_ban(ffd, DLE_OP_ADD,
			    (unsigned char *)&n, sizeof(n),
			    42.42, NULL);
			AN(r);
		}
	}
}
/*
  exp add add | add ... exp add add add

  rewrite     ^ pending

  ->

  exp | add ... exp add add add

*/

struct resurrect_bancount_s {
	unsigned	magic;
#define RBS_MAGIC	0xebdb77b5
	unsigned	m;
	unsigned	exp_seen;
};

static int v_matchproto_(fellow_resurrect_f)
resurrect_bancount(void *priv, const struct fellow_dle *e)
{
	struct resurrect_bancount_s *rbs;
	unsigned *n;

	CAST_OBJ_NOTNULL(rbs, priv, RBS_MAGIC);
	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);

	assert(e->version == 1);

	AZ(rbs->exp_seen);
	if (e->type == DLE_BAN_EXP_IMM) {
		rbs->exp_seen = 1;
		return (0);
	}

	assert(e->type == DLE_BAN_ADD_IMM);
	assert(e->u.ban_im0.cont == 0);
	assert(e->u.ban_im0.len == sizeof(unsigned));
	assert(e->u.ban_im0.ban_time == 42.42);

	n = (typeof(n))e->u.ban_im0.ban;
	DBG("n=%u rbs->m=%u", *n, rbs->m);
	assert(*n == rbs->m);
	rbs->m--;

	return (0);
}

static void v_matchproto_(t_resur_init_f)
resurrect_bancount_init(void **priv)
{
	struct resurrect_bancount_s *rbs;

	ALLOC_OBJ(rbs, RBS_MAGIC);
	AN(rbs);

	rbs->m = BANCOUNT - 1;
	AZ(rbs->exp_seen);

	*priv = rbs;
}

static void v_matchproto_(t_resur_fini_f)
resurrect_bancount_fini(void **priv)
{
	struct resurrect_bancount_s *rbs;

	AN(priv);
	AN(*priv);
	TAKE_OBJ_NOTNULL(rbs, priv, RBS_MAGIC);
	free(rbs);
}


const struct tc tc_log_append = {
	.name	 = "t_log_append test append",
	.t_f	 = t_log_append,
	.resur_init_f = resurrect_bancount_init,
	.resur_f = resurrect_bancount,
	.resur_fini_f = resurrect_bancount_fini
};

/* ----------------------------------------------------------------------
 * basic resurrection test
 */

static int v_matchproto_(fellow_resurrect_f)
resurrect_none(void *priv, const struct fellow_dle *e)
{

	(void) priv;

	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);

	if (e->type == DLE_BAN_EXP_IMM || e->type == DLE_BAN_ADD_IMM)
		return (0);

	WRONG("nothing must be resurrected");
}

static void v_matchproto_(t_f)
t_log_adddel_netzero(struct fellow_fd *ffd, void *priv)
{
	const unsigned max_reg_per_logblk =
	    (FELLOW_DISK_LOG_BLOCK_ENTRIES - 1) * DLE_REG_NREGION;
	struct buddy_off_extent alloc, region[max_reg_per_logblk];
	struct buddy_returns *rets;
	const unsigned nentries =
	    3 * FELLOW_DISK_LOG_BLOCK_ENTRIES;
	struct fellow_dle *e, entry[nentries];
	int i, ops;
	unsigned nreg, n, remain;
	uint8_t hash[32];
	double r;

	(void) priv;

	rets = BUDDY_RETURNS_STK(fellow_dskbuddy(ffd), BUDDY_RETURNS_MAX);

	// XXX ENOMEM in malloc for 100000
	ops = 111;

	while (ops--) {
		r = drand48();
		sha256(hash, &r, sizeof r);

		nreg = r * max_reg_per_logblk;
		for (i = 0; i < nreg; i++)
			region[i] = randalloc(fellow_dskbuddy(ffd), 1 << 16);

		e = entry;
		remain = nentries;
		fellow_dle_init(e, remain);

		// DLE_REG_ADD
		if (nreg == 0)
			n = 0;
		else
			n = fellow_dle_reg_fill(e, remain,
			    region, nreg, DLE_REG_ADD, hash);

		e += n;
		AN(remain > n);
		remain -= n;

		// DLE_OBJ_ADD
		alloc = randalloc(fellow_dskbuddy(ffd), 1 << 16);
		e->type = DLE_OBJ_ADD;
		memcpy(e->u.obj.hash, hash, 32);
		e->u.obj.start = fdb(alloc.off, alloc.size);

		e++;
		AN(remain);
		remain--;

		// DLE_REG_DEL_*
		if (nreg == 0)
			n = 0;
		else
			n = fellow_dle_reg_fill(e, remain,
			    region, nreg, DLE_REG_DEL_FREE, hash);

		e += n;
		AN(remain > n);
		remain -= n;

		// DLE_OBJ_DEL
		e->type = DLE_OBJ_DEL_FREE;
		memcpy(e->u.obj.hash, hash, 32);
		e->u.obj.start = fdb(alloc.off, alloc.size);

		e++;
		AN(remain);
		remain--;

		// submit
		fellow_log_dle_submit(ffd, entry, e - entry);

		/* to test DEL_FREE (not DEL_ALLOCED), we need to ensure
		 * that objects went into the log before returning the regions
		 */
		fellow_log_flush(ffd);

		// free
		AN(buddy_return_off_extent(rets, &alloc));
		for (i = 0; i < nreg; i++)
			AN(buddy_return_off_extent(rets, &region[i]));
	}

	buddy_return(rets);

	// DEL_ALLOCED

	r = drand48();
	sha256(hash, &r, sizeof r);

	fellow_dle_init(entry, 4);

	entry[0].type = DLE_REG_ADD;
	memcpy(entry[0].u.reg.hashpfx, hash, DLE_REG_HASHPFXSZ);
	entry[0].u.reg.region[0] = randalloc(fellow_dskbuddy(ffd), 1 << 15);

	alloc = randalloc(fellow_dskbuddy(ffd), FDB_SIZE_MAX);
	entry[1].type = DLE_OBJ_ADD;
	memcpy(entry[1].u.obj.hash, hash, 32);
	entry[1].u.obj.start = fdb(alloc.off, alloc.size);

	memcpy(&entry[2], &entry[0], sizeof entry[2]);
	entry[2].type = DLE_REG_DEL_ALLOCED;

	memcpy(&entry[3], &entry[1], sizeof entry[3]);
	entry[3].type = DLE_OBJ_DEL_ALLOCED;

	fellow_log_dle_submit(ffd, entry, 4);
}

const struct tc tc_adddel_netzero = {
	.name	 = "tc_adddel_netzero: add followed by del, no net effect",
	.t_f	 = t_log_adddel_netzero,
	.resur_f = resurrect_none
};

static void *
thr_log_adddel_netzero(void *priv)
{
	t_log_adddel_netzero(priv, NULL);
	return (NULL);
}

static void v_matchproto_(t_f)
t_log_adddel_conc(struct fellow_fd *ffd, void *priv)
{
	const unsigned n = 16;
	pthread_t thr[n];
	unsigned u;

	for (u = 0; u < n; u++)
		AZ(pthread_create(&thr[u], NULL, thr_log_adddel_netzero, ffd));
	for (u = 0; u < n; u++)
		AZ(pthread_join(thr[u], NULL));
}

const struct tc tc_adddel_conc = {
	.name	 = "tc_adddel_netzero with several threads",
	.t_f	 = t_log_adddel_conc,
	.resur_f = resurrect_none
};


/* ----------------------------------------------------------------------
 * Just keep all objects
 */

static int v_matchproto_(fellow_resurrect_f)
resurrect_keep(void *priv, const struct fellow_dle *e)
{

	(void) priv;
	(void) e;

	return (1);
}

const struct tc tc_adddel_keep = {
	.name	 = "tc_adddel_keep: tolerate objects in cache, "
	    "add followed by del, no net effect",
	.t_f	 = t_log_adddel_netzero,
	.resur_f = resurrect_keep
};


/* ----------------------------------------------------------------------
 * Add Reload Del (ARD) test
 */


static const int t_ard_ids[] = {
	1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 0
};

static void v_matchproto_(t_resur_init_f)
resurrect_ard_init(void **priv)
{
	int *ids = malloc(sizeof t_ard_ids);

	DBG("init sz %zu", sizeof t_ard_ids);

	AN(ids);
	memcpy(ids, t_ard_ids, sizeof t_ard_ids);
	*priv = ids;
}

static void v_matchproto_(t_resur_fini_f)
resurrect_ard_fini(void **priv)
{
	int *oids, *ids;

	AN(priv);
	AN(*priv);
	oids = ids = *priv;

	while (*ids != 0) {
		assert(*ids == -1);
		ids++;
	}

	(void) oids;
	DBG("all %zu ids seen", ids - oids);

	AN(*priv);
	free(*priv);
	*priv = NULL;
}

static int v_matchproto_(fellow_resurrect_f)
resurrect_ard(void *priv, const struct fellow_dle *e)
{
	int *ids = priv;
	int id;

	CHECK_OBJ_NOTNULL(e, FELLOW_DLE_MAGIC);
	assert(e->version == 1);

	if (e->type == DLE_BAN_EXP_IMM || e->type == DLE_BAN_ADD_IMM)
		return (0);

	assert(e->type == DLE_OBJ_ADD || e->type == DLE_OBJ_CHG);
	assert(e->u.obj.ttl == 301274);

	id = (e->u.obj.hash[0] << 8) | e->u.obj.hash[1];

	DBG("resurrect %d", id);
	while (*ids != 0) {
		if (*ids == id) {
			*ids = -1;
			break;
		}
		ids++;
	}

	// if this hits: id not found
	assert(*ids != 0);

	return (1);
}

static void
t_log_ard_impl(struct fellow_fd *ffd, void *priv, uint8_t type)
{
	// -1 : no trailing zero;
	const unsigned n_ids = sizeof t_ard_ids / sizeof *t_ard_ids - 1;
	struct fellow_dle *e, entry[n_ids];
	struct buddy_off_extent alloc, **allocsp, *allocs;
	unsigned u;
	int id;

	AN(priv);
	allocsp = priv;

	switch (type) {
	case DLE_OBJ_ADD:
		AZ(*allocsp);
		allocs = calloc(n_ids, sizeof *allocs);
		AN(allocs);
		*allocsp = allocs;
		break;
	case DLE_OBJ_DEL_ALLOCED:
		AN(*allocsp);
		allocs = *allocsp;
		break;
	default:
		WRONG("t_log_ard_* type");
	}

	fellow_dle_init(entry, n_ids);
	for (u = 0; u < n_ids; u++) {
		e = &entry[u];
		e->type = type;

		switch (type) {
		case DLE_OBJ_ADD:
			alloc = buddy_alloc1_off_extent(
			    fellow_dskbuddy(ffd), MIN_FELLOW_BLOCK, 0);
			allocs[u] = alloc;
			break;
		case DLE_OBJ_DEL_ALLOCED:
			alloc = allocs[u];
			assert(alloc.off >= 0);
			allocs[u] = buddy_off_extent_nil;
			break;
		default:
			WRONG("t_log_ard_* type (loop)");
		}

		e->u.obj.start = fdb(alloc.off, alloc.size);

		id = t_ard_ids[u];
		e->u.obj.hash[1] = id & 0xff;
		id >>= 8;
		e->u.obj.hash[0] = id & 0xff;

		e->u.obj.ttl = 301274;
	}

	fellow_log_dle_submit(ffd, entry, n_ids);

	switch (type) {
	case DLE_OBJ_ADD:
		break;
	case DLE_OBJ_DEL_ALLOCED:
		free(allocs);
		*allocsp = NULL;
		break;
	default:
		WRONG("t_log_ard_* type (end)");
	}
}

static void
t_log_ard_add(struct fellow_fd *ffd, void *priv)
{

	t_log_ard_impl(ffd, priv, DLE_OBJ_ADD);
	t_log_adddel_netzero(ffd, NULL);
}

static void
t_log_ard_del(struct fellow_fd *ffd, void *priv)
{

	t_log_ard_impl(ffd, priv, DLE_OBJ_DEL_ALLOCED);
	t_log_adddel_netzero(ffd, NULL);
}

static const struct tc tc_resur_add = {
	.name   = "tc_resur_add: add objects to resurrect after reload",
	.t_f	= t_log_ard_add,
	.resur_f = resurrect_none
};
static const struct tc tc_resur_del = {
	.name   = "tc_resur_del: del resurrected objects",
	.t_f	= t_log_ard_del,
	.resur_init_f = resurrect_ard_init,
	.resur_f = resurrect_ard,
	.resur_fini_f = resurrect_ard_fini
};

/* ----------------------------------------------------------------------
 *
 */

/*
 * test permutations on a nibble
 *
 * 1: rewrite
 * 2: flush
 * 4: exit after
 * 8: run next step
 */

static void
regime(struct fellow_fd *ffd, unsigned regime_spec)
{

	if (regime_spec & 1)
		fellow_log_rewrite(ffd);
	if (regime_spec & 2)
		fellow_log_flush_racy(ffd, UINT_MAX);
	if (regime_spec & 4)
		exit(0);
}

static void
dbg_regime(unsigned regime_spec)
{
	DBG("rewrite %u", regime_spec & 1);
	DBG("flush   %u", regime_spec & 2);
	DBG("exit    %u", regime_spec & 4);
	DBG("action  %u", regime_spec & 8);
}

static void
t_log_regime_netzero(struct fellow_fd *ffd, void *priv)
{
	unsigned regime_spec = *(unsigned *)priv;

	while (regime_spec) {
		dbg_regime(regime_spec);
		regime(ffd, regime_spec);
		if (regime_spec & 8)
			t_log_adddel_netzero(ffd, 0);
		regime_spec >>= 4;
	}
}

const struct tc tc_regime_netzero = {
	.name	 = "tc_regime_netzero: "
	    "netzero test with flush/rewrite at different places",
	.t_f	 = t_log_regime_netzero,
	.resur_f = resurrect_none
};

#ifdef DEBUG
const char * filename = "/tmp/fellowfile";
#else
const char * filename = "/tmp/fellowfile.ndebug";
#endif

int
runh(const struct tc *tc, void *t_priv, unsigned hash)
{
//	void *ioctx;
	struct fellow_fd *ffd;
	buddy_t mb, *membuddy = &mb;
	void *resur_priv = NULL;
	struct stvfe_tune tune[1];
	const size_t memsz = 10 * 1024 * 1024;
	const size_t dsksz = 100 * 1024 * 1024;
	const size_t objsz_hint = 1 * 1024 * 1024;


	AN(tc->name);
	DBG("==== %s hash %d", tc->name, hash);

	AZ(stvfe_tune_init(tune, memsz, dsksz, objsz_hint));
	tune->hash_log = hash;
	AZ(stvfe_tune_check(tune));

	buddy_init(membuddy, MIN_BUDDY_BITS, memsz,
	    BUDDYF(mmap), NULL, NULL, NULL);
	if (tc->resur_init_f)
		tc->resur_init_f(&resur_priv);
	ffd = fellow_log_init(filename, dsksz, objsz_hint,
	    NULL, NULL, membuddy, tune, fellow_simple_task_run, NULL);
	AN(ffd);
	fellow_log_open(ffd, tc->resur_f, resur_priv);
	if (tc->resur_fini_f)
		tc->resur_fini_f(&resur_priv);
	if (ffd == NULL)
		return (errno);

	tc->t_f(ffd, t_priv);

	fellow_log_close(&ffd);
	BWIT_ISEMPTY(membuddy->witness);
	buddy_fini(&membuddy, BUDDYF(unmap), NULL, NULL, NULL);
	AZ(ffd);
	return (0);
}

int
run(const struct tc *tc, void *t_priv)
{
	unsigned l, h;
	int r = 0;

	if (tc->child_f)
		tc->child_f();

#ifdef HAVE_XXHASH_H
	l = FH_LIM;
#else
	l = FH_SHA256 + 1;
#endif

	for (h = 1; h < l; h++) {
		if (fh_name[h] == NULL)
			continue;
		r = runh(tc, t_priv, h);
		if (r != 0)
			break;
	}
	return (r);
}

int
forkrun(const struct tc *tc, double killafter, void *t_priv)
{
	struct timespec timeo;
	sigset_t mask;
	pid_t pid;
	int i;

	sigemptyset(&mask);
	sigaddset(&mask, SIGCHLD);
	sigprocmask(SIG_BLOCK, &mask, NULL);

	pid = fork();
	assert(pid != -1);
	if (pid == 0)
		exit (run(tc, t_priv));

	if (killafter == 0) {
		pid = waitpid(pid, &i, 0);
		assert(pid != -1);
		if (! WIFEXITED(i) || WEXITSTATUS(i) != 0)
			exit(i);
		sigprocmask(SIG_UNBLOCK, &mask, NULL);
		return (WEXITSTATUS(i));
	}

	// from vtim.c
	timeo.tv_sec = (time_t)trunc(killafter);
	timeo.tv_nsec = (int)(1e9 * (killafter - timeo.tv_sec));

	DBG("killtimer %f", killafter);
	i = sigtimedwait(&mask, NULL, &timeo);

	if (tc == &tc_SIGCHLD)
		assert(i == SIGCHLD);	// self test
	else if (i < 0) {
		// XXX WHY EINTR? #31
		assert(errno == EAGAIN || errno == EINTR);
		i = kill(pid, SIGABRT);
		assert(i == 0 || i == ESRCH);
	} else
		assert(i == SIGCHLD);

	(void) waitpid(pid, &i, 0);
	sigprocmask(SIG_UNBLOCK, &mask, NULL);
	return (0);
}

#include <dirent.h>

static
int
rmcore(const struct dirent *d)
{
	const char * const pfx = "core.";
	const char *n;

	n = d->d_name;
	if (strncmp(n, pfx, strlen(pfx)) == 0)
		AZ(unlink(n));
	return (0);
}
static void rmcores(void)
{
	struct dirent **proforma;

	AZ(scandir(".", &proforma, rmcore, alphasort));
}

int
main(int argc, char **argv)
{
	int i;
	unsigned regime_spec;
	double t;
	const double tmax = 1;
	void *priv = NULL;
#ifdef HAVE_XXHASH_H
	uint8_t fht[2];

	fht[0] = fh_name[FH_XXH3_128] ? FH_XXH3_128 : FH_XXH32;
	fht[1] = fh_name[FH_XXH3_64]  ? FH_XXH3_64  : FH_XXH32;
#endif

	if (argc == 2 && *argv[1] != '-')
		filename = argv[1];

	// test forkrun SIGCHLD
	AZ(forkrun(&tc_SIGCHLD, 1.0, 0));

	AZ(run(&tc_adddel_netzero, 0));

	AZ(run(&tc_log_append, 0));

	AZ(run(&tc_adddel_conc, 0));

	// Test if we find back our objects.
	// if test is interrupted between _add and _del, it will
	// panic on "nothing must be resurrected"
	AZ(runh(&tc_resur_add, &priv, FH_SHA256));
#ifdef HAVE_XXHASH_H
	AZ(runh(&tc_resur_del, &priv, FH_XXH32));
#else
	AZ(runh(&tc_resur_del, &priv, FH_SHA256));
#endif
	AZ(run(&tc_adddel_netzero, NULL));	// check

	// same but keep across a close/open
#ifdef HAVE_XXHASH_H
	AZ(runh(&tc_resur_add, &priv, fht[0]));
	AZ(runh(&tc_adddel_keep, NULL, FH_SHA256));
	AZ(runh(&tc_resur_del, &priv, fht[1]));
#else
	AZ(runh(&tc_resur_add, &priv, FH_SHA256));
	AZ(runh(&tc_adddel_keep, NULL, FH_SHA256));
	AZ(runh(&tc_resur_del, &priv, FH_SHA256));
#endif
	AZ(run(&tc_adddel_netzero, NULL));	// check

	if (getenv("TEST_LOGS") ||
	    (argc == 2 && strcmp(argv[1], "--quicker") == 0))
		goto out;

	for (i = 0; i < 20; i++) {
		t = drand48() * tmax;
		/* terminate one run, then test if reading
		 * the fellowfile works. If it fails, we will
		 * have two core dumps (if enabled)
		 */
		AZ(forkrun(&tc_adddel_netzero, t, 0));
		AZ(run(&tc_adddel_netzero, 0));
		rmcores();
	}

	if (argc == 2 && strcmp(argv[1], "--quick") == 0)
		goto out;

	for (regime_spec = 0; regime_spec < 0x100; regime_spec++) {
		DBG("regime 0x%x", regime_spec);
		AZ(forkrun(&tc_regime_netzero, 0, &regime_spec));
	}
  out:
	printf("OK\n");
	return (0);
}
