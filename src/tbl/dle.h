/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 */

/* DLE_N(macro_name, short, value) */
DLE_N(DLE_OBJ_ADD,	   obj_add,	    (DLE_T_OBJ | DLE_OP_ADD))
DLE_N(DLE_OBJ_CHG,	   obj_chg,	    (DLE_T_OBJ | DLE_OP_CHG))
DLE_N(DLE_OBJ_DEL_ALLOCED, obj_del_alloced, (DLE_T_OBJ | DLE_OP_DEL_ALLOCED))
DLE_N(DLE_OBJ_DEL_FREE,    obj_del_free,    (DLE_T_OBJ | DLE_OP_DEL_FREE))
DLE_N(DLE_OBJ_DEL_THIN,    obj_del_thin,    (DLE_T_OBJ | DLE_OP_DEL_THIN))

DLE_N(DLE_REG_ADD,	   reg_add,	    (DLE_T_REG | DLE_OP_ADD))
DLE_N(DLE_REG_DEL_ALLOCED, reg_del_alloced, (DLE_T_REG | DLE_OP_DEL_ALLOCED))
DLE_N(DLE_REG_DEL_FREE,    reg_del_free,    (DLE_T_REG | DLE_OP_DEL_FREE))

DLE_N(DLE_BAN_ADD_IMM,	   ban_add_imm,     (DLE_T_BAN_IMM | DLE_OP_ADD))
DLE_N(DLE_BAN_ADD_REG,	   ban_add_reg,     (DLE_T_BAN_REG | DLE_OP_ADD))

DLE_N(DLE_BAN_EXP_IMM,	   ban_exp_imm,     (DLE_T_BAN_IMM | DLE_OP_CHG))
DLE_N(DLE_BAN_EXP_REG,	   ban_exp_reg,     (DLE_T_BAN_REG | DLE_OP_CHG))

#undef DLE_N
