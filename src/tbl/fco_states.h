/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * Format:
 *
 * type, state, comment,
 * lru, fdb, ptrsz, diskseg, refparent
 *
 * FCOSA_NOLRU
 * FCOSA_MAYLRU
 *
 * FCOSA_NOFDB
 * FCOSA_MAYFDB
 *
 * FCOSA_NOMEM
 * FCOA_MEM
 *
 * diskset: FDSA_NULL	no disk_seg pointer
 *	    FDSA_MAY	NULL or valid object for READFAIL
 *	    FDSA_UN	disk seg off/sz = 0
 *	    FDSA_ALLOC	disk seg off/sz > 0
 */

FCOSD(FCO, INIT,     "unused",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOSA_NOMEM, FDSA_NULL, FCOSA_NOPAREF)
FCOSD(FCO, BUSY,     "in memory, incomplete",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOA_MEM, FDSA_ALLOC, FCOSA_NOPAREF)
FCOSD(FCO, WRITING,  "transitioning BUSY->INCORE, being written",
      FCOSA_NOLRU,  FCOSA_MAYFDB,   FCOA_MEM, FDSA_ALLOC, FCOSA_NOPAREF)
FCOSD(FCO, READING,  "being read, signal via cond",
      FCOSA_NOLRU,  FCOSA_MAYFDB,   FCOA_MEM, FDSA_NULL, FCOSA_NOPAREF)
FCOSD(FCO, REDUNDANT,"aborted read",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOA_MEM, FDSA_NULL, FCOSA_NOPAREF)
FCOSD(FCO, MEM,      "only in mem, on lru unless a private object",
      FCOSA_MAYLRU, FCOSA_NOFDB, FCOA_MEM, FDSA_UN, FCOSA_NOPAREF)
FCOSD(FCO, INCORE,   "usable in mem, on lru unless a private object",
      FCOSA_MAYLRU, FCOSA_MAYFDB,   FCOA_MEM, FDSA_ALLOC, FCOSA_NOPAREF)
FCOSD(FCO, READFAIL,   "read/write error",
      FCOSA_NOLRU,  FCOSA_MAYFDB,   FCOA_MEM, FDSA_MAY, FCOSA_NOPAREF)
FCOSD(FCO, EVICT,    "only for obj, not on LRU",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOA_MEM, FDSA_MAY, FCOSA_NOPAREF)
