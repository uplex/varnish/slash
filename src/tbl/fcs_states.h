/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * Format:
 *
 * type, state, comment,
 * lru, fdb, ptrsz, diskseg, refparent
 *
 * FCOSA_NOLRU
 * FCOSA_MAYLRU
 *
 * FCOSA_NOFDB
 * FCOSA_FDB
 *
 * FCOSA_NOMEM
 * FCSA_MEM
 *
 * diskset: FDSA_NULL	no disk_seg pointer
 *	    FDSA_UN	disk seg off/sz = 0
 *	    FDSA_ALLOC	disk seg off/sz > 0
 */

FCOSD(FCS, INIT,     "unused",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOSA_NOMEM, FDSA_NULL, FCOSA_NOPAREF)
FCOSD(FCS, USABLE,   "has an fds associated",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOSA_NOMEM, FDSA_UN, FCOSA_NOPAREF)
FCOSD(FCS, BUSY,     "in memory, incomplete",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_PAREF)
FCOSD(FCS, WRITING,  "transitioning BUSY->INCORE, being written",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_PAREF)
FCOSD(FCS, DISK,     "can be fetched, off/sz filled, mem not alloced",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCOSA_NOMEM, FDSA_ALLOC, FCOSA_NOPAREF)
FCOSD(FCS, READING,  "being read, signal via cond",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_PAREF)
FCOSD(FCS, CHECK,    "in mem, needs to be checked",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_PAREF)
FCOSD(FCS, INCORE,   "usable in mem, on lru if refcnt == 0",
      FCOSA_MAYLRU, FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_PAREF)
FCOSD(FCS, MEM,      "only in mem, not on lru (only the fco is on lru)",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_UN, FCOSA_PAREF)
FCOSD(FCS, READFAIL,   "only for obj, not on LRU",
      FCOSA_NOLRU,  FCOSA_NOFDB, FCSA_MEM,   FDSA_ALLOC, FCOSA_NOPAREF)
