/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * TUNE(type, name, default, min, max)
 */

#ifndef TUNEH
#define TUNEH(t, n, d, min, max) TUNE(t, n, d, min, max)
#endif
#ifndef TUNEFAIL
#define TUNEFAIL(t, n, d, min, max) TUNE(t, n, d, min, max)
#endif

TUNE(unsigned, chunk_exponent, 20 /* 1MB*/, 6 /* 64b */, 28 /* 256MB */);
TUNE(unsigned, reserve_chunks, 1, 0, UINT_MAX);
TUNE(int8_t, cram, 1, -64, 64);
TUNE(unsigned, debug_flags, 0, 0, UINT_MAX);
#undef TUNE
#undef TUNEH
#undef TUNEFAIL
