// UPPER, lower, length
FH(SHA256,   sha256,   FSHA256_LEN)
#ifdef HAVE_XXHASH_H
FH(XXH32,    xxh32,    sizeof(XXH32_hash_t))
#if XXH_VERSION_NUMBER >= 800
FH(XXH3_64,  xxh3_64,  sizeof(XXH64_hash_t))
FH(XXH3_128, xxh3_128, sizeof(XXH128_hash_t))
#endif
#endif
#undef FH
