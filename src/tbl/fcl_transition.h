/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2024 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

FCLT(INIT, DISK, "existing segment added for load")
FCLT(INIT, EMBED, "embedded in fco/fbo")
FCLT(INIT, EMBED_BUSY, "writing embedded")
FCLT(INIT, BUSY, "filling segment");
FCLT(EMBED_BUSY, EMBED, "embedded in fco/fbo")
FCLT(BUSY, WRITING, "writing to disk")
FCLT(BUSY, REDUNDANT, "space requested but not committed")
FCLT(WRITING, INCORE, "done writing")
FCLT(WRITING, MEM, "writing failed")
FCLT(INCORE, DISK, "")
FCLT(DISK, READING, "")
FCLT(READING, CHECK, "")
FCLT(READING, READFAIL, "")
FCLT(CHECK, INCORE, "")
FCLT(CHECK, READFAIL, "")
#undef FCLT
