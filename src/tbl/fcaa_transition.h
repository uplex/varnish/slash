/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

FCAAT(INIT, USABLE, "segment has disk space")
FCAAT(INIT, DISK, "existing segment added for load")
FCAAT(USABLE, BUSY, "filling segment");
FCAAT(BUSY, WRITING, "writing to disk")
FCAAT(BUSY, USABLE, "space requested but not committed")
FCAAT(WRITING, INCORE, "done writing")
FCAAT(WRITING, MEM, "writing failed")
FCAAT(INCORE, DISK, "")
FCAAT(DISK, READING, "")
FCAAT(READING, CHECK, "")
FCAAT(READING, READFAIL, "")
FCAAT(CHECK, INCORE, "")
FCAAT(CHECK, READFAIL, "")
#undef FCAAT
