/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * TUNE(type, name, default, min, max)
 */

#include "fellow_hashes.h"

#ifndef TUNEH
#define TUNEH(t, n, d, min, max) TUNE(t, n, d, min, max)
#endif
#ifndef TUNEFAIL
#define TUNEFAIL(t, n, d, min, max) TUNE(t, n, d, min, max)
#endif

/* FELLOW_DISK_LOG_BLOCK_ENTRIES = 56, we assume two DLEs per object */
TUNE(unsigned, logbuffer_size, 14336, 28, UINT_MAX)
TUNE(float, logbuffer_flush_interval, 2.0, 0.0, FLT_MAX)

// log_rewrite_ratio > n_del / n_add triggers rewrite
// XXX sensible max value?
TUNE(float, log_rewrite_ratio, 0.5, 0.001, FLT_MAX)

// reserve chunk is the larger of chunk_exponent and result from logbuffer size
TUNE(unsigned, chunk_exponent, 20 /* 1MB*/, 12 /* 4KB */, 30 /* 1GB */)
TUNE(uint8_t, wait_table_exponent, 10, 6, 32)
TUNE(uint8_t, lru_exponent, 0, 0, 6)
TUNE(unsigned, dsk_reserve_chunks, 4, 2, UINT_MAX)
TUNE(unsigned, mem_reserve_chunks, 5, 0, UINT_MAX)
TUNE(size_t, objsize_max, 0, 0, SIZE_MAX)
TUNE(unsigned, objsize_update_min_log2_ratio, 1, 1, 64)
TUNE(unsigned, objsize_update_max_log2_ratio, 3, 1, 64)
TUNE(unsigned, objsize_update_min_occupancy, 25, 0, 100)
TUNE(unsigned, objsize_update_max_occupancy, 75, 0, 100)
TUNE(size_t, discard_immediate, 256 * 1024, 4096, SIZE_MAX)
// 31 is safe max for stack usage, further limited by memsz
TUNE(unsigned, readahead, 5, 0, 31)
TUNE(unsigned, io_batch_min, 8, 1, UINT_MAX)
// right now, the io ring size is hardcoded to 1024, so 512 is half that
TUNE(unsigned, io_batch_max, 512, 1, UINT_MAX)
TUNE(int8_t, cram, 1, -64, 64)
#ifdef HAVE_XXHASH_H
 #if XXH_VERSION_NUMBER >= 800
TUNEH(uint8_t, hash_obj, FH_XXH3_64, FH_NONE + 1, FH_LIM - 1)
TUNEH(uint8_t, hash_log, FH_XXH3_64, FH_NONE + 1, FH_LIM - 1)
 #else
TUNEH(uint8_t, hash_obj, FH_XXH32, FH_NONE + 1, FH_XXH32)
TUNEH(uint8_t, hash_log, FH_XXH32, FH_NONE + 1, FH_XXH32)
 #endif
#else
TUNEH(uint8_t, hash_obj, FH_SHA256, FH_NONE + 1, FH_LIM - 1)
TUNEH(uint8_t, hash_log, FH_SHA256, FH_NONE + 1, FH_LIM - 1)
#endif
// io error: 0=panic, 1=purge object
TUNEFAIL(unsigned, ioerr_obj, 0, 0, 1)
// io error: 0=panic, 1=fail storage
TUNEFAIL(unsigned, ioerr_log, 0, 0, 1)
// mem error: 0=panic, 1=purge object
TUNEFAIL(unsigned, allocerr_obj, 0, 0, 1)
// mem error: 0=panic, 1=fail storage
TUNEFAIL(unsigned, allocerr_log, 0, 0, 1)
TUNE(unsigned, panic_flags, 0, 0, UINT_MAX)
#undef TUNE
#undef TUNEH
#undef TUNEFAIL
