/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

FCOT(INIT, BUSY, "new object created")
FCOT(INIT, READING, "loading from disk")
FCOT(BUSY, WRITING, "writing to disk")
FCOT(WRITING, INCORE, "done writing")
FCOT(WRITING, MEM, "writing failed")
FCOT(REDUNDANT, EVICT,  "delete redundant object")
FCOT(INCORE, EVICT,  "evict when refcnt == 0")
FCOT(MEM, EVICT,  "evict when refcnt == 0")
FCOT(READING, INCORE, "")
FCOT(READING, REDUNDANT, "")
FCOT(READING, READFAIL, "")
FCOT(READFAIL, EVICT,  "evict when refcnt == 0")
#undef FCOT
