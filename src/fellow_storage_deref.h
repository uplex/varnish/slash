/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

// shared with deref worker thread
struct sfe_deref_mbx {
	unsigned			magic;
#define SFE_DEREF_MBX_MAGIC		0x2a849ae8
	unsigned			run:1;
	unsigned			working:1;
	// to signal that this struct can be torn down
	unsigned			detached:1;

	VTAILQ_HEAD(,objcore)		head;
	pthread_mutex_t			mtx;
	pthread_cond_t			cond;

	fellow_task_privstate		taskstate;
};

#define SFE_DEREF_BATCH (1<<19)

// owned by resurrect thread
struct sfe_deref {
	unsigned			n;
	VTAILQ_HEAD(,objcore)		head;
	struct sfe_deref_mbx		mbx;
};

static void
sfe_deref_send(struct sfe_deref *sfed)
{
	struct sfe_deref_mbx *mbx = &sfed->mbx;

	assert(! VTAILQ_EMPTY(&sfed->head));
	AZ(pthread_mutex_lock(&mbx->mtx));
	AN(mbx->run);
	while (! VTAILQ_EMPTY(&mbx->head))
		AZ(pthread_cond_wait(&mbx->cond, &mbx->mtx));
	VTAILQ_SWAP(&sfed->head, &mbx->head, objcore, lru_list);
	AZ(pthread_cond_signal(&mbx->cond));
	AZ(pthread_mutex_unlock(&mbx->mtx));
	assert(VTAILQ_EMPTY(&sfed->head));
	sfed->n = 0;
}

static inline void
sfe_deref_post(struct sfe_deref *sfed, struct objcore *oc)
{

	VTAILQ_INSERT_TAIL(&sfed->head, oc, lru_list);
	if (++sfed->n >= SFE_DEREF_BATCH &&
	    sfed->mbx.working == 0 &&
	    VTAILQ_EMPTY(&sfed->mbx.head))
		sfe_deref_send(sfed);
}

static void
sfe_deref_thread(struct worker *wrk, void *priv)
{
	struct objcore *oc, *next;
	struct sfe_deref_mbx *mbx;
	VTAILQ_HEAD(,objcore) head;
	vtim_real t_now;

	CAST_OBJ_NOTNULL(mbx, priv, SFE_DEREF_MBX_MAGIC);
	AZ(pthread_mutex_lock(&mbx->mtx));
	while (mbx->run) {
		VTAILQ_INIT(&head);
		while (mbx->run && VTAILQ_EMPTY(&mbx->head))
			AZ(pthread_cond_wait(&mbx->cond, &mbx->mtx));
		VTAILQ_SWAP(&head, &mbx->head, objcore, lru_list);
		assert(VTAILQ_EMPTY(&mbx->head));
		mbx->working = 1;
		AZ(pthread_cond_signal(&mbx->cond));
		AZ(pthread_mutex_unlock(&mbx->mtx));

		t_now = VTIM_real();
		//lint -e{850}
		VTAILQ_FOREACH_SAFE(oc, &head, lru_list, next) {
			VTAILQ_REMOVE(&head, oc, lru_list);
			HSH_DerefBoc(wrk, oc);
			LRU_Add(oc, t_now);
			(void)HSH_DerefObjCore(wrk, &oc, HSH_RUSH_POLICY);
		}

		AZ(pthread_mutex_lock(&mbx->mtx));
		mbx->working = 0;
	}
	VTAILQ_INIT(&head);
	VTAILQ_SWAP(&head, &mbx->head, objcore, lru_list);
	assert(VTAILQ_EMPTY(&mbx->head));
	AZ(mbx->detached);
	mbx->detached = 1;
	AZ(pthread_cond_signal(&mbx->cond));
	AZ(pthread_mutex_unlock(&mbx->mtx));

	t_now = VTIM_real();
	//lint -e{850}
	VTAILQ_FOREACH_SAFE(oc, &head, lru_list, next) {
		VTAILQ_REMOVE(&head, oc, lru_list);
		HSH_DerefBoc(wrk, oc);
		LRU_Add(oc, t_now);
		(void)HSH_DerefObjCore(wrk, &oc, HSH_RUSH_POLICY);
	}
}

static void
sfe_deref_mbx_init(struct sfe_deref_mbx *mbx)
{

	INIT_OBJ(mbx, SFE_DEREF_MBX_MAGIC);
	mbx->run = 1;

	VTAILQ_INIT(&mbx->head);
	AZ(pthread_mutex_init(&mbx->mtx, NULL));
	AZ(pthread_cond_init(&mbx->cond, NULL));
	AZ(sfe_taskrun(sfe_deref_thread, mbx, &mbx->taskstate));
}

static void
sfe_deref_mbx_fini(struct sfe_deref_mbx *mbx)
{

	AZ(pthread_mutex_lock(&mbx->mtx));
	AN(mbx->run);
	mbx->run = 0;
	AZ(pthread_cond_signal(&mbx->cond));
	while (mbx->detached == 0)
		AZ(pthread_cond_wait(&mbx->cond, &mbx->mtx));
	AZ(pthread_mutex_unlock(&mbx->mtx));

	assert(VTAILQ_EMPTY(&mbx->head));
	AZ(pthread_mutex_destroy(&mbx->mtx));
	AZ(pthread_cond_destroy(&mbx->cond));
}

static void
sfe_deref_init(struct sfe_deref *sfed)
{
	sfed->n = 0;
	VTAILQ_INIT(&sfed->head);
	sfe_deref_mbx_init(&sfed->mbx);
}

static void
sfe_deref_fini(struct sfe_deref *sfed)
{
	if (! VTAILQ_EMPTY(&sfed->head))
		sfe_deref_send(sfed);
	assert(VTAILQ_EMPTY(&sfed->head));
	sfed->n = 0;
	sfe_deref_mbx_fini(&sfed->mbx);
}
