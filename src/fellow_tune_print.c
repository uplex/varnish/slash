/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2024 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <unistd.h>
#include <stdint.h>

#include "vdef.h"
#include "vsb.h"

#include "fellow_tune.h"
#include "fellow_tune_print.h"

#define FMT_unsigned "%u"
#define FMT_uint8_t "%u"
#define FMT_int8_t "%d"
#define FMT_size_t "%zu"
#define FMT_float "%.4f"

void
fellow_tune_json(struct vsb *vsb, const struct stvfe_tune *tune)
{
//lint -save -e773 -e835
#define TUNE(t, n, d, min, max) \
	VSB_printf(vsb, "\"" #n "\": " FMT_ ## t ",\n", tune->n);
#include "tbl/fellow_tunables.h"
#define TUNE(t, n) \
	VSB_printf(vsb, "\"" #n "\": " FMT_ ## t ",\n", tune->n);
	TUNE(size_t, dsksz)
	TUNE(size_t, memsz)
#undef TUNE
#define TUNE(t, n) \
	VSB_printf(vsb, "\"" #n "\": " FMT_ ## t "\n", tune->n);
	TUNE(size_t, objsize_hint)
#undef TUNE
//lint -restore
}
