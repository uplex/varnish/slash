/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifndef BITSOF_H
#define BITSOF_H
// XXX autoconf to check for builtin, else use ffsl ffsll
#if SIZE_MAX == 0xffffffffffffffff
#define clzszt(v) __builtin_clzll(v)
#define ffsszt(v) __builtin_ffsll(v)
typedef unsigned long long bitf_word_t;
#define popcount(v) (unsigned)__builtin_popcountll(v)
#elif SIZE_MAX == 0xffffffff
#define clzszt(v) __builtin_clzl(v)
#define ffsszt(v) __builtin_ffsl(v)
typedef unsigned long bitf_word_t;
#define popcount(v) (unsigned)__builtin_popcount(v)
#else
#error unsupported size_t
#endif

#define maxbits(v) (sizeof(v) * 8)
#define bitsof(v) (maxbits(v) - (unsigned)clzszt(v))
#endif
