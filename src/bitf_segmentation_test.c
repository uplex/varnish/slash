/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// varnish-cache
#include <vdef.h>
#include <miniobj.h>
#include <vas.h>

#include "bitf_segmentation.h"

#define NBITF 32

static void
t_bitfs(const size_t idx) {
	struct bitfs *bitfs;
	struct bitf *bitf[NBITF];
	size_t o, off[NBITF + 1];
	char stk[bitfs_size(NBITF)];

	unsigned i, u;
	size_t sz, n;
	char *p;

	bitfs = bitfs_init(stk, sizeof stk);

	for (u = 0, o = 0; u < NBITF; u++) {
		sz = (size_t)(drand48() * 8192);
		if (sz < bitf_sz(1, idx))
			sz = bitf_sz(1, idx);
		p = malloc(sz);
		AN(p);
		n = sz_nbits(sz, idx);
		bitf[u] = bitf_init(p, n, sz, idx);
		AN(bitf[u]);
		off[u] = o;
		o += n;

		bitfs_add(bitfs, bitf[u]);
		AZ(bitfs_nset(bitfs));
		assert(bitfs_nbits(bitfs) == o);
	}
	off[u] = o;

	// limits
	for (u = 0; u < NBITF; u++) {
		assert(off[u + 1] > off[u]);
		assert(bitfs_lookup(bitfs, off[u])->bitf ==
		    bitf[u]);
		assert(bitfs_lookup(bitfs, off[u + 1] - 1)->bitf ==
		    bitf[u]);
	}

	// some random bits
	for (i = 0; i < 10240; i++) {
		for (u = 0; u < NBITF; u++) {
			n = off[u] + (size_t)
			    (drand48() * (off[u + 1] - off[u]));
			assert(bitfs_lookup(bitfs, n)->bitf ==
			    bitf[u]);
		}
	}


	for (u = 0; u < NBITF; u++)
		free(bitf[u]);
}

int
main(void) {
	t_bitfs(BITF_INDEX);
	t_bitfs(BITF_NOIDX);
	printf("OK\n");
	return (0);
}
