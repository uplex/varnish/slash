..
	SPDX-License-Identifier: LGPL-2.1-only
	Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
	Author: Nils Goroll <nils.goroll@uplex.de>
 *
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License
	as published by the Free Software Foundation; either version 2.1 of
	the License, or (at your option) any later version.
 *
	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.
 *
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
	02110-1301 USA Also add information on how to contact you by
	electronic and paper mail.

.. varnish_vsc_begin::	fellow
	:oneliner:	fellow Stevedore Counters (SLASH/ stevedores)
	:order:		70

.. varnish_vsc:: c_allocobj
	:type:	counter
	:level:	info
	:oneliner:	Requests to allocate an object

	Number of times the storage has been asked to create an object

.. varnish_vsc:: c_allocobj_fail
	:type:	counter
	:level:	info
	:oneliner:	Object creation failures

	Number of times the storage failed to create an object

.. varnish_vsc:: c_dsk_lru_wakeups
	:type:	counter
	:level:	info
	:oneliner:	Disk LRU thread wakeup events

	Number of times the disk LRU thread got woken up because of a waiting
	allocation request.

.. varnish_vsc:: c_dsk_lru_reserve_used_bytes
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes used from the disk reserve at LRU thread wakeups

	Number of bytes returned from the reserve.

	This number divided by ``c_dsk_lru_wakeups`` gives the average bytes used from
	the reserve per wakeup, which might help with sizing
	``dsk_reserve_chunks``.

	Note that this value is in bytes not in chunks to not be impacted by
	changes of ``chunk_exponent`` / ``chunk_bytes``.

.. varnish_vsc:: c_dsk_lru_nuke_fill_reserve
	:type:	counter
	:level:	info
	:oneliner:	Disk LRU thread nukes to fill the reserve

	Number of nukes from the disk LRU thread to fill the reserve

.. varnish_vsc:: c_dsk_lru_nuke_reserve_drained
	:type:	counter
	:level:	info
	:oneliner:	Disk LRU thread nukes with the reserve drained

	Number of nukes from the disk LRU thread while the reserve is empty.

.. varnish_vsc:: c_dsk_obj_get_present
	:type:	counter
	:level:	info
	:oneliner:	Access to a disk object which was already present

	Number of times a memory pointer was already present for a disk object

.. varnish_vsc:: c_dsk_obj_get_fail
	:type:	counter
	:level:	info
	:oneliner:	Access to a disk object failed

	Number of times access to a disk object failed

.. varnish_vsc:: c_dsk_obj_get
	:type:	counter
	:level:	info
	:oneliner:	Access to a disk object

	Number of times a disk object (vampireobject) was read and turned
	into a memory object (object)

.. varnish_vsc:: c_dsk_obj_get_coalesce
	:type:	counter
	:level:	info
	:oneliner:	Access to a disk object when already in progress

	Number of times a disk object (vampireobject) was accessed and
	a get operation was already in progress

.. varnish_vsc:: c_mem_lru_wakeups
	:type:	counter
	:level:	info
	:oneliner:	Memory LRU thread wakeup events

	Number of times the memory LRU thread got woken up because of a waiting
	allocation request.

.. varnish_vsc:: c_mem_lru_reserve_used_bytes
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes used from the memory reserve at LRU thread wakeups

	Number of bytes returned from the reserve.

	This number divided by ``c_mem_lru_wakeups`` gives the average bytes
	used from the reserve per wakeup, which might help with sizing
	``mem_reserve_chunks``.

	Note that this value is in bytes not in chunks to not be impacted by
	changes of ``chunk_exponent`` / ``chunk_bytes``.

.. varnish_vsc:: c_mem_lru_evict_fill_reserve
	:type:	counter
	:level:	info
	:oneliner:	Memory LRU thread evictions to fill the reserve

	Number of evictions from the memory LRU thread to fill the reserve.
	Evictions concern individual body segments, segment lists and object
	data with headers.

.. varnish_vsc:: c_mem_lru_evict_reserve_drained
	:type:	counter
	:level:	info
	:oneliner:	Memory LRU thread evictions with the reserve drained

	Number of evictions from the memory LRU thread while the reserve is
	empty. Evictions concern individual body segments, segment lists and
	object data with headers.

.. varnish_vsc:: c_mem_obj_fail
	:type:	counter
	:level:	info
	:oneliner:	A memory object failed

	Number of times a failed memory object is encountered. This is
	most likely because some IO failed after the intial creation
	of the in memory object. This could be a disk write error or a
	read error of segments or segment lists.

	Details are logged as ``SLT_Error`` to *vsl(7)*.

.. varnish_vsc:: c_mem_obj_free
	:type:	counter
	:level:	info
	:oneliner:	Memory object free operations

	Number of times a memory object was freed

.. varnish_vsc:: c_dsk_obj_free_thin
	:type:	counter
	:level:	info
	:oneliner:	Disk object free operations with thin deletion

	Number of times a disk object was freed by logging a thin deletion
	request to be executed with the next log rewrite.

.. varnish_vsc:: c_dsk_obj_free_get
	:type:	counter
	:level:	info
	:oneliner:	Disk object free operations with disk read

	Number of times a disk object was freed by reading the object.
	Each operation also adds one to one of the c_dsk_obj_get_*
	counters.

.. varnish_vsc:: c_mem_obj_mutate
	:type:	counter
	:level:	info
	:oneliner:	Memory object evictions

	Number of times an object was evicted from memory and turned
	into a disk object (vampireobject). The term mutate was chosen
	in favor of "vampirization" or similar.

.. varnish_vsc:: g_mem_bytes
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Memory Bytes outstanding

	Number of bytes allocated from memory

	Note: This number is only approximate

.. varnish_vsc:: g_mem_space
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Memory Bytes available

	Number of bytes left in memory

	Note: This number is only approximate

.. varnish_vsc:: g_mem_obj
	:type:	gauge
	:level:	info
	:oneliner:	Objects in memory

	Exact number of objects currently in memory (fco_infdb).

.. varnish_vsc:: g_dsk_bytes
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Disk Bytes outstanding

	Number of bytes allocated from disk,
	updated at logbuffer_flush_interval.

	Note: This number is only approximate

.. varnish_vsc:: g_dsk_space
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Disk Bytes available

	Number of bytes left in disk,
	updated at logbuffer_flush_interval.

	Note: This number is only approximate

.. varnish_vsc:: b_happy
	:type:	bitmap
	:level:	info
	:format: bitmap
	:oneliner:	Storage is online

	Whether or not the storage is online,
	updated at logbuffer_flush_interval.

	The happy value is 0 while the storage is loading or shutting
	down and 1 while it is open. This will probably change when we
	introduce error thresholds.

	The bitmap has the most recent value in the least significant bit.

.. varnish_vsc_end::	fellow
