/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include <stdint.h>
#include <stddef.h>

#include "fellow_hashes.h"

extern const char * const fh_name[FH_LIM];

/*
 * NOTE on memory requirements:
 *
 * the union always needs the maximum amount of all members (FSHA256_LEN == 32
 * Bytes), which is wasteful for smaller hashes like XXH64.
 *
 * We _could_ have distinct types for other stucts embedding hashes (mainly
 * fellow_disk_seg and fellow_disk_seglist), but that would complicate the code
 * considerably, mainly because fellow_disk_seglist has an array of
 * fellow_disk_seg.
 *
 * So at least for now, we do not use the optimization potential to reduce the
 * memory footprint with smaller hashes and only take their computational
 * advantage.
 */

union fh {
	uint8_t		sha256[32];
	uint32_t	xxh32;
	uint64_t	xxh3_64;
	uint8_t		xxh3_128[16];
};

void fh(uint8_t fht, union fh *fh, const void *p, size_t l);
int fhcmp(uint8_t fht, const union fh *fh, const void *p, size_t l);
