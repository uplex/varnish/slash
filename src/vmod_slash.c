/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <string.h>
#include <stdlib.h>
#include <limits.h>

#include <cache/cache.h>
#include <storage/storage.h>
#include <vcl.h>
#include <vsb.h>

//lint -e652 use a common structure
#define vmod_slash_buddy vmod_slash_stv
#define vmod_slash_fellow vmod_slash_stv
#include "vcc_slash_if.h" //lint -e763
//lint +e652

#include "buddy_storage.h"
#include "buddy_util.h"
#include "buddy_tune.h"
#include "buddy_tune_print.h"
#include "fellow_storage.h"
#include "fellow_tune.h"
#include "fellow_tune_print.h"
#include "fellow_hash.h"
#include "fellow_errhandling.h"

/* ------------------------------------------------------------
 * GENERIC
 */

struct VPFX(slash_stv) {
	unsigned				magic;
#define SLASH_BUDDY_MAGIC			0x2e620ee8
#define SLASH_FELLOW_MAGIC			0x2e620ee9
	unsigned				refcnt;

	VSLIST_ENTRY(VPFX(slash_stv))		list;
	struct stevedore *			storage;
	char					*name;
	size_t					size;
};

// no locking required, vmod init/fini happens in the cli thread
VSLIST_HEAD(slash_buddy_head, VPFX(slash_stv));
static struct slash_buddy_head slash_stevedores =
    VSLIST_HEAD_INITIALIZER(slash_stevedores);

// XXX also look up by path
static struct stevedore *
stv_find(const char *ident)
{
	struct stevedore *stv;

	STV_Foreach(stv)
	    if (! strcmp(stv->ident, ident))
		    return (stv);

	return (NULL);
}

// XXX also look up by path
static struct VPFX(slash_stv) *
sb_find(const char *name)
{
	struct VPFX(slash_stv)	*sb;

	VSLIST_FOREACH(sb, &slash_stevedores, list)
		if (! strcmp(sb->name, name))
			return (sb);

	return (NULL);
}

#define ifpositive(ctx, x, a)						\
	if (a < 0) {							\
		VRT_fail(ctx, #x " argument must be positive");	\
		return;							\
	}								\
	x = (size_t)a

/* ------------------------------------------------------------
 * BUDDY
 */

VCL_VOID
vmod_buddy__init(VRT_CTX, struct VPFX(slash_stv) **sbp,
const char *vcl_name, VCL_BYTES bytesa, VCL_BYTES mina)
{
	size_t bytes, min;
	struct VPFX(slash_stv)	*sb;

	AN(sbp);
	AZ(*sbp);

	ifpositive(ctx, bytes, bytesa);
	ifpositive(ctx, min, mina);

	sb = sb_find(vcl_name);
	if (sb != NULL) {
		CHECK_OBJ(sb, SLASH_BUDDY_MAGIC);
		sb->refcnt++;
		AN(sb->storage);
		*sbp = sb;
		return;
	}

	// stv_find

	ALLOC_OBJ(sb, SLASH_BUDDY_MAGIC);	//lint !e747
	AN(sb);
	sb->refcnt = 1;
	sb->storage = sbu_new(vcl_name, &bytes, &min);
	AN(sb->storage);
	REPLACE(sb->name, vcl_name);
	sb->size = bytes;

	VSLIST_INSERT_HEAD(&slash_stevedores, sb, list);
	*sbp = sb;
	return;
}

VCL_VOID
vmod_buddy__fini(struct VPFX(slash_stv) **sbp)
{
	struct VPFX(slash_stv)	*sb;

	TAKE_OBJ_NOTNULL(sb, sbp, SLASH_BUDDY_MAGIC);

	if (--sb->refcnt > 0)
		return;

	VSLIST_REMOVE(&slash_stevedores, sb, VPFX(slash_stv), list);
	REPLACE(sb->name, NULL);
	// if in use, the stevedore will silently remain intact
	sbu_fini(&sb->storage);
	FREE_OBJ(sb);
}

static VCL_STRING
buddy_tune(VCL_STEVEDORE stv, struct VARGS(buddy_tune) *args)
{
	static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
	struct stvbu_tune tune;
	size_t sz;
	const char *r;

	if (args->valid_chunk_bytes && args->valid_chunk_exponent)
		return("chunk_bytes and chunk_exponent are alternatives");

	if (args->valid_chunk_bytes) {
		assert(args->chunk_bytes > 0);
		sz = (size_t)args->chunk_bytes;
		args->chunk_exponent = log2up(sz);
		args->valid_chunk_exponent = 1;
		args->valid_chunk_bytes = 0;
	}

	AZ(pthread_mutex_lock(&mtx));
	sbu_tune_get(stv, &tune);
//lint -save -e750 -e734 loss of precision
#define CHK_float 0
#define CHK_size_t 0
#define CHK_int8_t 0
#define CHK_unsigned 1
#define TUNE(type, name, default, min, max)				\
	if (args->valid_ ## name) {					\
		if (/*lint --e(506,774,685,568)*/			\
		    CHK_ ## type && args->name < 0) {			\
			r = "Value of " #name " must not be negative";	\
			goto out;					\
		}							\
		tune.name = (type)args->name;				\
	}
#define TUNEH(type, name, default, min, max)				\
	if (args->valid_ ## name)					\
		tune.name = hash_e2u(args->name);
#define TUNEFAIL(type, name, default, min, max)				\
	if (args->valid_ ## name)					\
		tune.name = err_e2u(args->name);
#include "tbl/buddy_tunables.h"
#undef CHK_float
#undef CHK_size_t
#undef CHK_int8_t
#undef CHK_unsigned
//lint -restore

	r = sbu_tune_apply(stv, &tune);
  out:
	AZ(pthread_mutex_unlock(&mtx));
	return (r);
}

//lint -e{818}
VCL_STRING
vmod_buddy_tune(VRT_CTX, struct VPFX(slash_stv) *sb,
    struct VARGS(buddy_tune) *args)
{
	CHECK_OBJ_NOTNULL(sb, SLASH_BUDDY_MAGIC);

	(void) ctx;
	return (buddy_tune(sb->storage, args));
}

VCL_STEVEDORE
vmod_buddy_storage(VRT_CTX, struct VPFX(slash_stv) *sb)	//lint -e818
{
	CHECK_OBJ_NOTNULL(sb, SLASH_BUDDY_MAGIC);

	(void) ctx;
	return (sb->storage);
}

VCL_VOID vmod_buddy_as_transient(VRT_CTX, struct VPFX(slash_stv) *sb)
{
	CHECK_OBJ_NOTNULL(sb, SLASH_BUDDY_MAGIC);

	assert(ctx->method == VCL_MET_INIT);
	sbu_as_transient(sb->storage);
}

VCL_STRING
vmod_tune_buddy(VRT_CTX, struct VARGS(tune_buddy)*args)
{
	const char * const msg = "slash.tune_buddy() can only be used "
	    "on a buddy storage";
	struct VARGS(buddy_tune) fargs;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (! sbu_is(args->storage)) {
		VRT_fail(ctx, "%s", msg);
		return (msg);
	}

#define TUNE(type, name, default, min, max)		\
	fargs.valid_ ## name = args->valid_ ## name;	\
	fargs.name = args->name;
#define TUNEH(type, name, default, min, max)	\
	TUNE(type, name, default, min, max)
#define TUNEFAIL(type, name, default, min, max)	\
	TUNE(type, name, default, min, max)

	// chunk_bytes is only a vcl parameter
	TUNE(foo, chunk_bytes, foo, foo, foo)
#include "tbl/buddy_tunables.h"

	return (buddy_tune(args->storage, &fargs));
}

/* ------------------------------------------------------------
 * FELLOW
 */

VCL_VOID
vmod_fellow__init(VRT_CTX, struct VPFX(slash_stv) **sbp,
    const char *vcl_name, VCL_STRING path,
    VCL_BYTES dsksizea, VCL_BYTES memsizea, VCL_BYTES objsize_hinta,
    VCL_BOOL delete)
{
	struct VPFX(slash_stv)	*sb;
	size_t memsize, dsksize, objsize_hint;
	struct stevedore *stv;

	AN(sbp);
	AZ(*sbp);

	ifpositive(ctx, memsize, memsizea);
	ifpositive(ctx, dsksize, dsksizea);
	ifpositive(ctx, objsize_hint, objsize_hinta);

	sb = sb_find(vcl_name);
	if (sb != NULL && sb->magic == SLASH_FELLOW_MAGIC) {
		sb->refcnt++;
		AN(sb->storage);
		*sbp = sb;
		return;
	}

	stv = stv_find(vcl_name);
	if (stv == NULL)
		stv = sfe_new(vcl_name, path,
		    dsksize, memsize, objsize_hint, delete);
	if (stv == NULL) {
		VRT_fail(ctx, "fellow %s configuration failed: %s",
		    vcl_name, sfe_error());
		return;
	}

	ALLOC_OBJ(sb, SLASH_FELLOW_MAGIC);	//lint !e747
	AN(sb);
	sb->refcnt = 1;
	sb->storage = stv;
	REPLACE(sb->name, vcl_name);
	sb->size = dsksize;

	VSLIST_INSERT_HEAD(&slash_stevedores, sb, list);
	*sbp = sb;
	return;
}

VCL_VOID
vmod_fellow__fini(struct VPFX(slash_stv) **sbp)
{
	struct VPFX(slash_stv)	*sb;

	TAKE_OBJ_NOTNULL(sb, sbp, SLASH_FELLOW_MAGIC);

	if (--sb->refcnt > 0)
		return;

	VSLIST_REMOVE(&slash_stevedores, sb, VPFX(slash_stv), list);
	REPLACE(sb->name, NULL);

	sfe_fini(&sb->storage);
}

static unsigned
hash_e2u(VCL_ENUM e)
{
	if (e == VENUM(sha256))
		return (FH_SHA256);
	if (e == VENUM(xxh32))
		return (FH_XXH32);
	if (e == VENUM(xxh3_64))
		return (FH_XXH3_64);
	if (e == VENUM(xxh3_128))
		return (FH_XXH3_128);
	WRONG("hash venum");
}

static unsigned
err_e2u(VCL_ENUM e)
{
	if (e == VENUM(panic))
		return (FEH_PANIC);
	if (e == VENUM(purge))
		return (FEH_PURGE);
	if (e == VENUM(fail))
		return (FEH_FAIL);
	WRONG("err handling venum");
}

static VCL_STRING
fellow_tune(VCL_STEVEDORE stv, struct VARGS(fellow_tune) *args)
{
	static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
	struct stvfe_tune tune;
	size_t sz;
	const char *r;

	if (args->valid_chunk_bytes && args->valid_chunk_exponent)
		return("chunk_bytes and chunk_exponent are alternatives");

	if (args->valid_chunk_bytes) {
		assert(args->chunk_bytes > 0);
		sz = (size_t)args->chunk_bytes;
		args->chunk_exponent = log2up(sz);
		args->valid_chunk_exponent = 1;
		args->valid_chunk_bytes = 0;
	}

	AZ(pthread_mutex_lock(&mtx));
	sfe_tune_get(stv, &tune);
//lint -e734 loss of precision
#define CHK_float 0
#define CHK_size_t 0
#define CHK_int8_t 0
#define CHK_unsigned 1
#define CHK_uint8_t 1
#define TUNE(type, name, default, min, max)				\
	if (args->valid_ ## name) {					\
		if (/*lint --e(506,774,685,568)*/			\
		    CHK_ ## type && args->name < 0) {			\
			r = "Value of " #name " must not be negative";	\
			goto out;					\
		}							\
		tune.name = (type)args->name;				\
	}
#define TUNEH(type, name, default, min, max)				\
	if (args->valid_ ## name)					\
		tune.name = hash_e2u(args->name);
#define TUNEFAIL(type, name, default, min, max)				\
	if (args->valid_ ## name)					\
		tune.name = err_e2u(args->name);
#include "tbl/fellow_tunables.h"
#undef CHK_float
#undef CHK_size_t
#undef CHK_int8_t
#undef CHK_unsigned
#undef CHK_uint8_t
//lint +e734 loss of precision

	r = sfe_tune_apply(stv, &tune);	//lint !e539
  out:
	AZ(pthread_mutex_unlock(&mtx));
	return (r);
}

VCL_STRING
vmod_fellow_tune(VRT_CTX, struct VPFX(slash_stv) *sb,
    struct VARGS(fellow_tune) *args)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sb, SLASH_FELLOW_MAGIC);

	return (fellow_tune(sb->storage, args));
}

VCL_STEVEDORE
vmod_fellow_storage(VRT_CTX, struct VPFX(slash_stv) *sb)	//lint -e818
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sb, SLASH_FELLOW_MAGIC);

	(void) ctx;
	return (sb->storage);
}

VCL_VOID
vmod_fellow_as_transient(VRT_CTX, struct VPFX(slash_stv) *sb)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(sb, SLASH_FELLOW_MAGIC);

	assert(ctx->method == VCL_MET_INIT);
	sfe_as_transient(sb->storage);
}

VCL_VOID
vmod_as_transient(VRT_CTX, VCL_STEVEDORE stv)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	assert(ctx->method == VCL_MET_INIT);
	sfe_as_transient(TRUST_ME(stv));
}

VCL_STRING
vmod_tune_fellow(VRT_CTX, struct VARGS(tune_fellow)*args)
{
	const char * const msg = "slash.tune_fellow() can only be used "
	    "on a fellow storage";
	struct VARGS(fellow_tune) fargs;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (! sfe_is(args->storage)) {
		VRT_fail(ctx, "%s", msg);
		return (msg);
	}

#define TUNE(type, name, default, min, max)		\
	fargs.valid_ ## name = args->valid_ ## name;	\
	fargs.name = args->name;
#define TUNEH(type, name, default, min, max)	\
	TUNE(type, name, default, min, max)
#define TUNEFAIL(type, name, default, min, max)	\
	TUNE(type, name, default, min, max)

	// chunk_bytes is only a vcl parameter
	TUNE(foo, chunk_bytes, foo, foo, foo)
#include "tbl/fellow_tunables.h"

	return (fellow_tune(args->storage, &fargs));
}

VCL_STRING
vmod_get_tuning_json(VRT_CTX, VCL_STEVEDORE stv)
{
	struct vsb vsb[1];

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);

	if (! sfe_is(stv) && ! sbu_is(stv))
		return ("");


	WS_VSB_new(vsb, ctx->ws);
	VSB_cat(vsb, "{\n");
	VSB_indent(vsb, 2);
	VSB_printf(vsb, "\"name\": \"%s\",\n", stv->vclname);
	VSB_printf(vsb, "\"tuning\": {\n");
	VSB_indent(vsb, 2);
	if (sbu_is(stv)) {
		struct stvbu_tune tune;
		sbu_tune_get(stv, &tune);
		buddy_tune_json(vsb, &tune);
	}
	else {
		struct stvfe_tune tune;
		sfe_tune_get(stv, &tune);
		fellow_tune_json(vsb, &tune);
	}
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "}\n");
	VSB_indent(vsb, -2);
	VSB_cat(vsb, "}\n");
	return (WS_VSB_finish(vsb, ctx->ws, NULL));
}
