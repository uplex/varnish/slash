/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * remember change DLEs to rewrite them into ADD when the ADD
 * is seen
 */

//#include "config.h"

#include <vtree.h>

/*
 * NOTE ON MEMORY EFFICIENCY
 *
 * Because we potentially need to remember (in memory) one dle change per cache
 * object when bans change, we keep this struct as small as possible, and, being
 * lucky, we bring it down to _just_ 64 bytes...
 *
 * we do not have space for a magic value, but we check the hash at
 * least in _get()
 */

struct fellow_dlechg {
	struct fellow_dle_obj_chg		chg[1];
	union {
		VRBT_ENTRY(fellow_dlechg)	entry;
		VSLIST_ENTRY(fellow_dlechg)	freelist;
	} u;
};

static inline int
fellow_dlechg_cmp(const struct fellow_dlechg *a, const struct fellow_dlechg *b)
{

	const fellow_disk_block aa = a->chg->start;
	const fellow_disk_block bb = b->chg->start;

	if (aa.fdb < bb.fdb)
		return (-1);
	if (aa.fdb > bb.fdb)
		return (1);
	return (0);
}

#define DLECHG_COPY(to, fm) do {		\
		(to)->start = (fm)->start;	\
		(to)->hash[0] = (fm)->hash[0];	\
		(to)->hash[1] = (fm)->hash[1];	\
		(to)->hash[2] = (fm)->hash[2];	\
		(to)->hash[3] = (fm)->hash[3];	\
	} while(0)

#define DLECHG_OBJ_CHG_COPY(to, fm) do {	\
	DLECHG_COPY(to, fm);			\
	EXP_COPY(to, fm);			\
	(to)->ban = (fm)->ban;			\
	} while(0)

#define DLECHG_OK(to, fm) (				\
	    (to)->start.fdb == (fm)->start.fdb &&	\
	    (to)->hash[0] == (fm)->hash[0] &&		\
	    (to)->hash[1] == (fm)->hash[1] &&		\
	    (to)->hash[2] == (fm)->hash[2] &&		\
	    (to)->hash[3] == (fm)->hash[3])

#define DLECHG_DEL_THIN_MAGIC (-42.42)
#define DLECHG_DEL_THIN_CMP (DLECHG_DEL_THIN_MAGIC + 0.01)

#define DLECHG_OBJ_DEL_THIN_COPY(to, fm) do {				\
	DLECHG_COPY(to, fm);						\
	(to)->ttl	= (typeof((to)->ttl))DLECHG_DEL_THIN_MAGIC;	\
	(to)->grace	= (typeof((to)->grace))DLECHG_DEL_THIN_MAGIC;	\
	(to)->ban	= (typeof((to)->ban))DLECHG_DEL_THIN_MAGIC;	\
	(to)->t_origin	= (typeof((to)->t_origin))DLECHG_DEL_THIN_MAGIC;\
	(to)->keep	= (typeof((to)->keep))DLECHG_DEL_THIN_MAGIC;	\
	} while(0)

#define DLECHG_IS_OBJ_DEL_THIN(to) (			\
	(to)->ttl	< DLECHG_DEL_THIN_CMP &&	\
	(to)->grace	< DLECHG_DEL_THIN_CMP &&	\
	(to)->ban	< DLECHG_DEL_THIN_CMP &&	\
	(to)->t_origin	< DLECHG_DEL_THIN_CMP &&	\
	(to)->keep	< DLECHG_DEL_THIN_CMP)

#define DBGDLECHG(c, msg)				\
	FDBG(D_LOGS_ITER_BLOCK,			\
	    "  chg " msg " hash %02x%02x%02x%02x "	\
	    "off %zu sz %zu "				\
	    "ban %f t_ori %f ttl %f grace %f",		\
	    c->chg->hash[0], c->chg->hash[1],		\
	    c->chg->hash[2], c->chg->hash[3],		\
	    fdb_off(c->chg->start),			\
	    fdb_size(c->chg->start),			\
	    c->chg->ban, c->chg->t_origin,		\
	    c->chg->ttl, c->chg->grace)

VRBT_HEAD(fellow_dlechg_head, fellow_dlechg);

VRBT_GENERATE_INSERT_COLOR(fellow_dlechg_head, fellow_dlechg, u.entry, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(fellow_dlechg_head, fellow_dlechg, u.entry, static)
#endif
VRBT_GENERATE_INSERT(fellow_dlechg_head, fellow_dlechg, u.entry, fellow_dlechg_cmp, static)
VRBT_GENERATE_FIND(fellow_dlechg_head, fellow_dlechg, u.entry, fellow_dlechg_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(fellow_dlechg_head, fellow_dlechg, u.entry, static)
VRBT_GENERATE_REMOVE(fellow_dlechg_head, fellow_dlechg, u.entry, static)
#ifdef DEBUG
VRBT_GENERATE_NEXT(fellow_dlechg_head, fellow_dlechg, u.entry, static)
VRBT_GENERATE_MINMAX(fellow_dlechg_head, fellow_dlechg, u.entry, static)
#endif

BUDDY_POOL(dlechg_pool, 128);
BUDDY_POOL_GET_FUNC(dlechg_pool, static)

static void
dlechg_pool_fill(struct buddy_reqs *reqs, const void *priv)
{
	unsigned u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	(void) priv;

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_extent(reqs, sizeof(struct fellow_dlechg), 0));
}

struct fellow_dlechg_top {
	unsigned			magic;
#define FELLOW_DLECHG_TOP_MAGIC		0x710e71a2
	unsigned			nfree;	// mostly debug aid
	buddy_t				*buddy;
	struct fellow_dlechg_head	head;
	VSLIST_HEAD(,fellow_dlechg)	free;
	struct dlechg_pool		pool[1];
};

static struct fellow_dlechg *
fellow_dlechg_alloc(struct fellow_dlechg_top *fdct)
{
	struct fellow_dlechg *fdc;
	struct buddy_ptr_extent mem;

	fdc = VSLIST_FIRST(&fdct->free);
	if (fdc != NULL) {
		VSLIST_REMOVE_HEAD(&fdct->free, u.freelist);
		AN(fdct->nfree);
		fdct->nfree--;
	}
	else {
		mem = buddy_get_next_ptr_extent(
		    dlechg_pool_get(fdct->pool, NULL));
		AN(mem.ptr);
		assert(mem.size >= sizeof *fdc);
		fdc = mem.ptr;
	}

	memset(fdc, 0, sizeof *fdc);
	return (fdc);
}

static void
fellow_dlechg_top_init(struct fellow_dlechg_top *fdct, buddy_t *buddy)
{

	AN(fdct);
	INIT_OBJ(fdct, FELLOW_DLECHG_TOP_MAGIC);
	fdct->buddy = buddy;
	VRBT_INIT(&fdct->head);
	VSLIST_INIT(&fdct->free);
	BUDDY_POOL_INIT(fdct->pool, buddy, FEP_MEM_DLECHG,
	    dlechg_pool_fill, NULL);
}

enum fellow_dlechg_consistency {
	FDTF_INSIST = 0,
	FDTF_LEAK
};

static void
fellow_dlechg_top_fini(struct fellow_dlechg_top *fdct,
    enum fellow_dlechg_consistency how)
{
	struct fellow_dlechg *fdc, *save;
	struct buddy_returns *memret;
	struct buddy_ptr_extent mem;
	size_t sz;

	CHECK_OBJ_NOTNULL(fdct, FELLOW_DLECHG_TOP_MAGIC);
	assert(how == FDTF_INSIST || how == FDTF_LEAK);

#ifdef DEBUG
	VRBT_FOREACH(fdc, fellow_dlechg_head, &fdct->head)
	    DBGDLECHG(fdc, "LEFTOVER");
#endif

	assert(how == FDTF_LEAK || VRBT_EMPTY(&fdct->head));

	memret = BUDDY_RETURNS_STK(fdct->buddy, BUDDY_RETURNS_MAX);
	sz = buddy_rndup(fdct->buddy, sizeof *fdc);

	VSLIST_FOREACH_SAFE(fdc, &fdct->free, u.freelist, save) {
		mem = BUDDY_PTR_EXTENT(fdc, sz);
		AN(buddy_return_ptr_extent(memret, &mem));
	}
	buddy_return(memret);
	BUDDY_POOL_FINI(fdct->pool);
	FINI_OBJ(fdct);
}

static int
fellow_dlechg_add(struct fellow_dlechg_top *fdct, const struct fellow_dle *e)
{
	struct fellow_dlechg *fdc = fellow_dlechg_alloc(fdct);

	if (fdc == NULL)
		return (0);

	switch (e->type) {
	case DLE_OBJ_CHG:
		DLECHG_OBJ_CHG_COPY(fdc->chg, &e->u.obj);
		break;
	case DLE_OBJ_DEL_THIN:
		DLECHG_OBJ_DEL_THIN_COPY(fdc->chg, &e->u.obj);
		break;
	default:
		WRONG("dle type in fellow_dlechg_add");
	}

	AZ(VRBT_INSERT(fellow_dlechg_head, &fdct->head, fdc));
	return (1);
}

#ifdef DEBUG
#define DEBUGUNCONST
#else
#define DEBUGUNCONST const
#endif

static struct fellow_dlechg *
fellow_dlechg_find(DEBUGUNCONST struct fellow_dlechg_top *fdct,
    const struct fellow_dle *e)
{
	struct fellow_dlechg n, *fdc;

	assert(DLE_TYPE(e->type) == DLE_T_OBJ);
	memset(&n, 0, sizeof n);
	n.chg[0].start.fdb = e->u.obj.start.fdb;

	fdc = VRBT_FIND(fellow_dlechg_head, &fdct->head, &n);
#ifdef DEBUG
	if (fdc && ! DLECHG_OK(fdc->chg, &e->u.obj)) {
		DBGDLECHG(fdc, "FIND WRONG, removing");
		AN(VRBT_REMOVE(fellow_dlechg_head, &fdct->head, fdc));
		return (NULL);
	}
#endif
	return (fdc);
}

/* check if we have a change for obj_add and, if yes, return
 * the change rewritten as an add in ret
 */
static struct fellow_dle *
fellow_dlechg_get(struct fellow_dlechg_top *fdct, struct fellow_dle *e)
{
	struct fellow_dlechg *fdc;

	assert(e->type == DLEDSK(DLE_OBJ_ADD));

	fdc = fellow_dlechg_find(fdct, e);
	if (fdc == NULL)
		return (NULL);
	AN(VRBT_REMOVE(fellow_dlechg_head, &fdct->head, fdc));

	assert(DLECHG_OK(fdc->chg, &e->u.obj));

	if (DLECHG_IS_OBJ_DEL_THIN(fdc->chg))
		e->type = DLEDSK(DLE_OBJ_DEL_ALLOCED);
	else
		DLECHG_OBJ_CHG_COPY(&e->u.obj, fdc->chg);

	memset(fdc, 0, sizeof *fdc);
	VSLIST_INSERT_HEAD(&fdct->free, fdc, u.freelist);
	fdct->nfree++;
	return (e);
}
