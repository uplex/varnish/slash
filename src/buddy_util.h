/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "bitsof.h"

/*
 * min is the number of bits of the minimum allocation
 */
static inline size_t
rup_min(size_t size, unsigned minbits)
{
	size_t min;

	min = (size_t)1 << minbits;
	min--;
	size += min;
	size &= ~min;
	return (size);
}
static inline size_t
rdown_min(size_t size, unsigned minbits)
{
	size_t min;

	min = (size_t)1 << minbits;
	min--;
	return (size & ~min);
}

/* next log 2 of sz */
static inline unsigned
log2up(size_t sz)
{
	if (sz == 1)
		return (0);
	assert(sz > 1);
	sz--;
	sz = bitsof(sz);
	assert(sz > 0);
	return ((unsigned) sz);
}

// #define log2down(x) (log2up(x+1) - 1)

/* prev log 2 of sz */
static inline unsigned
log2down(size_t sz)
{
	assert(sz > 0);
	sz = bitsof(sz) - 1;
	return ((unsigned) sz);
}
