/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

// fellow_io_backend.h

#include "fellow_task.h"

// basically io_uring_cqe
struct fellow_io_status {
	uint64_t	info;
	int32_t	result;
	uint32_t	flags;	// errno for ioctl
};

/*
 * because the uring_cqe result member is 32bit (max pos value is 2GB-1), we
 * limit our max IO (and, thus, disk allocation) size to 1GB
 */
#define FIO_MAX_BITS 30
#define FIO_MAX (1<<FIO_MAX_BITS)

struct fellow_io_discard {
	uint64_t offset, len;
};

typedef struct fellow_ioctx fellow_ioctx_t;
#define IOCTX(x) ((fellow_ioctx_t *)(x))

fellow_ioctx_t *fellow_io_init(int, unsigned, void *, size_t,
    fellow_task_run_t);
void fellow_io_fini(fellow_ioctx_t **);
unsigned fellow_io_entries(fellow_ioctx_t *ctxp);
unsigned fellow_io_unsubmitted(fellow_ioctx_t *ctxp);
unsigned fellow_io_outstanding(fellow_ioctx_t *ctxp);
int fellow_io_write_async_enq(fellow_ioctx_t *ctx,
    uint64_t info, const void *buf, size_t bytes, off_t off);
int fellow_io_read_async_enq(fellow_ioctx_t *ctx,
    uint64_t info, void *buf, size_t bytes, off_t off);
int fellow_io_fallocate_enq(fellow_ioctx_t *ctxp, uint64_t info,
    int mode, uint64_t offset, uint64_t len);
unsigned fellow_io_blkdiscard_enq(fellow_ioctx_t *ctxp, uint64_t info,
    const struct fellow_io_discard *discards, unsigned n);

typedef void fellow_io_compl_cb(void *priv,
    struct fellow_io_status *status, unsigned n);
unsigned fellow_io_submit_and_wait(fellow_ioctx_t *ctx,
    struct fellow_io_status *results, unsigned space, unsigned min,
    fellow_io_compl_cb cb, void *priv);
unsigned
fellow_io_wait_completions_only(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results,
    unsigned space, unsigned min, fellow_io_compl_cb cb, void *priv);
