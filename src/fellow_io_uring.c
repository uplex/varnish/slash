/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <unistd.h>

#include "liburing.h"

// varnish
#include "miniobj.h"
#include "vdef.h"
#include "vas.h"

#ifndef HAVE_IO_URING_FREE_PROBE
#define io_uring_free_probe(x) free(x)
#endif

#include "fellow_io_backend.h"
#include "fellow_io_ioctl.h"

#define ERR(what, x) do {						\
		int _e = (x);						\
		fprintf(stderr, "%s: %s %s (%d)\n",			\
		    __func__, what, strerror(_e), _e);			\
	} while(0)

struct fellow_io_uring {
	unsigned			magic;
#define FELLOW_IO_URING_MAGIC		0xe4e12fcd
	unsigned			entries;
	unsigned			unsubmitted;
	unsigned			outstanding;
	uintptr_t			base;
	size_t				len;
	int				fd;
	uint8_t				sqe_flags;
	struct io_uring			ring;
	struct fellow_io_ioctl		*ioctl;
};

unsigned
fellow_io_blkdiscard_enq(fellow_ioctx_t *ctxp, uint64_t info,
    const struct fellow_io_discard *discards, unsigned n)
{
	struct fellow_io_uring *ctx;
	struct fellow_io_ioctl_blkdiscard_ret r;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);

	r = fellow_io_ioctl_blkdiscard_enq(ctx->ioctl, info, discards, n);

	ctx->outstanding += r.nslots;
	return (r.ndiscards);
}

/* --- actual uring */

#define IOURING_BUFBITS 30
#define IOURING_BUFMAX  (1 << IOURING_BUFBITS)

static inline int
regbuffer_idx(const struct fellow_io_uring *ctx, const void *p, unsigned nbytes)
{
	uintptr_t addr = (uintptr_t)p;
	uintptr_t base = ctx->base;
	size_t     len = ctx->len;

	if (base > 0 &&
	    addr >= base &&
	    addr + nbytes <= base + len) {
		addr -= ctx->base;
		addr >>= IOURING_BUFBITS;
		assert(addr <= INT_MAX);

		return ((int)addr);
	}
	return (-1);
}

enum regbuf_choice {
	REGBUF_INVAL = 0,
	REGBUF_UNAVAIL,
	REGBUF_SKIP,
	REGBUF_SYNC,
	REGBUF_BG
};

static enum regbuf_choice
fellow_io_uring_register_buffers_choice(void)
{
	const char *e;

	e = getenv("slash_fellow_options");
	if (e == NULL)
		return (REGBUF_BG);
	if (strstr(e, "skip-uring-register-buffers"))
		return (REGBUF_SKIP);
	if (strstr(e, "sync-uring-register-buffers"))
		return (REGBUF_SYNC);
	return (REGBUF_BG);
}

static void
fellow_io_uring_register_buffers(struct fellow_io_uring *ctx,
    char *base, size_t len)
{
	struct fellow_io_uring testctx;
	unsigned u, ne;
	int ret;
	size_t l;

	AN(ctx);
	AN(base);
	AN(len);

	l = (len >> IOURING_BUFBITS) + 1;	// 1 per GB
	assert(l < UINT_MAX);
	ne = (unsigned)l;

	struct iovec iov[ne];

	testctx.base = (uintptr_t)base;
	testctx.len = len;

	u = 0;
	while (len) {
		l = len;
		if (l > IOURING_BUFMAX)
			l = IOURING_BUFMAX;

		assert(u < ne);
		iov[u].iov_base = base;
		iov[u].iov_len = l;

		assert(l <= UINT_MAX);
		assert(u ==
		    (unsigned)regbuffer_idx(&testctx, base, (unsigned)l));


		base += l;
		len -= l;
		u++;
	}

	fprintf(stderr, "io_uring ctx %p registering %u %s\n", ctx, u,
	    u == 1 ? "buffer" : "buffers");
	ret = io_uring_register_buffers(&ctx->ring, iov, u);
	if (ret) {
		ERR("fellow_io_uring_register_buffers", -ret);
		return;
	}

	ctx->base = testctx.base;
	ctx->len = testctx.len;
}

struct fellow_io_uring_register_buffers_priv {
	unsigned			magic;
#define FELLOW_IOURBP_MAGIC		0x39ec788d
	fellow_task_privstate		taskstate;

	struct fellow_io_uring		*ctx;
	char				*base;
	size_t				len;
};

static void
fellow_io_uring_register_buffers_task(struct worker *wrk, void *priv)
{
	struct fellow_io_uring_register_buffers_priv *iourbp;

	(void) wrk;
	CAST_OBJ_NOTNULL(iourbp, priv, FELLOW_IOURBP_MAGIC);
	fellow_io_uring_register_buffers(iourbp->ctx,
	    iourbp->base, iourbp->len);
	FREE_OBJ(iourbp);
}

//lint -e{429} Custodial pointer 'iourbp' (line 191) has not been freed...
static void
fellow_io_uring_register_buffers_bg(struct fellow_io_uring *ctx,
    char *base, size_t len, fellow_task_run_t taskrun)
{
	struct fellow_io_uring_register_buffers_priv *iourbp;

	ALLOC_OBJ(iourbp, FELLOW_IOURBP_MAGIC);
	AN(iourbp);

	iourbp->ctx = ctx;
	iourbp->base = base;
	iourbp->len = len;

	AZ(taskrun(fellow_io_uring_register_buffers_task,
		iourbp, &iourbp->taskstate));
}

static void
test_task(struct worker *wrk, void *priv)
{
	int *answer = priv;

	(void) wrk;
	AN(answer);
	*answer = 42;
}

/* XXX very simplistic. Sufficient for fellow, but not otherwise */
static int shared_wq_fd = -1;

/* XXX BETTER WAY? https://github.com/axboe/liburing/issues/906 */
static unsigned flags_checked = 0;
static unsigned flags_supported = 0;

static unsigned
try_flag(unsigned flag)
{
	struct io_uring_params params;
	struct io_uring ring;
	const char *e;
	int ret;

	memset(&params, 0, sizeof params);
	params.flags = flag;

	ret = io_uring_queue_init_params(2, &ring, &params);
	if (ret == 0) {
		io_uring_queue_exit(&ring);
		return (flag);
	}

	if (ret == -EINVAL)
		return (0);

	e = strerror(0 - ret);
	if (e == NULL)
		e = "NIL";
	fprintf(stderr, "fellow: io_uring try: FATAL, got %d (%s)\n", ret, e);
	errno = 0 - ret;
	WRONG("Unexpected io_uring error. Is it available?");
}

static void
try_flags(void)
{

	if (flags_checked != 0)
		return;
	AZ(try_flag(0));
#ifdef IORING_SETUP_DEFER_TASKRUN
	flags_supported |= try_flag(IORING_SETUP_DEFER_TASKRUN);
#endif
#ifdef IORING_SETUP_COOP_TASKRUN
	flags_supported |= try_flag(IORING_SETUP_COOP_TASKRUN);
#endif
#if defined(IORING_SETUP_DEFER_TASKRUN) && defined(IORING_SETUP_COOP_TASKRUN)
	if ((flags_supported & (IORING_SETUP_DEFER_TASKRUN|IORING_SETUP_COOP_TASKRUN)) ==
	    (IORING_SETUP_DEFER_TASKRUN|IORING_SETUP_COOP_TASKRUN)) {
		// if both supported, prefer IORING_SETUP_DEFER_TASKRUN
		flags_supported &= ~IORING_SETUP_COOP_TASKRUN;
	}
#endif
	flags_checked = 1;
}

fellow_ioctx_t *
fellow_io_init(int fd, unsigned entries, void *base, size_t len,
    fellow_task_run_t taskrun)
{
	struct io_uring_params params;
	struct io_uring_probe *probe;
	struct fellow_io_uring *ctx;
	int ret, answer = 0;
	fellow_task_privstate taskstate;

	try_flags();

	probe = io_uring_get_probe();
	if (probe == NULL) {
		fprintf(stderr, "io_uring_get_probe() failed\n");
		return (NULL);
	}

	ALLOC_OBJ(ctx, FELLOW_IO_URING_MAGIC);
	AN(ctx);
	ctx->fd = fd;
	ctx->entries = entries;

	AZ(taskrun(test_task, &answer, &taskstate));

	memset(&params, 0, sizeof params);
	params.flags = flags_supported;

#ifdef IORING_SETUP_ATTACH_WQ
	if (shared_wq_fd >= 0) {
		params.flags |= IORING_SETUP_ATTACH_WQ;
		params.wq_fd = (unsigned)shared_wq_fd;
	}
#endif

	ret = io_uring_queue_init_params(entries, &ctx->ring, &params);

	if (ret < 0) {
		ERR("io_uring_queue_init", -ret);
		FREE_OBJ(ctx);
		return (NULL);
	}
	shared_wq_fd = ctx->ring.ring_fd;
	ret = io_uring_ring_dontfork(&ctx->ring);
	if (ret < 0) {
		ERR("io_uring_ring_dontfork", -ret);
		//lint -e{740}
		fellow_io_fini((fellow_ioctx_t **)(&ctx));
	}
#ifdef IOSQE_FIXED_FILE
	if (io_uring_register_files(&ctx->ring, &fd, 1) == 0)
		ctx->sqe_flags |= IOSQE_FIXED_FILE;
	else {
		fprintf(stderr, "io_uring register_files failed "
		    "despite IOSQE_FIXED_FILE defined\n");
	}

#define FEIOURING_SQE_FLAGS(ctx) ((ctx)->sqe_flags)
#define FEIOURING_FD(ctx) (FEIOURING_SQE_FLAGS(ctx) & IOSQE_FIXED_FILE ? 0 : (ctx)->fd)
#else
	fprintf(stderr, "io_uring IOSQE_FIXED_FILE unavailable\n");
#define FEIOURING_SQE_FLAGS(ctx) 0
#define FEIOURING_FD(ctx) ((ctx)->fd)
#endif

	while (answer != 42)
		(void) usleep(1000);

	enum regbuf_choice rbc = REGBUF_UNAVAIL;
	if (base && len &&
	    io_uring_opcode_supported(probe, (int)IORING_OP_READ_FIXED))
		rbc = fellow_io_uring_register_buffers_choice();

	io_uring_free_probe(probe);

	switch (rbc) {
	case REGBUF_UNAVAIL:
	case REGBUF_SKIP:
		break;
	case REGBUF_SYNC:
		fellow_io_uring_register_buffers(ctx, base, len);
		break;
	case REGBUF_BG:
		fellow_io_uring_register_buffers_bg(ctx, base, len, taskrun);
		break;
	default:
		WRONG("regbuf_choice");
	}

	ctx->ioctl = fellow_io_ioctl_init(fd, taskrun, entries);
	AN(ctx->ioctl);

	return (IOCTX(ctx));
}

void
fellow_io_fini(fellow_ioctx_t **ctxp)
{
	struct fellow_io_uring *ctx;

	if (*ctxp == NULL)
		return;

	TAKE_OBJ_NOTNULL(ctx, (void **)ctxp, FELLOW_IO_URING_MAGIC);

	struct fellow_io_status results[ctx->entries];

	(void) fellow_io_submit_and_wait(IOCTX(ctx), results, ctx->entries,
	    UINT_MAX, NULL, NULL);

	fellow_io_ioctl_fini(&ctx->ioctl);

	shared_wq_fd = -1;

	AZ(ctx->unsubmitted);
	AZ(ctx->outstanding);

	io_uring_queue_exit(&ctx->ring);
	FREE_OBJ(ctx);
}

unsigned
fellow_io_entries(fellow_ioctx_t *ctxp)
{
	struct fellow_io_uring *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);
	return (ctx->entries);
}

unsigned
fellow_io_outstanding(fellow_ioctx_t *ctxp)
{
	struct fellow_io_uring *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);
	return (ctx->unsubmitted + ctx->outstanding);
}

unsigned
fellow_io_unsubmitted(fellow_ioctx_t *ctxp)
{
	struct fellow_io_uring *ctx;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);
	return (ctx->unsubmitted);
}

int
fellow_io_read_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, void *buf, size_t bytes, off_t off)
{
	struct fellow_io_uring *ctx;
	struct io_uring_sqe *sqe;
	unsigned nbytes;
	uint64_t offset;
	int r;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);

	sqe = io_uring_get_sqe(&ctx->ring);
	if (sqe == NULL)
		return (0);

	assert(bytes <= UINT_MAX);
	assert(off > 0);
	nbytes = (typeof(nbytes))bytes;
	offset = (typeof(offset))off;

	r = regbuffer_idx(ctx, buf, nbytes);
	if (r >= 0)
		io_uring_prep_read_fixed(sqe, FEIOURING_FD(ctx),
		    buf, nbytes, offset, r);
	else
		io_uring_prep_read(sqe, FEIOURING_FD(ctx),
		    buf, nbytes, offset);

	sqe->flags = FEIOURING_SQE_FLAGS(ctx);
	sqe->user_data = info;
	ctx->unsubmitted++;
	return (1);
}

int
fellow_io_write_async_enq(fellow_ioctx_t *ctxp,
    uint64_t info, const void *buf, size_t bytes, off_t off)
{
	struct fellow_io_uring *ctx;
	struct io_uring_sqe *sqe;
	unsigned nbytes;
	uint64_t offset;
	int r;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);

	sqe = io_uring_get_sqe(&ctx->ring);
	if (sqe == NULL)
		return (0);

	assert(bytes <= UINT_MAX);
	assert(off > 0);
	nbytes = (typeof(nbytes))bytes;
	offset = (typeof(offset))off;

	r = regbuffer_idx(ctx, buf, nbytes);
	if (r >= 0)
		io_uring_prep_write_fixed(sqe, FEIOURING_FD(ctx),
		    buf, nbytes, offset, r);
	else
		io_uring_prep_write(sqe, FEIOURING_FD(ctx), buf,
		    nbytes, offset);

	sqe->flags = FEIOURING_SQE_FLAGS(ctx);
	sqe->user_data = info;
	ctx->unsubmitted++;
	return (1);
}

int
fellow_io_fallocate_enq(fellow_ioctx_t *ctxp, uint64_t info,
    int mode, uint64_t offset, uint64_t len)
{
	struct fellow_io_uring *ctx;
	struct io_uring_sqe *sqe;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);

	sqe = io_uring_get_sqe(&ctx->ring);
	if (sqe == NULL)
		return (0);
	io_uring_prep_fallocate(sqe, FEIOURING_FD(ctx), mode, offset, len);
	sqe->flags = FEIOURING_SQE_FLAGS(ctx);
	sqe->user_data = info;
	ctx->unsubmitted++;
	return (1);
}

// handle results, return advance
static uint32_t
fellow_io_uring_fill_results(struct io_uring *ring,
    struct fellow_io_status *results, unsigned space,
    struct fellow_io_ioctl *ioctl)
{
	struct io_uring_cqe *cqe;
	uint32_t head, advance;

	advance = 0;
	io_uring_for_each_cqe(ring, head, cqe) {
		AN(space);
		results->info = cqe->user_data;
		results->result = cqe->res;
		results->flags = cqe->flags;
		advance++;
		results++;
		if (--space == 0)
			break;
	}
	if (advance > 0)
		io_uring_cq_advance(ring, advance);

	advance += fellow_io_ioctl_peek(ioctl, results, space);
	return (advance);
}

/*
 * completions are registered in the space provided
 * in addition, cb is called
 */
unsigned
fellow_io_submit_and_wait(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results,
    unsigned space, unsigned min, fellow_io_compl_cb cb, void *priv)
{
	struct fellow_io_uring *ctx;
	struct fellow_io_status *end;
	unsigned u, lim, ret = 0;
	uint32_t advance;
	struct io_uring *ring;
	int r;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);
	ring = &ctx->ring;

	end = results + space;

	AN(results);
	AN(space);

	lim = ctx->unsubmitted + ctx->outstanding;
	if (space > lim)
		space = lim;
	if (min > space)
		min = space;

	/*lint -e{845} The right argument to operator '||' is certain to be 0
	 * strange, flexelint seems to disregard min != 0 argument?
	 */
	while (min > ret || min == 0) {
		advance = fellow_io_uring_fill_results(ring, results, space, ctx->ioctl);

		assert(advance <= space);
		space -= advance;

		if (cb != NULL && advance > 0)
			cb(priv, results, advance);

		assert(end > results);
		assert(end - results >= advance);
		results += advance;
		ret += advance;

		assert(ctx->outstanding >= advance);
		ctx->outstanding -= advance;

		if (ctx->unsubmitted)
			r = io_uring_submit(ring);
		else if (min > ret &&
		    fellow_io_ioctl_wait(ctx->ioctl) == 0)
			r = io_uring_submit_and_wait(ring, min - ret);
		else
			r = 0;

		if (r < 0) {
			assert(errno == EINTR);
			continue;
		}

		assert(r >= 0);
		u = (typeof(u))r;

		assert(u <= ctx->unsubmitted);
		ctx->outstanding += u;
		ctx->unsubmitted -= u;

		if (min == 0)
			return (ret);
	}
	return (ret);
}

/*
 * handle completions only - can happen unlocked, because from a single thread
 */
unsigned
fellow_io_wait_completions_only(fellow_ioctx_t *ctxp,
    struct fellow_io_status *results,
    unsigned space, unsigned min, fellow_io_compl_cb cb, void *priv)
{
	struct fellow_io_uring *ctx;
	uint32_t advance;
	unsigned lim, ret = 0;
	struct io_uring *ring;
	unsigned flags;

	CAST_OBJ_NOTNULL(ctx, (void *)ctxp, FELLOW_IO_URING_MAGIC);
	ring = &ctx->ring;

	flags = IORING_ENTER_GETEVENTS;
	// XXX IORING_ENTER_REGISTERED_RING ?

	AN(results);
	AN(space);

	lim = ctx->outstanding;
	if (space > lim)
		space = lim;
	if (min > space)
		min = space;

	while (min > ret) {
		advance = fellow_io_uring_fill_results(ring, results, space, ctx->ioctl);

		assert(advance <= space);
		space -= advance;

		if (cb != NULL && advance > 0)
			cb(priv, results, advance);

		results += advance;
		ret += advance;

		assert(ctx->outstanding >= advance);
		ctx->outstanding -= advance;

		if (min > ret &&
		    fellow_io_ioctl_wait(ctx->ioctl) == 0)
			AZ(io_uring_enter((unsigned)ring->enter_ring_fd, 0, min - ret, flags, NULL));
	}
	return (ret);
}
