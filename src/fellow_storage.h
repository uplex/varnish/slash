/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

const char * sfe_error(void);

struct stevedore *
sfe_new(const char *id, const char *filename,
    size_t dsksz, size_t memsz, size_t objsize_hint, unsigned discard);
void sfe_fini(struct stevedore **stvp);

struct stvfe_tune;
void sfe_tune_get(const struct stevedore *, struct stvfe_tune *);
const char *sfe_tune_apply(const struct stevedore *,
    const struct stvfe_tune *);

int sfe_is(VCL_STEVEDORE);
void sfe_as_transient(struct stevedore *);

#ifdef TODO
struct stevedore *
sbu_new(const char *id, size_t *sz, size_t *min, size_t maxchunksize,
    double reserve, int cram);
void sbu_reconfigure(struct stevedore *, size_t maxchunksize,
    double reserve, int cram);
void sbu_fini(struct stevedore **);
#endif
