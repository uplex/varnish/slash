The tests in this directory use dynamic deconfiguration of stevedores,
for which varnish-cache currently has no supporting facilities and
which probably need more attention before becoming usable.
