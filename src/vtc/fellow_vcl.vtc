varnishtest "test vcl-defined fellow"

varnish v1 -arg "-jnone -p debug=+vclrel -p vsl_mask=+expkill" -vcl {
	import slash;
	import std;

	backend none none;

	sub vcl_init {
		new fellow = slash.fellow(
			"${tmpdir}/fellow_vcl.stv",
			dsksize	=  100MB,
			memsize	=   1MB,
			objsize_hint	=  64KB);
		fellow.tune(chunk_bytes = 256KB, mem_reserve_chunks = 999999, dsk_reserve_chunks = 99999);
		fellow.as_transient();
	}
	sub vcl_synth {
		if (req.http.logbuffer-size) {
			set resp.http.e = fellow.tune(logbuffer_size =
				std.integer(req.http.logbuffer-size));
		}
		else if (req.http.logbuffer-flush-interval) {
			set resp.http.e = fellow.tune(logbuffer_flush_interval =
				std.duration(req.http.logbuffer-flush-interval));
		}
		else if (req.http.log-rewrite-ratio) {
			set resp.http.e = fellow.tune(log_rewrite_ratio =
				std.real(req.http.log-rewrite-ratio));
		}
		else if (req.http.chunk-bits) {
			set resp.http.e = fellow.tune(chunk_exponent =
				std.integer(req.http.chunk-bits));
		}
		else if (req.http.dsk-reserve-chunks) {
			set resp.http.e = fellow.tune(dsk_reserve_chunks =
				std.integer(req.http.dsk-reserve-chunks));
		}
		else if (req.http.mem-reserve-chunks) {
			set resp.http.e = fellow.tune(mem_reserve_chunks =
				std.integer(req.http.mem-reserve-chunks));
		}
		else {
			set resp.http.e = "Unknown tunable header";
		}

		if (resp.http.e == "") {
			unset resp.http.e;
		} else {
			set resp.status = 400;
			set resp.body = resp.http.e;
		}
		return (deliver);
	}
	sub vcl_recv {
		if (req.url == "/tune") {
			return (synth(200));
		}
		if (req.url == "/pass") {
			return (pass);
		}
	}
	sub vcl_hash {
		if (req.url == "/synth") {
			hash_data(std.random(0,999999999));
		}
	}
	sub vcl_backend_fetch {
		if (bereq.url == "/synth") {
			set bereq.backend = none;
		}
	}
	sub vcl_backend_error {
		set beresp.status = 200;
		set beresp.ttl = 1h;
		set beresp.storage = fellow.storage();
		set beresp.http.storage = beresp.storage;
		synthetic("""0123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef
123456789abcdef0123456789abcdef123456789abcdef0123456789abcdef""");
		return (deliver);
	}
	sub vcl_backend_response {
		set beresp.http.free_beresp = fellow.storage().free_space;
		set beresp.storage = fellow.storage();
		set beresp.http.storage = beresp.storage;
	}
	sub vcl_deliver {
		set resp.http.free_deliver = fellow.storage().free_space;
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
#	expect resp.http.free_beresp == 16384
#	expect resp.http.free_deliver == 15104

	txreq -url "/1"
	rxresp
	expect resp.status == 200

	txreq -url "/2"
	rxresp
	expect resp.status == 200

	txreq -url "/3"
	rxresp
	expect resp.status == 200

	txreq -url "/pass"
	rxresp
	expect resp.status == 200
#	expect resp.http.free_beresp == 15104
#	expect resp.http.free_deliver == 13888

	txreq
	rxresp
	expect resp.status == 200
#	expect resp.http.free_beresp == 16384
#	expect resp.http.free_deliver == 15104
} -run

client c1 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c2 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c3 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c4 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c5 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c6 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c7 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start
client c8 -repeat 16 {
	txreq -url "/synth"
	rxresp
	expect resp.status == 200
} -start

client c1 -wait
client c2 -wait
client c3 -wait
client c4 -wait
client c5 -wait
client c6 -wait
client c7 -wait
client c8 -wait

### TUNE

client c1 -repeat 1 {
	txreq -url "/tune" -hdr "logbuffer-size: 28"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
	txreq -url "/tune" -hdr "logbuffer-size: 27"
	rxresp
	expect resp.status == 400
	expect resp.body == "Value of logbuffer_size is too small, minimum is 28"
} -start

client c2 -repeat 1 {
	txreq -url "/tune" -hdr "logbuffer-flush-interval: 2m"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
	txreq -url "/tune" -hdr "logbuffer-flush-interval: -1s"
	rxresp
	expect resp.status == 400
	expect resp.body == "Value of logbuffer_flush_interval is too small, minimum is 0.0"
} -start

client c3 -repeat 1 {
	txreq -url "/tune" -hdr "log-rewrite-ratio: 0.1"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
	txreq -url "/tune" -hdr "log-rewrite-ratio: 0.0001"
	rxresp
	expect resp.status == 400
	expect resp.body == "Value of log_rewrite_ratio is too small, minimum is 0.001"
} -start

client c4 -repeat 1 {
	txreq -url "/tune" -hdr "chunk-bits: 16"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
	txreq -url "/tune" -hdr "chunk-bits: 11"
	rxresp
	expect resp.status == 400
	expect resp.body == "Value of chunk_exponent is too small, minimum is 12"
} -start

client c5 -repeat 1 {
	txreq -url "/tune" -hdr "dsk-reserve-chunks: 8"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
} -start

client c6 -repeat 1 {
	txreq -url "/tune" -hdr "mem-reserve-chunks: 2"
	rxresp
	expect resp.status == 200
	expect resp.body == ""
	txreq -url "/tune" -hdr "mem-reserve-chunks: -1"
	rxresp
	expect resp.status == 400
	expect resp.body == "Value of mem_reserve_chunks must not be negative"
} -start

client c1 -wait
client c2 -wait
client c3 -wait
client c4 -wait
client c5 -wait
client c6 -wait
