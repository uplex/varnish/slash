/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"
#include <assert.h>
#include "pow2_units.h"

const char * const pow2_units[POW2_MAX + 1] = {
	[0] = "1B",
	[1] = "2B",
	[2] = "4B",
	[3] = "8B",
	[4] = "16B",
	[5] = "32B",
	[6] = "64B",
	[7] = "128B",
	[8] = "256B",
	[9] = "512B",
	[10] = "1KB",
	[11] = "2KB",
	[12] = "4KB",
	[13] = "8KB",
	[14] = "16KB",
	[15] = "32KB",
	[16] = "64KB",
	[17] = "128KB",
	[18] = "256KB",
	[19] = "512KB",
	[20] = "1MB",
	[21] = "2MB",
	[22] = "4MB",
	[23] = "8MB",
	[24] = "16MB",
	[25] = "32MB",
	[26] = "64MB",
	[27] = "128MB",
	[28] = "256MB",
	[29] = "512MB",
	[30] = "1GB",
	[31] = "2GB",
	[32] = "4GB",
	[33] = "8GB",
	[34] = "16GB",
	[35] = "32GB",
	[36] = "64GB",
	[37] = "128GB",
	[38] = "256GB",
	[39] = "512GB",
	[40] = "1TB",
	[41] = "2TB",
	[42] = "4TB",
	[43] = "8TB",
	[44] = "16TB",
	[45] = "32TB",
	[46] = "64TB",
	[47] = "128TB",
	[48] = "256TB",
	[49] = "512TB",
	[50] = "1PB",
	[51] = "2PB",
	[52] = "4PB",
	[53] = "8PB",
	[54] = "16PB",
	[55] = "32PB",
	[56] = "64PB",
	[57] = "128PB",
	[58] = "256PB",
	[59] = "512PB",
	[60] = "1EB",
	[61] = "2EB",
	[62] = "4EB",
	[63] = "8EB",
	[64] = "16EB",
	// slash max: 64bits offset is realistic maximum while we use file io
	// with 64bit off_t
	[65] = "32EB",
	[66] = "64EB",
	[67] = "128EB",
	[68] = "256EB",
	[69] = "512EB",
	[70] = "1ZB",
	[71] = "2ZB",
	[72] = "4ZB",
	[73] = "8ZB",
	[74] = "16ZB",
	[75] = "32ZB",
	[76] = "64ZB",
	// slash max for 4K blocks (12 bits) + 64bits offset = 76 bits
	[77] = "*OV*",
	[78] = "*OV*",
	[79] = "*OV*",
	[80] = "*OV*"
};
