/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */
//				   mem			dsk
#define FEP_RESERVE	0	// reservation		reservation

#define FEP_GET	1	// obj_get		object, body

#define FEP_NEW	2	// busy_obj_alloc	busy_obj_alloc

#define FEP_OHLCK	3	// obj critical (oh lck)

#define FEP_MEM_REWR	4	// logs_rewrite
#define FEP_MEM_FLC	4	// fellow_logcache_*

#define FEP_SPC	5	// sfemem_getspace	busy_region_alloc
#define FEP_MEM_ITER	5	// fellow_cache_obj_readahead()

#define FEP_META	6	// fcsl, aux seg
#define FEP_SPCPRI	6	// sfemem_getspace	busy_region_alloc
#define FEP_MEM_ITERPRI	6	// fellow_cache_obj_readahead()

#define FEP_MEM_LOG	7	// logblk_new, ...
				// ... logbuffer_alloc_some, bitf
#define FEP_MEM_LBUF	7	// logbuffer_*
#define FEP_DSK_LOG	7	//			logbuffer_alloc_some

#define FEP_MEM_DLECHG	8	// fellow_dlechg_*
#define FEP_MEM_FREE	8	// regionlist
#define FEP_DSK_LOG_PRI 8	//			logbuffer_alloc_... free

/*
 * these assertions with comments explain the reasoning behind the
 * priorities
 */

static void __attribute__((constructor))
assert_fellow_pri(void)
{
	//lint --e{506} constant value boolean
	assert(FEP_RESERVE == 0);

	// anything is more important than the reserve
	assert(FEP_GET > FEP_RESERVE);

	/*
	 * busy objects are more imprtant than reads, because:
	 * - they have a backend thread, and generally take up more resources
	 * - new content is generally more important than old content
	 *   (note: this is about dsk objects only, objects already in ram
	 *    do not allocate anything)
	 */
	assert(FEP_NEW > FEP_GET);

	/*
	 * FEP_OHLCK is used for two situations:
	 * - an object has a GetAttr for OA_VARY. This is critical, because
	 *   Varnish-Cache holds the objhead mutex
	 * - dsk-lru needs to read the object to make room
	 *
	 * Obviously, this is more importantant than otherwise getting objects,
	 * but less important than a rewrite, which must not be kept back. Plus,
	 * the operation finishes quickly
	 */
	assert(FEP_OHLCK > FEP_NEW);

	/*
	 * logblocks for rewrites have priority over accepting new objects into
	 * cache, that is to say, we want to stop accepting new objects for a
	 * rewrite, if necessary and let LRU make room. DLE changes are the
	 * exception (see below)
	 */
	assert(FEP_MEM_REWR > FEP_OHLCK);
	assert(FEP_MEM_FLC == FEP_MEM_REWR);
	/*
	 * to any busy object or object iteration which we started holds onto
	 * memory. So to get free space, we need to ensure these operations
	 * complete. Thus, they have higher priority than the rewrite requests.
	 *
	 * Requests with these priorities are for pre-allocations
	 */
	assert(FEP_SPC > FEP_MEM_REWR);
	assert(FEP_MEM_ITER == FEP_SPC);

	/*
	 * besides FEP_META, which obviously is more important than body data,
	 * these match the aforementioned, but for the case where we have a sync
	 * wait on the memory allocation.
	 */

	assert(FEP_META > FEP_SPC);
	assert(FEP_SPCPRI == FEP_META);
	assert(FEP_MEM_ITERPRI == FEP_META);

	/*
	 * the remaining priorities are for critical log operations, on which
	 * basically everything else depends.
	 */
	assert(FEP_MEM_LOG > FEP_META);
	assert(FEP_MEM_LOG == FEP_MEM_LBUF);
	assert(FEP_MEM_LOG == FEP_DSK_LOG);

	assert(FEP_MEM_DLECHG > FEP_MEM_LOG);
	assert(FEP_MEM_DLECHG == FEP_MEM_FREE);
	assert(FEP_MEM_DLECHG == FEP_DSK_LOG_PRI);
}
