enum stvfe_scope {
	STVFE_INVAL = 0,
	STVFE_GLOBAL,
	STVFE_VCL_DISCARD,	// discard all objects
	STVFE_VCL_EMPTY	// fail unless storage is empty
};

const char *
sfe_mgt_tryopen(const char *path, enum stvfe_scope scope);
