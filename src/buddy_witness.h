/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifndef BUDDY_WITNESS

#define BWIT_TAKE(head, s, sz) (void)0
#define BWIT_RELEASE(head, s, sz) (void)0
#define BWIT_ASSERT_ALLOCED(w, s, sz) (void)0
#define BWIT_ASSERT_FREE(w, s, sz) (void)0
#define BWIT_ISEMPTY(head) (void)0
#define BWIT_DECL(name)
#define BWIT_SET(to, from) (void)0
#else

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#ifdef BUDDY_WITNESS_BACKTRACE
#include <execinfo.h>
#endif

#include <vtree.h>

VRBT_HEAD(bwit_head, bwit);

struct buddy_witness {
	struct bwit_head alloced, freed;
};

#define BWIT_TAKE(head, s, sz)						\
	bwit_take(head, __func__, __LINE__, (intptr_t)s, sz)
#define BWIT_RELEASE(head, s, sz)			\
	bwit_release(head, __func__, __LINE__, (intptr_t)s, sz)
#define BWIT_ASSERT_ALLOCED(w, s, sz)		\
	bwit_assert_alloced(w, s, sz)
#define BWIT_ASSERT_FREE(w, s, sz)		\
	bwit_assert_free(w, s, sz)
#define BWIT_ISEMPTY(head)			\
	bwit_isempty(head)
#define BWIT_DECL(name)				\
	struct buddy_witness name;
#define BWIT_SET(to, from)			\
	to = from

enum bwit_e {
	BW_INVAL = 0,
	BW_SINGLE,
	BW_START,
	BW_END,
	BW_LIM
};

static const char *bwit_type[BW_LIM] = {
	[BW_INVAL] = "*invalid*",
	[BW_SINGLE] = "single",
	[BW_START] = "start",
	[BW_END] = "end"
};

#ifdef BUDDY_WITNESS_BACKTRACE
#define BWIT_BT(elm)							\
	backtrace_symbols_fd((elm)->bt, (elm)->btn, fileno(stderr))
#else
#define BWIT_BT(elm) (void) 0
#endif

#define BWIT_PRINTe(elm)						\
	do {								\
		fprintf(stderr, "%s %zd == %p "		\
		    "from %s line %d \n",				\
		    bwit_type[(elm)->type], (elm)->p, (void *)(elm)->p, \
		    (elm)->func, (elm)->line);				\
		BWIT_BT(elm);						\
	} while(0)

#define BWIT_PRINTE(elm, x, ...)					\
	do {								\
		fprintf(stderr, "existing %s %zd == %p "		\
		    "from %s line %d " x "\n",				\
		    bwit_type[(elm)->type], (elm)->p, (void *)(elm)->p, \
		    (elm)->func, (elm)->line,				\
		    __VA_ARGS__);					\
		BWIT_BT(elm);						\
	} while(0)

#define BWIT_FAILE(...)							\
	do {								\
		BWIT_PRINTE(__VA_ARGS__);				\
		abort();						\
	} while(0)

#define BWIT_FAIL(x, ...)						\
	do {								\
		fprintf(stderr, x, __VA_ARGS__);			\
		abort();						\
	} while(0)

#define BT_BUF_SIZE 9

struct bwit {
	unsigned		magic;
#define BWIT_MAGIC	0x984220ff
	enum bwit_e		type;
	struct bwit_head	*head;
	VRBT_ENTRY(bwit)	entry;
	intptr_t		p;
	const char		*func;
	int			line;
#ifdef BUDDY_WITNESS_BACKTRACE
	int			btn;
	void			*bt[BT_BUF_SIZE];
#endif
};

static inline int
bwit_cmp(const struct bwit *b1, const struct bwit *b2)
{
	if (b1->p < b2->p)
		return (-1);
	if (b1->p > b2->p)
		return (1);
	return (0);
}

VRBT_GENERATE_INSERT_COLOR(bwit_head, bwit, entry, static)
//VRBT_GENERATE_FIND(bwit_head, bwit, entry, bwit_cmp, static)
VRBT_GENERATE_NFIND(bwit_head, bwit, entry, bwit_cmp, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(bwit_head, bwit, entry, static)
#endif
VRBT_GENERATE_INSERT(bwit_head, bwit, entry, bwit_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(bwit_head, bwit, entry, static)
VRBT_GENERATE_REMOVE(bwit_head, bwit, entry, static)
VRBT_GENERATE_NEXT(bwit_head, bwit, entry, static)
VRBT_GENERATE_PREV(bwit_head, bwit, entry, static)
VRBT_GENERATE_MINMAX(bwit_head, bwit, entry, static)

static void
bwit_mark_single(struct bwit_head *head, const char *func, int line,
    intptr_t s)
{
	struct bwit *new_s, *elm;

	new_s = calloc(1, sizeof *new_s);
	AN(new_s);

	new_s->magic = BWIT_MAGIC;
	new_s->type = BW_SINGLE;
	new_s->p = s;
	new_s->func = func;
	new_s->line = line;
#ifdef BUDDY_WITNESS_BACKTRACE
	new_s->btn = backtrace(new_s->bt, BT_BUF_SIZE);
#endif

	elm = VRBT_NFIND(bwit_head, head, new_s);
	if (elm != NULL) {
		CHECK_OBJ(elm, BWIT_MAGIC);
		if (elm->p == s || elm->type == BW_END) {
			BWIT_FAILE(elm, "conflicts with %zd (%p)",
			    s, (void *)s);
		}
	}
	AZ(elm->head);
	elm->head = head;
	AZ(VRBT_INSERT(bwit_head, head, new_s));
}

// s..e   ...  s..e
static inline void
bwit_mark(struct bwit_head *head, const char *func, int line,
    intptr_t s, size_t sz)
{
	struct bwit *new_s, *new_e, *elm;
	intptr_t e = s + sz - 1;

	assert(s >= 0);

	if (e == s) {
		bwit_mark_single(head, func, line, s);
		return;
	}

	assert(e > s);

	new_s = calloc(2, sizeof *new_s);
	AN(new_s);
	new_e = new_s + 1;

	new_s->magic = new_e->magic = BWIT_MAGIC;
	new_s->type = BW_START;
	new_e->type = BW_END;
	new_s->p = s;
	new_e->p = e;
	new_s->func = new_e->func = func;
	new_s->line = new_e->line = line;
#ifdef BUDDY_WITNESS_BACKTRACE
	new_s->btn = new_e->btn = backtrace(new_s->bt, BT_BUF_SIZE);
	memcpy(new_e->bt, new_s->bt, sizeof new_e->bt);
#endif

	elm = VRBT_NFIND(bwit_head, head, new_s);
	if (elm != NULL) {
		CHECK_OBJ(elm, BWIT_MAGIC);
		if (elm->p <= e) {
			BWIT_FAILE(elm, "in %zd...%zd (%p...%p)",
			    s, e, (void *)s, (void *)e);
		}
	}
	AZ(new_s->head);
	new_s->head = head;
	AZ(new_e->head);
	new_e->head = head;
	AZ(VRBT_INSERT(bwit_head, head, new_s));
	AZ(VRBT_INSERT(bwit_head, head, new_e));
}

struct bwit_tuple {
	struct bwit *s, *e;
};

/*
 * finds the closest allocation for the extent, it can be wider
 * or smaller
 */

static struct bwit_tuple
bwit_find(struct bwit_head *head, intptr_t s, size_t sz)
{
	struct bwit_tuple elm = {NULL, NULL};
	struct bwit needle[1];
	intptr_t e = s + sz - 1;

	assert(s >= 0);

	INIT_OBJ(needle, BWIT_MAGIC);

	needle->p = s;
	elm.s = VRBT_NFIND(bwit_head, head, needle);
	if (elm.s == NULL)
		return (elm);
	else if (elm.s->p == s && elm.s->type == BW_SINGLE)
		return (elm);
	else if (elm.s->type == BW_END) {
		elm.e = elm.s;
		elm.s = VRBT_PREV(bwit_head, head, elm.s);
	}
	else
		elm.e = VRBT_NEXT(bwit_head, head, elm.s);

	CHECK_OBJ_NOTNULL(elm.s, BWIT_MAGIC);
	assert(elm.s->head == head);

	if (elm.s->type != BW_START) {
		BWIT_FAIL("find: type %s for start %zd == %p",
		    bwit_type[(elm.s)->type], s, (void *)s);
	}

	if (elm.e == NULL) {
		fprintf(stderr, "no end %zd == %p found for:\n",
		    e, (void *)e);
		BWIT_PRINTe(elm.s);
		abort();
	}
	CHECK_OBJ_NOTNULL(elm.e, BWIT_MAGIC);
	assert(elm.e->head == head);
	if (elm.e->type != BW_END) {
		BWIT_FAIL("find: type %s for end %zd == %p",
		    bwit_type[(elm.e)->type], e, (void *)e);
	}

	/* our only guarantees are
	 * - elm.s is a BW_START
	 * - elm.e is a BW_END
	 * - elm.s is left of elm.e
	 */
	assert(elm.s->type == BW_START);
	assert(elm.e->type == BW_END);
	assert(elm.s->p < elm.e->p);
	return (elm);
}

static void
bwit_clear(struct bwit_head *head, intptr_t s, size_t sz)
{
	struct bwit_tuple elm = bwit_find(head, s, sz);
	intptr_t e = s + sz - 1;

	if (elm.s == NULL)
		BWIT_FAIL("clear: no start %zd == %p found",
		    s, (void *)s);

	AN(elm.s);
	if (elm.s->p != s)
		BWIT_FAILE(elm.s, "clear: start does not match %zd == %p",
		    s, (void *)s);
	AN(VRBT_REMOVE(bwit_head, head, elm.s));

	if (elm.e) {
		if (elm.e->p != e)
			BWIT_FAILE(elm.e, "clear: end does not match %zd == %p",
			    e, (void *)e);
		AN(VRBT_REMOVE(bwit_head, head, elm.e));
	}
	else
		assert(sz == 1);
	free(elm.s);
}

static inline void
bwit_aisle(struct bwit_head *head, intptr_t s, size_t sz)
{
	struct bwit *elm, *elm2, *next, needle[1];
	intptr_t e = s + sz - 1;

	INIT_OBJ(needle, BWIT_MAGIC);
	needle->p = s;

	elm = VRBT_NFIND(bwit_head, head, needle);
	CHECK_OBJ_ORNULL(elm, BWIT_MAGIC);
	if (elm != NULL && elm->type == BW_END) {
		assert(elm->p >= s);
		elm->p = s - 1;
		elm = VRBT_NEXT(bwit_head, head, elm);
	}
	while (elm != NULL) {
		CHECK_OBJ(elm, BWIT_MAGIC);
		assert(elm->head == head);
		if (elm->p > e)
			return;
		next = VRBT_NEXT(bwit_head, head, elm);
		switch (elm->type) {
		case BW_START:
			elm2 = next;
			CHECK_OBJ_NOTNULL(elm2, BWIT_MAGIC);
			assert(elm2->head == head);
			next = VRBT_NEXT(bwit_head, head, elm2);
			assert(elm2->type == BW_END);
			if (elm2->p > e + 1) {
				elm->p = e + 1;
				return;
			}
			/* start/end within our range, remove */
			AN(VRBT_REMOVE(bwit_head, head, elm2));
			/* FALLTHROUGH */
		case BW_SINGLE:
			AN(VRBT_REMOVE(bwit_head, head, elm));
			free(elm);
			break;
		default:
			WRONG("element type");
		}
		elm = next;
	}
}

static inline void
bwit_take(struct buddy_witness *witness, const char *func, int line,
    intptr_t s, size_t sz)
{

	AN(witness);

	bwit_aisle(&witness->freed, s, sz);
	bwit_mark(&witness->alloced, func, line, s, sz);
}

static inline void
bwit_release(struct buddy_witness *witness, const char *func, int line,
    intptr_t s, size_t sz)
{

	AN(witness);

	bwit_mark(&witness->freed, func, line, s, sz);
	bwit_clear(&witness->alloced, s, sz);
}

/* can be from a larger allocation */
static inline void
bwit_assert_alloced(struct buddy_witness *witness, intptr_t s, size_t sz)
{
	intptr_t e = s + sz - 1;
	struct bwit_tuple elm;

	AN(witness);
	elm = bwit_find(&witness->alloced, s, sz);

	AN(elm.s);
	assert(elm.s->p <= s);

	assert(sz == 1 || elm.e);

	if (elm.e)
		assert(elm.e->p >= e);
}

static inline void
bwit_assert_free(struct buddy_witness *witness, intptr_t s, size_t sz)
{
	intptr_t e = s + sz - 1;
	struct bwit_tuple elm;

	AN(witness);
	elm = bwit_find(&witness->alloced, s, sz);

	if (elm.s == NULL)
		return;
	if (elm.e == NULL) {
		assert(elm.s->type == BW_SINGLE);
		assert(elm.s->p < s || elm.s->p > e);
		return;
	}
	assert(elm.s->type == BW_START);
	assert(elm.e->type == BW_END);

	assert(elm.s->p > e || elm.e->p < s);
}

static inline void
bwit_isempty(struct buddy_witness *witness)
{
	struct bwit *elm, *elm2, *next;

	AN(witness);

	if (VRBT_EMPTY(&witness->alloced)) {
		VRBT_FOREACH_SAFE(elm, bwit_head, &witness->freed, next) {
			CHECK_OBJ_NOTNULL(elm, BWIT_MAGIC);

			AN(VRBT_REMOVE(bwit_head, &witness->freed, elm));
			if (elm->type == BW_SINGLE) {
				free(elm);
				continue;
			}
			assert(elm->type == BW_START);

			elm2 = next;
			next = VRBT_NEXT(bwit_head, &witness->freed, next);

			CHECK_OBJ_NOTNULL(elm2, BWIT_MAGIC);
			assert(elm2->type == BW_END);
			AN(VRBT_REMOVE(bwit_head, &witness->freed, elm2));

			free(elm);
		}
		return;
	}

	fprintf(stderr, "unreleased allocations witnessed:\n");
	VRBT_FOREACH(elm, bwit_head, &witness->alloced) {
		CHECK_OBJ_NOTNULL(elm, BWIT_MAGIC);
		BWIT_PRINTE(elm, "%s", "");
	}
	abort();
}

#endif
