/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>	// vsl.h needs

#include "cache/cache_varnishd.h"

#include "cache/cache_obj.h"
#include "cache/cache_objhead.h"

#include "storage/storage.h"

// mapping
#include "common/heritage.h"
#include "common/vsmw.h"

#include "VSC_main.h"
#include "VSC_buddy.h"

#include "vnum.h"
#include "vtim.h"
#include "vapi/vsl.h"

#include "buddy.h"
#include "buddy_tune.h"

/* Storage -----------------------------------------------------------*/

#define SBU_PTR(sbu) ((unsigned char *)(sbu)->mem.ptr)
#define SBU_SIZE(sbu) ((sbu)->mem.size - (sbu)->embed)

struct sbu {
	unsigned		magic;
#define SBU_MAGIC		0xb2f17fec
	// enbedded area at the end
	unsigned		embed;

	VTAILQ_ENTRY(sbu)	list;

	struct buddy_ptr_extent	sbu_mem;
	struct buddy_ptr_extent	mem;
	ssize_t			len;
};

/* Object ------------------------------------------------------------*/

VTAILQ_HEAD(sbu_head, sbu);

struct obu {
	unsigned		magic;
#define OBU_MAGIC		0x29fa9e49
	// space available for use outside the API (sbu embedding)
	unsigned		avail;

	/* Fixed size attributes */
#define OBJ_FIXATTR(U, l, s)			\
	uint8_t			fa_##l[s];
#include "tbl/obj_attr.h"

	/* Variable size attributes */
#define OBJ_VARATTR(U, l)			\
	uint8_t			*va_##l;
#include "tbl/obj_attr.h"

#define OBJ_VARATTR(U, l)			\
	unsigned		va_##l##_len;
#include "tbl/obj_attr.h"

	/* Auxiliary attributes */
#define OBJ_AUXATTR(U, l)			\
	struct sbu		*aa_##l;
#include "tbl/obj_attr.h"

	struct sbu		*objstore;
	struct sbu_head		list;
};


/* Stevedore ---------------------------------------------------------*/

struct stvbu {
	unsigned		magic;
#define STVBU_MAGIC		0xdfef7a31
	unsigned		min_bits;
	buddy_t			buddy;
	pthread_t		nukethread;
	struct VSC_buddy	*stats;
	struct vsc_seg		*vsc_seg;

	// once
	unsigned		shutdown;

	// changeable
	struct stvbu_tune	tune;
};

#define CAST_BUDDY(buddy, priv) do {				\
		struct stvbu *_stvbu;				\
		CAST_OBJ_NOTNULL(_stvbu, priv, STVBU_MAGIC);	\
		buddy = &_stvbu->buddy;				\
		CHECK_OBJ(buddy, BUDDY_MAGIC);			\
	} while(0)

static unsigned sbu_bits = 0;


/* ----------------------------------------------------------------------
 * fwd decl
 */
static void sbu_upd_space(struct stvbu *stvbu);

/* ----------------------------------------------------------------------
 * low level functions
 */



#ifdef FREEPAGE_WHEN
#define WHEN ,when
#else
#define WHEN
#endif

static struct sbu *
sbu_embed(struct obu *o)
{
	struct sbu *objstore, *st;

	CHECK_OBJ_NOTNULL(o, OBU_MAGIC);

	if (o->avail < sizeof *st)
		return (NULL);

	objstore = o->objstore;
	CHECK_OBJ_NOTNULL(objstore, SBU_MAGIC);

	objstore->embed += sizeof *st;
	o->avail -= sizeof *st;

	st = (struct sbu *)((uintptr_t)objstore->mem.ptr +
	    objstore->mem.size - objstore->embed);
	return (st);
}

#define SBU_TYPES				\
	SBUT(OBJ),				\
	SBUT(BODY),	/* can be less */	\
	SBUT(FIXED)	/* buf, aux or last body */
enum
sbu_alloc_what_e {
	SBU_INVAL = 0,
#define SBUT(x) SBU_ ## x
	SBU_TYPES
};
#undef SBUT

static const char * const sbu_types[] = {
#define SBUT(x) [SBU_ ## x] = #x
	SBU_TYPES
};
#undef SBUT

static struct sbu *
sbu_alloc(/*lint -e{818}*/ struct worker *wrk,
    const struct stevedore *stv, const struct objcore *oc,
    struct sbu *st, size_t sizea, enum sbu_alloc_what_e what)
{
	struct buddy_ptr_extent sbu_mem, mem;
	size_t chunksize, rnd, size = sizea;
	struct buddy_reqs *reqs;
	const char *embed_diag;
	struct stvbu *stvbu;
	int8_t cram = 0;
	unsigned nalloc;
	buddy_t *buddy;
#ifdef FREEPAGE_WHEN
	double when;

	if (oc == NULL)
		when = VTIM_real();
	else {
		CHECK_OBJ(oc, OBJCORE_MAGIC);
		when = oc->t_origin + oc->ttl;
	}
#else
	(void)oc;
#endif

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	buddy = &stvbu->buddy;
	CHECK_OBJ(buddy, BUDDY_MAGIC);
	chunksize = (size_t)1 << stvbu->tune.chunk_exponent;

	nalloc = (st == NULL) ? 2 : 1;

	if (size == 0)
		size = chunksize;
	else if (what == SBU_BODY && size >= chunksize)
		size = chunksize;
	else if (what == SBU_BODY && stvbu->tune.cram != 0) {
		cram = stvbu->tune.cram;
		rnd = (size_t)1 << log2down(size);
		if (rnd != size)
			cram += cram > 0 ? -1 : 1;
		size = rnd;
	}
	else if (what == SBU_OBJ) {
		AZ(st);
		size = (size_t)1 << log2up(size + sizeof *st);
		nalloc = 1;
	}
	else if (st == NULL && size + sizeof *st <= ((size_t)1 << log2up(size))) {
		// embed unless we exceed a page
		size += sizeof *st;
		nalloc = 1;
	}


	reqs = BUDDY_REQS_STK(buddy, 2);
	BUDDY_REQS_PRI(reqs, 1);
	AN(BUDDYF(req_extent)(reqs, size, cram WHEN));
	if (nalloc == 2)
		AN(BUDDYF(req_extent)(reqs, sizeof *st, 0 WHEN));
	if (BUDDYF(alloc_wait)(reqs) != nalloc) {
		BUDDYF(alloc_async_done)(reqs);
		stvbu->stats->c_fail++;
		return (NULL);
	}

	mem = buddy_get_ptr_extent(reqs, 0);
	sbu_mem = buddy_ptr_extent_nil;

	if (nalloc == 2) {
		sbu_mem = buddy_get_ptr_extent(reqs, 1);
		AZ(st);
		st = sbu_mem.ptr;
		AN(st);
		INIT_OBJ(st, SBU_MAGIC);
		embed_diag = "sbu_mem";
	}
	else if (st != NULL) {
		// embedded st passed in
		assert(nalloc == 1);
		INIT_OBJ(st, SBU_MAGIC);
		embed_diag = "obj";
	}
	else {
		// embed st in mem
		assert(nalloc == 1);
		st = (struct sbu *)((uintptr_t)mem.ptr + mem.size - sizeof *st);
		AN(st);
		INIT_OBJ(st, SBU_MAGIC);
		assert(sizeof *st <= UINT_MAX);
		st->embed = (unsigned)sizeof *st;
		embed_diag = "embed";
	}

	st->sbu_mem = sbu_mem;
	st->mem = mem;

	assert(SBU_SIZE(st) <= SSIZE_MAX);
	AN(SBU_PTR(st));

	BUDDYF(alloc_async_done)(reqs);

	stvbu->stats->c_req++;
	stvbu->stats->c_bytes += sbu_mem.size + mem.size;
	stvbu->stats->g_alloc++;

	if (stvbu->tune.debug_flags & 0x01) {
		VSLb(wrk->vsl, SLT_Debug, "sbu_alloc(%zu, %s) mem(%zu, %d)=%zu st=%s",
		    sizea, sbu_types[what], size, cram, SBU_SIZE(st), embed_diag);
	}

	return (st);
}

static size_t
sbu_ret(struct buddy_returns *rets, struct sbu **stp)
{
	struct buddy_ptr_extent	sbu_mem;
	struct buddy_ptr_extent	mem;
	struct sbu *st;
	size_t sz;

	TAKE_OBJ_NOTNULL(st, stp, SBU_MAGIC);
	sbu_mem = st->sbu_mem;
	mem = st->mem;
	sz = st->sbu_mem.size + st->mem.size;

	if (sbu_mem.ptr != NULL)
		AN(BUDDYF(return_ptr_extent)(rets, &sbu_mem));
	AN(BUDDYF(return_ptr_extent)(rets, &mem));

	return (sz);
}

// SML API should not leave out stv argument to avoid storing another
// pointer. So we don't use it.

static void
sbu_free(const struct stevedore *stv, struct sbu *st)
{
	struct stvbu *stvbu;
	struct buddy_returns *rets;
	size_t sz;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	CHECK_OBJ_NOTNULL(st, SBU_MAGIC);

	rets = BUDDY_RETURNS_STK(&stvbu->buddy, 2);
	sz = sbu_ret(rets, &st);
	BUDDYF(return)(rets);

	stvbu->stats->c_freed += sz;
	stvbu->stats->g_alloc--;

	sbu_upd_space(stv->priv);
}

/* ----------------------------------------------------------------------
 * obj_methods
 */
static void v_matchproto_(objslim_f)
sbu_slim(struct worker *, struct objcore *);

static void v_matchproto_(objfree_f)
sbu_objfree(struct worker *wrk, struct objcore *oc)
{
	struct obu *o;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	sbu_slim(wrk, oc);
	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);
	o->magic = 0;

	if (oc->boc == NULL && oc->stobj->stevedore->lru != NULL)
		LRU_Remove(oc);

	sbu_free(oc->stobj->stevedore, o->objstore);

	memset(oc->stobj, 0, sizeof oc->stobj);

	wrk->stats->n_object--;
}

// upstream ref 33ffbbc5b04849428621fc29b69d826dae3a1847
static int v_matchproto_(objiterator_f)
sbu_iterator(struct worker *wrk, struct objcore *oc,
    void *priv, objiterate_f *func, int final)
{
	struct boc *boc;
	enum boc_state_e state;
	//
	struct obu *obj;
	struct sbu *st, *checkpoint = NULL;
	//
	const struct stevedore *stv;
	size_t checkpoint_len = 0;
	size_t len = 0;
	int ret = 0, ret2;
	size_t ol, nl, sl, stl;
	void *p;
	size_t l;
	unsigned u;

	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(obj, oc->stobj->priv, OBU_MAGIC);

	boc = HSH_RefBoc(oc);

	if (boc == NULL) {
		VTAILQ_FOREACH_REVERSE_SAFE(
		    st, &obj->list, sbu_head, list, checkpoint) {

			u = 0;
			if (VTAILQ_PREV(st, sbu_head, list) == NULL)
				u |= OBJ_ITER_END;
			if (final)
				u |= OBJ_ITER_FLUSH;
			if (ret == 0 && st->len > 0)
				ret = func(priv, u, SBU_PTR(st), st->len);
			if (final) {
				VTAILQ_REMOVE(&obj->list, st, list);
				sbu_free(stv, st);
			} else if (ret)
				break;
		}
		return (ret);
	}

	p = NULL;
	l = 0;

	u = 0;
	if (boc->BOC_FETCHED_SO_FAR == 0) {
		ret = func(priv, OBJ_ITER_FLUSH, NULL, (ssize_t)0);
		if (ret)
			return (ret);
	}
	while (1) {
		ol = len;
#if VRT_MAJOR_VERSION < 19U || (VRT_MAJOR_VERSION == 19 && VRT_MINOR_VERSION < 1)
		nl = ObjWaitExtend(wrk, oc, ol);
		if (boc->state == BOS_FAILED) {
			ret = -1;
			break;
		}
		if (nl == ol) {
			if (boc->state == BOS_FINISHED)
				break;
			continue;
		}
		state = boc->state;
#else
		nl = ObjWaitExtend(wrk, oc, ol, &state);
		if (state == BOS_FAILED) {
			ret = -1;
			break;
		}
		if (nl == ol) {
			assert (state == BOS_FINISHED);
			break;
		}
		assert(nl > ol);
#endif
		Lck_Lock(&boc->mtx);
		assert(VTAILQ_FIRST(&obj->list));
		if (checkpoint == NULL) {
			st = VTAILQ_LAST(&obj->list, sbu_head);
			sl = 0;
		} else {
			st = checkpoint;
			sl = checkpoint_len;
			assert(ol >= checkpoint_len);
			ol -= checkpoint_len;
		}
		while (st != NULL) {
			assert(st->len >= 0);
			stl = (size_t)st->len;
			if (stl > ol) {
				p = SBU_PTR(st) + ol;
				l = stl - ol;
				len += l;
				break;
			}
			assert(ol >= stl);
			assert(nl > stl);
			ol -= stl;
			nl -= stl;
			sl += stl;
			st = VTAILQ_PREV(st, sbu_head, list);
			if (final && checkpoint != NULL) {
				if (checkpoint == boc->stevedore_priv)
					boc->stevedore_priv = NULL;
				else
					VTAILQ_REMOVE(&obj->list, checkpoint, list);
				sbu_free(stv, checkpoint);
			}
			checkpoint = st;
			checkpoint_len = sl;
		}
		CHECK_OBJ_NOTNULL(obj, OBU_MAGIC);
		CHECK_OBJ_NOTNULL(st, SBU_MAGIC);
		st = VTAILQ_PREV(st, sbu_head, list);
		if (st != NULL && st->len == 0)
			st = NULL;
		Lck_Unlock(&boc->mtx);
		assert(l > 0 || state == BOS_FINISHED);
		u = 0;
		if (st == NULL || final)
			u |= OBJ_ITER_FLUSH;
		if (st == NULL && state == BOS_FINISHED)
			u |= OBJ_ITER_END;
		ret = func(priv, u, p, (ssize_t)l);
		if (ret)
			break;
	}
	HSH_DerefBoc(wrk, oc);
	if ((u & OBJ_ITER_END) == 0) {
		ret2 = func(priv, OBJ_ITER_END, NULL, (size_t)0);
		if (ret == 0)
			ret = ret2;
	}
	return (ret);
}

static int v_matchproto_(objgetspace_f)
sbu_getspace(struct worker *wrk, struct objcore *oc, ssize_t *sz,
    uint8_t **ptr)
{
	struct obu *o;
	struct sbu *st;
	size_t stl;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	AN(sz);
	AN(ptr);
	assert(*sz >= 0);

	// XXX VC >7.4 remove workaround
	// https://github.com/varnishcache/varnish-cache/pull/4056
	if (*sz == cache_param->fetch_chunksize)
		*sz = 0;

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);
	CHECK_OBJ_NOTNULL(oc->boc, BOC_MAGIC);

	st = VTAILQ_FIRST(&o->list);
	if (st != NULL) {
		assert(st->len >= 0);
		stl = (size_t)st->len;

		if (stl < SBU_SIZE(st)) {
			*sz = (ssize_t)(SBU_SIZE(st) - stl);
			*ptr = SBU_PTR(st) + stl;
			assert (*sz > 0);
			return (1);
		}
	}

	st = sbu_alloc(wrk, oc->stobj->stevedore, oc, sbu_embed(o),
	    (size_t)*sz, SBU_BODY);
	if (st == NULL)
		return (0);

	CHECK_OBJ_NOTNULL(oc->boc, BOC_MAGIC);
	Lck_Lock(&oc->boc->mtx);
	VTAILQ_INSERT_HEAD(&o->list, st, list);
	Lck_Unlock(&oc->boc->mtx);

	assert(st->len >= 0);
	stl = (size_t)st->len;
	*sz = (ssize_t)(SBU_SIZE(st) - stl);
	assert (*sz > 0);
	*ptr = SBU_PTR(st) + stl;
	return (1);
}

static void v_matchproto_(objextend_f)
sbu_memend(struct worker *wrk, struct objcore *oc, ssize_t l)
{
	struct obu *o;
	struct sbu *st;
	size_t stl, ll;

	(void) wrk;
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	ll = (size_t)l;
	assert((ssize_t)ll == l);

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);
	st = VTAILQ_FIRST(&o->list);
	CHECK_OBJ_NOTNULL(st, SBU_MAGIC);
	assert(st->len >= 0);
	stl = (size_t)st->len;
	assert(stl + ll <= SBU_SIZE(st));
	st->len += l;
}

static void v_matchproto_(objtrimstore_f)
sbu_trimstore(struct worker *wrk, struct objcore *oc)
{
	const struct stevedore *stv;
	struct stvbu *stvbu;
	struct sbu *st, *new;
	struct obu *o;
	buddy_t *buddy;
	size_t stl, osz;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	CHECK_OBJ_NOTNULL(oc->boc, BOC_MAGIC);

	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	CAST_BUDDY(buddy, stv->priv);

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);
	st = VTAILQ_FIRST(&o->list);

	if (st == NULL)
		return;

	AZ(oc->boc->stevedore_priv);

	if (st->len == 0) {
		Lck_Lock(&oc->boc->mtx);
		VTAILQ_REMOVE(&o->list, st, list);
		Lck_Unlock(&oc->boc->mtx);
		oc->boc->stevedore_priv = st;
		return;
	}

	assert(st->len >= 0);
	stl = (size_t)st->len;
	osz = SBU_SIZE(st);

	new = NULL;
	if (log2up(stl) < log2up(osz)) {
		/* reallocate */
		new = sbu_alloc(wrk, stv, oc, sbu_embed(o), stl, SBU_FIXED);
	}
	if (new == NULL) {
		BUDDYF(trim1_ptr_extent)(buddy, &st->mem, stl);
		assert(osz >= SBU_SIZE(st));
		stvbu->stats->c_trim++;
		stvbu->stats->c_trimmed += (osz - SBU_SIZE(st));
	}
	sbu_upd_space(stv->priv);
	if (new == NULL)
		return;

	new->len = (ssize_t)stl;
	assert(SBU_SIZE(new) >= stl);
	memcpy(SBU_PTR(new), SBU_PTR(st), stl);
	Lck_Lock(&oc->boc->mtx);
	assert(st == VTAILQ_FIRST(&o->list));
	VTAILQ_REMOVE(&o->list, st, list);
	VTAILQ_INSERT_HEAD(&o->list, new, list);
	Lck_Unlock(&oc->boc->mtx);

	oc->boc->stevedore_priv = st;
}

static void v_matchproto_(objbocdone_f)
sbu_bocdone(struct worker *wrk, struct objcore *oc, struct boc *boc)
{
	const struct stevedore *stv;
	struct sbu *f;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);
	CHECK_OBJ_NOTNULL(boc, BOC_MAGIC);
	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);

	if (stv->lru != NULL) {
		if (isnan(wrk->lastused))
			wrk->lastused = VTIM_real();
		LRU_Add(oc, wrk->lastused);	// approx timestamp is OK
	}

	if (boc->stevedore_priv == NULL)
		return;
	TAKE_OBJ_NOTNULL(f, &boc->stevedore_priv, SBU_MAGIC);
	sbu_free(stv, f);
}

static void v_matchproto_(objslim_f)
sbu_slim(struct worker *wrk, struct objcore *oc)
{
	const struct stevedore *stv;
	struct stvbu *stvbu;
	struct buddy_returns *rets;
	struct obu *o;
	struct sbu *st;
	unsigned n = 0;
	size_t sz = 0;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);

	stv = oc->stobj->stevedore;
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);

	rets = BUDDY_RETURNS_STK(&stvbu->buddy, BUDDY_RETURNS_MAX);

#define OBJ_AUXATTR(U, l)					\
	do {							\
		if (o->aa_##l != NULL) {			\
			n++;					\
			sz += sbu_ret(rets, &o->aa_##l);	\
			o->aa_##l = NULL;			\
		}						\
	} while (0);
#include "tbl/obj_attr.h"

	while ((st = VTAILQ_FIRST(&o->list)) != NULL) {
		CHECK_OBJ_NOTNULL(st, SBU_MAGIC);
		VTAILQ_REMOVE(&o->list, st, list);
		n++;
		sz += sbu_ret(rets, &st);
	}

	BUDDYF(return)(rets);

	stvbu->stats->c_freed += sz;
	stvbu->stats->g_alloc -= n;

	sbu_upd_space(stv->priv);
}

static const void * v_matchproto_(objgetattr_f)
sbu_getattr(struct worker *wrk, struct objcore *oc, enum obj_attr attr,
   ssize_t *len)
{
	struct obu *o;
	ssize_t dummy;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	if (len == NULL)
		len = &dummy;

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);

	switch (attr) {
		/* Fixed size attributes */
#define OBJ_FIXATTR(U, l, s)						\
	case OA_##U:							\
		*len = sizeof o->fa_##l;				\
		return (o->fa_##l);
#include "tbl/obj_attr.h"

		/* Variable size attributes */
#define OBJ_VARATTR(U, l)						\
	case OA_##U:							\
		if (o->va_##l == NULL)					\
			return (NULL);					\
		*len = o->va_##l##_len;					\
		return (o->va_##l);
#include "tbl/obj_attr.h"

		/* Auxiliary attributes */
#define OBJ_AUXATTR(U, l)						\
	case OA_##U:							\
		if (o->aa_##l == NULL)					\
			return (NULL);					\
		CHECK_OBJ_NOTNULL(o->aa_##l, SBU_MAGIC);		\
		*len = o->aa_##l->len;					\
		return (SBU_PTR(o->aa_##l));
#include "tbl/obj_attr.h"

	default:
		break;
	}
	WRONG("Unsupported OBJ_ATTR");
}

static void * v_matchproto_(objsetattr_f)
sbu_setattr(struct worker *wrk, struct objcore *oc, enum obj_attr attr,
    ssize_t len, const void *ptr)
{
	struct obu *o;
	void *retval = NULL;
	struct sbu *st;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);
	st = o->objstore;

	switch (attr) {
		/* Fixed size attributes */
#define OBJ_FIXATTR(U, l, s)						\
	case OA_##U:							\
		assert(len == sizeof o->fa_##l);			\
		retval = o->fa_##l;					\
		break;
#include "tbl/obj_attr.h"

		/* Variable size attributes */
#define OBJ_VARATTR(U, l)						\
	case OA_##U:							\
		if (o->va_##l##_len > 0) {				\
			AN(o->va_##l);					\
			assert(len == o->va_##l##_len);			\
			retval = o->va_##l;				\
		} else if (len > 0) {					\
			assert(len <= UINT_MAX);			\
			assert(st->len + len <= SBU_SIZE(st));		\
			o->va_##l = SBU_PTR(st) + st->len;			\
			st->len += len;					\
			o->va_##l##_len = len;				\
			retval = o->va_##l;				\
		}							\
		break;
#include "tbl/obj_attr.h"

		/* Auxiliary attributes */
#define OBJ_AUXATTR(U, l)						\
	case OA_##U:							\
		if (o->aa_##l != NULL) {				\
			CHECK_OBJ_NOTNULL(o->aa_##l, SBU_MAGIC);	\
			assert(len == o->aa_##l->len);			\
			retval = SBU_PTR(o->aa_##l);			\
			break;						\
		}							\
		if (len == 0)						\
			break;						\
		o->aa_##l = sbu_alloc(wrk, oc->stobj->stevedore,	\
		    oc, sbu_embed(o), len, SBU_FIXED);			\
		if (o->aa_##l == NULL)					\
			break;						\
		CHECK_OBJ_NOTNULL(o->aa_##l, SBU_MAGIC);		\
		assert(len <= SBU_SIZE(o->aa_##l));			\
		o->aa_##l->len = len;					\
		retval = SBU_PTR(o->aa_##l);				\
		break;
#include "tbl/obj_attr.h"

	default:
		WRONG("Unsupported OBJ_ATTR");
		break;
	}

	if (retval != NULL && ptr != NULL) {
		assert(len > 0);
		memcpy(retval, ptr, (size_t)len);
	}
	return (retval);
}

static const struct obj_methods sbu_methods = {
	.objfree	= sbu_objfree,
	.objiterator	= sbu_iterator,
	.objgetspace	= sbu_getspace,
	.objextend	= sbu_memend,
	.objgetattr	= sbu_getattr,
	.objsetattr	= sbu_setattr,
	// optional
	.objtrimstore	= sbu_trimstore,
	.objbocdone	= sbu_bocdone,
	.objslim	= sbu_slim,
	.objtouch	= LRU_Touch,
	.objsetstate	= NULL // TODO?
};

/* Error messages ----------------------------------------------------
 *
 * for argv configuration, we ultimately call ARGV_ERR, which calls exit()
 *
 * to also support vmod configuration (addition of stevedores from vcl),
 * we can not use that, so have a static buffer
 */

static char errbuf[1024] = "";
#define STVERR(...) do {					\
		bprintf(errbuf, __VA_ARGS__);			\
		return (errbuf);				\
} while (0)

/* ----------------------------------------------------------------------
 * stevedore
 */

// XXX compat with mgmt use
#ifndef ARGV_ERR
#include <stdio.h>
#define ARGV_ERR(...)							\
	do {								\
		fprintf(stderr, "Error: " __VA_ARGS__);			\
		return;						\
	} while(0)
#endif

static void *
sbu_mapper(size_t *sz, void *priv)
{
	struct stevedore *stv;

	CAST_OBJ_NOTNULL(stv, priv, STEVEDORE_MAGIC);

	fprintf(stderr, "buddy: metadata (bitmap) memory: %zu bytes\n",
	    *sz);
	return (VSMW_Allocf(heritage.proc_vsmw, NULL,
	    VSM_CLASS_SLASH, *sz, "buddy.%s", stv->vclname));
}

static void
sbu_umapper(void **ptr, size_t sz, void **priv)
{
	(void) sz;
	(void) priv;

	VSMW_Free(heritage.proc_vsmw, ptr);
}

static const char *
sbu_init(struct stevedore *parent, size_t *sz, size_t *min)
{
	unsigned min_bits;
	struct stvbu *stvbu;
	struct stvbu_tune tune;
	const char *err;

	sbu_bits = log2up(sizeof(struct sbu));
	if (sbu_bits < MIN_BUDDY_BITS)
		sbu_bits = MIN_BUDDY_BITS;

	min_bits = log2up(*min);
	if (min_bits < sbu_bits)
		min_bits = sbu_bits;
	*min = (size_t)1 << min_bits;

	*sz = rdown_min(*sz, min_bits);

	err = stvbu_tune_init(&tune, *sz);
	if (err != NULL) {
		STVERR("buddy tune error: %s", err);
	}

	ALLOC_OBJ(stvbu, STVBU_MAGIC);
	XXXAN(stvbu);

	stvbu->min_bits = min_bits;
	stvbu->tune = tune;
	stvbu->tune.minpage = *min;
	parent->priv = stvbu;
	return (NULL);
}

#define szarg(n, name)							\
do {									\
	err = VNUM_2bytes(av[n], &name, (uintmax_t)0);			\
	if (err != NULL)						\
		ARGV_ERR("(-sbuddy) %s \"%s\": %s\n", #name, av[n], err); \
} while(0)

static void
sbu_cfg(struct stevedore *parent, int ac, char * const *av)
{
	size_t sz = 0, minpage = 64;
	const char *err;

	if (ac < 1 || ac > 2 || *av[0] == '\0')
		ARGV_ERR("(-sbuddy) need 1 or 2 arguments: size[,minpage]\n");
	szarg(0, sz);
	if (ac == 2 && *av[1] != '\0')
		szarg(1, minpage);

	err = sbu_init(parent, &sz, &minpage);
	if (err)
		ARGV_ERR("(-sbuddy) %s\n", err);
}

/* get a copy of the current tuning settings */
void
sbu_tune_get(const struct stevedore *stv, struct stvbu_tune *tune)
{
	const struct stvbu *stvbu;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);

	AN(tune);
	*tune = stvbu->tune;
}

/* apply tuning settings and return NULL or
 * return error string if validation failed
 */
const char *
sbu_tune_apply(const struct stevedore *stv, const struct stvbu_tune *tuna)
{
	struct stvbu *stvbu;
	struct stvbu_tune tune;
	const char *err;
	int chg;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);

	tune = *tuna;
	err = stvbu_tune_check(&tune);
	if (err != NULL)
		return (err);

	chg  = (stvbu->tune.reserve_chunks != tune.reserve_chunks) ||
	    (stvbu->tune.chunk_exponent != tune.chunk_exponent);

	stvbu->tune = tune;

	if (chg)
		BUDDYF(wait_kick)(&stvbu->buddy);

	return (NULL);
}

static void
reserve_free(buddy_t *buddy, struct buddy_ptr_page *r, unsigned n)
{
	struct buddy_returns *rets =
	    BUDDY_RETURNS_STK(buddy, BUDDY_RETURNS_MAX);

	if (n == 0)
		return;

	AN(r);

	while (n--)
		AN(BUDDYF(return_ptr_page)(rets, r++));

	BUDDYF(return)(rets);
}

static void
reserve_req(struct buddy_reqs *reqs, unsigned n, unsigned bits)
{
#ifdef FREEPAGE_WHEN
	const double when = 0;
#endif

	if (n > BUDDY_REQS_MAX)
		n = BUDDY_REQS_MAX;
	while (n--)
		AN(BUDDYF(req_page)(reqs, bits, 0 WHEN));
}

static void
reserve_fill(struct buddy_ptr_page *r, struct buddy_reqs *reqs, uint8_t n)
{
	uint8_t u;

	for (u = 0; u < n; u++) {
		AZ(r->ptr);
		*r++ = buddy_get_ptr_page(reqs, u);
	}
}

static void *
sbu_nukethread(struct worker *wrk, void *arg)
{
	struct buddy_reqs *reqs;
	const struct stevedore *stv;
	struct stvbu *stvbu;
	buddy_t *buddy;
	struct vsl_log vsl;
	struct buddy_ptr_page *r = NULL;
	unsigned l, i, filled = 0, nr = 0;
	uint8_t n;
	size_t sz;

	CAST_OBJ_NOTNULL(stv, arg, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	buddy = &stvbu->buddy;
	CHECK_OBJ(buddy, BUDDY_MAGIC);

	reqs = BUDDY_REQS_STK(buddy, BUDDY_REQS_MAX);
	BUDDY_REQS_PRI(reqs, 0);

	AZ(wrk->vsl);
	wrk->vsl = &vsl;
	VSL_Setup(wrk->vsl, NULL, (size_t)0);

	while (!stvbu->shutdown) {
		// reserve pointers
		while (nr != stvbu->tune.reserve_chunks) {
			if (nr != 0)
				AN(r);
			reserve_free(buddy, r, filled);
			filled = 0;
			nr = stvbu->tune.reserve_chunks;
			VSLb(wrk->vsl, SLT_Debug,
			    "sbu reserve config nr=%u chunk_exponent=%u",
			    nr, stvbu->tune.chunk_exponent);
			if (nr == 0)
				break;

			sz = nr * sizeof *r;
			r = realloc(r, sz);
			AN(r);
			memset(r, 0, sz);
			break;
		};

		// fill reserve
		while (filled < nr && buddy->waiting == 0) {
			AN(r);
			reserve_req(reqs, nr - filled,
			    stvbu->tune.chunk_exponent);
			(void) BUDDYF(alloc_async)(reqs);

			wrk->strangelove = INT_MAX;
			while (buddy->waiting) {
				if (LRU_NukeOne(wrk, stv->lru))
					continue;
				wrk->strangelove++;
				break;
			}
			stvbu->stats->c_lru_nuke_fill_reserve +=
				(unsigned)(INT_MAX - wrk->strangelove);

			n = BUDDYF(alloc_async_ready)(reqs);
			if (n == reqs->n) {
				assert(filled + n <= nr);
				reserve_fill(r + filled, reqs, n);
				filled += n;
			}

			BUDDYF(alloc_async_done)(reqs);
		}
#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sbu reserve fill: %u", filled);
#endif

		VSL_Flush(wrk->vsl, 0);
		Pool_Sumstat(wrk);
		BUDDYF(wait_needspace)(buddy);
		stvbu->stats->c_lru_wakeups++;

		// drain reserve
		while (filled > 0 && buddy->waiting > 0) {
			i = --filled;

			AN(r);
			AN(r[i].ptr);
			stvbu->stats->c_lru_reserve_used_bytes +=
				(size_t)1 << r[i].bits;
			BUDDYF(return1_ptr_page)(buddy, &r[i]);
		}

#ifdef LRU_NOISE
		if (nr)
			VSLb(wrk->vsl, SLT_Debug,
			    "sbu reserve use : %u left", filled);
#endif

		wrk->strangelove = INT_MAX;
		l = buddy->waiting;
		while (buddy->waiting) {
			if (LRU_NukeOne(wrk, stv->lru))
				continue;
			assert(wrk->strangelove < INT_MAX);
			wrk->strangelove++;
			// dont fail if making progress
			if (buddy->waiting < l) {
				l = buddy->waiting;
				continue;
			}
#ifdef LRU_NOISE
			VSLb(wrk->vsl, SLT_Debug,
			    "sbu %u waiting, failing one", l);
#endif
			BUDDYF(wait_fail)(buddy);
			break;
		}
		stvbu->stats->c_lru_nuke_reserve_drained +=
			(unsigned)(INT_MAX - wrk->strangelove);

#ifdef LRU_NOISE
		VSLb(wrk->vsl, SLT_Debug, "sbu nuked %u",
		    INT_MAX - wrk->strangelove);
#endif
	}
	reserve_free(buddy, r, filled);
	free(r);
	return (NULL);
}

static void v_matchproto_(storage_open_f)
sbu_open(struct stevedore *stv)
{
	struct stvbu *stvbu;

	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);

	BUDDYF(init)(&stvbu->buddy, stvbu->min_bits,
	    stvbu->tune.sz, BUDDYF(mmap), NULL,
	    sbu_mapper, stv);
	stv->lru = LRU_Alloc();
	stvbu->stats = VSC_buddy_New(NULL, &stvbu->vsc_seg, stv->ident);
	WRK_BgThread(&stvbu->nukethread, "sbu-nuker", sbu_nukethread, stv);
}

static struct obu *
sbu_mkobj(const struct stevedore *stv, struct objcore *oc, void *ptr)
{
	struct obu *o;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	AN(stv->methods);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	assert(PAOK(ptr));

	o = ptr;
	INIT_OBJ(o, OBU_MAGIC);

	VTAILQ_INIT(&o->list);

	oc->stobj->stevedore = stv;
	oc->stobj->priv = o;
	oc->stobj->priv2 = 0;
	return (o);
}

static int v_matchproto_(storage_allocobj_f)
sbu_allocobj(struct worker *wrk, const struct stevedore *stv,
    struct objcore *oc, unsigned wsl)
{
	struct obu *o;
	struct sbu *st;
	size_t ltot, av;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CHECK_OBJ_NOTNULL(oc, OBJCORE_MAGIC);

	ltot = sizeof(struct obu) + PRNDUP(wsl);

	st = sbu_alloc(wrk, stv, oc, NULL, ltot, SBU_OBJ);
	if (st == NULL)
		return (0);

	// XXX should we do this with every allocation?
	sbu_upd_space(stv->priv);

	CHECK_OBJ_NOTNULL(st, SBU_MAGIC);
	o = sbu_mkobj(stv, oc, SBU_PTR(st));
	CHECK_OBJ_NOTNULL(o, OBU_MAGIC);
	st->len = sizeof(*o);
	o->objstore = st;
	av = SBU_SIZE(st);
	assert(av >= ltot);
	av -= ltot;
	assert(av <= UINT_MAX);
	o->avail = (unsigned)av;
	return (1);
}

static void
sbu_panic_st(struct vsb *vsb, const char *hd, const struct sbu *st)
{
	(void)VSB_printf(vsb, "%s = %p {ptr=%p, len=%zu, space=%zu},\n",
	    hd, st, (void *)SBU_PTR(st), st->len, SBU_SIZE(st));
}

static void
sbu_panic(struct vsb *vsb, const struct objcore *oc)
{
	struct obu *o;
	struct sbu *st;

	(void)VSB_printf(vsb, "Buddy = %p,\n", oc->stobj->priv);
	if (oc->stobj->priv == NULL)
		return;
	CAST_OBJ_NOTNULL(o, oc->stobj->priv, OBU_MAGIC);

	sbu_panic_st(vsb, "Obj", o->objstore);

#define OBJ_FIXATTR(U, l, sz) \
	VSB_printf(vsb, "%s = ", #U); \
	VSB_quote(vsb, (const void*)o->fa_##l, sz, VSB_QUOTE_HEX); \
	VSB_printf(vsb, ",\n");

#define OBJ_VARATTR(U, l) \
	VSB_printf(vsb, "%s = {len=%u, ptr=%p},\n", \
	    #U, o->va_##l##_len, o->va_##l);

#define OBJ_AUXATTR(U, l)						\
	do {								\
		if (o->aa_##l != NULL) sbu_panic_st(vsb, #U, o->aa_##l);\
	} while(0);

#include "tbl/obj_attr.h"

	VTAILQ_FOREACH(st, &o->list, list) {
		sbu_panic_st(vsb, "Body", st);
	}
}

static void
sbu_upd_space(struct stvbu *stvbu)
{
	buddy_t *buddy;
	size_t space;

	CHECK_OBJ_NOTNULL(stvbu, STVBU_MAGIC);
	buddy = &stvbu->buddy;

	space = BUDDYF(space)(buddy, 0);
	stvbu->stats->g_space = space;
	stvbu->stats->g_bytes = BUDDYF(size)(buddy) - space;
}

static VCL_BYTES v_matchproto_(stv_var_free_space)
sbu_free_space(const struct stevedore *stv)
{
	struct stvbu *stvbu;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	sbu_upd_space(stvbu);
	return ((VCL_BYTES)stvbu->stats->g_space);
}

static VCL_BYTES v_matchproto_(stv_var_used_space)
sbu_used_space(const struct stevedore *stv)
{
	struct stvbu *stvbu;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	CAST_OBJ_NOTNULL(stvbu, stv->priv, STVBU_MAGIC);
	sbu_upd_space(stvbu);
	return ((VCL_BYTES)stvbu->stats->g_bytes);
}

static void * v_matchproto_(storage_allocbuf_t)
sbu_allocbuf(struct worker *wrk, const struct stevedore *stv, size_t size,
    uintptr_t *ppriv)
{
	struct sbu *st;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	AN(ppriv);

	if (size > UINT_MAX)
		return (NULL);
	st = sbu_alloc(wrk, stv, NULL, NULL, size, SBU_FIXED);
	if (st == NULL)
		return (NULL);
	assert(SBU_SIZE(st) >= size);
	st->len = (ssize_t)size;
	*ppriv = (uintptr_t)st;
	return (SBU_PTR(st));
}

static void v_matchproto_(storage_freebuf_t)
sbu_freebuf(struct worker *wrk, const struct stevedore *stv, uintptr_t priv)
{
	struct sbu *st;

	CHECK_OBJ_NOTNULL(wrk, WORKER_MAGIC);
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);

	CAST_OBJ_NOTNULL(st, (void *)priv, SBU_MAGIC);
	sbu_free(stv, st);
}

/*lint -e{785}*/
static const struct stevedore sbu_stevedore = {
	.magic		=	STEVEDORE_MAGIC,
	.name		=	"buddy",
	.init		=	sbu_cfg,
	.open		=	sbu_open,
	.allocobj	=	sbu_allocobj,
	.panic		=	sbu_panic,
	.methods	=	&sbu_methods,
	.var_free_space =	sbu_free_space,
	.var_used_space =	sbu_used_space,
	.allocbuf	=	sbu_allocbuf,
	.freebuf	=	sbu_freebuf,
};

static void __attribute__((constructor))
buddy_stv_register(void)
{
	STV_Register(&sbu_stevedore, NULL);
}

// Basically STV_Config
struct stevedore *
sbu_new(const char *name, size_t *sz, size_t *min)
{
	struct stevedore *stv;
	char *id;

	stv = malloc(sizeof *stv);
	AN(stv);

	*stv = sbu_stevedore;
	AN(stv->name);

	id = strdup(name);
	AN(id);
	stv->ident = id;
	stv->vclname = id;

	if (sbu_init(stv, sz, min)) {
		free(id);
		free(stv);
		INCOMPL();
		//return (NULL);
	}

	AN(stv->open);
	stv->open(stv);

	AN(stv->allocobj);
	AN(stv->methods);

	return (stv);
}

int
sbu_is(VCL_STEVEDORE stv)
{
	struct stvbu *stvbu;

	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);
	stvbu = stv->priv;
	return (stvbu != NULL && stvbu->magic == STVBU_MAGIC);
}

void
sbu_fini(struct stevedore **stvp)
{
	struct stevedore *stv;
	struct stvbu *stvbu;
	buddy_t *buddy;

	TAKE_OBJ_NOTNULL(stv, stvp, STEVEDORE_MAGIC);
	CAST_BUDDY(buddy, stv->priv);

	if (BUDDYF(size)(buddy) != BUDDYF(space)(buddy, 1))
		return;

	TAKE_OBJ_NOTNULL(stvbu, &stv->priv, STVBU_MAGIC);
	stvbu->shutdown = 1;
	// if any free function is called after this point,
	// it will assert
	buddy->waiting = UINT_MAX;
	BUDDYF(wait_kick)(buddy);
	AZ(pthread_join(stvbu->nukethread, NULL));

	// stv->open
	LRU_Free(&stv->lru);
	BUDDYF(fini)(&buddy, BUDDYF(unmap), NULL, sbu_umapper, NULL);
	VSC_buddy_Destroy(&stvbu->vsc_seg);
	FREE_OBJ(stvbu);
}

void
sbu_as_transient(struct stevedore *stv)
{
	CHECK_OBJ_NOTNULL(stv, STEVEDORE_MAGIC);

	stv_transient = stv;
}
