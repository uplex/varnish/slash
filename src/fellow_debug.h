/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifdef DEBUG

#define D_LOGS_ITER		1
#define D_LOGS_ITER_BLOCK	(1<<1)
#define D_LOG_IO		(1<<2)
#define D_LOG_FLUSH		(1<<3)
#define D_LOG_ALLOC		(1<<4)

#ifndef FDBGL
#if 1
#define FDBGL 0x0
#else
#define FDBGL ( 0							\
	| D_LOGS_ITER							\
	| D_LOG_IO							\
	)
#endif
#endif

#define FDBG(l, x, ...)						\
	if (l & FDBGL)							\
		fprintf(stderr, #l " " x "\n", __VA_ARGS__)
#else
#define FDBG(l, x, ...) (void)0
#endif
