/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#ifdef HAVE_LINUX_FS_H
#include <linux/fs.h>
#include <sys/ioctl.h>
#endif
#ifdef HAVE_FALLOCATE
//lint -e(537)
#include <fcntl.h>
#endif

/*
 * list of regions to free:
 *
 * while we iterate on-disk logs, we can not free extra log blocks immediately
 * and while we flush and rewrite logs, we can not free data blocks immediately,
 * they represent our current log and data which is still referenced by the
 * previous log.
 *
 * We need to finish connecting the new log (LBUF_CAN_REF) before we can free
 * them so make sure that when the log is read, it finds the data which was
 * there before the free of the region.
 *
 */
struct regl {
	unsigned			magic;
#define REGL_MAGIC			0xe946b3c2
	uint16_t			n, space;
	struct buddy_ptr_page		alloc;
	VSTAILQ_ENTRY(regl)		list;
	struct buddy_off_extent	arr[];
};

BUDDY_POOL(rl_pool, 4);	// XXX 4 good?
BUDDY_POOL_GET_FUNC(rl_pool, static)

struct regionlist {
	unsigned			magic;
#define REGIONLIST_MAGIC		0xeb869815
	size_t				size;
	VSTAILQ_HEAD(,regl)		head;
	struct rl_pool			pool[1];
};

// usage: struct regl_stk(42)
#define regionlist_stk(size)					\
	{							\
		unsigned n, space;				\
		struct regionlist *regionlist;			\
		struct buddy_off_extent	arr[size];	\
	}

#define regionlist_stk_init(size, rl) {			\
		.n = 0,						\
		.space = (size),				\
		.regionlist = (rl),				\
		.arr = {{0}}					\
	}

//lint -e773
#define regionlist_stk_decl(name, size, rl)				\
	struct regionlist_stk(size) name = regionlist_stk_init(size, rl)
//lint +e773

#define regionlist_stk_flush(name)					\
do {									\
	if (name.n > 0) {						\
		regionlist_add(name.regionlist, name.arr, name.n);	\
	}								\
	name.n = 0;							\
} while(0)

#define regionlist_onlystk_add(name, o, s)				\
do {									\
	assert(name.n < name.space);					\
	name.arr[name.n].off = (o);					\
	name.arr[name.n].size = (s);					\
	name.n++;							\
} while(0)

#define regionlist_stk_add(name, o, s)					\
do {									\
	if (name.n == name.space) {					\
		regionlist_stk_flush(name);				\
	}								\
	regionlist_onlystk_add(name, o, s);				\
} while(0)

#ifdef DEBUG
static const int8_t regl_bits = 12;
#else
static const int8_t regl_bits = 16;	// XXX 64k reasonable?
#endif

static uint8_t regl_minbits;
static int8_t regl_bits_cram;

static int8_t
regl_cram(const uint8_t bits, int8_t cram)
{
	int8_t c;

	assert(bits > regl_minbits);
	// should we also support negative cram?
	assert(cram >= 0);
	assert(bits < INT8_MAX);
	c = (int8_t)(bits - regl_minbits);
	assert(c >= 0);
	if (cram > c)
		cram = c;

	return (cram);
}

static void __attribute__((constructor))
init_regl(void)
{
	unsigned b =
	    log2up(2 * sizeof(struct regl) + sizeof(struct regionlist));

	assert(b <= UINT8_MAX);
	regl_minbits = (typeof(regl_minbits))b;
	regl_bits_cram = regl_cram(regl_bits, regl_bits);
}

static void
regl_fill(struct buddy_reqs *reqs, const void *priv)
{
	unsigned u;

	CHECK_OBJ_NOTNULL(reqs, BUDDY_REQS_MAGIC);
	(void) priv;

	for (u = 0; u < reqs->space; u++)
		AN(buddy_req_page(reqs, regl_bits, regl_bits_cram));
}

static struct regl *
regl_init(const struct buddy_ptr_page alloc, size_t off)
{
	struct regl *r;
	size_t sz, b;

	sz = (size_t)1 << alloc.bits;
	sz -= off;
	assert(sz >= 2 * sizeof *r);

	r = (void *)((char *)alloc.ptr + off);
	memset(r, 0, sz);

	sz -= sizeof *r;
	b = sz / sizeof *r->arr;
	assert(b <= UINT16_MAX);

	r->magic = REGL_MAGIC;
	r->space = (typeof(r->space))b;
	r->alloc = alloc;
	AZ(r->n);
	AZ(r->arr[0].off);
	AZ(r->arr[r->space - 1].off);
	return (r);
}

static struct regionlist *
regionlist_init(struct buddy_ptr_page alloc, buddy_t *membuddy)
{
	struct regionlist *rl;
	struct regl *r;
	size_t sz;

	sz = (size_t)1 << alloc.bits;
	assert(sz >= (2 * sizeof *r + sizeof *rl));

	rl = alloc.ptr;
	INIT_OBJ(rl, REGIONLIST_MAGIC);
	VSTAILQ_INIT(&rl->head);

	BUDDY_POOL_INIT(rl->pool, membuddy, FEP_MEM_FREE, regl_fill, NULL);

	r = regl_init(alloc, sizeof(*rl));
	AN(r);

	VSTAILQ_INSERT_HEAD(&rl->head, r, list);
	return (rl);
}

static struct regionlist *
regionlist_alloc(buddy_t *membuddy)
{
	struct buddy_ptr_page alloc;

	alloc = buddy_alloc1_ptr_page_wait(membuddy, FEP_MEM_FREE,
	    regl_bits, regl_bits_cram);
	AN(alloc.ptr);

	return (regionlist_init(alloc, membuddy));
}

static struct regionlist *
regionlist_alloc_nowait(buddy_t *membuddy)
{
	struct buddy_ptr_page alloc;

	alloc = buddy_alloc1_ptr_page(membuddy, regl_bits, regl_bits_cram);
	if (alloc.ptr == NULL)
		return (NULL);

	return (regionlist_init(alloc, membuddy));
}

static unsigned
regionlist_used(const struct regionlist *rl)
{
	struct regl *r;

	CHECK_OBJ_NOTNULL(rl, REGIONLIST_MAGIC);
	r = VSTAILQ_FIRST(&rl->head);
	if (r == NULL)
		return (0);
	return (r->n);
}

static void
regionlist_append(struct regionlist *to, struct regionlist **fromp)
{
	struct regionlist *from;

	CHECK_OBJ_NOTNULL(to, REGIONLIST_MAGIC);
	TAKE_OBJ_NOTNULL(from, fromp, REGIONLIST_MAGIC);
	BUDDY_POOL_FINI(from->pool);
	to->size += from->size;
	VSTAILQ_CONCAT(&to->head, &from->head);
}

static struct regl *
regionlist_extend(struct regionlist *rl)
{
	struct buddy_ptr_page alloc;
	struct regl *regl;

	CHECK_OBJ_NOTNULL(rl, REGIONLIST_MAGIC);

	alloc = buddy_get_next_ptr_page(rl_pool_get(rl->pool, NULL));
	AN(alloc.ptr);

	regl = regl_init(alloc, (size_t)0);
	VSTAILQ_INSERT_TAIL(&rl->head, regl, list);

	return (regl);
}

static void
regionlist_add(struct regionlist *rl,
    const struct buddy_off_extent *regs, unsigned n)
{
	struct regl *regl;
	uint16_t i, nn;
	size_t size;

	CHECK_OBJ_NOTNULL(rl, REGIONLIST_MAGIC);
	regl = VSTAILQ_LAST(&rl->head, regl, list);
	CHECK_OBJ_NOTNULL(regl, REGL_MAGIC);

	while (n) {
		AN(regl->space);
		if (regl->n == regl->space)
			regl = regionlist_extend(rl);

		CHECK_OBJ_NOTNULL(regl, REGL_MAGIC);
		assert(regl->space > regl->n);

		i = regl->space - regl->n;

		if (i < n)
			nn = i;
		else
			nn = (typeof(nn))n;

		memcpy(&regl->arr[regl->n], regs, nn * sizeof *regs);
		size = 0;
		for (i = 0; i < nn; i++)
			size += regs[i].size;
		rl->size += size;
		regl->n += nn;
		regs += nn;
		n -= nn;
	}
}

static void
regionlist_free(struct regionlist **rlp, buddy_t *dskbuddy)
{
	struct buddy_returns *dskret, *memret;
	struct buddy *membuddy;
	struct regionlist *rl;
	struct regl *regl, *save;
	size_t size, tot;
	unsigned n;

	TAKE_OBJ_NOTNULL(rl, rlp, REGIONLIST_MAGIC);

	membuddy = rl->pool->reqs[0].reqs.buddy;
	BUDDY_POOL_FINI(rl->pool);

	dskret = BUDDY_RETURNS_STK(dskbuddy, BUDDY_RETURNS_MAX);
	memret = BUDDY_RETURNS_STK(membuddy, BUDDY_RETURNS_MAX);
	tot = rl->size;

	VSTAILQ_FOREACH_SAFE(regl, &rl->head, list, save) {
		CHECK_OBJ_NOTNULL(regl, REGL_MAGIC);

		size = 0;
		for (n = 0; n < regl->n; n++) {
			size += regl->arr[n].size;
			AN(buddy_return_off_extent(dskret, &regl->arr[n]));
		}
		assert(tot >= size);
		tot -= size;

		AN(buddy_return_ptr_page(memret, &regl->alloc));
	}
	AZ(tot);

	buddy_return(dskret);
	buddy_return(memret);
}

static void
regionlist_discard(struct fellow_fd *ffd, void *ioctx,
    struct regionlist **rlp)
{
	const struct stvfe_tune *tune;
	struct regl *this, *next;
	struct regionlist *rl;

	CHECK_OBJ_NOTNULL(ffd, FELLOW_FD_MAGIC);
	tune = ffd->tune;
	CHECK_OBJ_NOTNULL(tune, STVFE_TUNE_MAGIC);

	TAKE_OBJ_NOTNULL(rl, rlp, REGIONLIST_MAGIC);

	if ((ffd->cap & FFD_CAN_ANY) == 0) {
		regionlist_free(&rl, ffd->dskbuddy);
		return;
	}

	VSTAILQ_FOREACH_SAFE(this, &rl->head, list, next) {
		CHECK_OBJ_NOTNULL(this, REGL_MAGIC);
		fellow_io_regions_discard(ffd, ioctx, this->arr, this->n,
		    tune->discard_immediate, next == NULL ? 1 : 0);
	}

	regionlist_free(&rl, ffd->dskbuddy);
}
