/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * stash away objects by a (ban) time (double)
 */

//#include "config.h"

#include <vtree.h>

/* list head for objects under the same ban time */
struct festash {
	unsigned			magic;
#define FESTASH_MAGIC			0x970999bf
	vtim_real			ban_time;
	VRBT_ENTRY(festash)		entry;
	VTAILQ_HEAD(,objcore)		objcs;
};

static inline int
festash_cmp(const struct festash *a, const struct festash *b)
{
	CHECK_OBJ_NOTNULL(a, FESTASH_MAGIC);
	CHECK_OBJ_NOTNULL(b, FESTASH_MAGIC);

	if (a->ban_time < b->ban_time)
		return (-1);
	if (a->ban_time > b->ban_time)
		return (1);
	return (0);
}

VRBT_HEAD(festash_head, festash);

VRBT_GENERATE_INSERT_COLOR(festash_head, festash, entry, static)
#ifdef VRBT_GENERATE_INSERT_FINISH
VRBT_GENERATE_INSERT_FINISH(festash_head, festash, entry, static)
#endif
VRBT_GENERATE_INSERT(festash_head, festash, entry, festash_cmp, static)
VRBT_GENERATE_REMOVE_COLOR(festash_head, festash, entry, static)
VRBT_GENERATE_REMOVE(festash_head, festash, entry, static)
VRBT_GENERATE_NEXT(festash_head, festash, entry, static)
VRBT_GENERATE_MINMAX(festash_head, festash, entry, static)

struct festash_top {
	unsigned			magic;
#define FESTASH_TOP_MAGIC		0x73539071
	buddy_t			*buddy;
	struct festash_head		head;
	struct festash			*new;
	struct ban			*dummy_ban;
	struct worker			*wrk;
	vtim_real			lru_time;
	vtim_real			*earliest;
};

static inline void
festash_init(struct festash *fes)
{
	INIT_OBJ(fes, FESTASH_MAGIC);
	VTAILQ_INIT(&fes->objcs);
}

static struct festash *
festash_alloc(buddy_t *buddy)
{
	struct festash *fes;
	struct buddy_ptr_extent mem =
	    buddy_alloc1_ptr_extent(buddy, sizeof(*fes), 0);

	XXXAN(mem.ptr);
	fes = mem.ptr;
	festash_init(fes);
	return (fes);
}

static void
festash_free(buddy_t *buddy, struct festash **fesp)
{
	struct buddy_ptr_extent mem;
	struct festash *fes;
	size_t sz;

	TAKE_OBJ_NOTNULL(fes, fesp, FESTASH_MAGIC);
	assert(VTAILQ_EMPTY(&fes->objcs));
	sz = buddy_rndup(buddy, sizeof *fes);
	mem = BUDDY_PTR_EXTENT(fes, sz);
	buddy_return1_ptr_extent(buddy, &mem);
}

static void
festash_top_init(struct festash_top *fet, buddy_t *buddy,
    struct ban *dummy_ban, struct worker *wrk, vtim_real lru_time,
    vtim_real *earliest)
{

	AN(fet);
	AN(earliest);

	INIT_OBJ(fet, FESTASH_TOP_MAGIC);
	fet->buddy = buddy;
	VRBT_INIT(&fet->head);
	fet->dummy_ban = dummy_ban;
	fet->wrk = wrk;
	fet->lru_time = lru_time;
	fet->earliest = earliest;
}

static void
festash_top_fini(struct festash_top *fet)
{
	struct festash *fes;

	CHECK_OBJ_NOTNULL(fet, FESTASH_TOP_MAGIC);

	if (! VRBT_EMPTY(&fet->head)) {
		fprintf(stderr, "ban times not found:");
		VRBT_FOREACH(fes, festash_head, &fet->head)
		    fprintf(stderr, " %f", fes->ban_time);
		fprintf(stderr, "\n");
		abort();
	}

	FINI_OBJ(fet);
}

static void
festash_insert(struct festash_top *fet, struct objcore *oc, vtim_real ban_time)
{
	struct festash *fes;

	CHECK_OBJ_NOTNULL(fet, FESTASH_TOP_MAGIC);
	AN(oc);

	if (fet->new == NULL)
		fet->new = festash_alloc(fet->buddy);
	CHECK_OBJ_NOTNULL(fet->new, FESTASH_MAGIC);

	fet->new->ban_time = ban_time;
	fes = VRBT_INSERT(festash_head, &fet->head, fet->new);
	if (fes == NULL) {
		fes = fet->new;
		fet->new = NULL;
	}
	AN(fes);

	VTAILQ_INSERT_TAIL(&fes->objcs, oc, lru_list);
}

/*
 * time has passed since we stashed away the object, and it might have
 * already expired.
 * if so, we need to hold the reference until we are done loading and the
 * log is open
 *
 * t already is inclusive of a margin
 *
 */
static void
festash_work_fes(struct festash_top * fet,struct festash *fes, struct ban *ban)
{
	struct objcore *oc, *ocs;

	//lint -e{850} loop variable oc modified
	VTAILQ_FOREACH_SAFE(oc, &fes->objcs, lru_list, ocs) {
		VTAILQ_REMOVE(&fes->objcs, oc, lru_list);
		assert(oc->ban == fet->dummy_ban);

		if (oc->ban != ban) {
			BAN_DestroyObj(oc);
			BAN_RefBan(oc, ban);
		}

		// sync sfe_resurrect(), festash_top_fini()
		HSH_DerefBoc(fet->wrk, oc);
		LRU_Add(oc, fet->lru_time);
		(void)HSH_DerefObjCore(fet->wrk, &oc, HSH_RUSH_POLICY);
	}
	AN(VRBT_REMOVE(festash_head, &fet->head, fes));
}

/*
 * we work the stash, moving all objects to the right ban
 *
 * we (ab)use the lru list to keep track of our objects
 *
 * our stashed objects
 * - still have a reference from insertion
 * - are not yet added to the lru
 */
static void
festash_top_work(struct festash_top *fet, unsigned has_bans)
{
	struct festash *fes, *fess;
	struct ban *ban = fet->dummy_ban;
	vtim_real ban_time;

	//lint -e{850} loop variable fes modified
	VRBT_FOREACH_SAFE(fes, festash_head, &fet->head, fess) {
		if (has_bans) {
			ban_time = fes->ban_time;
			if (ban_time < *fet->earliest)
				ban_time = *fet->earliest;
			ban = BAN_FindBan(ban_time);
			if (ban == NULL)
				continue;
		}
		festash_work_fes(fet, fes, ban);
		festash_free(fet->buddy, &fes);
		AZ(fes);
	}
	if (fet->new)
		festash_free(fet->buddy, &fet->new);
}

VRBT_GENERATE_FIND(festash_head, festash, entry, festash_cmp, static)
static void
festash_return(struct festash_top *fet, struct festash **fesp)
{
	struct festash *fes;

	TAKE_OBJ_NOTNULL(fes, fesp, FESTASH_MAGIC);
	assert(VTAILQ_EMPTY(&fes->objcs));
	festash_init(fes);
	if (fet->new == NULL)
		fet->new = fes;
	else
		festash_free(fet->buddy, &fes);
}

static void
festash_work_one(struct festash_top *fet, vtim_real ban_time)
{
	struct festash n, *fes;
	struct ban *ban;

	INIT_OBJ(&n, FESTASH_MAGIC);
	n.ban_time = ban_time;

	fes = VRBT_FIND(festash_head, &fet->head, &n);

	if (fes == NULL)
		return;
	ban = BAN_FindBan(ban_time);
	AN(ban);
	festash_work_fes(fet, fes, ban);
	festash_return(fet, &fes);
	AZ(fes);
}
