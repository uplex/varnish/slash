/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 *
 * This file contains functions for use in tests without varnishd, providing
 * replacements for varnishd facilities
 */

#include "config.h"

#include <assert.h>
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fellow_testenv.h"

/*
 * super trivial implementation for running a task
 * in a separete thread, to mimic the varnish worker pool
 * where it is not available (in threads)
 *
 * note this is also SUPER INEFFICIENT !
 */

struct simple_task {
	fellow_task_func_t	*func;
	void			*priv;
};

#define AZ(foo) do { assert((foo) == 0); } while (0)
#define AN(foo) do { assert((foo) != 0); } while (0)

static pthread_attr_t attr;

static void __attribute__((constructor))
attr_init(void)
{
	size_t pg, sz;

	pg = (size_t)sysconf(_SC_PAGESIZE);
	pg--;
	sz = (size_t)64 * 1024;
	sz += pg;
	sz &= ~pg;

	AZ(pthread_attr_init(&attr));
	AZ(pthread_attr_setstacksize(&attr, sz));
	AZ(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED));
}

static void __attribute__((destructor))
attr_destroy(void)
{
	AZ(pthread_attr_destroy(&attr));
}

static void *
runtask(void *priv)
{
	struct simple_task task, *taskp;

	AN(priv);
	taskp = priv;
	task = *taskp;
	free(taskp);
	task.func(NULL, task.priv);
	return (NULL);
}

int
fellow_simple_task_run(fellow_task_func_t func, void *priv,
    fellow_task_privstate *foo)
{
	pthread_t thread;
	struct simple_task *task;

	(void)foo;

	task = malloc(sizeof *task);
	AN(task);

	task->func = func;
	task->priv = priv;

	//lint -e{429} task not freed here
	return (pthread_create(&thread, &attr, runtask, task));
}

// ref varnishd mgt_main.c main()
static void __attribute__((constructor))
ignore_signals(void)
{
	struct sigaction sac;

	memset(&sac, 0, sizeof sac);
	sac.sa_handler = SIG_IGN;
	sac.sa_flags = SA_RESTART;
	AZ(sigaction(SIGPIPE, &sac, NULL));
	AZ(sigaction(SIGHUP, &sac, NULL));
}
