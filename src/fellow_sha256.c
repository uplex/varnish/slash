/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "config.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "vsha256.h"
#include "fellow_sha256.h"

void
sha256(unsigned char hash[FSHA256_LEN], const void *p, size_t l)
{
	VSHA256_CTX ctx;

	//lint -e{506}
	assert(FSHA256_LEN == VSHA256_LEN);
	VSHA256_Init(&ctx);
	VSHA256_Update(&ctx, p, l);
	VSHA256_Final(hash, &ctx);
}

int
sha256cmp(const unsigned char *in, const void *p, size_t l)
{
	unsigned char hash[FSHA256_LEN];

	sha256(hash, p, l);
	return (memcmp(in, hash, (size_t)FSHA256_LEN));
}
