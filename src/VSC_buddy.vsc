..
	SPDX-License-Identifier: LGPL-2.1-only
	Copyright 2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
	Author: Nils Goroll <nils.goroll@uplex.de>
 *
	This library is free software; you can redistribute it and/or
	modify it under the terms of the GNU Lesser General Public License
	as published by the Free Software Foundation; either version 2.1 of
	the License, or (at your option) any later version.
 *
	This library is distributed in the hope that it will be useful, but
	WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
	Lesser General Public License for more details.
 *
	You should have received a copy of the GNU Lesser General Public
	License along with this library; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
	02110-1301 USA Also add information on how to contact you by
	electronic and paper mail.

.. varnish_vsc_begin::	buddy
	:oneliner:	buddy Stevedore Counters (SLASH/ stevedores)
	:order:		70

.. varnish_vsc:: c_req
	:type:	counter
	:level:	info
	:oneliner:	Allocator requests successful

	Number of times the storage successfully provided a storage segment.

.. varnish_vsc:: c_fail
	:type:	counter
	:level:	info
	:oneliner:	Allocator failures

	Number of times the storage has failed to provide a storage segment.

.. varnish_vsc:: c_bytes
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes allocated

	Number of total bytes allocated by this storage.

.. varnish_vsc:: c_freed
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes freed

	Number of total bytes returned to this storage.

.. varnish_vsc:: c_trim
	:type:	counter
	:level:	info
	:oneliner:	Trim calls

	Number trim calls.

.. varnish_vsc:: c_trimmed
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes trimmed

	Number of total bytes trimmed because of oversized allocations.

.. varnish_vsc:: c_lru_wakeups
	:type:	counter
	:level:	info
	:oneliner:	LRU thread wakeup events

	Number of times the LRU thread got woken up because of a waiting
	allocation request.

.. varnish_vsc:: c_lru_reserve_used_bytes
	:type:	counter
	:level:	info
	:format: bytes
	:oneliner:	Bytes used from the reserve at LRU thread wakeups

	Number of bytes returned from the reserve.

	This number divided by ``c_lru_wakeups`` gives the average bytes used
	from the reserve per wakeup, which might help with sizing the reserve.

	Note that this value is in bytes not in chunks to not be impacted by
	changes of ``chunk_exponent`` / ``chunk_bytes``.

.. varnish_vsc:: c_lru_nuke_fill_reserve
	:type:	counter
	:level:	info
	:oneliner:	LRU thread nukes to fill the reserve

	Number of nukes from the LRU thread to fill the reserve

.. varnish_vsc:: c_lru_nuke_reserve_drained
	:type:	counter
	:level:	info
	:oneliner:	LRU thread nukes with the reserve drained

	Number of nukes from the LRU thread while the reserve is empty.

.. varnish_vsc:: g_alloc
	:type:	gauge
	:level:	info
	:oneliner:	Allocations outstanding

	Number of storage allocations outstanding.

.. varnish_vsc:: g_bytes
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Bytes outstanding

	Number of bytes allocated from the storage.

	Note: This number is only approximate

.. varnish_vsc:: g_space
	:type:	gauge
	:level:	info
	:format: bytes
	:oneliner:	Bytes available

	Number of bytes left in the storage.

	Note: This number is only approximate

.. varnish_vsc_end::	buddy
