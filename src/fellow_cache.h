/*-
 * SPDX-License-Identifier: LGPL-2.1-only
 * Copyright 2022,2023 UPLEX Nils Goroll Systemoptimierung. All rights reserved.
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA Also add information on how to contact you by
 * electronic and paper mail.
 */

#include "fellow_task.h"


struct fellow_busy;
struct fellow_cache;
struct fellow_cache_obj;
struct fellow_fd;
struct fellow_dle;
struct stvfe_tune;

enum fellow_cache_res_e {
	fcr_ok = 0,
	fcr_allocerr = 1,
	fcr_ioerr = 2,
	FCR_LIM
} __attribute__ ((__packed__));

extern const char * const fellow_cache_res_s[FCR_LIM];

struct fellow_cache_res {
	union {
		int		integer;
		void		*ptr;
		const char	*err;
	} r;
	enum fellow_cache_res_e status;
}  __attribute__ ((__packed__));

struct fellow_cache *
fellow_cache_init(struct fellow_fd *, buddy_t *, struct stvfe_tune *,
    fellow_task_run_t, struct VSC_fellow *stats);
void fellow_cache_fini(struct fellow_cache **);

void fellow_cache_obj_wait_written(struct fellow_cache_obj *fco);
void fellow_cache_obj_delete(struct fellow_cache *fc,
    struct fellow_cache_obj *fco, const uint8_t hash[32]);
void fellow_cache_obj_deref(struct fellow_cache *fc,
    struct fellow_cache_obj *fco);

struct fellow_cache_res fellow_cache_obj_get(struct fellow_cache *fc,
    struct objcore **ocp, uintptr_t priv2, unsigned crit);
struct fellow_cache_res
fellow_cache_obj_iter(struct fellow_cache *fc, struct fellow_cache_obj *fco,
    void *priv, objiterate_f func, int final);
struct fellow_cache_res
fellow_cache_obj_getattr(struct fellow_cache *fc,
    struct fellow_cache_obj *fco,
    enum obj_attr attr, size_t *len);
int fellow_cache_obj_lru_touch(struct fellow_cache_obj *fco);

struct fellow_cache_res
fellow_busy_obj_alloc(struct fellow_cache *fc,
    struct fellow_cache_obj **fcop, uintptr_t *priv2,
    unsigned wsl);
struct fellow_cache_res
fellow_busy_obj_getspace(struct fellow_busy *fbo,
    size_t *sz, uint8_t **ptr);

void fellow_cache_panic(struct vsb *vsb, const struct fellow_cache_obj *fco);
void fellow_busy_panic(struct vsb *vsb, const struct fellow_busy *fbo);

void fellow_busy_obj_extend(struct fellow_busy *fbo, size_t l);
void fellow_busy_obj_trimstore(struct fellow_busy *fbo);

// bocdone in three phases to keep struct objcore out of fellow_cache.c
void fellow_busy_done(struct fellow_busy *fbo, struct objcore *oc, unsigned);
void *fellow_busy_setattr(struct fellow_busy *fbo,
    enum obj_attr attr, size_t len, const void *ptr);
