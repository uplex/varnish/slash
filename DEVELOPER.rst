NOTES FOR SLASH/ DEVELOPERS
===========================

Run the varnish-cache test suite with SLASH/
--------------------------------------------

.. _`varnishd_args_prepend`: https://github.com/varnishcache/varnish-cache/commit/ad3fa105b35654f768cdd684b13241d05b3e661f

Using the `varnishd_args_prepend`_ macro, most VTCs from the
varnish-cache test suite can be run with buddy/fellow in place of the
built-in storage engines. The exception are tests which explicitly
define a storage named ``s0`` and those which check for counters of
the malloc/umem stevedores.

Invocation example::

  ./varnishtest \
  -D varnishd_args_prepend='-E/tmp/lib/varnish/vmods/libvmod_slash.so -sfellow,fellow.stv,10MB,1MB,64KB' \
  -ki -j20 $(grep -EL -- '(-ss0|SM..s0)' tests/[b-x]*vtc) 2>&1 | tee slash_log

As of 2023-07, some tests fill fail on the assertion that no objects
are leaked::

  ***  v1    debug|Info: Child (3823881) said varnishd: buddy.c:1359: buddy_fini: Assertion `freemap_space(map) == map->size' failed.

These cases are not necessarily a SLASH/ error and still need to be
investigated.
