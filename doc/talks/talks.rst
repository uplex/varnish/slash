TALKS ABOUT SLASH/
==================

I have presented different aspects of SLASH/ at conferences.

All Systems GO! 2024
--------------------

.. _ASG 2024 talk: https://media.ccc.de/v/all-systems-go-2024-305-using-iouring-for-storage
.. _ASG 2024 slides PDF: 2024092x_all_systems_go_using_io_uring_for_storage_n.pdf
.. _ASG 2024 slides ODP: 2024092x_all_systems_go_using_io_uring_for_storage_n.odp

The `ASG 2024 talk`_ focusses on the use of io_uring in SLASH/fellow.

Slides are available as PDF and Open Document Format:

* `ASG 2024 slides PDF`_

* `ASG 2024 slides ODP`_

Errata:

* The example for the overhead on parge 7 is wrong (see the FOSDEM talk for a
  correction)

FOSDEM 2025
-----------

.. _FOSDEM 2025 talk: https://fosdem.org/2025/schedule/event/fosdem-2025-4479-a-memory-allocator-with-only-0-006-fixed-overhead-written-from-scratch/
.. _FOSDEM 2025 slides PDF: buddy_allocator.pdf
.. _FOSDEM 2025 slides ODP: buddy_allocator.odp

The `FOSDEM 2025 talk`_ goes into details of the buddy memory allocator
implementation in SLASH/. Unfortunately, there was a problem with the video box,
so in the video, the slides can only be seen on the screen.

Also, I just came out of a cold and apologize for my bad voice and at times the
lack of focus.

Slides are available as PDF and Open Document Format:

* `FOSDEM 2025 slides PDF`_

* `FOSDEM 2025 slides ODP`_
