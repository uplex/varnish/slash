@@
expression p, bits, space;
@@

-bitf_init(p, bits, space)
+bitf_init(p, bits, space, BITF_INDEX)

@@
expression bits;
@@

-bitf_sz(bits)
+bitf_sz(bits, BITF_INDEX)

@@
expression sz;
@@

-sz_nbits(sz)
+sz_nbits(sz, BITF_INDEX)
